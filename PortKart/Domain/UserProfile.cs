﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class UserProfile
    {
        public int UserId { get; set; }
        public int CompanyId  { get; set; }
        public string CompanyName { get; set; }
        public string CompanyRegNo { get; set; }
        public int? CountryId { get; set; }
        public string CountryName { get; set; }
        public int? IndustryTypeId { get; set; }
        public string IndustryTypeName { get; set; }

        public DateTime? ExpiryDate { get; set; }
        public string City { get; set; }
        public string Website { get; set; }
        public string EmailID { get; set; }
        public string Address { get; set; }
        public string CountryCode { get; set; }
        public string Phone { get; set; }
        public string LogoPath { get; set; }
        public string AttachmentPath { get; set; }
        public string About { get; set; }
        public byte[] Logo { get; set; }

        public byte[] Attachment { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

        private List<ProfileSocialMedia> _SocialMediaList = new List<ProfileSocialMedia>();
        public List<ProfileSocialMedia> SocialMediaList { get { return _SocialMediaList; } set { _SocialMediaList = value; } }
     
        private List<ContactPerson> _ContactPersonList = new List<ContactPerson>();
        public List<ContactPerson> ContactPersonList { get { return _ContactPersonList; } set { _ContactPersonList = value; } }
        
        private List<CompanyBranch> _CompanyBranchList = new List<CompanyBranch>();
        public List<CompanyBranch> CompanyBranchList { get { return _CompanyBranchList; } set { _CompanyBranchList = value; } }

        public string LogoBase64 { get; set; }
        public string LogoContentType { get; set; }
        public string LogoFilename { get; set; }
        public string LogoFormat { get; set; }


        public string AttachBase64 { get; set; }
        public string AttachContentType { get; set; }
        public string AttachFilename { get; set; }
        public string AttachFormat { get; set; }
    }
}
