﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class AgencySearchList
    {
        private List<AgencyService> _AgencyServiceList = new List<AgencyService>();
        public List<AgencyService> AgencyServiceList { get { return _AgencyServiceList; } set { _AgencyServiceList = value; } }

        private List<AgencyServiceDetails> _FServiceList = new List<AgencyServiceDetails>();
        public List<AgencyServiceDetails> FServiceList { get { return _FServiceList; } set { _FServiceList = value; } }

        public int totalcount { get; set; }
    }
}
