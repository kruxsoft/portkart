﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class VesselPortExcell
    {
        public int PortId { get; set; }
        public string Code { get; set; }
        public string Port { get; set; }
        public string Country { get; set; }
        public string Airport { get; set; }
        public string Un_Locode { get; set; }
        public string TimeZone { get; set; }
        public decimal Latitude { get; set; }
        public decimal LatitudeDegree { get; set; }
        public decimal LatitudeMinutes { get; set; }
        public string LatitudeDirection { get; set; }
        public decimal Longitude { get; set; }
        public decimal LongitudeDegree { get; set; }
        public decimal LongitudeMinutes { get; set; }
        public string LongitudeDirection { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool IsActive { get; set; }

        public string RowNo { get; set; }
    }
}
