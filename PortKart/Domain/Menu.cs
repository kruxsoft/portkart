﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Domain
{
    public class Menu
    {
        public int MenuId { get; set; }

        public string MenuName { get; set; }

        public string MenuURL { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool IsActive { get; set; }
    }
}