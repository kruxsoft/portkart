﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class ChangePassword
    {
        public string userName { get; set; }

        [Display(Name = "New password")]
        [Required(ErrorMessage = "Please enter a new password.")]
        public string password { get; set; }

        [Display(Name = "User")]
        [Required(ErrorMessage = "Please select an User.")]
        public int UserId { get; set; }

        public int ModifiedBy { get; set; }

    }
}
