﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class OwnerTemplateMapping
    {
        public int UserId { get; set; }
        public int OwnerId { get; set; }
        public string Owner { get; set; }
        public int TemplateId { get; set; }
        public string TemplateName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string StartDateAsString { get; set; }
        public string EndDateAsString { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

        private List<TemplateDetails> _Offerdetails = new List<TemplateDetails>();
        public List<TemplateDetails> OffereDetails { get { return _Offerdetails; } set { _Offerdetails = value; } }

        public int[] PortIds { get; set; }
        public int OwnerTemplateId { get; set; }
        
    }
}
