﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
   public class Template
    {
        public int TemplateId { get; set; }
      
        public string TemplateName { get; set; }
        public int AgencyId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

        private List<TemplateDetails> _TemplateDetail = new List<TemplateDetails>();
        public List<TemplateDetails> TemplateDetail { get { return _TemplateDetail; } set { _TemplateDetail = value; } }

    }
}
