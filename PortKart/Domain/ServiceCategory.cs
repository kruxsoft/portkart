﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
   public  class ServiceCategory
    {
        public int ServiceCategoryId { get; set; }

        public string ServiceCategoryCode { get; set; }
        public string ServiceCategoryName { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedDateAsString { get; set; }

        //for datatable
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
    }
}
