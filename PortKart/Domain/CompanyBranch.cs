﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class CompanyBranch
    {
        public int BranchId { get; set; }
        public int CompanyId { get; set; }
        [Display(Name = "Country")]
        public int? CountryId { get; set; }
        public string BranchName { get; set; }
        public string Address { get; set; }
        public string CountryCode { get; set; }
        public string Phone { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string ContactPerson { get; set; }
        public string CP_CountryCode { get; set; }
        public string CP_Phone { get; set; }
        public bool IsPreview { get; set; }
    }
}
