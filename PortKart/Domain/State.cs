﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class State
    {
        //public State() { }
        //public State(int stateId, string statename)
        //{
        //    StateID = stateId;
        //    StateName = statename;
        //}
        public int StateID { get; set; }

        public string StateName { get; set; }
    }
}
