﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Login
    {
        public int UserId { get; set; }

        // [Required(ErrorMessage = "Please enter an User Name.")]
        public string UserName { get; set; }

        [Display(Name = "Email Id")]
        [Required(ErrorMessage = "Please enter a EmailID.")]
        [StringLength(254, ErrorMessage = "Max length of Email Address field should be less than or equal to 254 characters.")]
        [EmailAddress(ErrorMessage = "Invalid Email Address.")]
        public string emailId { get; set; }

        [Required(ErrorMessage = "Please enter a Password.")]
        public string Password { get; set; }

        public string LoginType { get; set; }
        public bool IsActive { get; set; }
    }
}
