﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class TemplateDetails
    {

        public int TemplateDetailId { get; set; }
        public int TemplateId { get; set; }
        public int ServiceId { get; set; }
        public int CurrencyId { get; set; }
        public string Type { get; set; }
        public decimal Offer { get; set; }
        public string CostType { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool IsParent { get; set; }
        public int ParentId { get; set; }
        public string ServiceCategoryName { get; set; }
        public string ServiceName { get; set; }

        public string Option { get; set; }


    }
}
