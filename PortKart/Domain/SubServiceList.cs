﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class SubServiceList
    {
       
        public int ParentServiceId { get; set; }
        public string Option { get; set; }
        public string ServiceName { get; set; }
        public int ServiceId { get; set; }
        public int Sort { get; set; }      
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
