﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class UserStatuses
    {
        public int UserStatusId { get; set; }
        public string UserStatus { get; set; }
    }
}
