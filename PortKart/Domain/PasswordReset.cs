﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class PasswordReset
    {
        public int UserId { get; set; }
        public int UserEmailId { get; set; }

        [Display(Name = "Password")]
        [Required(ErrorMessage = "Please enter a Password.")]
        [RegularExpression(@"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?!.* )(?=.*?[#?!@('@')$%^&*-]).{8,}$", ErrorMessage = "Invalid Password")]
        public string Password { get; set; }


        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Email Id")]
        [Required(ErrorMessage = "Please enter a EmailID.")]
        [StringLength(254, ErrorMessage = "Max length of Email Address field should be less than or equal to 254 characters.")]
        [EmailAddress(ErrorMessage = "Invalid Email Address.")]
        public string EmailId { get; set; }


        public string EncPassword { get; set; }

    }
}
