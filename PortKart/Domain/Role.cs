﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Role
    {
        public int RoleId { get; set; }

        [Display(Name = "Role Name")]
        [Required(ErrorMessage = "Please enter a Role Name.")]
        [StringLength(50, ErrorMessage = "Max length of Role Name field should be less than or equal to 50 characters.")]
        public string RoleName { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public int ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }

        [Display(Name = "Status")]
        public bool IsActive { get; set; }
    }
}
