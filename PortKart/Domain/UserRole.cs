﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class UserRole
    {
        public int UserRoleId { get; set; }
        [Display(Name = "User")]
        [Required(ErrorMessage = "Please select an User.")]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

   

        [Display(Name = "Role")]
        [Required(ErrorMessage = "Please select a Role.")]
        public int RoleId { get; set; }

        public string RoleName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailId { get; set; }
        public string MobileNumber { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }

        public int UserStatusID { get; set; }
        public string UserStatus { get; set; }

        public bool EmailVerified { get; set; }

        //for datatable
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }

        public int CompanyId { get; set; }
         public string UserGroup { get; set; }
    }
}
