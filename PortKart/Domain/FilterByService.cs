﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class FilterByService
    {
        public int ServiceId { get; set; }
        public int ParentId { get; set; }
        public bool IsParent { get; set; }
    }
}
