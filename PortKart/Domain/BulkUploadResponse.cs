﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class BulkUploadResponse
    {
        public int Total { get; set; }
        public int Success { get; set; }
        public int Failed { get; set; }
        private DBResponse _DBResponse = new DBResponse();
        public DBResponse DBResponse { get { return _DBResponse; } set { _DBResponse = value; } }
        private List<BulkUploadError> _ErrorList = new List<BulkUploadError>();
        public List<BulkUploadError> ErrorList { get { return _ErrorList; } set { _ErrorList = value; } }
    }
    public class BulkUploadError
    {
        public string ErrorMessage { get; set; }
        public string RowNo { get; set; }
    }
}
