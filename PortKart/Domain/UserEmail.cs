﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class UserEmail
    {
        public int UserEmailId { get; set; }
        public int UserId { get; set; }
        public string Email { get; set; }
        public string EmailBody { get; set; }
        public bool SentStatus { get; set; }
        public bool IsExpire { get; set; }
        public string EmailType { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
         public string EmailCode { get; set; }
    }
}
