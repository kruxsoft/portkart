﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
   public class MailSettings
    {
        public string SmtpHost { get; set; }
        public string MailUsername { get; set; }
        public string MailPassword { get; set; }
        public int MailPort { get; set; }
        public string MailerEmail { get; set; }
        public string MailerName { get; set; }
        public bool EnableSSL { get; set; }
      public bool isAuthrequired { get; set; }
    }
}
