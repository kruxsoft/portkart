﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class AgencyServiceDetails
    {
        public int AgencyDetailId { get; set; }
        public int AgencyServiceId { get; set; }
        public int ServiceId { get; set; }
        public int CurrencyId { get; set; }
        public decimal Amount { get; set; }
        public decimal ActualAmount { get; set; }
        public int TemplateId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool IsParent { get; set; }
        public int ParentId { get; set; }
        public string Currency { get; set; }
        public string Code { get; set; }
        public string ServiceCode { get; set; }
        public string ServiceName { get; set; }
        public string ServiceCategoryName { get; set; }
        public int ServiceCategoryId { get; set; }
        public bool IsGroup { get; set; }
        public string CostType { get; set; }
        public string Description { get; set; }
        public string Option { get; set; }
        public string OgOption { get; set; }
        public string Sort { get; set; }
        public int Tat { get; set; }
        public string TatDescription { get; set; }
        public bool IsActive { get; set; }
        public decimal Offer { get; set; }
        public string OfferType { get; set; }

    }
}
