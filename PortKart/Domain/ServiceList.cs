﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class ServiceList
    {
        public int ServiceId { get; set; }

     
        public string ServiceCode { get; set; }

        public string ServiceName { get; set; }

       
        public bool IsActive { get; set; }

        public int[] ServiceCategoryId { get; set; }

        public string ServiceCategoryName { get; set; }

        public bool IsGroup { get; set; }

        public string CostType { get; set; }

        public string Description { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public int ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }

        private List<SubServiceList> _SubServiceList = new List<SubServiceList>();
        public List<SubServiceList> SubServiceList { get { return _SubServiceList; } set { _SubServiceList = value; } }

        //for datatable
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }

        public string ModifiedDateAsString { get; set; }

        public int SubServiceCount { get; set; }
    }
}
