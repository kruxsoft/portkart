﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class RoleMenu
    {
        [Display(Name = "Role Name")]
        [Required(ErrorMessage = "Please select a Role.")]
        public int RoleId { get; set; }

        [Display(Name = "Menu Name")]
        [Required(ErrorMessage = "Please select a Menu.")]
        public int MenuId { get; set; }

        public int ParentId { get; set; }
        public bool IsParent { get; set; }
        public int MenuOrder { get; set; }
        public bool HasChildren { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string MenuName { get; set; }
        public string MenuURL { get; set; }
        public string RoleName { get; set; }

        public string Icon { get; set; }
    }
}