﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class AgencyService
    {
        public int AgencyServiceId { get; set; }
        public int PortId { get; set; }
        public int OwnerId { get; set; }
        public int AgencyId { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate  {get; set; }
        public string CompanyName { get; set; }
        public string LogoPath { get; set; }
        public string About { get; set; }
        public int ServiceCount { get; set; }
        public string Port { get; set; }
        public int CompanyId { get; set; }
        public decimal SortAmount { get; set; }
        public decimal Rating { get; set; }
        private List<AgencyServiceDetails> _ServiceDetails = new List<AgencyServiceDetails>();
        public List<AgencyServiceDetails> ServiceDetails { get { return _ServiceDetails; } set { _ServiceDetails = value; } }

        private List<AgencyServiceDetails> _Fservicelist = new List<AgencyServiceDetails>();
        public List<AgencyServiceDetails> Fservicelist { get { return _Fservicelist; } set { _Fservicelist = value; } }


    }
}
