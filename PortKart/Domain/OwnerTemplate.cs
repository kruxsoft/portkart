﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class OwnerTemplate
    {
        public int OwnerTemplateId { get; set; }

        [Display(Name = "Owner")]  
        public int UserId { get; set; }
         public int OwnerId { get; set; }
        public string Owner { get; set; }
   
        public int TemplateId { get; set; }

        public string TemplateName { get; set; }

        public string[] PortId { get; set; }

        public string Port { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public string StartDateAsString { get; set; }
        public string EndDateAsString { get; set; }
        
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

        private List<VesselPort> _PortList = new List<VesselPort>();
        public List<VesselPort> PortList { get { return _PortList; } set { _PortList = value; } }
    }
}
