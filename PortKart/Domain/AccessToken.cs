﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class AccessToken
    {
        public int expires_in { get; set; }
        public string access_token { get; set; }
    }
}
