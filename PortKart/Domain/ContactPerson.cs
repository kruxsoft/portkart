﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class ContactPerson
    {
        public int ContactPersonId { get; set; }
        public int CompanyId { get; set; }
        public string Name { get; set; }
        [Display(Name = "Country Code")]
        public string CountryCode { get; set; }
        [Display(Name = "Phone No")]
        public string PhoneNo { get; set; }

        public bool IsPreview { get; set; }
    }
}
