﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class DataTableServerSideResult<T>
    {
        public string draw { get; set; }
        public int recordsFiltered { get; set; }
        public int recordsTotal { get; set; }
        private List<T> _data = new List<T>();
        public List<T> data { get { return _data; } set { _data = value; } }
    }
}
