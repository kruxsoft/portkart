﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class AgencyFilter
    {
        public int PortId { get; set; }
        public int[] ServiceCategoryId { get; set; }
        public int[] ServiceId { get; set; }
        public string ViewBy { get; set; }
        public string SortBy { get; set; }
        public int skip { get; set; }
        public int take { get; set; }
        public int OwnerId { get; set; }
        public bool IsFirst { get; set; }
        public bool AllChecked { get; set; }

        private List<FilterByService> _FilterByService = new List<FilterByService>();
        public List<FilterByService> FilterByService { get { return _FilterByService; } set { _FilterByService = value; } }
    }
}
