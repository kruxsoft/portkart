﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class ProfileSocialMedia
    {
        public int SocialMediaId { get; set; }
        public int CompanyId { get; set; }
        public string Link { get; set; }
        public string Type { get; set; }
    }
}
