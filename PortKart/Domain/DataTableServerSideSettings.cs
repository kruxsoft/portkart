﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class DataTableServerSideSettings
    {
        public int pageSize { get; set; }
        public int skip { get; set; }
        public string sortColumn { get; set; }
        public string sortColumnDir { get; set; }
        public string searchValue { get; set; }
        private IDictionary<string, string> _columnSearch = new Dictionary<string, string>();
        public IDictionary<string, string> ColumnSearch{ get { return _columnSearch; } set { _columnSearch = value; } }
    }
}
