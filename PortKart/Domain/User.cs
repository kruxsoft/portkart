﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class User
    {
        public int UserId { get; set; }


        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserGroup { get; set; }

        public string Password { get; set; }


        public string ConfirmPassword { get; set; }

        public string EmailId { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string CompanyRegNo { get; set; }

        public string CompanyName { get; set; }

        public string MobileNumber { get; set; }

        public string CountryCode { get; set; }

        public int ModifiedBy { get; set; }
        public string RoleName { get; set; }
        public int CreatedBy { get; set; }
        public int UserStatusId { get; set; }
        public string UserStatus { get; set; }

        //for datatable
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
    }
}
