﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class DBResponse
    {
        public string MessageType { get; set; }
        public string Message { get; set; }
    }
}
