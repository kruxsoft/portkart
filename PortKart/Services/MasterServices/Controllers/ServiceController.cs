﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repository;

namespace MasterServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServiceController : ControllerBase
    {
        private readonly IServiceRepository _ServiceRepository;

        public ServiceController(IServiceRepository ServiceRepository)
        {
            _ServiceRepository = ServiceRepository;
        }

        #region GetServices
        [Route("GetServices")]
        [HttpGet]
        public IEnumerable<ServiceList> GetServices()
        {
            List<ServiceList> sObj = new List<ServiceList>();
            try
            {
                sObj = _ServiceRepository.GetServices();
                return sObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion
        #region LoadService
        [Route("LoadSubService")]
        [HttpGet]
        public IEnumerable<SubServiceList> LoadSubService(int id)
        {
            List<SubServiceList> sObj = new List<SubServiceList>();
            try
            {
                sObj = _ServiceRepository.LoadSubService(id);
                return sObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion
        #region LoadService
        [Route("LoadService")]
        [HttpGet]
        public IEnumerable<ServiceList> LoadService(int id)
        {
            List<ServiceList> sObj = new List<ServiceList>();
            try
            {
                sObj = _ServiceRepository.LoadService(id);
                return sObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion
        #region AddService
        [Route("AddService")]
        [HttpPost]
        public DBResponse AddService([FromBody] ServiceList sc)
        {

            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _ServiceRepository.AddService(sc);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dbResponse;
        }
        #endregion

        #region GetServiceListById
        [Route("GetServiceListById/{id}")]
        [HttpGet]
        public ServiceList GetServiceListById(int id)
        {

            ServiceList cObj = new ServiceList();
            try
            {
                cObj = _ServiceRepository.GetServiceListById(id);
                return cObj;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region GetSubServiceListById
        [Route("GetSubServiceListById/{parentServiceid}/{Serviceid}")]
        [HttpGet]
        public List<SubServiceList> GetSubServiceListById(int parentServiceID, int serviceID)
        {

            List<SubServiceList> cObj = new List<SubServiceList>();
            try
            {
                cObj = _ServiceRepository.GetSubServiceListById(parentServiceID, serviceID);
                return cObj;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region UpdateServiceList
        [Route("UpdateServiceList")]
        [HttpPost]
        public DBResponse UpdateServiceList([FromBody] ServiceList Service)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _ServiceRepository.UpdateServiceList(Service);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dbResponse;
        }
        #endregion

        #region DeleteServiceList
        [Route("DeleteServiceList")]
        [HttpPost]
        public DBResponse DeleteServiceList([FromBody] ServiceList sc)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _ServiceRepository.DeleteServiceList(sc);
                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetServicesServerSide
        [Route("GetServicesServerSide")]
        [HttpPost]
        public DataTableServerSideResult<ServiceList> GetServicesServerSide(DataTableServerSideSettings DTtablesettings)
        {
            DataTableServerSideResult<ServiceList> sObj = new DataTableServerSideResult<ServiceList>();
            try
            {
                sObj = _ServiceRepository.GetServicesServerSide(DTtablesettings);
                return sObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion
        #region AddSubService
        [Route("AddSubService")]
        [HttpPost]
        public DBResponse AddSubService([FromBody] SubServiceList sc)
        {


            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _ServiceRepository.AddSubService(sc);
            }
            catch (Exception ex)
            {



                throw ex;
            }
            return dbResponse;
        }
        #endregion

        #region UpdateSubService
        [Route("UpdateSubService")]
        [HttpPost]
        public DBResponse UpdateSubService([FromBody] SubServiceList sc)
        {


            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _ServiceRepository.UpdateSubService(sc);
            }
            catch (Exception ex)
            {



                throw ex;
            }
            return dbResponse;
        }
        #endregion

        #region DeleteSubService
        [Route("DeleteSubService")]
        [HttpPost]
        public DBResponse DeleteSubService([FromBody] SubServiceList sc)
        {


            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _ServiceRepository.DeleteSubService(sc);
            }
            catch (Exception ex)
            {



                throw ex;
            }
            return dbResponse;
        }
        #endregion

        //#region GetServiceForOwner
        //[Route("GetServiceForOwner")]
        //[HttpGet]
        //public List<ServiceList> GetServiceForOwner()
        //{
        //    List<ServiceList> sObj = new List<ServiceList>();
        //    try
        //    {
        //        sObj = _ServiceRepository.GetServiceForOwner();
        //        return sObj;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}
        //#endregion
    }
}