﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repository;
using ClosedXML.Excel;
using System.Data;

namespace MasterServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VesselPortController : ControllerBase
    {
        private readonly IVesselPortRepository _vesselPortRepository;

        public VesselPortController(IVesselPortRepository VesselPortRepository)
        {
            _vesselPortRepository = VesselPortRepository;
        }

        #region GetAllVesselPorts
        [Route("GetAllVesselPorts")]
        [HttpGet]
        public IEnumerable<VesselPort> GetAllVesselPorts()
        {
            List<VesselPort> sObj = new List<VesselPort>();
            try
            {
                sObj = _vesselPortRepository.GetAllVesselPorts();
                return sObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region GetVesselPortList
        [Route("GetVesselPortList")]
        [HttpGet]
        public IEnumerable<VesselPort> GetVesselPortList()
        {
            List<VesselPort> sObj = new List<VesselPort>();
            try
            {
                sObj = _vesselPortRepository.GetVesselPortList();
                return sObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region AddVesselPort
        [Route("AddVesselPort")]
        [HttpPost]
        public DBResponse AddVesselPort([FromBody]VesselPort vp)
        {

            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _vesselPortRepository.AddVesselPort(vp);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dbResponse;
        }
        #endregion

        #region GetVesselPortById
        [Route("GetVesselPortById/{id}")]
        [HttpGet]
        public VesselPort GetVesselPortById(int id)
        {

            VesselPort cObj = new VesselPort();
            try
            {
                cObj = _vesselPortRepository.GetVesselPortById(id);
                return cObj;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region UpdateVesselPort
        [Route("UpdateVesselPort")]
        [HttpPost]
        public DBResponse UpdateVesselPort([FromBody]VesselPort vesselPort)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _vesselPortRepository.UpdateVesselPort(vesselPort);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dbResponse;
        }
        #endregion

        #region DeleteVesselPort
        [Route("DeleteVesselPort")]
        [HttpPost]
        public DBResponse DeleteVesselPort([FromBody]VesselPort vp)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _vesselPortRepository.DeleteVesselPort(vp);
                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region excell import
        [Route("VesselPortExcellImport")]
        [HttpPost]
        public  BulkUploadResponse VesselPortExcellImport()
        {
            BulkUploadResponse bulkdbResponse = new BulkUploadResponse();
            List<VesselPortExcell> VesselPortList = new List<VesselPortExcell>();

            try
            {

                var CreatedBy = 1;
                var filecount = Request.Form.Files.Count;
                if (filecount < 1) {
                    bulkdbResponse.DBResponse.MessageType = "ERROR";
                    bulkdbResponse.DBResponse.Message = "No files found";
                    return bulkdbResponse;
                }
                var file = Request.Form.Files[0];
            
                string filename = file.FileName;
                string[] ext = (filename).Split('.');
                if (ext.Length < 2 || (ext[1] != "xlsx" && ext[1] != "xls"))
                {
                    bulkdbResponse.DBResponse.MessageType = "ERROR";
                    bulkdbResponse.DBResponse.Message = "Valid Excell file not found";
                    return bulkdbResponse;
                }

                //now read individual part into STREAM
              

                    //handle the stream here
                    using (var stream = file.OpenReadStream())
                    using (XLWorkbook excelWorkbook = new XLWorkbook(stream))
                        {
                            var ws = excelWorkbook.Worksheet(1);
                            var firstRowUsed = ws.FirstRowUsed();
                            var firstPossibleAddress = ws.Row(firstRowUsed.RowNumber()).FirstCell().Address;
                            var lastPossibleAddress = ws.LastCellUsed().Address;
                            var VesselPortRange = ws.Range(firstPossibleAddress, lastPossibleAddress).RangeUsed();
                    var tablecount = ws.Tables.Count();
                    IXLTables tsTables = ws.Tables;
                    IXLTable VesselPortTable = tsTables.FirstOrDefault();
                    if(tablecount==0)
                     VesselPortTable = VesselPortRange.AsTable();

                        VesselPortList = VesselPortTable.DataRange.Rows().Select(vesselportRow => new VesselPortExcell
                            {
                            Code = vesselportRow.Field("Code").GetString() ,
                            Port = vesselportRow.Field("Port").GetString(),
                            Country = vesselportRow.Field("Country").GetString(),
                            Airport = vesselportRow.Field("Airport").GetString(),
                            Un_Locode = vesselportRow.Field("UN/LOCODE").GetString(),
                            TimeZone = vesselportRow.Field("Time Zone").GetString(),
                            Latitude = vesselportRow.Field("Latitude") ==null?0: Convert.ToDecimal(vesselportRow.Field("Latitude").GetString()),
                            LatitudeDegree = vesselportRow.Field("Latitude Degree") == null ? 0 : Convert.ToDecimal(vesselportRow.Field("Latitude Degree").GetString()),
                            LatitudeMinutes = vesselportRow.Field("Latitude Minutes") == null ? 0 : Convert.ToDecimal(vesselportRow.Field("Latitude Minutes").GetString()),
                            LatitudeDirection = vesselportRow.Field("Latitude Direction").GetString(),
                            Longitude = vesselportRow.Field("Longitude") == null ? 0 : Convert.ToDecimal(vesselportRow.Field("Longitude").GetString()),
                            LongitudeDegree = vesselportRow.Field("Longitude Degree") == null ? 0 : Convert.ToDecimal(vesselportRow.Field("Longitude Degree").GetString()),
                            LongitudeMinutes = vesselportRow.Field("Longitude Minutes") == null ? 0 : Convert.ToDecimal(vesselportRow.Field("Longitude Minutes").GetString()),
                            LongitudeDirection = vesselportRow.Field("Longitude Direction").GetString(),
                            IsActive = vesselportRow.Field("Active").GetString()=="Y"?true:false,
                            RowNo = vesselportRow.RowNumber().ToString()
                            }).ToList();
                        }
                    
                
              
                
            
               // bulkdbResponse = _vesselPortRepository.BulkUploadEmployee(EmployeeList, CreatedBy);
                return bulkdbResponse;
            }
            catch (Exception ex)
            {
                throw ex;

            }

        }
        #endregion

        #region GetAllVesselPortsServerSide
        [Route("GetAllVesselPortsServerSide")]
        [HttpPost]
        public DataTableServerSideResult<VesselPort> GetAllVesselPortsServerSide(DataTableServerSideSettings DTtablesettings)
        {
            DataTableServerSideResult<VesselPort> sObj = new DataTableServerSideResult<VesselPort>();
            try
            {
                sObj = _vesselPortRepository.GetAllVesselPortsServerSide(DTtablesettings);
                return sObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion
    }
}