﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Domain;
using Repository;

namespace MasterServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServiceCategoryController : ControllerBase
    {
        private readonly IServiceCategoryRepository _serviceCategoryRepository;
        
        public ServiceCategoryController(IServiceCategoryRepository ServiceCategoryRepository)
        {
            _serviceCategoryRepository = ServiceCategoryRepository;
        }

        #region GetAllServiceCategories
        [Route("GetAllServiceCategories")]
        [HttpGet]
        public IEnumerable<ServiceCategory> GetAllServiceCategories()
        {
            List<ServiceCategory> sObj = new List<ServiceCategory>();
            try
            {
                sObj = _serviceCategoryRepository.GetAllServiceCategories();
                return sObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion



        #region GetServiceCategoryList
        [Route("GetServiceCategoryList")]
        [HttpGet]
        public IEnumerable<ServiceCategory> GetServiceCategoryList()
        {
            List<ServiceCategory> sObj = new List<ServiceCategory>();
            try
            {
                sObj = _serviceCategoryRepository.GetServiceCategoryList();
                return sObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion



        #region AddServiceCategory
        [Route("AddServiceCategory")]
        [HttpPost]
        public DBResponse AddServiceCategory([FromBody]ServiceCategory sc)
        {
           
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _serviceCategoryRepository.AddServiceCategory(sc);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            return dbResponse;
        }
        #endregion

        #region GetServiceCategoryById
        [Route("GetServiceCategoryById/{id}")]
        [HttpGet]
        public ServiceCategory GetServiceCategoryById(int id)
        {

            ServiceCategory cObj = new ServiceCategory();
            try
            {
                cObj = _serviceCategoryRepository.GetServiceCategoryById(id);
                return cObj;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        #endregion

        #region UpdateServiceCategory
        [Route("UpdateServiceCategory")]
        [HttpPost]
        public DBResponse UpdateServiceCategory([FromBody]ServiceCategory serviceCategory)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _serviceCategoryRepository.UpdateServiceCategory(serviceCategory);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dbResponse;
        }
        #endregion

        #region DeleteServiceCategory
        [Route("DeleteServiceCategory")]
        [HttpPost]
        public DBResponse DeleteServiceCategory([FromBody]ServiceCategory sc)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _serviceCategoryRepository.DeleteServiceCategory(sc);
                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetAllServiceCategoriesServerSide
        [Route("GetAllServiceCategoriesServerSide")]
        [HttpPost]
        public DataTableServerSideResult<ServiceCategory> GetAllServiceCategoriesServerSide(DataTableServerSideSettings DTtablesettings)
        {
            DataTableServerSideResult<ServiceCategory> sObj = new DataTableServerSideResult<ServiceCategory>();
            try
            {
                sObj = _serviceCategoryRepository.GetAllServiceCategoriesServerSide(DTtablesettings);
                return sObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion
    }
}