﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repository;

namespace MasterServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountryController : ControllerBase
    {
        private readonly ICountryRepository _countryRepository;

        public CountryController(ICountryRepository CountryRepository)
        {
            _countryRepository = CountryRepository;
        }
        #region GetAllCountries
        [Route("GetAllCountries")]
        [HttpGet]
        public IEnumerable<Country> GetAllCountries()
        {
            List<Country> sObj = new List<Country>();
            try
            {
                sObj = _countryRepository.GetAllCountries();
                return sObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region GetCountryList
        [Route("GetCountryList")]
        [HttpGet]
        public IEnumerable<Country> GetCountryList()
        {
            List<Country> sObj = new List<Country>();
            try
            {
                sObj = _countryRepository.GetCountryList();
                return sObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region AddCountry
        [Route("AddCountry")]
        [HttpPost]
        public DBResponse AddCountry([FromBody]Country sc)
        {

            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _countryRepository.AddCountry(sc);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dbResponse;
        }
        #endregion


        #region GetCountryById
        [Route("GetCountryById/{id}")]
        [HttpGet]
        public Country GetCountryById(int id)
        {

            Country cObj = new Country();
            try
            {
                cObj = _countryRepository.GetCountryById(id);
                return cObj;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion


        #region UpdateCountry
        [Route("UpdateCountry")]
        [HttpPost]
        public DBResponse UpdateCountry([FromBody]Country country)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _countryRepository.UpdateCountry(country);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dbResponse;
        }
        #endregion

        #region DeleteCountry
        [Route("DeleteCountry")]
        [HttpPost]
        public DBResponse DeleteCountry([FromBody]Country sc)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _countryRepository.DeleteCountry(sc);
                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion



    }
}