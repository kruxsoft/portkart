﻿using Domain;
using Microsoft.AspNetCore.Mvc;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CurrencyController : ControllerBase
    {
        private readonly ICurrencyRepository _currencyRepository;

        public CurrencyController(ICurrencyRepository CurrencyRepository)
        {
            _currencyRepository = CurrencyRepository;
        }

        #region GetAllCurrencies
        [Route("GetAllCurrencies")]
        [HttpGet]
        public IEnumerable<CurrencyDomain> GetAllCurrencies()
        {
            List<CurrencyDomain> sObj = new List<CurrencyDomain>();
            try
            {
                sObj = _currencyRepository.GetAllCurrencies();
                return sObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion



        #region GetCurrencyList
        [Route("GetCurrencyList")]
        [HttpGet]
        public IEnumerable<CurrencyDomain> GetCurrencyList()
        {
            List<CurrencyDomain> sObj = new List<CurrencyDomain>();
            try
            {
                sObj = _currencyRepository.GetCurrencyList();
                return sObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion



        #region AddCurrency
        [Route("AddCurrency")]
        [HttpPost]
        public DBResponse AddCurrency([FromBody] CurrencyDomain sc)
        {

            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _currencyRepository.AddCurrency(sc);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dbResponse;
        }
        #endregion

        #region GetCurrencyById
        [Route("GetCurrencyById/{id}")]
        [HttpGet]
        public CurrencyDomain GetCurrencyById(int id)
        {

            CurrencyDomain cObj = new CurrencyDomain();
            try
            {
                cObj = _currencyRepository.GetCurrencyById(id);
                return cObj;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region UpdateCurrency
        [Route("UpdateCurrency")]
        [HttpPost]
        public DBResponse UpdateCurrency([FromBody] CurrencyDomain currency)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _currencyRepository.UpdateCurrency(currency);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dbResponse;
        }
        #endregion

        #region DeleteCurrency
        [Route("DeleteCurrency")]
        [HttpPost]
        public DBResponse DeleteCurrency([FromBody] CurrencyDomain sc)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _currencyRepository.DeleteCurrency(sc);
                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
