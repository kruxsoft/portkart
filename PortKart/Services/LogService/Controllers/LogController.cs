﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Repository;

namespace LogService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LogController : ControllerBase
    {
        private readonly ILogRepository _logRepository;
        private readonly ILogger<LogController> _logger;
        public LogController(ILogRepository logRepository,  ILogger<LogController> logger)
        {
            _logRepository = logRepository;
            _logger = logger;
          //  _logger.LogDebug(1, "NLog injected into LogController");
        }

        #region GetAllUsers
        [HttpPost]
        [Route("SaveLog")]
        public void SaveLog([FromBody] LogModel lm)
        {
            _logger.LogInformation("Inside get SaveLog() function in UserController.cs -->UserService.");
            try
            {
                DBResponse res = new DBResponse();
                res = _logRepository.SaveLog(lm);
                _logger.LogInformation(res.Message);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
        }
        #endregion
    }
}