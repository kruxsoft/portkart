﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Repository;

namespace OwnerService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OwnerController : ControllerBase
    {
        private readonly IOwnerRepository _OwnerRepository;
        private readonly ILogger<OwnerController> _logger;
        public OwnerController(IOwnerRepository OwnerRepository, ILogger<OwnerController> logger)
        {
            _OwnerRepository = OwnerRepository;
            _logger = logger;
        }

        #region GetAgencyServiceForOwner
        [Route("GetAgencyServiceForOwner")]
        [HttpGet]
        public List<AgencyService> GetAgencyServiceForOwner()
        {
            _logger.LogInformation("Inside GetAgencyServiceForOwner() function in OwnerController.cs -->UserService.");
            List<AgencyService> userRoles = new List<AgencyService>();
            try
            {
                userRoles = _OwnerRepository.GetAgencyServiceForOwner();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return userRoles;
        }
        #endregion

        #region GetVesselPortList
        [Route("GetVesselPortList")]
        [HttpGet]
        public List<VesselPort> GetVesselPortList(string search)
        {
            _logger.LogInformation("Inside GetVesselPortList() function in OwnerController.cs -->UserService.");
            List<VesselPort> VesselPort = new List<VesselPort>();
            try
            {
                VesselPort = _OwnerRepository.GetVesselPortList(search);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return VesselPort;
        }
        #endregion

        #region GetServiceCategoryList
        [Route("GetServiceCategoryList")]
        [HttpGet]
        public List<ServiceCategory> GetServiceCategoryList()
        {
            _logger.LogInformation("Inside GetServiceCategoryList() function in OwnerController.cs -->UserService.");
            List<ServiceCategory> ServiceCategory = new List<ServiceCategory>();
            try
            {
                ServiceCategory = _OwnerRepository.GetServiceCategoryList();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return ServiceCategory;
        }
        #endregion

        #region GetServiceList
        [Route("GetServiceList")]
        [HttpPost]
        public List<ServiceList> GetServiceList(string[] ids)
        {
            _logger.LogInformation("Inside GetServiceList() function in OwnerController.cs -->UserService.");
            List<ServiceList> ServiceList = new List<ServiceList>();
            try
            {
                ServiceList = _OwnerRepository.GetServiceList(ids);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return ServiceList;
        }
        #endregion

        #region Search
        [Route("Search")]
        [HttpPost]
        public AgencySearchList Search(AgencyFilter ids)
        {
            _logger.LogInformation("Inside Search() function in OwnerController.cs -->UserService.");
            AgencySearchList ServiceList = new AgencySearchList();
            try
            {
                ServiceList = _OwnerRepository.Search(ids);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return ServiceList;
        }
        #endregion

        #region SearchService
        [Route("SearchService")]
        [HttpPost]
        public List<AgencyServiceDetails> SearchService(AgencyFilter ids)
        {
            _logger.LogInformation("Inside SearchService() function in OwnerController.cs -->UserService.");
            List<AgencyServiceDetails> ServiceList = new List<AgencyServiceDetails>();
            try
            {
                ServiceList = _OwnerRepository.SearchService(ids);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return ServiceList;
        }
        #endregion

        #region FilterAgency
        [Route("FilterAgency")]
        [HttpPost]
        public AgencySearchList FilterAgency(AgencyFilter ids)
        {
            _logger.LogInformation("Inside FilterAgency() function in OwnerController.cs -->UserService.");
            AgencySearchList ServiceList = new AgencySearchList();
            try
            {
                ServiceList = _OwnerRepository.FilterAgency(ids);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return ServiceList;
        }
        #endregion

        #region GetAgentsForCompare
        [Route("GetAgentsForCompare")]
        [HttpPost]
        public AgencySearchList GetAgentsForCompare(AgencyFilter ids)
        {
            _logger.LogInformation("Inside GetAgentsForCompare() function in OwnerController.cs -->UserService.");
            AgencySearchList ServiceList = new AgencySearchList();
            try
            {
                ServiceList = _OwnerRepository.GetAgentsForCompare(ids);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return ServiceList;
        }
        #endregion
    }
}