﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Repository;

namespace UserService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserRoleController : ControllerBase
    {
        private readonly IUserRoleRepository _userRoleRepository;
        private readonly ILogger<UserRoleController> _logger;
        public UserRoleController(IUserRoleRepository UserRoleRepositor, ILogger<UserRoleController> logger)
        {
            _userRoleRepository = UserRoleRepositor;
            _logger = logger;
        }

        #region GetUserRoles
        [Route("GetUserRoles")]
        [HttpGet]
        public IEnumerable<UserRole> GetUserRoles()
        {
            _logger.LogInformation("Inside GetUserRoles() function in UserRoleController.cs -->UserService.");
            List<UserRole> userRoles = new List<UserRole>();
            try
            {
                userRoles = _userRoleRepository.GetUserRoles();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return userRoles;
        }
        #endregion

    

        #region AddUserRole
        [Route("AddUserRole")]
        [HttpPost]
        public DBResponse AddUserRole([FromBody] UserRole userRole)
        {
            _logger.LogInformation("Inside AddUserRole() function in UserRoleController.cs -->UserService.");
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _userRoleRepository.AddUserRole(userRole);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return dbResponse;
        }
        #endregion

        #region DeleteUserRole 
        [Route("DeleteUserRole")]
        [HttpPost]
        public DBResponse DeleteUserRole([FromBody]UserRole userRole)
        {
            _logger.LogInformation("Inside DeleteUserRole() function in UserRoleController.cs -->UserService.");
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _userRoleRepository.DeleteUserRole(userRole);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return dbResponse;
        }
        #endregion

        #region GetUserRolesListServerSide
        [Route("GetUserRolesListServerSide")]
        [HttpPost]
        public DataTableServerSideResult<UserRole> GetUserRolesListServerSide(DataTableServerSideSettings DTtablesettings)
        {
            _logger.LogInformation("Inside GetUserRolesListServerSide() function in UserRoleController.cs -->UserService.");
            DataTableServerSideResult<UserRole> cObj = new DataTableServerSideResult<UserRole>();
            try
            {
                cObj = _userRoleRepository.GetUserRolesListServerSide(DTtablesettings);

            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return cObj;
        }
        #endregion GetRoles
    }
}