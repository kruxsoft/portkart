﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Repository;

namespace UserService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserStatusController : ControllerBase
    {
        private readonly IUserStatusRepository _userStatusRepository;
        private readonly ILogger<UserStatusController> _logger;
        public UserStatusController(IUserStatusRepository UserStatusRepository, ILogger<UserStatusController> logger)
        {
            _userStatusRepository = UserStatusRepository;
            _logger = logger;
        }
        #region GetUserStatus
        [HttpGet]
        [Route("GetUserStatus")]
        public List<UserStatuses> GetUserStatus()
        {
            _logger.LogInformation("Inside GetUserStatus() function in UserStatusController.cs -->UserService.");
            List<UserStatuses> uobj = new List<UserStatuses>();
            try
            {
                uobj = _userStatusRepository.GetUserStatus();
                return uobj;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
        }
        #endregion
    }
}