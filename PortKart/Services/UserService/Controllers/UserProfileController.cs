﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Repository;

namespace UserUserProfile.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserProfileController : ControllerBase
    {
        private readonly IUserProfileRepository _UserProfileRepository;
        private IHostEnvironment _env;
        private readonly string WebUrl;
        private readonly ILogger<UserProfileController> _logger;
        public UserProfileController(IUserProfileRepository UserProfileRepository, IHostEnvironment env, IConfiguration configuration
            , ILogger<UserProfileController> logger)
        {
            _UserProfileRepository = UserProfileRepository;
            _env = env;
            WebUrl = configuration.GetSection("AppSettings").GetSection("WebUrl").Value;
            _logger = logger;
        }

        #region GetUserProfile
        [Route("GetUserProfile/{id}")]
        [HttpGet]
        public UserProfile GetUserProfile(int id)
        {
            _logger.LogInformation("Inside GetUserProfile() function in UserProfileController.cs -->UserService.");
            UserProfile sObj = new UserProfile();
            try
            {
                sObj = _UserProfileRepository.GetUserProfile(id);
                return sObj;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }

        }
        #endregion

        #region AddUserProfile
        [Route("AddUserProfile")]
        [HttpPost]
        public DBResponse AddUserProfile(UserProfile pobj)
        {
            _logger.LogInformation("Inside AddUserProfile() function in UserProfileController.cs -->UserService.");
            DBResponse sObj = new DBResponse();
            try
            {
                if (pobj.LogoBase64 != null)
                { pobj.LogoPath = AddImage(pobj); }

                if (pobj.AttachBase64 != null)
                { pobj.AttachmentPath = AddAttachment(pobj); }

                sObj = _UserProfileRepository.AddUserProfile(pobj);
                return sObj;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }

        }
        #endregion

        #region UpdateUserProfile
        [Route("UpdateUserProfile")]
        [HttpPost]
        public DBResponse UpdateUserProfile(UserProfile pobj)
        {
            _logger.LogInformation("InsideUpdateUserProfile() function in UserProfileController.cs -->UserService.");
            DBResponse sObj = new DBResponse();
            try
            {
                if (pobj.LogoBase64 != null)
                { pobj.LogoPath = AddImage(pobj); }
                if (pobj.AttachBase64 != null)
                { pobj.AttachmentPath = AddAttachment(pobj); }
                sObj = _UserProfileRepository.UpdateUserProfile(pobj);
                return sObj;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }

        }
        #endregion

        public string AddImage(UserProfile uobj)
        {
            _logger.LogInformation("Inside  AddImage() function in UserProfileController.cs -->UserService.");
            try
            {
                var webRoot = _env.ContentRootPath;
                var PathWithFolderName = System.IO.Path.Combine(webRoot, "UploadedFiles/Images");

                if (!Directory.Exists(PathWithFolderName))
                {
                    // Try to create the directory.
                    DirectoryInfo di = Directory.CreateDirectory(PathWithFolderName);

                }
                var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(uobj.LogoFormat);
                var filepath = WebUrl + "UserProfile/Images?id=" + fileName;
                var fullpath = System.IO.Path.Combine(PathWithFolderName, fileName);

                string Base64String = uobj.LogoBase64;

                byte[] bytes = Convert.FromBase64String(Base64String);

                System.IO.File.WriteAllBytes(fullpath, bytes);

                return filepath;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                return "";
            }
        }

        public string AddAttachment(UserProfile uobj)
        {
            _logger.LogInformation("Inside  AddImage() function in UserProfileController.cs -->UserService.");
            try
            {
                var webRoot = _env.ContentRootPath;
                var PathWithFolderName = System.IO.Path.Combine(webRoot, "UploadedFiles/Files");

                if (!Directory.Exists(PathWithFolderName))
                {
                    // Try to create the directory.
                    DirectoryInfo di = Directory.CreateDirectory(PathWithFolderName);

                }
                var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(uobj.AttachFormat);
                var filepath = WebUrl + "UserProfile/Files?id=" + fileName;
                var fullpath = System.IO.Path.Combine(PathWithFolderName, fileName);

                string Base64String = uobj.AttachBase64;

                byte[] bytes = Convert.FromBase64String(Base64String);

                System.IO.File.WriteAllBytes(fullpath, bytes);

                return filepath;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                return "";
            }
        }


        #region GetAllIndustryType
        [Route("GetAllIndustryType")]
        [HttpGet]
        public List<IndustryType> GetAllIndustryType()
        {
            _logger.LogInformation("Inside GetAllIndustryType() function in UserProfileController.cs -->UserService.");
            List<IndustryType> sObj = new List<IndustryType>();
            try
            {
                sObj = _UserProfileRepository.GetAllIndustryType();
                return sObj;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }

        }
        #endregion

        #region AddContactPerson
        [Route("AddContactPerson")]
        [HttpPost]
        public int AddContactPerson([FromBody] ContactPerson sc)
        {


            int val = 0;
            try
            {
                val = _UserProfileRepository.AddContactPerson(sc);
            }
            catch (Exception ex)
            {



                throw ex;
            }
            return val;
        }
        #endregion
        #region DeleteContactPerson
        [Route("DeleteContactPerson") ]
        [HttpGet]
        public DBResponse DeleteContactPerson( int CPId, int UserId)
        {


            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _UserProfileRepository.DeleteContactPerson(CPId, UserId);
            }
            catch (Exception ex)
            {



                throw ex;
            }
            return dbResponse;
        }
        #endregion

        #region AddBranch
        [Route("AddBranch")]
        [HttpPost]
        public int AddBranch([FromBody] CompanyBranch sc)
        {


            int val = 0;
            try
            {
                val = _UserProfileRepository.AddBranch(sc);
            }
            catch (Exception ex)
            {



                throw ex;
            }
            return val;
        }
        #endregion
        #region DeleteBranch
        [Route("DeleteBranch")]
        [HttpGet]
        public DBResponse DeleteBranch(int BRId, int UserId)
        {


            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _UserProfileRepository.DeleteBranch(BRId, UserId);
                return dbResponse;
            }
            catch (Exception ex)
            {



                throw ex;
            }
           
        }
        #endregion


        #region AddSocialMedia
        [Route("AddSocialMedia")]
        [HttpPost]
        public DBResponse AddSocialMedia([FromBody] List<ProfileSocialMedia> sc)
        {


            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _UserProfileRepository.AddSocialMedia(sc);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dbResponse;
        }
        #endregion

        #region AddAboutsUS
        [Route("AddAboutsUS")]
        [HttpPost]
        public DBResponse AddAboutsUS([FromBody] Company sc)
        {


            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _UserProfileRepository.AddAboutsUS(sc);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dbResponse;
        }
        #endregion
    }
}