﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Domain;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;
using Repository;

namespace UserService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserStatusRepository _userStatusRepository;
        private readonly string WebUrl;
        private readonly IOptions<MailSettings> _mailSettings;
        private readonly IHostEnvironment _hostingEnvironment;
        private readonly ILogger<UserController> _logger;
        public UserController(IUserRepository UserRepository, IUserStatusRepository UserStatusRepository, 
            IConfiguration configuration, IOptions<MailSettings> settings, IHostEnvironment hostingEnvironment, ILogger<UserController> logger)
        {
            _userRepository = UserRepository;
            _userStatusRepository = UserStatusRepository;
            WebUrl = configuration.GetSection("AppSettings").GetSection("WebUrl").Value;
            _mailSettings = settings;
            _hostingEnvironment = hostingEnvironment;
            _logger = logger;
            _logger.LogDebug(1, "NLog injected into UserController");
        }

        #region GetAllUsers
        [Authorize]
        [HttpGet]
        [Route("GetAllUsers")]
        public IActionResult GetAllUsers()
        {
            _logger.LogInformation("Inside get GetAllUsers() function in UserController.cs -->UserService.");
            List<User> uobj = new List<User>();
            try
            {
                uobj = _userRepository.GetAllUsers();
                return new JsonResult(uobj);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error,ex,ex.Message,null);
                throw ex;
            }
        }
        #endregion

        #region GetUsersList
        [Route("GetUsersList")]
        [HttpGet]
        public List<User> GetUsersList()
        {
            _logger.LogInformation("Inside get GetUsersList() function in UserController.cs -->UserService.");
            List<User> uobj = new List<User>();
            try
            {
                uobj = _userRepository.GetUsersList();
                return uobj;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
        }
        #endregion

        #region GetUsers
        [Route("Login/GetUsers")]
        [HttpPost]
        public IEnumerable<UserRole> GetUsers(Login lObj)
        {
            _logger.LogInformation("Inside GetUsers() function in UserController.cs -->UserService.");
            List<UserRole> userlist = new List<UserRole>();
            try
            {
                userlist = _userRepository.GetUsers(lObj.emailId, lObj.Password).ToList();


            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
            }

            return userlist;
        }
        #endregion

        #region Register
        [Route("Login/Register")]
        [Route("Register")]
        [HttpPost]
        public async Task<DBResponse> Register([FromBody]User lObj)
        {
            _logger.LogInformation("Inside Register() function in UserController.cs -->UserService.");
            DBResponse DBResponse = new DBResponse();
            try
            {
                DBResponse = _userRepository.Register(lObj);
                if(DBResponse.MessageType=="SUCCESS")
                {
                    var EmailCode = EncMD5(lObj.EmailId);
                    var Verficationlink = WebUrl + "User/VerifyEmail?Ec=" + EmailCode;
                    await SendRegistrationEmail(lObj, Verficationlink, EmailCode);
                }

            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }

            return DBResponse;
        }
        #endregion

        #region GetUsersById
        [Route("GetUsersById/{userId}")]
        [HttpGet]
        public User GetUsersById(int userId)
        {
            _logger.LogInformation("Inside get GetUsersById() function in UserController.cs -->UserService.");
            User uObj = new User();
            try
            {
                uObj = _userRepository.GetUserById(userId);
                return uObj;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
        }
        #endregion

        #region UpdateUser
        [Route("UpdateUser")]
        [HttpPost]
        public async Task<DBResponse> UpdateUser([FromBody]User cs)
        {
            _logger.LogInformation("Inside post UpdateUser() function in UserController.cs -->UserService.");
            DBResponse dbResponse = new DBResponse();
            User uObj = new User();
            try
            {
                uObj = _userRepository.GetUserById(cs.UserId);
                dbResponse = _userRepository.UpdateUser(cs);
                if (dbResponse.MessageType == "SUCCESS" && uObj.EmailId.ToLower()!=cs.EmailId.ToLower())
                {
                    var EmailCode = EncMD5(cs.EmailId);
                    var Verficationlink = WebUrl + "User/VerifyEmail?Ec=" + EmailCode;
                    await SendRegistrationEmail(cs, Verficationlink, EmailCode);
                }
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return dbResponse;
        }
        #endregion

        #region Delete
        [Route("Delete")]
        [HttpPost]
        public DBResponse Delete([FromBody]User cs)
        {
            _logger.LogInformation("Inside post Delete() function in UserController.cs -->UserService.");
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _userRepository.DeleteUser(cs);
                return dbResponse;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
        }
        #endregion

        #region VerifyEmail
        [Route("UserEmail/VerifyEmail/{EmailCode}")]
        [HttpGet]
        public DBResponse VerifyEmail(string EmailCode)
        {
            _logger.LogInformation("Inside get VerifyEmail() function in UserController.cs -->UserService.");
            DBResponse uObj = new DBResponse();
            try
            {
                uObj = _userRepository.VerifyEmail(EmailCode);
                return uObj;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
        }
        #endregion

        #region ResetPassword
        [Route("UserEmail/ResetPassword/{id}")]
        [HttpGet]
        public async Task<DBResponse> ResetPassword(int id)
        {
            _logger.LogInformation("Inside get ResetPassword() function in UserController.cs -->UserService.");
            DBResponse Robj = new DBResponse();
            User uObj = new User();
            try
            {
                uObj = _userRepository.GetUserById(id);
                var EmailCode = EncMD5(uObj.EmailId);
                var Passwordlink = WebUrl + "User/NewPassword?Ec=" + EmailCode;
                Robj=await SendResetPasswordEmail(uObj, Passwordlink, EmailCode);
                return Robj;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
        }
        #endregion

        #region GetUserbyEmailCode
        [Route("UserEmail/GetUserbyEmailCode/{EmailCode}")]
        [HttpGet]
        public PasswordReset GetUserbyEmailCode(string EmailCode)
        {
            _logger.LogInformation("Inside get GetUserbyEmailCode function in UserController.cs -->UserService.");
            PasswordReset uObj = new PasswordReset();
            try
            {
                uObj = _userRepository.GetUserbyEmailCode(EmailCode);
                return uObj;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
        }
        #endregion

        #region NewPassword
        [Route("UserEmail/NewPassword")]
        [HttpPost]
        public DBResponse NewPassword([FromBody]PasswordReset lObj)
        {
            _logger.LogInformation("Inside post NewPassword() function in UserController.cs -->UserService.");
            DBResponse DBResponse = new DBResponse();
            try
            {
                DBResponse = _userRepository.NewPassword(lObj);
               
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }

            return DBResponse;
        }
        #endregion

        #region EncMD5

        public string EncMD5(string password)
        {
            _logger.LogInformation("Inside EncMD5() function in UserController.cs -->UserService.");
            MD5 md5 = new MD5CryptoServiceProvider();
            UTF8Encoding encoder = new UTF8Encoding();
            Byte[] originalBytes = encoder.GetBytes(password);
            Byte[] encodedBytes = md5.ComputeHash(originalBytes);
            password = BitConverter.ToString(encodedBytes).Replace("-", "");
            var result = password.ToLower();
            return result;
        }

        #endregion

        #region SendRegistrationEmail
        public async Task<DBResponse> SendRegistrationEmail(User lobj, string vrfylink,string EmailCode)
        {
            _logger.LogInformation("Inside SendRegistrationEmail() function in UserController.cs -->UserService.");
            var SentStatus = false;
            //email config
            try
            {
                //email config
                var filePath = Path.Combine(_hostingEnvironment.ContentRootPath, "EmailTemplate/EmailVerification.html");
                var htmlBody = System.IO.File.ReadAllText(filePath);

                #region normal SMTP
                MailMessage message = new MailMessage();
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(_mailSettings.Value.SmtpHost, _mailSettings.Value.MailPort);
                message.From = new MailAddress(_mailSettings.Value.MailerEmail, _mailSettings.Value.MailerName);
                message.To.Add(new MailAddress(lobj.EmailId));
                message.Subject = "Email Verification";
                message.IsBodyHtml = true; //to make message body as html  
                message.Body = htmlBody.Replace("[verifylink]", vrfylink);
                smtp.Port = _mailSettings.Value.MailPort;
                smtp.Host = _mailSettings.Value.SmtpHost;
                smtp.EnableSsl = _mailSettings.Value.EnableSSL;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(_mailSettings.Value.MailUsername, _mailSettings.Value.MailPassword);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
                #endregion

                //var mimeMessage = new MimeMessage();
                //mimeMessage.From.Add(new MailboxAddress(_mailSettings.Value.MailerEmail, _mailSettings.Value.MailerEmail));
                //mimeMessage.To.Add(new MailboxAddress(lobj.EmailId, lobj.EmailId));
                //mimeMessage.Subject = "Email Verification";
                //mimeMessage.Body = new TextPart("html")
                //{
                //    Text = htmlBody.Replace("[verifylink]", vrfylink)
                //};
                //using (var client = new MailKit.Net.Smtp.SmtpClient())
                //{
                //    await client.ConnectAsync(_mailSettings.Value.SmtpHost);
                //    // Note: only needed if the SMTP server requires authentication
                //    if (_mailSettings.Value.isAuthrequired)
                //        await client.AuthenticateAsync(_mailSettings.Value.MailUsername, _mailSettings.Value.MailPassword);

                //    await client.SendAsync(mimeMessage);
                //    await client.DisconnectAsync(true);
                //}
                SentStatus = true;

            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                SentStatus = false;
            }
          
            //repository
            UserEmail UserEmail = new UserEmail();
            UserEmail.Email = lobj.EmailId;
            UserEmail.EmailBody = vrfylink;
            UserEmail.EmailType= "R";
            UserEmail.EmailCode= EmailCode;
            UserEmail.SentStatus = SentStatus;
            UserEmail.CreatedBy = lobj.CreatedBy;
            DBResponse DBResponse = new DBResponse();
            try
            {
                DBResponse = _userRepository.AddUserEmail(UserEmail);

            }
            catch (Exception ex)
            {
                // throw ex;
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
            }
            if(SentStatus)
            {
                DBResponse.Message = "A verification link has been sent to your email address";
                DBResponse.MessageType = "SUCCESS";
            }
            else
            {
                DBResponse.Message = "There was an unexpected error and the verification mail was not sent.";
                DBResponse.MessageType = "ERROR";
            }
            return DBResponse;
        }
        #endregion

        #region SendResetPasswordEmail
        public async Task<DBResponse> SendResetPasswordEmail(User lobj, string Passwordlink, string EmailCode)
        {
            _logger.LogInformation("Inside  SendResetPasswordEmail() function in UserController.cs -->UserService.");
            var SentStatus = false;
            DBResponse DBResponse = new DBResponse();

            try
            {
                //email config
                var filePath = Path.Combine(_hostingEnvironment.ContentRootPath, "EmailTemplate/ResetPassword.html");
                var htmlBody = System.IO.File.ReadAllText(filePath);

                #region normal SMTP(obsolete)
                MailMessage message = new MailMessage();
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                message.From = new MailAddress(_mailSettings.Value.MailerEmail, _mailSettings.Value.MailerName);
                message.To.Add(new MailAddress(lobj.EmailId));
                message.Subject = "Reset Password";
                message.IsBodyHtml = true; //to make message body as html  
                message.Body = htmlBody.Replace("[passwordlink]", Passwordlink);
                smtp.Port = _mailSettings.Value.MailPort;
                smtp.Host = _mailSettings.Value.SmtpHost;
                smtp.EnableSsl = _mailSettings.Value.EnableSSL;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(_mailSettings.Value.MailUsername, _mailSettings.Value.MailPassword);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
                #endregion

                //var mimeMessage = new MimeMessage();
                //mimeMessage.From.Add(new MailboxAddress(_mailSettings.Value.MailerEmail,_mailSettings.Value.MailerEmail));
                //mimeMessage.To.Add(new MailboxAddress(lobj.EmailId,lobj.EmailId));
                //mimeMessage.Subject = "Reset Password";
                //mimeMessage.Body = new TextPart("html")
                //{
                //    Text = htmlBody.Replace("[passwordlink]", Passwordlink)
                //};
                //using (var client = new MailKit.Net.Smtp.SmtpClient())
                //{
                //        await client.ConnectAsync(_mailSettings.Value.SmtpHost);
                //    // Note: only needed if the SMTP server requires authentication
                //    if(_mailSettings.Value.isAuthrequired)
                //    await client.AuthenticateAsync(_mailSettings.Value.MailUsername, _mailSettings.Value.MailPassword);

                //    await client.SendAsync(mimeMessage);
                //    await client.DisconnectAsync(true);
                //}
                    SentStatus = true;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                DBResponse.MessageType = "ERROR";
                DBResponse.Message =  "There was an unexpected error and the mail was not send.";
                return DBResponse;
            }
            //repository
            try
            {
            UserEmail UserEmail = new UserEmail();
            UserEmail.Email = lobj.EmailId;
            UserEmail.EmailBody = Passwordlink;
            UserEmail.EmailType = "P";
            UserEmail.EmailCode = EmailCode;
            UserEmail.SentStatus = SentStatus;
            UserEmail.CreatedBy = lobj.CreatedBy;
           
            DBResponse = _userRepository.AddUserEmail(UserEmail);


            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                DBResponse.MessageType = "ERROR";
                DBResponse.Message = "There was an unexpected error and the details were not saved.";
            }
            return DBResponse;
        }
        #endregion

        #region GetAllUsersServerSide
        [Authorize]
        [HttpPost]
        [Route("GetAllUsersServerSide")]
        public DataTableServerSideResult<User> GetAllUsersServerSide(DataTableServerSideSettings DTtablesettings)
        {
            _logger.LogInformation("Inside post GetAllUsersServerSide() function in UserController.cs -->UserService.");
            DataTableServerSideResult<User> uobj = new DataTableServerSideResult<User>();
            try
            {
                uobj = _userRepository.GetAllUsersServerSide(DTtablesettings);
                return uobj;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
        }
        #endregion

        #region ForgotPassword
        [Route("Login/ForgotPassword")]
        [HttpPost]
        public async Task<DBResponse> ForgotPassword([FromBody]User uobj)
        {
            _logger.LogInformation("Inside post ForgotPassword() function in UserController.cs -->UserService.");
            DBResponse Robj = new DBResponse();
            try
            {
                Robj = _userRepository.CheckEmailId(uobj.EmailId);
                if(Robj.MessageType=="SUCCESS")
                {
                    var EmailCode = EncMD5(uobj.EmailId);
                    var Passwordlink = WebUrl + "User/NewPassword?Ec=" + EmailCode;
                    Robj = await SendResetPasswordEmail(uobj, Passwordlink, EmailCode);
                }
                
                return Robj;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
        }
        #endregion

        #region ResendVerify
        [Route("Login/ResendVerify")]
        [HttpPost]
        public async Task<DBResponse> ResendVerify([FromBody]User uobj)
        {
            _logger.LogInformation("Inside post ResendVerify() function in UserController.cs -->UserService.");
            DBResponse obj = new DBResponse();
            DBResponse Robj = new DBResponse();
            try
            {
                obj = _userRepository.CheckEmailId(uobj.EmailId);
                if (obj.MessageType == "SUCCESS")
                {
                    var EmailCode = EncMD5(uobj.EmailId);
                    var Verficationlink = WebUrl + "User/VerifyEmail?Ec=" + EmailCode;
                    Robj=await SendRegistrationEmail(uobj, Verficationlink, EmailCode);
                }
                else
                {
                    Robj.Message = "Email address is not valid or no registered user with that address exists";
                    Robj.MessageType = "ERROR";
                }

                return Robj;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
        }
        #endregion


     
    }
}