﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Repository;

namespace AgencyService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AgencyController : ControllerBase
    {
        private readonly IAgencyRepository _AgencyRepository;
        private readonly ILogger<AgencyController> _logger;
        public AgencyController(IAgencyRepository AgencyRepository, ILogger<AgencyController> logger)
        {
            _AgencyRepository = AgencyRepository;
            _logger = logger;
        }

        #region LoadAgencyService
        [Route("LoadAgencyService")]
        [HttpGet]
        public List<AgencyServiceDetails> LoadAgencyService(int PortId,int AgencyId)
        {
            _logger.LogInformation("Inside LoadAgencyService() function in AgencyController.cs -->UserService.");

            List<AgencyServiceDetails> cObj = new List<AgencyServiceDetails>();
            try
            {
                cObj = _AgencyRepository.LoadAgencyService(PortId,AgencyId);
                return cObj;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
        }
        #endregion

        #region GetVesselPortList
        [Route("GetVesselPortList")]
        [HttpGet]
        public List<VesselPort> GetVesselPortList(string search)
        {
            _logger.LogInformation("Inside GetVesselPortList() function in AgencyController.cs -->UserService.");
            List<VesselPort> VesselPort = new List<VesselPort>();
            try
            {
                VesselPort = _AgencyRepository.GetVesselPortList(search);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return VesselPort;
        }
        #endregion

        #region GetOwnerList
        [Route("GetOwnerList")]
        [HttpGet]
        public List<User> GetOwnerList(string search)
        {
            _logger.LogInformation("Inside GetVesselPortList() function in AgencyController.cs -->UserService.");
            List<User> ownerlist = new List<User>();
            try
            {
                ownerlist = _AgencyRepository.GetOwnerList(search);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return ownerlist;
        }
        #endregion

        #region GetServiceList
        [Route("GetServiceList")]
        [HttpGet]
        public List<ServiceList> GetServiceList()
        {
            _logger.LogInformation("Inside GetServiceList() function in AgencyController.cs -->AgencyService.");
            List<ServiceList> ServiceList = new List<ServiceList>();
            try
            {
                ServiceList = _AgencyRepository.GetServiceList();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return ServiceList;
        }
        #endregion

        #region LoadServicebyId
        [Route("LoadServicebyId")]
        [HttpPost]
        public List<AgencyServiceDetails> LoadServicebyId(string[] ids)
        {
            _logger.LogInformation("Inside GetServiceList() function in AgencyController.cs -->AgencyService.");
            List<AgencyServiceDetails> ServiceList = new List<AgencyServiceDetails>();
            try
            {
                ServiceList = _AgencyRepository.LoadServicebyId(ids);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return ServiceList;
        }
        #endregion

        #region SaveAgencyService
        [Route("SaveAgencyService")]
        [HttpPost]
        public DBResponse SaveAgencyService(Domain.AgencyService obj)
        {
            _logger.LogInformation("Inside SaveAgencyService() function in AgencyController.cs -->AgencyService.");
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _AgencyRepository.SaveAgencyService(obj);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return dbResponse;
        }
        #endregion

        #region UpdateAgencyService
        [Route("UpdateAgencyService")]
        [HttpPost]
        public DBResponse UpdateAgencyService(Domain.AgencyService obj)
        {
            _logger.LogInformation("Inside UpdateAgencyService() function in AgencyController.cs -->AgencyService.");
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _AgencyRepository.UpdateAgencyService(obj);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return dbResponse;
        }
        #endregion

        #region GetServiceListByCategory
        [Route("GetServiceListByCategory")]
        [HttpPost]
        public List<ServiceList> GetServiceListByCategory(string[] ids)
        {
            _logger.LogInformation("Inside GetServiceListByCategory() function in AgencyController.cs -->UserService.");
            List<ServiceList> ServiceList = new List<ServiceList>();
            try
            {
                ServiceList = _AgencyRepository.GetServiceListByCategory(ids);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return ServiceList;
        }
        #endregion

        #region GetServiceCategoryList
        [Route("GetServiceCategoryList")]
        [HttpGet]
        public List<ServiceCategory> GetServiceCategoryList()
        {
            _logger.LogInformation("Inside GetServiceCategoryList() function in AgencyController.cs -->UserService.");
            List<ServiceCategory> ServiceCategory = new List<ServiceCategory>();
            try
            {
                ServiceCategory = _AgencyRepository.GetServiceCategoryList();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return ServiceCategory;
        }
        #endregion

        #region LoadOwnerTemplate
        [Route("LoadOwnerTemplate")]
        [HttpGet]
        public OwnerTemplateMapping LoadOwnerTemplate(int OwnerId, int AgencyId)
        {
            _logger.LogInformation("Inside LoadOwnerTemplate() function in AgencyController.cs -->UserService.");
            OwnerTemplateMapping template = new OwnerTemplateMapping();
            try
            {
                template = _AgencyRepository.LoadOwnerTemplate(OwnerId, AgencyId);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return template;
        }
        #endregion

        #region DeleteService
        [Route("DeleteService/{id}")]
        [HttpGet]
        public DBResponse DeleteService(int id)
        {
            _logger.LogInformation("Inside DeleteService() function in AgencyController.cs -->AgencyService.");
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _AgencyRepository.DeleteService(id);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return dbResponse;
        }
        #endregion
    }
}