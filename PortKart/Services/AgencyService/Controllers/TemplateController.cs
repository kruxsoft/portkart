﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Repository;

namespace AgencyService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TemplateController : ControllerBase
    {
        private readonly ITemplateRepository _templateRepository;
        private readonly ILogger<TemplateController> _logger;

        public TemplateController(ITemplateRepository TemplateRepository, ILogger<TemplateController> logger)
        {
            _templateRepository = TemplateRepository;
            _logger = logger;
        }



        //#region AddTemplate
        //[Route("AddTemplate")]
        //[HttpPost]
        //public DBResponse AddTemplate([FromBody]Template sc)
        //{

        //    DBResponse dbResponse = new DBResponse();
        //    try
        //    {
        //        dbResponse = _templateRepository.AddTemplate(sc);
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //    return dbResponse;
        //}
        //#endregion

        #region GetServiceList
        [Route("GetServiceList")]
        [HttpGet]
        public List<ServiceList> GetServiceList()
        {
            _logger.LogInformation("Inside GetServiceList() function in TemplateController.cs -->AgencyService.");
            List<ServiceList> ServiceList = new List<ServiceList>();
            try
            {
                ServiceList = _templateRepository.GetServiceList();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return ServiceList;
        }
        #endregion
        #region GetTemplateList
        [Route("GetTemplateList")]
        [HttpGet]
        public List<Template> GetTemplateList()
        {
            _logger.LogInformation("Inside GetTemplateList() function in TemplateController.cs -->AgencyService.");
            List<Template> TemplateList = new List<Template>();
            try
            {
                TemplateList = _templateRepository.GetTemplateList();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return TemplateList;
        }
        #endregion


        #region LoadTemplateService
        [Route("LoadTemplateService")]
        [HttpGet]
        public List<TemplateDetails> LoadTemplateService(int TemplateId, int AgencyId)
        {
            _logger.LogInformation("Inside LoadTemplateService() function in TemplateController.cs -->UserService.");

            List<TemplateDetails> cObj = new List<TemplateDetails>();
            try
            {
                cObj = _templateRepository.LoadTemplateService(TemplateId, AgencyId);
                return cObj;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
        }
        #endregion

        #region LoadServicebyId
        [Route("LoadServicebyId/{id}")]
        [HttpGet]
        public List<AgencyServiceDetails> LoadServicebyId(int id)
        {
            _logger.LogInformation("Inside GetServiceList() function in AgencyController.cs -->AgencyService.");
            List<AgencyServiceDetails> ServiceList = new List<AgencyServiceDetails>();
            try
            {
                ServiceList = _templateRepository.LoadServicebyId(id);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return ServiceList;
        }
        #endregion

        #region SaveAgencyServiceTemplate
        [Route("SaveAgencyServiceTemplate")]
        [HttpPost]
        public DBResponse SaveAgencyServiceTemplate(Domain.Template obj)
        {
            _logger.LogInformation("Inside SaveAgencyServiceTemplate() function in Template.cs -->Template.");
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _templateRepository.SaveAgencyServiceTemplate(obj);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return dbResponse;
        }
        #endregion


        #region GetAllOwnerTemplateList
        [Route("GetAllOwnerTemplateList")]
        [HttpGet]
        public IEnumerable<OwnerTemplate> GetAllOwnerTemplateList()
        {
            List<OwnerTemplate> sObj = new List<OwnerTemplate>();
            try
            {
                sObj = _templateRepository.GetAllOwnerTemplateList();
                return sObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetOwnerTemplateList
        [Route("GetOwnerTemplateList")]
        [HttpGet]
        public IEnumerable<OwnerTemplate> GetOwnerTemplateList()
        {
            List<OwnerTemplate> sObj = new List<OwnerTemplate>();
            try
            {
                sObj = _templateRepository.GetOwnerTemplateList();
                return sObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion




        #region AddOwnerTemplate
        [Route("AddOwnerTemplate")]
        [HttpPost]
        public DBResponse AddOwnerTemplate([FromBody] OwnerTemplate sc)
        {

            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _templateRepository.AddOwnerTemplate(sc);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dbResponse;
        }
        #endregion


        #region GetOwnerTemplateById
        [Route("GetOwnerTemplateById/{id}")]
        [HttpGet]
        public OwnerTemplate GetOwnerTemplateById(int id)
        {

            OwnerTemplate cObj = new OwnerTemplate();
            try
            {
                cObj = _templateRepository.GetOwnerTemplateById(id);
                return cObj;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion


        #region UpdateOwnerTemplate
        [Route("UpdateOwnerTemplate")]
        [HttpPost]
        public DBResponse UpdateOwnerTemplate([FromBody] OwnerTemplate sc)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _templateRepository.UpdateOwnerTemplate(sc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dbResponse;
        }
        #endregion

        #region DeleteOwnerTemplate
        [Route("DeleteOwnerTemplate")]
        [HttpPost]
        public DBResponse DeleteOwnerTemplate([FromBody] OwnerTemplate sc)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _templateRepository.DeleteOwnerTemplate(sc);
                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region GetUserOwnerList
        [Route("GetUserOwnerList")]
        [HttpGet]
        public List<User> GetUserOwnerList()
        {
            _logger.LogInformation("Inside get GetUserOwnerList() function in UserController.cs -->UserService.");
            List<User> uobj = new List<User>();
            try
            {
                uobj = _templateRepository.GetUserOwnerList();
                return uobj;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
        }
        #endregion


        #region GetPortInOwnerTemplate
        [Route("GetPortInOwnerTemplate")]
        [HttpGet]
        public IEnumerable<VesselPort> GetPortInOwnerTemplate()
        {
            List<VesselPort> sObj = new List<VesselPort>();
            try
            {
                sObj = _templateRepository.GetPortInOwnerTemplate();
                return sObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region GetTemplateNameList
        [Route("GetTemplateNameList")]
        [HttpGet]
        public List<Template> GetTemplateNameList(string search)
        {
            _logger.LogInformation("Inside GetTemplateNameList() function in templateController.cs ");
            List<Template> sObj = new List<Template>();
            try
            {
                sObj = _templateRepository.GetTemplateNameList(search);

            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return sObj;
        }
        #endregion

        #region UpdateServiceTemplate
        [Route("UpdateServiceTemplate")]
        [HttpPost]
        public DBResponse UpdateServiceTemplate(Domain.Template obj)
        {
            _logger.LogInformation("Inside UpdateServiceTemplate() function in TemplateController.cs -->ServiceTemplate.");
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _templateRepository.UpdateServiceTemplate(obj);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return dbResponse;
        }
        #endregion

        #region GetVesselPortList
        [Route("GetVesselPortList")]
        [HttpGet]
        public List<VesselPort> GetVesselPortList(string search)
        {
            _logger.LogInformation("Inside GetVesselPortList() function in AgencyController.cs -->UserService.");
            List<VesselPort> VesselPort = new List<VesselPort>();
            try
            {
                VesselPort = _templateRepository.GetVesselPortList(search);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return VesselPort;
        }
        #endregion

    }
}
