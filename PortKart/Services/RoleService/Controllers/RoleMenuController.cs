﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repository;

namespace RoleService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleMenuController : ControllerBase
    {
        private readonly IRoleRepository _roleRepository;
        private readonly IMenuRepository _menuRepository;
        private readonly IRoleMenuRepository _rolemenuRepository;
        public RoleMenuController(IRoleRepository RoleRepository,IMenuRepository menuRepository,IRoleMenuRepository roleMenuRepository)
        {
            _roleRepository = RoleRepository;
            _menuRepository = menuRepository;
            _rolemenuRepository = roleMenuRepository;
        }

        #region GetRoles
        // GET: api/RoleMenu
        public IEnumerable<Role> GetRoles()
        {
            List<Role> cObj = new List<Role>();
            try
            {
                cObj = _roleRepository.GetRoles();
                return cObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region GetMenus
        // GET: api/RoleMenu
        public IEnumerable<Menu> GetMenus()
        {
            List<Menu> mObj = new List<Menu>();
            try
            {
                mObj = _menuRepository.GetMenus();

                return mObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region GetRoleMenus
        [Route("GetRoleMenus/{roleid}")]
        [HttpGet]
        public IEnumerable<RoleMenu> GetRoleMenus(int roleid)
        {
            List<RoleMenu> mObj = new List<RoleMenu>();
            try
            {
                mObj = _rolemenuRepository.GetRolemenus(roleid);

                return mObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region GetAllRoleMenus
        [Route("GetAllRoleMenus/")]
        [HttpGet]
        public IEnumerable<RoleMenu> GetAllRoleMenus()
        {
            List<RoleMenu> mObj = new List<RoleMenu>();
            try
            {
                mObj = _rolemenuRepository.GetAllRoleMenus();
                return mObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region AddRolemenu

        [Route("AddRolemenu")]
        [HttpPost]
        public DBResponse AddRoleMenu([FromBody]RoleMenu rl)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _rolemenuRepository.AddRolemenu(rl);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dbResponse;
        }
        #endregion

        #region DeleteRolemenu
        [Route("DeleteRolemenu")]
        [HttpPost]
        public DBResponse DeleteRolemenu([FromBody]RoleMenu roleMenu)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _rolemenuRepository.DeleteRolemenu(roleMenu);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dbResponse;
        }
        #endregion

        #region GetMenuList
        [Route("GetMenuList")]
        [HttpGet]
        public List<RoleMenu> GetMenuList()
        {
            List<RoleMenu> roleMenus = new List<RoleMenu>();
            try
            {
                roleMenus = _rolemenuRepository.GetMenuList();
                return roleMenus;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}