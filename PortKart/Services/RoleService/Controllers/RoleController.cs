﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repository;

namespace RoleService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IRoleRepository _roleRepository;
        public RoleController(IRoleRepository RoleRepository)
        {
            _roleRepository = RoleRepository;
        }

        #region GetRoles

        [HttpGet]
        [Route("GetRoles")]
        public IActionResult GetRoles()
        {
            List<Role> uobj = new List<Role>();
            try
            {
                uobj = _roleRepository.GetRoles();
                return new JsonResult(uobj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetRolesList
        [Route("GetRolesList")]
        [HttpGet]
        public IEnumerable<Role> GetRolesList()
        {

            List<Role> cObj = new List<Role>();
            try
            {
                cObj = _roleRepository.GetRolesList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return cObj;
        }
        #endregion GetRoles

        #region GetRoleById
        [Route("GetRoleById/{id}")]
        [HttpGet]
        public Role GetRoleById(int id)
        {

            Role rObj = new Role();
            try
            {
                rObj = _roleRepository.GetRoleById(id);

                return rObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion GetRoleById

        #region AddRole
        [Route("AddRole")]
        [HttpPost]
        public DBResponse AddRole([FromBody]Role rl)
        {

            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _roleRepository.AddRole(rl);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dbResponse;
        }
        #endregion AddRole

        #region UpdateRole
        [Route("UpdateRole")]
        [HttpPost]
        public DBResponse UpdateRole([FromBody]Role rl)
        {

            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _roleRepository.UpdateRole(rl);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dbResponse;
        }
        #endregion UpdateRole

        #region DeleteRole
        [Route("DeleteRole")]
        [HttpPost]
        public DBResponse DeleteRole([FromBody]Role role)
        {

            DBResponse dbResponse = new DBResponse();
            try
            {
                dbResponse = _roleRepository.DeleteRole(role);

                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion DeleteRole

        #region GetRoleNameByRoleId
        [Route("GetRoleNameByRoleId/{roleId}")]
        [HttpGet]
        public string GetRoleNameByRoleId(int roleId)
        {

            string roleName = "";
            try
            {
                roleName = _roleRepository.GetRoleNameByRoleId(roleId);

                return roleName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}