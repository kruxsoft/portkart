﻿using Domain;
using NLog;
using PortKart_WEB.Models;
using PortKart_WEB.Security;
using PortKart_WEB.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;


namespace PortKart_WEB.Controllers
{
    [AuthorizationFilter]
    [RollCheck]
    public class VesselPortController : Controller
    {
        // GET: VesselPort
        #region GLOBAL VARIABLE
        public CommonMethods pObj = new CommonMethods();
        private static readonly Logger databaseLogger = LogManager.GetLogger("databaseLogger");
        private static readonly Logger fileLogger = LogManager.GetLogger("fileLogger");
        #endregion

        #region Index
        public ActionResult Index()
        {
            fileLogger.Info("Inside Index() function in VesselPortController.cs -->Web.");
            return View();
        }
        #endregion

        #region GetVesselPortList
        public ActionResult GetVesselPortList()
        {
            fileLogger.Info("Inside GetVesselPortList() function in VesselPortController.cs -->Web.");
            List<VesselPortModel> serviceCategory = new List<VesselPortModel>();
            try
            {
                serviceCategory = GetAllVesselPorts();
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Erron on GetVesselPortList() funcion in VesselPortController.cs -->Web..");
                databaseLogger.Error(ex, "Erron on GetVesselPortList() funcion in VesselPortController.cs -->Web..");

                throw ex;
            }
            var jsonresult = Json(serviceCategory, JsonRequestBehavior.AllowGet);
            jsonresult.MaxJsonLength = int.MaxValue;
            return jsonresult;
        }
        #endregion

        #region GetAllVesselPorts
        List<VesselPortModel> GetAllVesselPorts()
        {
            fileLogger.Info("Inside GetAllVesselPorts() function in VesselPortController.cs -->Web.");
            List<VesselPortModel> sObj = new List<VesselPortModel>();
            using (HttpClient hObj = pObj.Client())
            {
                var responseTask = hObj.GetAsync("VesselPort/GetAllVesselPorts");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<VesselPortModel>>();
                    readTask.Wait();
                    sObj = readTask.Result;
                }
            }
            return sObj;
        }
        #endregion

        #region AddVesselPort
        [HttpGet]
        public ActionResult AddVesselPort()
        {
            fileLogger.Info("Inside AddVesselPort() function in VesselPortController.cs -->Web.");
            BindCountry();
            return View();
        }
        #endregion

        #region AddVesselPort
        [HttpPost]
        public ActionResult AddVesselPort(VesselPortModel vesselPort)
        {

            fileLogger.Info("Inside AddVesselPort() function in VesselPortController.cs -->Web.");
            if (ModelState.IsValid)
            {
                try
                {
                    DBResponse dbResponse = new DBResponse();
                    vesselPort.CreatedBy = Convert.ToInt32(Session["UserId"]);
                    vesselPort.ModifiedBy = Convert.ToInt32(Session["UserId"]);

                    using (HttpClient hObj = pObj.Client())
                    {
                        var responseTask = hObj.PostAsJsonAsync<VesselPortModel>("VesselPort/AddVesselPort", vesselPort);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                    }
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {
                            BindCountry();
                            return View();
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on AddVesselPort() funcion in VesselPortController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on AddVesselPort() funcion in VesselPortController.cs -->Web..");

                    throw ex;
                }
                return RedirectToAction("Index", "VesselPort");
            }
            else
            {
                BindCountry();
                return View();
            }
        }
        #endregion

        #region UpdateVesselPort
        [HttpGet]
        public ActionResult UpdateVesselPort(int id)
        {
            fileLogger.Info("Inside UpdateVesselPort() function in VesselPortController.cs -->Web.");
            VesselPortModel cobj = new VesselPortModel();
            try
            {
                BindCountry();
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("VesselPort/GetVesselPortById/" + id);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<VesselPortModel>();
                        readTask.Wait();
                        cobj = readTask.Result;
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on UpdateVesselPort() funcion in VesselPortController.cs -->Web..");
                databaseLogger.Error(ex, "Error on UpdateVesselPort() funcion in VesselPortController.cs -->Web..");
                throw ex;
            }
            return View(cobj);
        }
        #endregion

        #region UpdateVesselPort
        [HttpPost]
        public ActionResult UpdateVesselPort(VesselPortModel VesselPortModel)
        {
            fileLogger.Info("Inside UpdateVesselPort() function in VesselPortController.cs -->Web.");
            if (ModelState.IsValid)
            {
                try
                {
                    DBResponse dbResponse = new DBResponse();
                    VesselPortModel.ModifiedBy = Convert.ToInt32(Session["UserId"]);
                    using (HttpClient hObj = pObj.Client())
                    {
                        var responseTask = hObj.PostAsJsonAsync<VesselPortModel>("VesselPort/UpdateVesselPort", VesselPortModel);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                        if (dbResponse != null)
                        {
                            TempData["Message"] = dbResponse.Message;
                            TempData["MessageType"] = dbResponse.MessageType;
                            if (dbResponse.MessageType == "ERROR")
                            {
                                BindCountry();
                                return View();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on UpdateVesselPort() funcion in VesselPortController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on UpdateVesselPort() funcion in VesselPortController.cs -->Web..");

                    throw ex;
                }
                return RedirectToAction("Index", "VesselPort");
            }
            else
            {
                BindCountry();
                return View();
            }
        }
        #endregion

        #region DeleteVesselPort
        public ActionResult DeleteVesselPort(int id)
        {
            fileLogger.Info("Inside DeleteVesselPort() function in VesselPortController.cs -->Web.");
            DBResponse dbResponse = new DBResponse();
            try
            {
                VesselPortModel VesselPortModel = new VesselPortModel();
                VesselPortModel.PortId = id;
                VesselPortModel.ModifiedBy = id;
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.PostAsJsonAsync<VesselPortModel>("VesselPort/DeleteVesselPort", VesselPortModel);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<DBResponse>();
                        readTask.Wait();
                        dbResponse = readTask.Result;
                    }
                }
                if (dbResponse != null)
                {
                    TempData["Message"] = dbResponse.Message;
                    TempData["MessageType"] = dbResponse.MessageType;
                    if (dbResponse.MessageType == "ERROR")
                    {
                        return RedirectToAction("Index", "VesselPort");
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on DeleteVesselPort() funcion in VesselPortController.cs -->Web..");
                databaseLogger.Error(ex, "Error on DeleteVesselPort() funcion in VesselPortController.cs -->Web..");
                throw ex;
            }
            return RedirectToAction("Index", "VesselPort");
        }
        #endregion

        #region BindCountry
        public void BindCountry()
        {
            fileLogger.Info("Inside BindCountry() function in VesselPortController.cs -->Web.");
            List<Country> country = new List<Country>();
            country = pObj.BindCountry();
            ViewBag.Country = new SelectList(country, "CountryId", "CountryName");
        }
        #endregion

        #region  ImportExcell

        [HttpPost]
        public ActionResult ImportExcell(HttpPostedFileBase postedfile)
        {
            fileLogger.Info("Inside ImportExcell() function in VesselPortController.cs -->Web.");

            try
            {
                BulkUploadResponse dbResponse = new BulkUploadResponse();

                using (HttpClient hObj = pObj.Client())
                {
                    using (var content = new MultipartFormDataContent())
                    {

                        byte[] Bytes = new byte[postedfile.InputStream.Length + 1];
                        postedfile.InputStream.Read(Bytes, 0, Bytes.Length);
                        var fileContent = new ByteArrayContent(Bytes);
                        fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") { FileName = postedfile.FileName };
                        content.Add(fileContent);
                        var responseTask = hObj.PostAsync("VesselPort/VesselPortExcellImport", content);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<BulkUploadResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                        // if()
                    }
                }
                // ViewBag.Message = dbResponse.Message;
                return View();

            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on ImportExcell() funcion in VesselPortController.cs -->Web..");
                databaseLogger.Error(ex, "Error on ImportExcell() funcion in VesselPortController.cs -->Web..");

                ViewBag.Message = "File upload failed!!";
                return View();
            }

        }
        #endregion

        #region GetVesselPortListServerSide
        public ActionResult GetVesselPortListServerSide()
        {
            fileLogger.Info("Inside GetVesselPortListServerSide() function in VesselPortController.cs -->Web.");
            DataTableServerSideResult<VesselPortModel> serviceCategory = new DataTableServerSideResult<VesselPortModel>();
            try
            {
                serviceCategory = GetAllVesselPortsServerSide();
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Erron GetVesselPortList() funcion in VesselPortController.cs -->Web..");
                 databaseLogger.Error(ex, "Erron GetVesselPortList() funcion in VesselPortController.cs -->Web..");

                throw ex;
            }
            var jsonresult = Json(serviceCategory, JsonRequestBehavior.AllowGet);
            jsonresult.MaxJsonLength = int.MaxValue;
            return jsonresult;
        }
        #endregion

        #region GetAllVesselPorts
         DataTableServerSideResult<VesselPortModel> GetAllVesselPortsServerSide()
        {
            fileLogger.Info("Inside GetAllVesselPortsServerSide() function in VesselPortController.cs -->Web.");
            DataTableServerSideResult<VesselPortModel> DTresult = new DataTableServerSideResult<VesselPortModel>();
            DataTableServerSideSettings DTtablesettings = new DataTableServerSideSettings();

            //Datatable parameter
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            //paging parameter
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //sorting parameter
            DTtablesettings.sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            DTtablesettings.sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            //full search value
            DTtablesettings.searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

            //individual column search
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[0][name]").FirstOrDefault(), Request.Form.GetValues("columns[0][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[1][name]").FirstOrDefault(), Request.Form.GetValues("columns[1][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[2][name]").FirstOrDefault(), Request.Form.GetValues("columns[2][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[3][name]").FirstOrDefault(), Request.Form.GetValues("columns[3][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[4][name]").FirstOrDefault(), Request.Form.GetValues("columns[4][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[5][name]").FirstOrDefault(), Request.Form.GetValues("columns[5][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[6][name]").FirstOrDefault(), Request.Form.GetValues("columns[6][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[7][name]").FirstOrDefault(), Request.Form.GetValues("columns[7][search][value]").FirstOrDefault()); 
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[8][name]").FirstOrDefault(), Request.Form.GetValues("columns[8][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[9][name]").FirstOrDefault(), Request.Form.GetValues("columns[9][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[10][name]").FirstOrDefault(), Request.Form.GetValues("columns[10][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[11][name]").FirstOrDefault(), Request.Form.GetValues("columns[11][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[12][name]").FirstOrDefault(), Request.Form.GetValues("columns[12][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[13][name]").FirstOrDefault(), Request.Form.GetValues("columns[13][search][value]").FirstOrDefault());

            DTtablesettings.pageSize = length != null ? Convert.ToInt32(length) : 0;
            DTtablesettings.skip = start != null ? Convert.ToInt32(start) : 0;
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    
                        var responseTask = hObj.PostAsJsonAsync<DataTableServerSideSettings>("VesselPort/GetAllVesselPortsServerSide", DTtablesettings);
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DataTableServerSideResult<VesselPortModel>>();
                            readTask.Wait();
                            DTresult = readTask.Result;
                        }
                    
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on GetAllVesselPortsServerSide() funcion in VesselPortController.cs -->Web..");
                databaseLogger.Error(ex, "Error on GetAllVesselPortsServerSide() funcion in VesselPortController.cs -->Web..");

                throw ex;
            }
            return DTresult;
        }
        #endregion
    }
}