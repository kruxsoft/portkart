﻿using Domain;
using NLog;
using PortKart_WEB.Security;
using PortKart_WEB.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace PortKart_WEB.Controllers
{
    [AuthorizationFilter]
    [AuthoriseAction(Group ="Agency")]
    public class AgencyController : Controller
    {
        // GET: Agency
        #region GLOBAL VARIABLE
        public CommonMethods pObj = new CommonMethods();
        private static readonly Logger databaseLogger = LogManager.GetLogger("databaseLogger");
        private static readonly Logger fileLogger = LogManager.GetLogger("fileLogger");
        #endregion

        #region Index
        public ActionResult Index()
        {
            BindCurrency();
            BindServiceCategoryList();
            BindService();
            return View();
        }
        #endregion

        #region GetVesselPortList
        [HttpGet]
        public JsonResult GetVesselPortList(string search)
        {
            fileLogger.Info("Inside GetVesselPortList() function in AgencyController.cs -->Web.");
            List<VesselPort> ServiceList = new List<VesselPort>();
            List<Select2Model> ResultList = new List<Select2Model>();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("Agency/GetVesselPortList?search=" + search);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<VesselPort>>();
                        readTask.Wait();

                        ServiceList = readTask.Result;
                    }

                }
                //ViewBag.PortList = ServiceList.Take(50);
                if (ServiceList.Count > 0)
                    ResultList = ServiceList.Select(x => new Select2Model { id = x.PortId, text = x.Port }).ToList();

                var jsonresult = Json(ResultList, JsonRequestBehavior.AllowGet);
                jsonresult.MaxJsonLength = int.MaxValue;
                return jsonresult;
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get GetVesselPortList() funcion in AgencyController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get GetVesselPortList() funcion in AgencyController.cs -->Web..");

                throw ex;
            }



        }
        #endregion

        #region LoadAgencyService
        [HttpGet]
        public JsonResult LoadAgencyService(int id)
        {
            fileLogger.Info("Inside LoadAgencyService() function in AgencyController.cs -->Web.");
            List<AgencyServiceDetails> ServiceList = new List<AgencyServiceDetails>();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var AgencyId = Convert.ToInt32(Session["UserId"]);
                    var responseTask = hObj.GetAsync("Agency/LoadAgencyService?PortId=" + id+ "&AgencyId="+ AgencyId);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<AgencyServiceDetails>>();
                        readTask.Wait();

                        ServiceList = readTask.Result;
                    }

                }

                var jsonresult = Json(ServiceList, JsonRequestBehavior.AllowGet);
                jsonresult.MaxJsonLength = int.MaxValue;
                return jsonresult;
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get LoadAgencyService() funcion in AgencyController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get LoadAgencyService() funcion in AgencyController.cs -->Web..");

                throw ex;
            }



        }
        #endregion

        #region LoadServicebyId
        [HttpPost]
        public JsonResult LoadServicebyId(string[] ids)
        {
            fileLogger.Info("Inside LoadServicebyId() function in AgencyController.cs -->Web.");
            List<AgencyServiceDetails> ServiceList = new List<AgencyServiceDetails>();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.PostAsJsonAsync<string[]>("Agency/LoadServicebyId/", ids);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<AgencyServiceDetails>>();
                        readTask.Wait();

                        ServiceList = readTask.Result;
                    }

                }

                var jsonresult = Json(ServiceList, JsonRequestBehavior.AllowGet);
                jsonresult.MaxJsonLength = int.MaxValue;
                return jsonresult;
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get LoadServicebyId() funcion in AgencyController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get LoadServicebyId() funcion in AgencyController.cs -->Web..");

                throw ex;
            }



        }
        #endregion

        #region SaveAgencyService
        [HttpPost]
        public JsonResult SaveAgencyService(AgencyService obj)
        {
            fileLogger.Info("Inside SaveAgencyService() function in AgencyController.cs -->Web.");
            DBResponse dbResponse = new DBResponse();

                try
                {

                    obj.AgencyId = Convert.ToInt32(Session["UserId"]);
                    obj.CreatedBy = Convert.ToInt32(Session["UserId"]);
                    using (HttpClient hObj = pObj.Client())
                    {
                       
                        var responseTask = hObj.PostAsJsonAsync<AgencyService>("Agency/SaveAgencyService", obj);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                        else
                        {
                            fileLogger.Error(result.StatusCode.ToString(), "Connection Error on SaveAgencyService() funcion in AgencyController.cs -->Web..");
                            databaseLogger.Error(result.StatusCode.ToString(), "Connection Error on SaveAgencyService() funcion in AgencyController.cs -->Web..");
                            dbResponse.Message = "Failed to save agency service details";
                            dbResponse.MessageType = "ERROR";
                            return Json(dbResponse);
                        }
                    }

                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on SaveAgencyService() funcion in AgencyController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on SaveAgencyService() funcion in AgencyController.cs -->Web..");

                    throw ex;
                }
                return Json(dbResponse);
            
        }
        #endregion

        #region UpdateAgencyService
        [HttpPost]
        public JsonResult UpdateAgencyService(AgencyService obj)
        {
            fileLogger.Info("Inside UpdateAgencyService() function in AgencyController.cs -->Web.");
            DBResponse dbResponse = new DBResponse();
                try
                {

                    obj.AgencyId = Convert.ToInt32(Session["UserId"]);
                    obj.CreatedBy = Convert.ToInt32(Session["UserId"]);
                    using (HttpClient hObj = pObj.Client())
                    {

                        var responseTask = hObj.PostAsJsonAsync<AgencyService>("Agency/UpdateAgencyService", obj);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                        else
                        {
                            fileLogger.Error(result.StatusCode.ToString(), "Connection Error on UpdateAgencyService() funcion in AgencyController.cs -->Web..");
                            databaseLogger.Error(result.StatusCode.ToString(), "Connection Error on UpdateAgencyService() funcion in AgencyController.cs -->Web..");
                            dbResponse.Message = "Failed to update agency service details";
                            dbResponse.MessageType = "ERROR";
                            return Json(dbResponse);
                        }
                    }

                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on UpdateAgencyService() funcion in AgencyController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on UpdateAgencyService() funcion in AgencyController.cs -->Web..");

                    throw ex;
                }
                return Json(dbResponse);
            
        }
        #endregion

        #region BindCurrency
        public void BindCurrency()
        {
            fileLogger.Info("Inside BindCurrency() function in AgencyController.cs -->Web.");
            List<CurrencyDomain> currencylist = new List<CurrencyDomain>();
            using (HttpClient hObj = pObj.Client())
            {
                var responseTask = hObj.GetAsync("Currency/GetCurrencyList");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<CurrencyDomain>>();
                    readTask.Wait();

                    currencylist = readTask.Result;
                }

            }
            ViewBag.CurrencyList = currencylist;
        }
        #endregion

        #region BindService
        public void BindService()
        {
            fileLogger.Info("Inside BindService() function in AgencyController.cs -->Web.");
            List<ServiceList> list = new List<ServiceList>();
            using (HttpClient hObj = pObj.Client())
            {
                var responseTask = hObj.GetAsync("Agency/GetServiceList");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<ServiceList>>();
                    readTask.Wait();

                    list = readTask.Result;
                }

            }
            ViewBag.ServiceList = list;
        }
        #endregion

        #region BindServiceCategoryList
        public void BindServiceCategoryList()
        {
            fileLogger.Info("Inside BindServiceCategoryList() function in AgencyController.cs -->Web.");
            List<ServiceCategory> ServiceList = new List<ServiceCategory>();

            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("Agency/GetServiceCategoryList");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<ServiceCategory>>();
                        readTask.Wait();

                        ServiceList = readTask.Result;
                    }

                }
                ViewBag.ServiceCategoryList = ServiceList;
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get BindServiceCategoryList() funcion in AgencyController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get BindServiceCategoryList() funcion in AgencyController.cs -->Web..");

                throw ex;
            }

        }
        #endregion

        #region LoadServiceList
        [HttpPost]
        public JsonResult LoadServiceList(string[] ids)
        {
            fileLogger.Info("Inside GetServiceList() function in AgencyController.cs -->Web.");
            List<ServiceList> ServiceList = new List<ServiceList>();
            if (ids == null)
            {
                ids = new string[] { };
            }
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.PostAsJsonAsync<string[]>("Agency/GetServiceListByCategory", ids);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<ServiceList>>();
                        readTask.Wait();

                        ServiceList = readTask.Result;
                    }

                }

                return Json(ServiceList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get GetServiceList() funcion in AgencyController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get GetServiceList() funcion in AgencyController.cs -->Web..");

                throw ex;
            }


        }
        #endregion

        #region GetOwnerList
        [HttpGet]
        public JsonResult GetOwnerList(string search)
        {
            fileLogger.Info("Inside GetOwnerList() function in AgencyController.cs -->Web.");
            List<User> UserList = new List<User>();
            List<Select2Model> ResultList = new List<Select2Model>();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("Agency/GetOwnerList?search=" + search);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<User>>();
                        readTask.Wait();

                        UserList = readTask.Result;
                    }

                }
                //ViewBag.PortList = ServiceList.Take(50);
                if (UserList.Count > 0)
                    ResultList = UserList.Select(x => new Select2Model { id = x.UserId, text = (x.FirstName+" "+x.LastName+" ("+x.EmailId+")" )}).ToList();

                var jsonresult = Json(ResultList, JsonRequestBehavior.AllowGet);
                jsonresult.MaxJsonLength = int.MaxValue;
                return jsonresult;
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get GetOwnerList() funcion in AgencyController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get GetOwnerList() funcion in AgencyController.cs -->Web..");

                throw ex;
            }



        }
        #endregion

        #region LoadOwnerTemplate
        [HttpGet]
        public JsonResult LoadOwnerTemplate(int id)
        {
            fileLogger.Info("Inside LoadOwnerTemplate() function in AgencyController.cs -->Web.");
            OwnerTemplateMapping template = new OwnerTemplateMapping();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var AgencyId = Convert.ToInt32(Session["UserId"]);
                    var responseTask = hObj.GetAsync("Agency/LoadOwnerTemplate?OwnerId=" + id + "&AgencyId=" + AgencyId);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<OwnerTemplateMapping>();
                        readTask.Wait();

                        template = readTask.Result;
                    }

                }

                var jsonresult = Json(template, JsonRequestBehavior.AllowGet);
                jsonresult.MaxJsonLength = int.MaxValue;
                return jsonresult;
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get LoadOwnerTemplate() funcion in AgencyController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get LoadOwnerTemplate() funcion in AgencyController.cs -->Web..");

                throw ex;
            }



        }
        #endregion

        #region DeleteService
        [HttpGet]
        public JsonResult DeleteService(int id)
        {
            fileLogger.Info("Inside DeleteService() function in AgencyController.cs -->Web.");
            DBResponse dbResponse = new DBResponse();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("Agency/DeleteService/" + id);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<DBResponse>();
                        readTask.Wait();

                        dbResponse = readTask.Result;
                    }
                    else
                    {
                        fileLogger.Error(result.StatusCode.ToString(), "Connection Error on DeleteService() funcion in AgencyController.cs -->Web..");
                        databaseLogger.Error(result.StatusCode.ToString(), "Connection Error on DeleteService() funcion in AgencyController.cs -->Web..");
                        dbResponse.Message = "Failed to delete agency service details";
                        dbResponse.MessageType = "ERROR";
                        return Json(dbResponse);
                    }
                }

                var jsonresult = Json(dbResponse, JsonRequestBehavior.AllowGet);
                jsonresult.MaxJsonLength = int.MaxValue;
                return jsonresult;
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get DeleteService() funcion in AgencyController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get DeleteService() funcion in AgencyController.cs -->Web..");

                throw ex;
            }



        }
        #endregion
    }
}