﻿using Domain;
using NLog;
using PortKart_WEB.Security;
using PortKart_WEB.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace PortKart_WEB.Controllers
{
    [AuthorizationFilter]
    public class OwnerController : Controller
    {
        // GET: Owner
        #region GLOBAL VARIABLE
        public CommonMethods pObj = new CommonMethods();
        private static readonly Logger databaseLogger = LogManager.GetLogger("databaseLogger");
        private static readonly Logger fileLogger = LogManager.GetLogger("fileLogger");
        #endregion

        [AuthoriseAction(Group = "Owner")]

        #region Index
        public ActionResult Index()
        {
            // BindService();
            // GetVesselPortList();
            GetServiceCategoryList();

            List<ServiceList> Srvlist = new List<ServiceList>();
            Srvlist = GetServiceList(new string[] { });
            ViewBag.ServiceList = Srvlist;
            return View();
        }
        #endregion

        #region Compare
        public ActionResult Compare(int[] ids)
        {
            //BindService();
            //BindAgencyService();
            return View();
        }
        #endregion


        #region BindAgencyService
        public void BindAgencyService()
        {
            fileLogger.Info("Inside BindAgencyService() function in OwnerController.cs -->Web.");
            List<AgencyService> ServiceList = new List<AgencyService>();
            using (HttpClient hObj = pObj.Client())
            {
                var responseTask = hObj.GetAsync("Owner/GetAgencyServiceForOwner");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<AgencyService>>();
                    readTask.Wait();

                    ServiceList = readTask.Result;
                }

            }
            ViewBag.AgencyCount = ServiceList.Count;
        }
        #endregion

        #region GetVesselPortList
        [HttpGet]
        public JsonResult GetVesselPortList(string search)
        {
            fileLogger.Info("Inside GetVesselPortList() function in OwnerController.cs -->Web.");
            List<VesselPort> ServiceList = new List<VesselPort>();
            List<Select2Model> ResultList = new List<Select2Model>();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("Owner/GetVesselPortList?search=" + search);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<VesselPort>>();
                        readTask.Wait();

                        ServiceList = readTask.Result;
                    }

                }
                //ViewBag.PortList = ServiceList.Take(50);
                if (ServiceList.Count > 0)
                    ResultList = ServiceList.Select(x => new Select2Model { id = x.PortId, text = x.Port }).ToList();

                var jsonresult = Json(ResultList, JsonRequestBehavior.AllowGet);
                jsonresult.MaxJsonLength = int.MaxValue;
                return jsonresult;
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get GetVesselPortList() funcion in OwnerController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get GetVesselPortList() funcion in OwnerController.cs -->Web..");

                throw ex;
            }



        }
        #endregion

        #region GetServiceCategoryList
        public void GetServiceCategoryList()
        {
            fileLogger.Info("Inside GetServiceCategoryList() function in OwnerController.cs -->Web.");
            List<ServiceCategory> ServiceList = new List<ServiceCategory>();

            try {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("Owner/GetServiceCategoryList");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<ServiceCategory>>();
                        readTask.Wait();

                        ServiceList = readTask.Result;
                    }

                }
                ViewBag.ServiceCategoryList = ServiceList;
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get GetServiceCategoryList() funcion in OwnerController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get GetServiceCategoryList() funcion in OwnerController.cs -->Web..");

                throw ex;
            }

        }
        #endregion

        #region GetServiceList
        public List<ServiceList> GetServiceList(string[] ids)
        {
            fileLogger.Info("Inside GetServiceList() function in OwnerController.cs -->Web.");
            List<ServiceList> ServiceList = new List<ServiceList>();

            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.PostAsJsonAsync<string[]>("Owner/GetServiceList", ids);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<ServiceList>>();
                        readTask.Wait();

                        ServiceList = readTask.Result;
                    }

                }

                return ServiceList;
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get GetServiceList() funcion in OwnerController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get GetServiceList() funcion in OwnerController.cs -->Web..");

                throw ex;
            }


        }
        #endregion

        #region LoadServiceList
        [HttpPost]
        public JsonResult LoadServiceList(string[] ids)
        {
            fileLogger.Info("Inside GetServiceList() function in OwnerController.cs -->Web.");
            List<ServiceList> ServiceList = new List<ServiceList>();
            if(ids==null)
            {
                ids = new string[] { };
            }
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.PostAsJsonAsync<string[]>("Owner/GetServiceList", ids);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<ServiceList>>();
                        readTask.Wait();

                        ServiceList = readTask.Result;
                    }

                }

                return Json(ServiceList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get GetServiceList() funcion in OwnerController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get GetServiceList() funcion in OwnerController.cs -->Web..");

                throw ex;
            }


        }
        #endregion

        #region Search
        [HttpPost]
        public JsonResult Search(AgencyFilter obj)
        {
            fileLogger.Info("Inside Search() function in OwnerController.cs -->Web.");
            AgencySearchList ServiceList = new AgencySearchList();

            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    obj.OwnerId = Convert.ToInt32(Session["UserId"]);
                    var responseTask = hObj.PostAsJsonAsync<AgencyFilter>("Owner/Search", obj);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<AgencySearchList>();
                        readTask.Wait();

                        ServiceList = readTask.Result;
                    }

                }
                return Json(ServiceList, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get GetServiceList() funcion in OwnerController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get GetServiceList() funcion in OwnerController.cs -->Web..");

                throw ex;
            }

        }
        #endregion

        #region SearchService
        public JsonResult SearchService(AgencyFilter obj)
        {
            fileLogger.Info("Inside SearchService() function in OwnerController.cs -->Web.");
            List<AgencyServiceDetails> ServiceList = new List<AgencyServiceDetails>();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.PostAsJsonAsync<AgencyFilter>("Owner/SearchService", obj);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<AgencyServiceDetails>>();
                        readTask.Wait();

                        ServiceList = readTask.Result;
                    }

                }
                return Json(ServiceList, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get GetServiceList() funcion in OwnerController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get GetServiceList() funcion in OwnerController.cs -->Web..");

                throw ex;
            }


        }
        #endregion

        #region FilterAgency
        [HttpPost]
        public JsonResult FilterAgency(AgencyFilter obj)
        {
            fileLogger.Info("Inside FilterAgency() function in OwnerController.cs -->Web.");
            AgencySearchList ServiceList = new AgencySearchList();

            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    obj.OwnerId = Convert.ToInt32(Session["UserId"]);
                    var responseTask = hObj.PostAsJsonAsync<AgencyFilter>("Owner/FilterAgency", obj);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<AgencySearchList>();
                        readTask.Wait();

                        ServiceList = readTask.Result;
                    }

                }
                return Json(ServiceList, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get GetServiceList() funcion in OwnerController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get GetServiceList() funcion in OwnerController.cs -->Web..");

                throw ex;
            }

        }
        #endregion

        #region GetAgentsForCompare
        [HttpPost]
        public JsonResult GetAgentsForCompare(AgencyFilter obj)
        {
            fileLogger.Info("Inside GetAgentsForCompare() function in OwnerController.cs -->Web.");
            AgencySearchList ServiceList = new AgencySearchList();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.PostAsJsonAsync<AgencyFilter>("Owner/GetAgentsForCompare",obj);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<AgencySearchList>();
                        readTask.Wait();

                        ServiceList = readTask.Result;
                    }

                }
                return Json(ServiceList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get GetAgentsForCompare() funcion in OwnerController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get GetAgentsForCompare() funcion in OwnerController.cs -->Web..");

                throw ex;
            }



        }
        #endregion
    }


}