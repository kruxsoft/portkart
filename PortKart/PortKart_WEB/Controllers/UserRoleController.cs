﻿#region Header
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain;
using System.Net.Http;
using Repository;
using PortKart_WEB.Security;
using PortKart_WEB.Util;
using System.Configuration;
using Newtonsoft.Json;
using NLog;
#endregion

namespace PortKart_WEB.Controllers
{
        
    [AuthorizationFilter]
    [RollCheck]
    public class UserRoleController : Controller
    {
        #region Global Variable
        public CommonMethods pObj = new CommonMethods();
        string roleName = string.Empty;
        private static readonly Logger databaseLogger = LogManager.GetLogger("databaseLogger");
        private static readonly Logger fileLogger = LogManager.GetLogger("fileLogger");
        #endregion

        #region Index
        public ActionResult Index()
        {
            fileLogger.Info("Inside Index() function in UserRoleController.cs -->Web.");
            return View();
        }
        #endregion

        #region GetUserRolesList
        public ActionResult GetUserRolesList()
        {
            fileLogger.Info("GetUserRolesList Index() function in UserRoleController.cs -->Web.");
            List<UserRole> userRole = new List<UserRole>();
            try
            {
                SetRoleName();
                if (roleName == "SA" || roleName == "AD")
                    userRole = GetUserRoles();
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on GetUserRolesList() funcion in UserRoleController.cs -->Web..");
                databaseLogger.Error(ex, "Error on on GetUserRolesList() funcion in UserRoleController.cs -->Web..");
                throw ex;
            }
            return Json(userRole, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetUserRole
        public List<UserRole> GetUserRoles()
        {
            fileLogger.Info("Inside GetUserRoles() function in UserRoleController.cs -->Web.");
            List<UserRole> userRoles = new List<UserRole>();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("UserRole/GetUserRoles/");
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<UserRole>>();
                        readTask.Wait();
                        userRoles = readTask.Result;
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Erroron GetUserRoles() funcion in UserRoleController.cs -->Web..");
                databaseLogger.Error(ex, "Error on GetUserRoles() funcion in UserRoleController.cs -->Web..");
                throw ex;
            }
            return userRoles;
        }

        #endregion

        #region AddUserRole
        [HttpGet]
        public ActionResult AddUserRole()
        {
            fileLogger.Info("Inside Get AddUserRole() function in UserRoleController.cs -->Web.");
            BindRole();
            SetRoleName();
            if (roleName == "SA" || roleName == "AD")
                BindUser();            
            return View();
        }


        [HttpPost]
        public ActionResult AddUserRole(UserRole cs)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    DBResponse dbResponse = new DBResponse();
                    using (HttpClient hObj = pObj.Client())
                    {
                        cs.CreatedBy = Convert.ToInt32(Session["UserId"]);
                        var responseTask = hObj.PostAsJsonAsync<UserRole>("UserRole/AddUserRole", cs);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                    }
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {
                            BindRole();
                            BindUser();
                            return View();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return RedirectToAction("Index", "UserRole");
            }
            else
            {
                BindRole();
                BindUser();
                return View();
            }
           }
        #endregion

        #region BindRole
        public void BindRole()
        {
            fileLogger.Info("Inside  BindRole() function in UserRoleController.cs -->Web.");
            List<Role> role = new List<Role>();
            role = pObj.BindRole();
            ViewBag.Role = new SelectList(role, "RoleId", "RoleName");
        }
        #endregion

        #region BindUser
        public void BindUser()
        {
            fileLogger.Info("Inside  BindUser() function in UserRoleController.cs -->Web.");
            List<User> users = new List<User>();
            using (HttpClient hObj = pObj.Client())
            {
                var accesstoken = "";
                Login Lobj = new Login();
                Lobj = (Login)Session["LoginObj"];
                accesstoken = pObj.GenerateAccessToken(Lobj.emailId, Lobj.Password);
                Session["AccessToken"] = accesstoken;
                hObj.DefaultRequestHeaders.Add("Authorization", $"Bearer {accesstoken}");
                var responseTask = hObj.GetAsync("User/GetUsersList");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<User>>();
                    readTask.Wait();
                    users = readTask.Result;
                }
            }
            ViewBag.User = new  SelectList((from s in users
                                               select new
                                               {
                                                   UserId = s.UserId,
                                                   UserName = s.FirstName + " " + s.LastName+" ("+s.EmailId+")"
                                               }),    "UserId",    "UserName",    null);
        }
        #endregion

        #region DeleteUserRole
        public ActionResult DeleteUserRole(int UserId, int RoleId)
        {
            fileLogger.Info("Inside  DeleteUserRole() function in UserRoleController.cs -->Web.");
            try
            {
                DBResponse dbResponse = new DBResponse();
                UserRole userRole = new UserRole();
                userRole.UserId = UserId;
                userRole.RoleId = RoleId;
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.PostAsJsonAsync<UserRole>("UserRole/DeleteUserRole/", userRole);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<DBResponse>();
                        readTask.Wait();
                        dbResponse = readTask.Result;
                    }
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {
                            return View();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on Post DeleteUserRole() funcion in UserRoleController.cs -->Web..");
                databaseLogger.Error(ex, "Error on Post DeleteUserRole() funcion in UserRoleController.cs -->Web..");
                throw ex;
            }
            return RedirectToAction("Index", "UserRole");
        }
        #endregion

        #region SetRoleName

        public void SetRoleName()
        {
            fileLogger.Info("Inside  SetRoleName() function in UserRoleController.cs -->Web.");
            roleName = pObj.GetRoleNameByRoleId(Convert.ToInt32(Session["RoleId"]));
            ViewBag.roleName = roleName;
        }

        #endregion

        #region GetUserRolesListServerSide
        public ActionResult GetUserRolesListServerSide()
         {
            fileLogger.Info("Inside  GetUserRolesListServerSide() function in UserRoleController.cs -->Web.");
            DataTableServerSideResult<UserRole> userRole = new DataTableServerSideResult<UserRole>();
            try
            {
                SetRoleName();
                if (roleName == "SA" || roleName == "AD")
                    userRole = GetUserRoleServerSide();
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on GetUserRolesListServerSide() funcion in UserRoleController.cs -->Web..");
                databaseLogger.Error(ex, "Error on  GetUserRolesListServerSide() funcion in UserRoleController.cs -->Web..");
                throw ex;
            }
            return Json(userRole, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetUserRoleServerSide
        [HttpPost]
        public DataTableServerSideResult<UserRole> GetUserRoleServerSide()
        {
            DataTableServerSideResult<UserRole> DTresult = new DataTableServerSideResult<UserRole>();
            DataTableServerSideSettings DTtablesettings = new DataTableServerSideSettings();

            //Datatable parameter
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            //paging parameter
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //sorting parameter
            DTtablesettings.sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            DTtablesettings.sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            //full search value
            DTtablesettings.searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

            //individual column search
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[0][name]").FirstOrDefault(), Request.Form.GetValues("columns[0][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[1][name]").FirstOrDefault(), Request.Form.GetValues("columns[1][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[2][name]").FirstOrDefault(), Request.Form.GetValues("columns[2][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[3][name]").FirstOrDefault(), Request.Form.GetValues("columns[3][search][value]").FirstOrDefault());


            DTtablesettings.pageSize = length != null ? Convert.ToInt32(length) : 0;
            DTtablesettings.skip = start != null ? Convert.ToInt32(start) : 0;
            //Database query
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.PostAsJsonAsync<DataTableServerSideSettings>("UserRole/GetUserRolesListServerSide", DTtablesettings);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<DataTableServerSideResult<UserRole>>();
                        readTask.Wait();
                        DTresult = readTask.Result;
                        fileLogger.Debug("Inside  GetUserRolesListServerSide() function in UserRoleController.cs -->Web. UserRole object value =>" + DTresult);

                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on GetUserRolesListServerSide() funcion in UserRoleController.cs -->Web..");
                databaseLogger.Error(ex, "Error on  GetUserRolesListServerSide() funcion in UserRoleController.cs -->Web..");
                throw ex;
            }
            DTresult.draw = draw;
            return DTresult;
        }
        #endregion
    }
}