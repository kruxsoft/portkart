﻿#region Headers
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Dapper;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using Repository;
using Domain;
using System.Net.Http;
using PortKart_WEB.Util;
using PortKart_WEB.Security;
using NLog;
#endregion
namespace PortKart_WEB.Controllers
{
    [AuthorizationFilter]
    public class DashBoardController : Controller
    {
        #region Global Variables
        CommonMethods pObj = new CommonMethods();
        private static readonly Logger databaseLogger = LogManager.GetLogger("databaseLogger");
        private static readonly Logger fileLogger = LogManager.GetLogger("fileLogger");
        string roleName = string.Empty;
        #endregion

        #region Index
        public ActionResult Index()
        {
            fileLogger.Info("Inside Index() function in DashBoardController.cs -->Web.");
            return View();
        }
        #endregion   

        #region SetRoleName
        public string SetRoleName()
        {
            fileLogger.Info("Inside SetRoleName() function in DashBoardController.cs -->Web.");
            int roleId = Convert.ToInt32(Session["RoleId"]);
            roleName = pObj.GetRoleNameByRoleId(roleId);
            ViewBag.RoleName = roleName;
            return roleName;
        }
        #endregion
    }
        
}