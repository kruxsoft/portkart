﻿#region Header
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PortKart_WEB.Security;
using PortKart_WEB.Util;
using Domain;
using System.Net.Http;
using NLog;
#endregion

namespace PortKart_WEB.Controllers
{
    [AuthorizationFilter]
    public class ChangePasswordController : Controller
    {
        #region GLOBAL VARIABLE
        public CommonMethods pObj = new CommonMethods();
        string roleName = string.Empty;
        private static readonly Logger databaseLogger = LogManager.GetLogger("databaseLogger");
        private static readonly Logger fileLogger = LogManager.GetLogger("fileLogger");

        #endregion

        #region BindUser
        public void BindUser()
        {
            fileLogger.Info("Inside BindUser() function in ChangePasswordController.cs -->Web.");
            List<User> user = new List<User>();

            SetRoleName();
            if (roleName == "SA" || roleName == "AD")
                user = pObj.BindUser();
            ViewBag.User = new SelectList(user, "UserId", "UserName");
        }
        #endregion

        #region UpdatePassword
        [HttpGet]
        public ActionResult UpdatePassword()
        {
            fileLogger.Info("Inside UpdatePassword() function in ChangePasswordController.cs -->Web.");
            BindUser();
            return View();
        }
        [HttpPost]
        public ActionResult UpdatePassword(ChangePassword cs)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    DBResponse dbResponse = new DBResponse();
                    using (HttpClient hObj = pObj.Client())
                    {
                        cs.ModifiedBy = Convert.ToInt32(Session["UserId"]);
                        cs.password = pObj.EncMD5(cs.password);
                        var responseTask = hObj.PostAsJsonAsync<ChangePassword>("ChangePassword/UpdatePassword", cs);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                    }
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {
                            BindUser();
                            return View();
                        }
                        if (dbResponse.MessageType == "SUCCESS")
                        {
                            ModelState.Clear();
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Erron UpdatePassword() funcion in ChangePasswordController.cs -->Web..");
                    databaseLogger.Error(ex, "Erron UpdatePassword() funcion in ChangePasswordController.cs -->Web..");
                    throw ex;
                }
                BindUser();
                return View();
            }
            else
            {
                BindUser();
                return View();
            }
        }
        #endregion

        #region SetRoleName
        public string SetRoleName()
        {
            fileLogger.Info("Inside SetRoleName() function in ChangePasswordController.cs -->Web.");
            int roleId = Convert.ToInt32(Session["RoleId"]);
            roleName = pObj.GetRoleNameByRoleId(roleId);
            ViewBag.RoleName = roleName;
            return roleName;
        }
        #endregion
    }
}