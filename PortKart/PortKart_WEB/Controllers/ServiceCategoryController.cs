﻿using PortKart_WEB.Security;
using PortKart_WEB.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PortKart_WEB.Models;
using System.Net.Http;
using Domain;
using NLog;

namespace PortKart_WEB.Controllers
{
    [AuthorizationFilter]
    [RollCheck]
    public class ServiceCategoryController : Controller
    {
        #region GLOBAL VARIABLE
        public CommonMethods pObj = new CommonMethods();
        private static readonly Logger databaseLogger = LogManager.GetLogger("databaseLogger");
        private static readonly Logger fileLogger = LogManager.GetLogger("fileLogger");
        #endregion

        #region Index
        public ActionResult Index()
        {
            fileLogger.Info("Inside Index() function in ServiceCategoryController.cs -->Web.");
            return View();
        }
        #endregion

        #region GetServiceCategoryList
        public ActionResult GetServiceCategoryList()
        {
            fileLogger.Info("Inside GetServiceCategoryList() function in ServiceCategoryController.cs -->Web.");

            List<ServiceCategoryModel> serviceCategory = new List<ServiceCategoryModel>();
            try
            {
                serviceCategory = GetAllServiceCategories();
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on GetServiceCategoryList() funcion in ServiceCategoryController.cs -->Web..");
                databaseLogger.Error(ex, "Error on on GetServiceCategoryList() funcion in ServiceCategoryController.cs -->Web..");
                throw ex;
            }
            return Json(serviceCategory, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetAllServiceCategories
        List<ServiceCategoryModel> GetAllServiceCategories()
        {
            fileLogger.Info("Inside GetAllServiceCategories() function in ServiceCategoryController.cs -->Web.");
            List<ServiceCategoryModel> sObj = new List<ServiceCategoryModel>();
            using (HttpClient hObj = pObj.Client())
            {
                var responseTask = hObj.GetAsync("ServiceCategory/GetAllServiceCategories");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<ServiceCategoryModel>>();
                    readTask.Wait();
                    sObj = readTask.Result;
                }
            }
            return sObj;
        }
        #endregion

        #region AddServieCategory
        [HttpGet]
        public ActionResult AddServiceCategory()
        {
            fileLogger.Info("Inside AddServiceCategory() function in ServiceCategoryController.cs -->Web.");

            return View();
        }
        #endregion

        #region AddServieCategory
        [HttpPost]
        public ActionResult AddServiceCategory(ServiceCategoryModel ServiceCategory)
        {
            fileLogger.Info("Inside AddServiceCategory() function in ServiceCategoryController.cs -->Web.");

            if (ModelState.IsValid)
            {
                try
                {
                    DBResponse dbResponse = new DBResponse();
                    ServiceCategory.CreatedBy = Convert.ToInt32(Session["UserId"]);
                    ServiceCategory.ModifiedBy = Convert.ToInt32(Session["UserId"]);

                    using (HttpClient hObj = pObj.Client())
                    {
                        var responseTask = hObj.PostAsJsonAsync<ServiceCategoryModel>("ServiceCategory/AddServiceCategory", ServiceCategory);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                    }
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {
                            return View();
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on AddServiceCategory() funcion in ServiceCategoryController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on on AddServiceCategory() funcion in ServiceCategoryController.cs -->Web..");
                    throw ex;
                }
                return RedirectToAction("Index", "ServiceCategory");
            }
            else
            {
                return View();
            }
        }
        #endregion

        #region UpdateServiceCategory
        [HttpGet]
        public ActionResult UpdateServiceCategory(int id)
        {
            fileLogger.Info("Inside get UpdateServiceCategory() function in ServiceCategoryController.cs -->Web.");

            ServiceCategoryModel cobj = new ServiceCategoryModel();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("ServiceCategory/GetServiceCategoryById/" + id);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<ServiceCategoryModel>();
                        readTask.Wait();
                        cobj = readTask.Result;
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get UpdateServiceCategory() funcion in ServiceCategoryController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get UpdateServiceCategory() funcion in ServiceCategoryController.cs -->Web..");
                throw ex;
            }
            return View(cobj);
        }
        #endregion

        #region UpdateServiceCategory
        [HttpPost]
        public ActionResult UpdateServiceCategory(ServiceCategoryModel ServicecategoryModel)
        {
            fileLogger.Info("Inside post UpdateServiceCategory() function in ServiceCategoryController.cs -->Web.");

            if (ModelState.IsValid)
            {
                try
                {
                    DBResponse dbResponse = new DBResponse();
                    ServicecategoryModel.ModifiedBy = Convert.ToInt32(Session["UserId"]);
                    using (HttpClient hObj = pObj.Client())
                    {
                        var responseTask = hObj.PostAsJsonAsync<ServiceCategoryModel>("ServiceCategory/UpdateServiceCategory", ServicecategoryModel);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                        if (dbResponse != null)
                        {
                            TempData["Message"] = dbResponse.Message;
                            TempData["MessageType"] = dbResponse.MessageType;
                            if (dbResponse.MessageType == "ERROR")
                            {
                                return View();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on post UpdateServiceCategory() funcion in ServiceCategoryController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on post UpdateServiceCategory() funcion in ServiceCategoryController.cs -->Web..");
                    throw ex;
                }
                return RedirectToAction("Index", "ServiceCategory");
            }
            else
            {
                return View();
            }
        }
        #endregion

        #region DeleteServiceCategory
        public ActionResult DeleteServiceCategory(int id)
        {
            fileLogger.Info("Inside DeleteServiceCategory() function in ServiceCategoryController.cs -->Web.");

            DBResponse dbResponse = new DBResponse();
            try
            {
                ServiceCategoryModel serviceCategoryModel = new ServiceCategoryModel();
                serviceCategoryModel.ServiceCategoryId = id;
                serviceCategoryModel.ModifiedBy = id;
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.PostAsJsonAsync<ServiceCategoryModel>("ServiceCategory/DeleteServiceCategory", serviceCategoryModel);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<DBResponse>();
                        readTask.Wait();
                        dbResponse = readTask.Result;
                    }
                }
                if (dbResponse != null)
                {
                    TempData["Message"] = dbResponse.Message;
                    TempData["MessageType"] = dbResponse.MessageType;
                    if (dbResponse.MessageType == "ERROR")
                    {
                        return RedirectToAction("Index", "ServiceCategory");
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on DeleteServiceCategory() funcion in ServiceCategoryController.cs -->Web..");
                databaseLogger.Error(ex, "Error on DeleteServiceCategory() funcion in ServiceCategoryController.cs -->Web..");
                throw ex;
            }
            return RedirectToAction("Index", "ServiceCategory");
        }
        #endregion

        #region GetServiceCategoryListServerSide
        public ActionResult GetServiceCategoryListServerSide()
        {
            fileLogger.Info("Inside GetServiceCategoryListServerSide() function in ServiceCategoryController.cs -->Web.");

            DataTableServerSideResult<ServiceCategoryModel> serviceCategory = new DataTableServerSideResult<ServiceCategoryModel>();
            try
            {
                serviceCategory = GetAllServiceCategoriesServerSide();
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on GetServiceCategoryListServerSide() funcion in ServiceCategoryController.cs -->Web..");
                databaseLogger.Error(ex, "Error on GetServiceCategoryListServerSide() funcion in ServiceCategoryController.cs -->Web..");
                throw ex;
            }
            return Json(serviceCategory, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetAllServiceCategoriesServerSide
        DataTableServerSideResult<ServiceCategoryModel> GetAllServiceCategoriesServerSide()
        {
            fileLogger.Info("Inside GetAllServiceCategoriesServerSide() function in ServiceCategoryController.cs -->Web.");
            DataTableServerSideResult<ServiceCategoryModel> DTresult = new DataTableServerSideResult<ServiceCategoryModel>();
            DataTableServerSideSettings DTtablesettings = new DataTableServerSideSettings();

            //Datatable parameter
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            //paging parameter
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //sorting parameter
            DTtablesettings.sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            DTtablesettings.sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            //full search value
            DTtablesettings.searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

            //individual column search
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[0][name]").FirstOrDefault(), Request.Form.GetValues("columns[0][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[1][name]").FirstOrDefault(), Request.Form.GetValues("columns[1][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[2][name]").FirstOrDefault(), Request.Form.GetValues("columns[2][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[3][name]").FirstOrDefault(), Request.Form.GetValues("columns[3][search][value]").FirstOrDefault());


            DTtablesettings.pageSize = length != null ? Convert.ToInt32(length) : 0;
            DTtablesettings.skip = start != null ? Convert.ToInt32(start) : 0;
            try
            {
                using (HttpClient hObj = pObj.Client())
                {

                    var responseTask = hObj.PostAsJsonAsync<DataTableServerSideSettings>("ServiceCategory/GetAllServiceCategoriesServerSide", DTtablesettings);
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<DataTableServerSideResult<ServiceCategoryModel>>();
                        readTask.Wait();
                        DTresult = readTask.Result;
                    }

                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on GetAllServiceCategoriesServerSide() funcion in ServiceCategoryController.cs -->Web..");
                databaseLogger.Error(ex, "Error on  GetAllServiceCategoriesServerSide() funcion in ServiceCategoryController.cs -->Web..");
                throw ex;
            }
            return DTresult;
        }
        #endregion
    }
}