﻿#region Headers
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain;
using Dapper;
using Repository;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using PortKart_WEB.Security;
using PortKart_WEB.Util;
using Newtonsoft.Json;
using PortKart_WEB.Models;
using NLog;
#endregion
namespace PortKart_WEB.Controllers
{
    [AuthorizationFilter]
    [RollCheck]
    public class UserController : Controller
    {
        //User

        #region Global Variables
        public CommonMethods pObj = new CommonMethods();
        string roleName = string.Empty;
        private static readonly Logger databaseLogger = LogManager.GetLogger("databaseLogger");
        private static readonly Logger fileLogger = LogManager.GetLogger("fileLogger");
        #endregion Global Variables

        #region SetRoleName
        public string SetRoleName()
        {
            fileLogger.Info("Inside SetRoleName() function in UserController.cs -->Web.");
            int roleId = Convert.ToInt32(Session["RoleId"]);
            roleName = pObj.GetRoleNameByRoleId(roleId);
            ViewBag.RoleName = roleName;
            return roleName;
        }
        #endregion

        #region Index
     
        public ActionResult Index()
        {
            fileLogger.Info("Inside Index() function in UserController.cs -->Web.");
            return View();
        }
        #endregion

        #region GetUserList
        public ActionResult GetUserList()
        {
            fileLogger.Info("Inside GetUserList() function in UserController.cs -->Web.");
            List<UserModel> cobj = new List<UserModel>();
            try
            {
                cobj = GetAllUsers();
               
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on GetUserList() funcion in UserController.cs -->Web..");
                databaseLogger.Error(ex, "Error on on GetUserList() funcion in UserController.cs -->Web..");
                throw ex;
            }
            return Json(cobj, JsonRequestBehavior.AllowGet);
            //return View(cobj);
        }
        #endregion

        #region GetAllUsers
        public List<UserModel> GetAllUsers()
        {
            fileLogger.Info("Inside GetAllUsers() function in UserController.cs -->Web.");
            List<UserModel> users = new List<UserModel>();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    SetRoleName();
                    if (roleName == "SA" || roleName == "AD")
                    {
                        var accesstoken = "";
                      //  if(Session["AccessToken"]!=null)
                      //  {
                      //      accesstoken = Session["AccessToken"].ToString();
                      //  }
                      //  else
                       // {
                            Login Lobj = new Login();
                            Lobj = (Login)Session["LoginObj"];
                            accesstoken = pObj.GenerateAccessToken(Lobj.emailId,Lobj.Password);
                            Session["AccessToken"] = accesstoken;
                       // }
                        hObj.DefaultRequestHeaders.Add("Authorization", $"Bearer {accesstoken}");
                        var responseTask = hObj.GetAsync("User/GetAllUsers");
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<List<UserModel>>();
                            readTask.Wait();
                            users = readTask.Result;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on GetAllUsers() funcion in UserController.cs -->Web..");
                databaseLogger.Error(ex, "Error on  GetAllUsers() funcion in UserController.cs -->Web..");
                throw ex;
            }
            return users;
        }
        #endregion
        
        #region Delete
        public ActionResult Delete(int id)
        {
            fileLogger.Info("Inside Delete() function in UserController.cs -->Web.");
            try
            {
                UserModel user = new UserModel();
                user.UserId = id;
                user.ModifiedBy = Convert.ToInt32(Session["UserId"]);
                DBResponse dbResponse = new DBResponse();
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.PostAsJsonAsync<UserModel>("User/Delete", user);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<DBResponse>();
                        readTask.Wait();
                        dbResponse = readTask.Result;
                    }
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {
                            return RedirectToAction("Index", "User");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on Delete() funcion in UserController.cs -->Web..");
                databaseLogger.Error(ex, "Error on  Delete() funcion in UserController.cs -->Web..");
                throw ex;
            }
            return RedirectToAction("Index", "User");
        }
        #endregion

        #region AddUser
 
        [HttpGet]

        public ActionResult AddUser()
        {
            fileLogger.Info("Inside Get AddUser() function in UserController.cs -->Web.");
            roleName = SetRoleName();
            return View();
        }
        #endregion

        #region AddUser
        [HttpPost]
        public ActionResult AddUser(UserModel cs)
        {
            fileLogger.Info("Inside post AddUser() function in UserController.cs -->Web.");
            var errors = ModelState.Where(x => x.Value.Errors.Any())
                .Select(x => new { x.Key, x.Value.Errors });
            if (ModelState.IsValid)
            {
                try
                {
                    cs.CreatedBy = Convert.ToInt32(Session["UserId"]);
                    cs.Password = pObj.EncMD5(cs.Password);
                    cs.UserStatusId = 4;
                    DBResponse dbResponse = new DBResponse();
                    using (HttpClient hObj = pObj.Client())
                    {
                        var accesstoken = "";
                        Login Lobj = new Login();
                        Lobj = (Login)Session["LoginObj"];
                        accesstoken = pObj.GenerateAccessToken(Lobj.emailId, Lobj.Password);
                        Session["AccessToken"] = accesstoken;
                        hObj.DefaultRequestHeaders.Add("Authorization", $"Bearer {accesstoken}");
                        var responseTask = hObj.PostAsJsonAsync<UserModel>("User/Register", cs);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                    }
                    
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {
                            roleName = SetRoleName();
                            return View();
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on AddUser() funcion in UserController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on  AddUser() funcion in UserController.cs -->Web..");
                    throw ex;
                }
                return RedirectToAction("Index", "User");
            }
            else
            {
                roleName = SetRoleName();
                return View();
            }
        }
        #endregion

        #region UpdateUser
        [HttpGet]
        public ActionResult UpdateUser(int id)
        {
            fileLogger.Info("Inside get UpdateUser() function in UserController.cs -->Web.");
            roleName = SetRoleName();
            GetUserStatus();
            UserModel user = new UserModel();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var accesstoken = "";
                    Login Lobj = new Login();
                    Lobj = (Login)Session["LoginObj"];
                    accesstoken = pObj.GenerateAccessToken(Lobj.emailId, Lobj.Password);
                    Session["AccessToken"] = accesstoken;
                    hObj.DefaultRequestHeaders.Add("Authorization", $"Bearer {accesstoken}");
                    var responseTask = hObj.GetAsync("User/GetUsersById/" + id);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<UserModel>();
                        readTask.Wait();
                        user = readTask.Result;
                        user.ConfirmPassword = user.Password;
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get UpdateUser() funcion in UserController.cs -->Web..");
                databaseLogger.Error(ex, "Error on  get UpdateUser() funcion in UserController.cs -->Web..");
                throw ex;
            }
            return View(user);
        }
        #endregion

        #region UpdateUser
        [HttpPost]
        public ActionResult UpdateUser(UserModel cs)
        {
            fileLogger.Info("Inside post UpdateUser() function in UserController.cs -->Web.");
            var errorcount = 0;
            var errorkey = "";
            var ismodelvalid = false;
            foreach (var modelStateKey in ModelState.Keys)
            {
                var modelStateVal = ModelState[modelStateKey];
                foreach (var error in modelStateVal.Errors)
                {
                    errorkey = modelStateKey;
                    if(errorkey!="Password")
                        errorcount = errorcount + 1;
                    // You may log the errors if you want
                }
            }
            if(errorcount < 1)
            {
                ismodelvalid = true;
            }
            else
            {
                ismodelvalid = ModelState.IsValid;
            }
            if (ismodelvalid)
            {
                try
                {
                    GetUserStatus();
                    DBResponse dbResponse = new DBResponse();
                    using (HttpClient hObj = pObj.Client())
                    {
                        var accesstoken = "";
                        Login Lobj = new Login();
                        Lobj = (Login)Session["LoginObj"];
                        accesstoken = pObj.GenerateAccessToken(Lobj.emailId, Lobj.Password);
                        Session["AccessToken"] = accesstoken;
                        hObj.DefaultRequestHeaders.Add("Authorization", $"Bearer {accesstoken}");
                        cs.ModifiedBy = Convert.ToInt32(Session["UserId"]);
                        var responseTask = hObj.PostAsJsonAsync<UserModel>("User/UpdateUser", cs);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                        else
                        {
                            dbResponse.MessageType = "ERROR";
                            dbResponse.Message = "User details failed to update";
                        }
                    }
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {
                            roleName = SetRoleName();
                            return View();
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on post UpdateUser() funcion in UserController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on  post UpdateUser() funcion in UserController.cs -->Web..");
                    throw ex;
                }
                return RedirectToAction("Index", "User");
            }
            else
            {
                GetUserStatus();
                roleName = SetRoleName();
                return View();
            }
        }
        #endregion

        #region GetUserStatus
        public void GetUserStatus()
        {
            fileLogger.Info("Inside GetUserStatus() function in UserController.cs -->Web.");
            List<UserStatuses> statuses = new List<UserStatuses>();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("UserStatus/GetUserStatus");
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<UserStatuses>>();
                        readTask.Wait();
                        statuses = readTask.Result;
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on GetUserStatus() funcion in UserController.cs -->Web..");
                databaseLogger.Error(ex, "Error on  AddGetUserStatusUser() funcion in UserController.cs -->Web..");
                throw ex;
            }
            ViewBag.UserStatus = new SelectList(statuses, "UserStatusId", "UserStatus");
        }
        #endregion

        #region VerifyEmail
        [AllowAnonymous]
        [HttpGet]
        public ActionResult VerifyEmail(string Ec)
        {
            fileLogger.Info("Inside VerifyEmail() function in UserController.cs -->Web.");
            DBResponse response = new DBResponse();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    
                    var responseTask = hObj.GetAsync("UserEmail/VerifyEmail/" + Ec);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<DBResponse>();
                        readTask.Wait();
                        response = readTask.Result;
                      
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on VerifyEmail() funcion in UserController.cs -->Web..");
                databaseLogger.Error(ex, "Error on  VerifyEmail() funcion in UserController.cs -->Web..");
                throw ex;
            }
            ViewBag.Emailverified = false;
            if(response.MessageType=="SUCCESS")
            {
                ViewBag.Emailverified = true;
            }
            return View();
        }
        #endregion

        #region Reset Password
        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPassword(int id)
        {
            fileLogger.Info("Inside post ResetPassword() function in UserController.cs -->Web.");
            DBResponse response = new DBResponse();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {

                    var responseTask = hObj.GetAsync("UserEmail/ResetPassword/" + id);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<DBResponse>();
                        readTask.Wait();
                        response = readTask.Result;

                    }
                    else
                    {
                        response.MessageType = "ERROR";
                        response.Message = "Unable to reset password";
                    }
                    if (response != null)
                    {
                        TempData["Message"] = response.Message;
                        TempData["MessageType"] = response.MessageType;
                   
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on ResetPassword() funcion in UserController.cs -->Web..");
                databaseLogger.Error(ex, "Error on  ResetPassword() funcion in UserController.cs -->Web..");
                throw ex;
            }
           
            return RedirectToAction("Index", "User");
        }
        #endregion

        #region New Password
        [HttpGet]
        [AllowAnonymous]
        public ActionResult NewPassword(string Ec)
        {
            fileLogger.Info("Inside  NewPassword() function in UserController.cs -->Web.");
            PasswordReset user = new PasswordReset();
            ViewBag.isExpired = true;
            try
            {
                using (HttpClient hObj = pObj.Client())
                {

                    var responseTask = hObj.GetAsync("UserEmail/GetUserbyEmailCode/" + Ec);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<PasswordReset>();
                        readTask.Wait();
                        user = readTask.Result;

                    }
                 
                  
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on NewPassword() funcion in UserController.cs -->Web..");
                databaseLogger.Error(ex, "Error on  NewPassword() funcion in UserController.cs -->Web..");
                throw ex;
            }

            if (user.UserId > 0)
            {
                ViewBag.isExpired = false;
            }
            return View(user);
        }
        #endregion

        #region New Password
        [HttpPost]
        [AllowAnonymous]
        public ActionResult NewPassword(PasswordReset user)
        {
            fileLogger.Info("Inside  NewPassword() function in UserController.cs -->Web.");
            if (ModelState.IsValid)
            {
                ViewBag.isExpired = false;
                DBResponse response = new DBResponse();
                try
                {
                    using (HttpClient hObj = pObj.Client())
                    {
                      user.EncPassword =pObj.EncMD5(user.Password);
                        var responseTask = hObj.PostAsJsonAsync<PasswordReset>("UserEmail/NewPassword", user);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            response = readTask.Result;

                        }
                        else
                        {
                            response.MessageType = "ERROR";
                            response.Message = "Failed to reset password";
                        }
                    }
                    if (response != null)
                    {
                        TempData["Message"] = response.Message;
                        TempData["MessageType"] = response.MessageType;
                        if (response.MessageType == "ERROR")
                        {
                            return View(user);
                        }
                    }


                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on NewPassword() funcion in UserController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on  NewPassword() funcion in UserController.cs -->Web..");
                    throw ex;
                }


                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.isExpired = false;
                return View();
            }
        }
        #endregion

        #region GetUserListServerSide
        public ActionResult GetUserListServerSide()
        {
            fileLogger.Info("Inside  GetUserListServerSide() function in UserController.cs -->Web.");
            DataTableServerSideResult<UserModel> cobj = new DataTableServerSideResult<UserModel>();
            try
            {
                cobj = GetAllUsersServerSide();

            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on GetUserListServerSide() funcion in UserController.cs -->Web..");
                databaseLogger.Error(ex, "Error on  GetUserListServerSide() funcion in UserController.cs -->Web..");
                throw ex;
            }
            return Json(cobj, JsonRequestBehavior.AllowGet);
            //return View(cobj);
        }
        #endregion


        #region GetAllUsersServerSide
        public DataTableServerSideResult<UserModel> GetAllUsersServerSide()
        {
            fileLogger.Info("Inside GetAllUsersServerSide() function in UserController.cs -->Web.");
            DataTableServerSideResult<UserModel> DTresult = new DataTableServerSideResult<UserModel>();
            DataTableServerSideSettings DTtablesettings = new DataTableServerSideSettings();

            //Datatable parameter
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            //paging parameter
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //sorting parameter
            DTtablesettings.sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            DTtablesettings.sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            //full search value
            DTtablesettings.searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

            //individual column search
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[0][name]").FirstOrDefault(), Request.Form.GetValues("columns[0][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[1][name]").FirstOrDefault(), Request.Form.GetValues("columns[1][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[2][name]").FirstOrDefault(), Request.Form.GetValues("columns[2][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[3][name]").FirstOrDefault(), Request.Form.GetValues("columns[3][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[4][name]").FirstOrDefault(), Request.Form.GetValues("columns[4][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[5][name]").FirstOrDefault(), Request.Form.GetValues("columns[5][search][value]").FirstOrDefault());


            DTtablesettings.pageSize = length != null ? Convert.ToInt32(length) : 0;
            DTtablesettings.skip = start != null ? Convert.ToInt32(start) : 0;
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    SetRoleName();
                    if (roleName == "SA" || roleName == "AD")
                    {
                        var accesstoken = "";
                     
                        Login Lobj = new Login();
                        Lobj = (Login)Session["LoginObj"];
                        accesstoken = pObj.GenerateAccessToken(Lobj.emailId, Lobj.Password);
                        Session["AccessToken"] = accesstoken;
                        // }
                        hObj.DefaultRequestHeaders.Add("Authorization", $"Bearer {accesstoken}");
                        var responseTask = hObj.PostAsJsonAsync<DataTableServerSideSettings>("User/GetAllUsersServerSide", DTtablesettings);
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DataTableServerSideResult<UserModel>>();
                            readTask.Wait();
                            DTresult = readTask.Result;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on GetAllUsersServerSide() funcion in UserController.cs -->Web..");
                databaseLogger.Error(ex, "Error on  GetAllUsersServerSide() funcion in UserController.cs -->Web..");
                throw ex;
            }
            return DTresult;
        }
        #endregion
    }
}


