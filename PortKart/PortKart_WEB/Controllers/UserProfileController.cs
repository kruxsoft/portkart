﻿using Domain;
using Newtonsoft.Json;
using NLog;
using PortKart_WEB.Models;
using PortKart_WEB.Security;
using PortKart_WEB.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace PortKart_WEB.Controllers
{
    [AuthorizationFilter]
    public class UserProfileController : Controller
    {
        // GET: UserProfile

        #region GLOBAL VARIABLE
        public CommonMethods pObj = new CommonMethods();
        private static readonly Logger databaseLogger = LogManager.GetLogger("databaseLogger");
        private static readonly Logger fileLogger = LogManager.GetLogger("fileLogger");
        #endregion

        [HttpGet]
        public ActionResult Index()
        {
            UserProfileModel uobj = new UserProfileModel();
            uobj = GetUserProfile();
            BindCountry();
            BindIndustryType();
            return View(uobj);
        }

        [HttpPost]
        public ActionResult Index(UserProfileModel uobj,HttpPostedFileBase LogoFile, HttpPostedFileBase AttachFile)
        {
       
            fileLogger.Info("Inside Index() function in VesselPortController.cs -->Web.");
            if (ModelState.IsValid)
            {
                try
                {
                    DBResponse dbResponse = new DBResponse();
                    if(LogoFile!=null)
                    {
                        var ContentLength = LogoFile.ContentLength;
                        string theFileName = Path.GetFileName(LogoFile.FileName);
                        byte[] thePictureAsBytes = new byte[LogoFile.ContentLength];
                        using (BinaryReader theReader = new BinaryReader(LogoFile.InputStream))
                        {
                            thePictureAsBytes = theReader.ReadBytes(LogoFile.ContentLength);
                        }
                        string thePictureDataAsString = Convert.ToBase64String(thePictureAsBytes);
                        uobj.LogoBase64 = thePictureDataAsString;
                        uobj.LogoContentType = LogoFile.ContentType;
                        var ext = LogoFile.FileName.Substring(LogoFile.FileName.LastIndexOf('.'));
                        uobj.LogoFormat = ext.ToLower();
                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".jpeg", ".gif", ".png" };
                        int MaxContentLength = 1024 * 1024 * 2;
                        if (!AllowedFileExtensions.Contains(uobj.LogoFormat))
                        {

                            TempData["Message"] = string.Format("Please Upload image of type .jpg,.jpeg,.gif,.png.");
                            TempData["MessageType"] = "ERROR";
                            BindCountry();
                            BindIndustryType();
                            return View(uobj);
                        }
                        else if (ContentLength > MaxContentLength)
                        {

                            TempData["Message"] = string.Format("Maximum Logo file size is 2MB");
                            TempData["MessageType"] = "ERROR";
                            BindCountry();
                            BindIndustryType();
                            return View(uobj);
                        }
                    }
                    if (AttachFile != null)
                    {
                        var ContentLength = AttachFile.ContentLength;
                        string theFileName = Path.GetFileName(AttachFile.FileName);
                        byte[] thePictureAsBytes = new byte[AttachFile.ContentLength];
                        using (BinaryReader theReader = new BinaryReader(AttachFile.InputStream))
                        {
                            thePictureAsBytes = theReader.ReadBytes(AttachFile.ContentLength);
                        }
                        string thePictureDataAsString = Convert.ToBase64String(thePictureAsBytes);
                        uobj.AttachBase64 = thePictureDataAsString;
                        uobj.AttachContentType = AttachFile.ContentType;
                        var ext = AttachFile.FileName.Substring(AttachFile.FileName.LastIndexOf('.'));
                        uobj.AttachFormat = ext.ToLower();
                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".jpeg", ".gif", ".png" };
                        int MaxContentLength = 1024 * 1024 * 2;
                       
                    }
                    string jsonString = JsonConvert.SerializeObject(uobj);
                    var url = "";
                    if(uobj.CompanyId>0)
                    {
                        uobj.ModifiedBy = Convert.ToInt32(Session["UserId"]);
                        url = "UserProfile/UpdateUserProfile";
                    }
                    else
                    {
                        uobj.CreatedBy = Convert.ToInt32(Session["UserId"]);
                        url = "UserProfile/AddUserProfile";
                    }
                   
                    

                    using (HttpClient hObj = pObj.Client())
                    {
                        
                        var responseTask = hObj.PostAsJsonAsync<UserProfileModel>(url, uobj);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                        else
                        {
                            dbResponse.Message = "Failed to save the details";
                            dbResponse.MessageType = "ERROR";
                        }
                    }
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {
                            BindCountry();
                            BindIndustryType();
                            return View(uobj);
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on Index() funcion in VesselPortController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on Index() funcion in VesselPortController.cs -->Web..");

                    throw ex;
                }
                return RedirectToAction("PreviewIndex", "UserProfile");
            }
            else
            {
                TempData["Message"] = "Failed to save the details";
                TempData["MessageType"] = "ERROR";
                BindCountry();
                BindIndustryType();
                return View(uobj);
            }
        }
        private UserProfileModel GetUserProfile()
        {
            fileLogger.Info("Inside get GetUserProfile() function in UserProfileController.cs -->Web.");

            UserProfileModel cobj = new UserProfileModel();
            try
            {

                using (HttpClient hObj = pObj.Client())
                {
                    var id = Convert.ToInt32(Session["UserId"]);
                    var responseTask = hObj.GetAsync("UserProfile/GetUserProfile/"+id);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<UserProfileModel>();
                        readTask.Wait();
                        cobj = readTask.Result;
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get GetUserProfile() funcion in UserProfileController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get GetUserProfile() funcion in UserProfileController.cs -->Web..");

                throw ex;
            }
            return cobj;
        }
        private UserProfileModel GetCompanyProfile(int id)
        {
            fileLogger.Info("Inside get GetCompanyProfile() function in UserProfileController.cs -->Web.");

            UserProfileModel cobj = new UserProfileModel();
            try
            {

                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("UserProfile/GetUserProfile/" + id);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<UserProfileModel>();
                        readTask.Wait();
                        cobj = readTask.Result;
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get GetUserProfile() funcion in UserProfileController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get GetUserProfile() funcion in UserProfileController.cs -->Web..");

                throw ex;
            }
            return cobj;
        }
        #region BindCountry
        public void BindCountry()
        {
            fileLogger.Info("Inside BindCountry() function in UserProfileController.cs -->Web.");
            List<Country> country = new List<Country>();
            country = pObj.BindCountry();
            ViewBag.Country = new SelectList(country, "CountryId", "CountryName");
            ViewBag.CountryList = country;
        }
        #endregion

        #region BindIndustryType
            public void BindIndustryType()
            {
            fileLogger.Info("Inside BindIndustryType() function in UserProfileController.cs -->Web.");
            List<IndustryType> Industry = new List<IndustryType>();
            using (HttpClient hObj = pObj.Client())
            {
                var responseTask = hObj.GetAsync("UserProfile/GetAllIndustryType");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<IndustryType>>();
                    readTask.Wait();

                    Industry = readTask.Result;
                }

            }
            ViewBag.IndustryType = new SelectList(Industry, "IndustryTypeId", "IndustryTypeName");
        }
        #endregion

        #region AddContactPerson
        [HttpPost]
        public ActionResult AddContactPerson(ContactPerson CP)
        {
            fileLogger.Info("Inside AddContactPerson() function in UserController.cs -->Web.");
            DBResponse dbResponse = new DBResponse();
            int contactPersonId = 0;
            if (ModelState.IsValid)
            {
                try
                {

                    using (HttpClient hObj = pObj.Client())
                    {
                        var responseTask = hObj.PostAsJsonAsync<ContactPerson>("UserProfile/AddContactPerson", CP);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<int>();
                            readTask.Wait();
                            contactPersonId =  readTask.Result;
                        }
                    }
                    //if (dbResponse != null)
                    //{
                    //    TempData["Message"] = dbResponse.Message;
                    //    TempData["MessageType"] = dbResponse.MessageType;
                    //    if (dbResponse.MessageType == "ERROR")
                    //    {
                           

                    //    }
                    //}
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on AddContactPerson() funcion in ServiceController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on AddContactPerson() funcion in ServiceController.cs -->Web..");

                    throw ex;
                }
                return Json(contactPersonId);
            }
            else
            {
                return Json(contactPersonId);
            }
        }
        #endregion
        #region DeleteContactPerson
        [HttpGet]
        public ActionResult DeleteContactPerson(int id)
        {
            fileLogger.Info("Inside AddContactPerson() function in UserController.cs -->Web.");
            DBResponse dbResponse = new DBResponse();
            int userId = Convert.ToInt32(Session["UserId"]);
            if (ModelState.IsValid)
            {
                try
                {

                    using (HttpClient hObj = pObj.Client())
                    {
                        var responseTask = hObj.GetAsync("UserProfile/DeleteContactPerson?CPId="+id+"&UserId="+userId);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                    }
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {
                            
                        }
                    }

                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on DeleteContactPerson() funcion in ServiceController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on DeleteContactPerson() funcion in ServiceController.cs -->Web..");

                    throw ex;
                }
                return Json(dbResponse);
            }
            else
            {
                return Json(dbResponse);
            }
        }
        #endregion

        #region AddBranch
        [HttpPost]
        public ActionResult AddBranch(CompanyBranch CP)
        {
            fileLogger.Info("Inside AddBranch() function in UserController.cs -->Web.");
            DBResponse dbResponse = new DBResponse();
            int branchid = 0;
            if (ModelState.IsValid)
            {
                try
                {

                    using (HttpClient hObj = pObj.Client())
                    {
                        var responseTask = hObj.PostAsJsonAsync<CompanyBranch>("UserProfile/AddBranch", CP);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<int>();
                            readTask.Wait();
                            branchid = readTask.Result;
                        }
                    }
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {


                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on AddContactPerson() funcion in ServiceController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on AddContactPerson() funcion in ServiceController.cs -->Web..");

                    throw ex;
                }
                return Json(branchid);
            }
            else
            {
                return Json(branchid);
            }
        }
        #endregion
        #region DeleteBranch
        [HttpGet]
        public ActionResult DeleteBranch(int id)
        {
            fileLogger.Info("Inside DeleteBranch() function in UserController.cs -->Web.");
            DBResponse dbResponse = new DBResponse();
            int userId = Convert.ToInt32(Session["UserId"]);
            if (ModelState.IsValid)
            {
                try
                {

                    using (HttpClient hObj = pObj.Client())
                    {
                        var responseTask = hObj.GetAsync("UserProfile/DeleteBranch?BRId="+id+"&UserId="+userId);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on DeleteBranch() funcion in ServiceController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on DeleteBranch() funcion in ServiceController.cs -->Web..");

                    throw ex;
                }
                return Json(dbResponse);
            }
            else
            {
                dbResponse.Message = "Failed to delete Branch";
                dbResponse.MessageType = "ERROR";
                return Json(dbResponse);
            }
        }
        #endregion

        #region AddSocialMedia
        [HttpPost]
        public ActionResult AddSocialMedia(List<ProfileSocialMedia> CP)
        {
            fileLogger.Info("Inside AddSocialMedia() function in UserController.cs -->Web.");
            DBResponse dbResponse = new DBResponse();
            if (ModelState.IsValid)
            {
                try
                {

                    using (HttpClient hObj = pObj.Client())
                    {
                        var responseTask = hObj.PostAsJsonAsync<List<ProfileSocialMedia>>("UserProfile/AddSocialMedia", CP);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                    }
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {


                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on AddSocialMedia() funcion in UserProfileController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on AddSocialMedia() funcion in UserProfileController.cs -->Web..");

                    throw ex;
                }
                return Json(dbResponse);
            }
            else
            {
                return Json(dbResponse);
            }
        }
        #endregion

        #region AddAboutsUS
        [HttpPost]
        public ActionResult AddAboutsUS(Company CP)
        {
            fileLogger.Info("Inside AddAboutsUS() function in UserController.cs -->Web.");
            DBResponse dbResponse = new DBResponse();
            if (ModelState.IsValid)
            {
                try
                {

                    using (HttpClient hObj = pObj.Client())
                    {
                        var responseTask = hObj.PostAsJsonAsync<Company>("UserProfile/AddAboutsUS", CP);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                    }
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {


                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on AddSocialMedia() funcion in UserProfileController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on AddSocialMedia() funcion in UserProfileController.cs -->Web..");

                    throw ex;
                }
                return Json(dbResponse);
            }
            else
            {
                return Json(dbResponse);
            }
        }
        #endregion

        #region Images
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Images(string id)
        {
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("UserImages/Images/" + id);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStreamAsync();
                        readTask.Wait();
                        var stream = readTask.Result;
                        return File(stream, result.Content.Headers.ContentType.ToString(), id);
                    }
                    else
                    {
                        return Content("No file found");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region Files
        [HttpGet]
        public ActionResult Files(string id)
        {
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("UserImages/Files/" + id);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStreamAsync();
                        readTask.Wait();
                        var stream = readTask.Result;
                        return File(stream, result.Content.Headers.ContentType.ToString(), id);
                    }
                    else
                    {
                        return Content("No file found");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        [HttpGet]
        public ActionResult PreviewIndex()
        {
            UserProfileModel uobj = new UserProfileModel();
            uobj = GetUserProfile();
            BindCountry();
            BindIndustryType();
            if (uobj.CompanyId > 0)
            {
               
                return View(uobj);
            }
            else
            {
                return View("Index",uobj);
            }
          
        }
        [HttpGet]
        public ActionResult PreviewIndexOnPage()
        {
            UserProfileModel uobj = new UserProfileModel();
            uobj = GetUserProfile();
            BindCountry();
            BindIndustryType();
            if (uobj.CompanyId > 0)
            {

                return View(uobj);
            }
            else
            {
                return View("Index", uobj);
            }

        }
        public ActionResult CompanyProfile(int id)
        {
            UserProfileModel uobj = new UserProfileModel();
            uobj = GetCompanyProfile(id);
            BindCountry();
            BindIndustryType();
            ViewBag.IsCompanyProfile = true;
            return View("PreviewIndex",uobj);

           
        }

        [HttpPost]
        public ActionResult PreviewIndex(UserProfileModel uobj, HttpPostedFileBase LogoFile)
        {

            fileLogger.Info("Inside Index() function in VesselPortController.cs -->Web.");
            if (ModelState.IsValid)
            {
                try
                {
                    DBResponse dbResponse = new DBResponse();
                    if (LogoFile != null)
                    {
                        string theFileName = Path.GetFileName(LogoFile.FileName);
                        byte[] thePictureAsBytes = new byte[LogoFile.ContentLength];
                        using (BinaryReader theReader = new BinaryReader(LogoFile.InputStream))
                        {
                            thePictureAsBytes = theReader.ReadBytes(LogoFile.ContentLength);
                        }
                        string thePictureDataAsString = Convert.ToBase64String(thePictureAsBytes);
                        uobj.LogoBase64 = thePictureDataAsString;
                        uobj.LogoContentType = LogoFile.ContentType;
                        var ext = LogoFile.FileName.Substring(LogoFile.FileName.LastIndexOf('.'));
                        uobj.LogoFormat = ext.ToLower();
                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".jpeg", ".gif", ".png" };
                        int MaxContentLength = 1024 * 1024 * 5;
                        if (!AllowedFileExtensions.Contains(uobj.LogoFormat))
                        {

                            TempData["Message"] = string.Format("Please Upload image of type .jpg,.jpeg,.gif,.png.");
                            TempData["MessageType"] = "ERROR";
                            BindCountry();
                            BindIndustryType();
                            return View(uobj);
                        }
                        else if (LogoFile.ContentLength > MaxContentLength)
                        {

                            TempData["Message"] = string.Format("Please Upload a file upto 5 mb.");
                            TempData["MessageType"] = "ERROR";
                            BindCountry();
                            BindIndustryType();
                            return View(uobj);
                        }
                    }
                    string jsonString = JsonConvert.SerializeObject(uobj);
                    var url = "";
                    if (uobj.CompanyId > 0)
                    {
                        uobj.ModifiedBy = Convert.ToInt32(Session["UserId"]);
                        url = "UserProfile/UpdateUserProfile";
                    }
                    else
                    {
                        uobj.CreatedBy = Convert.ToInt32(Session["UserId"]);
                        url = "UserProfile/AddUserProfile";
                    }



                    using (HttpClient hObj = pObj.Client())
                    {
                        var responseTask = hObj.PostAsJsonAsync<UserProfileModel>(url, uobj);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                        else
                        {
                            dbResponse.Message = "Failed to save the details";
                            dbResponse.MessageType = "ERROR";
                        }
                    }
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {
                            BindCountry();
                            BindIndustryType();
                            return View(uobj);
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on Index() funcion in VesselPortController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on Index() funcion in VesselPortController.cs -->Web..");

                    throw ex;
                }
                return RedirectToAction("PreviewIndex", "UserProfile");
            }
            else
            {
                BindCountry();
                BindIndustryType();
                return View(uobj);
            }
        }
    }
}