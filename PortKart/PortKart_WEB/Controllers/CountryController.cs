﻿using Domain;
using NLog;
using PortKart_WEB.Models;
using PortKart_WEB.Security;
using PortKart_WEB.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace PortKart_WEB.Controllers
{
    [AuthorizationFilter]
    [RollCheck]
    public class CountryController : Controller
    {
        #region GLOBAL VARIABLE
        public CommonMethods pObj = new CommonMethods();
        private static readonly Logger databaseLogger = LogManager.GetLogger("databaseLogger");
        private static readonly Logger fileLogger = LogManager.GetLogger("fileLogger");
        #endregion
        // GET: Country

        #region Index
        public ActionResult Index()
        {
            fileLogger.Info("Inside Index() function in CountryController.cs -->Web.");
            return View();
        }
        #endregion

        #region GetCountryList
        public ActionResult GetCountryList()
        {
            fileLogger.Info("Inside GetCountryList() function in CountryController.cs -->Web.");
            List<CountryModel> country = new List<CountryModel>();
            try
            {
                country = GetAllCountries();
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on GetCountryList() funcion in CountryController.cs -->Web..");
                databaseLogger.Error(ex, "Error on on GetUserRolesList() funcion in CountryController.cs -->Web..");
                throw ex;
            }
            return Json(country, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetAllCountries
        List<CountryModel> GetAllCountries()
        {
            fileLogger.Info("Inside GetAllCountries() function in CountryController.cs -->Web.");
            List<CountryModel> sObj = new List<CountryModel>();
            using (HttpClient hObj = pObj.Client())
            {
                var responseTask = hObj.GetAsync("Country/GetAllCountries");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<CountryModel>>();
                    readTask.Wait();
                    sObj = readTask.Result;
                }
            }
            return sObj;
        }
        #endregion

        #region AddCountry
        [HttpGet]
        public ActionResult AddCountry()
        {
            fileLogger.Info("Inside AddCountry() function in CountryController.cs -->Web.");
            return View();
        }
        #endregion

        #region AddCountry
        [HttpPost]
        public ActionResult AddCountry(CountryModel country)
        {
            fileLogger.Info("Inside AddCountry() function in CountryController.cs -->Web.");
            if (ModelState.IsValid)
            {
                try
                {
                    DBResponse dbResponse = new DBResponse();
                    country.CreatedBy = Convert.ToInt32(Session["UserId"]);
                    country.ModifiedBy = Convert.ToInt32(Session["UserId"]);

                    using (HttpClient hObj = pObj.Client())
                    {
                        var responseTask = hObj.PostAsJsonAsync<CountryModel>("Country/AddCountry", country);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                    }
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {
                            return View();
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on AddCountry() funcion in CountryController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on on AddCountry() funcion in CountryController.cs -->Web..");
                    throw ex;
                }
                return RedirectToAction("Index", "Country");
            }
            else
            {
                return View();
            }
        }
        #endregion

        #region UpdateCountry
        [HttpGet]
        public ActionResult UpdateCountry(int id)
        {
            fileLogger.Info("Inside UpdateCountry() function in CountryController.cs -->Web.");
            CountryModel cobj = new CountryModel();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("Country/GetCountryById/" + id);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<CountryModel>();
                        readTask.Wait();
                        cobj = readTask.Result;
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on UpdateCountry() funcion in CountryController.cs -->Web..");
                databaseLogger.Error(ex, "Error on on UpdateCountry() funcion in CountryController.cs -->Web..");
                throw ex;
            }
            return View(cobj);
        }
        #endregion

        #region UpdateCountry
        [HttpPost]
        public ActionResult UpdateCountry(CountryModel country)
        {
            fileLogger.Info("Inside UpdateCountry() function in CountryController.cs -->Web.");
            if (ModelState.IsValid)
            {
                try
                {
                    DBResponse dbResponse = new DBResponse();
                    country.ModifiedBy = Convert.ToInt32(Session["UserId"]);
                    using (HttpClient hObj = pObj.Client())
                    {
                        var responseTask = hObj.PostAsJsonAsync<CountryModel>("Country/UpdateCountry", country);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                        if (dbResponse != null)
                        {
                            TempData["Message"] = dbResponse.Message;
                            TempData["MessageType"] = dbResponse.MessageType;
                            if (dbResponse.MessageType == "ERROR")
                            {
                                return View();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on UpdateCountry() funcion in CountryController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on on UpdateCountry() funcion in CountryController.cs -->Web..");
                    throw ex;
                }
                return RedirectToAction("Index", "Country");
            }
            else
            {
                return View();
            }
        }
        #endregion

        #region DeletCountry
        public ActionResult DeleteCountry(int id)
        {
            fileLogger.Info("Inside DeleteCountry() function in CountryController.cs -->Web.");
            DBResponse dbResponse = new DBResponse();
            try
            {
                CountryModel country = new CountryModel();
                country.CountryId = id;
                country.ModifiedBy = id;
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.PostAsJsonAsync<CountryModel>("Country/DeleteCountry", country);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<DBResponse>();
                        readTask.Wait();
                        dbResponse = readTask.Result;
                    }
                }
                if (dbResponse != null)
                {
                    TempData["Message"] = dbResponse.Message;
                    TempData["MessageType"] = dbResponse.MessageType;
                    if (dbResponse.MessageType == "ERROR")
                    {
                        return RedirectToAction("Index", "Country");
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on DeleteCountry() funcion in CountryController.cs -->Web..");
                databaseLogger.Error(ex, "Error on on DeleteCountry() funcion in CountryController.cs -->Web..");
                throw ex;
            }
            return RedirectToAction("Index", "Country");
        }
        #endregion

    }
}