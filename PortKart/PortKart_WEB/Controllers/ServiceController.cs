﻿using Domain;
using NLog;
using PortKart_WEB.Models;
using PortKart_WEB.Security;
using PortKart_WEB.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace PortKart_WEB.Controllers
{
    [AuthorizationFilter]
    [RollCheck]
    public class ServiceController : Controller
    {
        #region GLOBAL VARIABLE
        public CommonMethods pObj = new CommonMethods();
        private static readonly Logger databaseLogger = LogManager.GetLogger("databaseLogger");
        private static readonly Logger fileLogger = LogManager.GetLogger("fileLogger");
        #endregion

        // GET: Service
        #region Index
        public ActionResult Index()
        {
            fileLogger.Info("Inside Index() function in ServiceController.cs -->Web.");
            return View();
        }
        #endregion

        #region GetServiceList
        public ActionResult GetServiceList()
        {
            fileLogger.Info("Inside GetServiceList() function in ServiceController.cs -->Web.");

            List<ServiceModel> services = new List<ServiceModel>();
            try
            {
                services = GetServices();
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on GetServiceList() funcion in ServiceController.cs -->Web..");
                databaseLogger.Error(ex, "Error on GetServiceList() funcion in ServiceController.cs -->Web..");

                throw ex;
            }
            return Json(services, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetSubServiceListById
        public ActionResult GetSubServiceListById(int parentServiceID, int serviceID)
        {
            fileLogger.Info("Inside GetSubServiceListById() function in ServiceController.cs -->Web.");

            List<SubServiceModel> services = new List<SubServiceModel>();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("Service/GetSubServiceListById?parentServiceID=" + parentServiceID + " ,serviceID=" + serviceID);

                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<SubServiceModel>>();
                        readTask.Wait();
                        services = readTask.Result;
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on GetServiceList() funcion in ServiceController.cs -->Web..");
                databaseLogger.Error(ex, "Error on GetServiceList() funcion in ServiceController.cs -->Web..");

                throw ex;
            }
            return Json(services, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region LoadService
        [HttpPost]
        public ActionResult LoadService(int id)
        {
            fileLogger.Info("Inside LoadService() function in ServiceController.cs -->Web.");

            List<ServiceModel> services = new List<ServiceModel>();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("Service/LoadService?ID="+id);

                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<ServiceModel>>();
                        readTask.Wait();
                        services = readTask.Result;
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on LoadService() funcion in ServiceController.cs -->Web..");
                databaseLogger.Error(ex, "Error on LoadService() funcion in ServiceController.cs -->Web..");

                throw ex;
            }
            return Json(services, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Get Services
        public List<ServiceModel> GetServices()
        {
            fileLogger.Info("Inside get GetServices() function in ServiceController.cs -->Web.");

            List<ServiceModel> cobj = new List<ServiceModel>();
            try
            {

                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("Service/GetServices");
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<ServiceModel>>();
                        readTask.Wait();
                        cobj = readTask.Result;
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get GetServices() funcion in ServiceController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get GetServices() funcion in ServiceController.cs -->Web..");

                throw ex;
            }
            return cobj;
        }
        #endregion

        #region BindServiceCategory
        public void BindServiceCategory()
        {
            fileLogger.Info("Inside BindServiceCategory() function in ServiceController.cs -->Web.");
            List<ServiceCategory> sc = new List<ServiceCategory>();
            sc = pObj.BindServiceCategory();
            ViewBag.ServiceCategory = new SelectList(sc, "ServiceCategoryId", "ServiceCategoryName");
        }
        #endregion

        #region AddService
        [HttpGet]
        public ActionResult AddService()
        {
            fileLogger.Info("Inside AddService() function in ServiceController.cs -->Web.");

            BindServiceCategory();
            return View();
        }
        #endregion

        #region AddService
        [HttpPost]
        public ActionResult AddService(Models.ServiceModel Service)
        {
            fileLogger.Info("Inside AddService() function in ServiceController.cs -->Web.");

            if (ModelState.IsValid)
            {
                try
                {
                    
                    DBResponse dbResponse = new DBResponse();
                    Service.CreatedBy = Convert.ToInt32(Session["UserId"]);
                    using (HttpClient hObj = pObj.Client())
                    {
                        var responseTask = hObj.PostAsJsonAsync<ServiceModel>("Service/AddService", Service);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                    }
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {
                            BindServiceCategory();
                            return View();
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on AddService() funcion in ServiceController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on AddService() funcion in ServiceController.cs -->Web..");

                    throw ex;
                }
                return RedirectToAction("Index", "Service");
            }
            else
            {
                BindServiceCategory();
                return View();
            }
        }
        #endregion

        #region Update ServiceList
        public ActionResult UpdateServiceList(int id)
        {
            fileLogger.Info("Inside UpdateServiceList() function in ServiceController.cs -->Web.");
            ServiceModel cobj = new ServiceModel();
            try
            {
                BindServiceCategory();
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("Service/GetServiceListById/" + id);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<ServiceModel>();
                        readTask.Wait();
                        cobj = readTask.Result;
                       
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on UpdateServiceList() funcion in ServiceController.cs -->Web..");
                databaseLogger.Error(ex, "Error on UpdateServiceList() funcion in ServiceController.cs -->Web..");

                throw ex;
            }
            return View(cobj);
        }
        #endregion

        #region Update ServiceList
        [HttpPost]
        public ActionResult UpdateServiceList(Models.ServiceModel ServiceList)
        {
            fileLogger.Info("Inside UpdateServiceList() function in ServiceController.cs -->Web.");
            if (ModelState.IsValid)
            {
                try
                {
                    DBResponse dbResponse = new DBResponse();
                    ServiceList.ModifiedBy = Convert.ToInt32(Session["UserId"]);
                    using (HttpClient hObj = pObj.Client())
                    {
                        var responseTask = hObj.PostAsJsonAsync<ServiceModel>("Service/UpdateServiceList", ServiceList);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                        if (dbResponse != null)
                        {
                            TempData["Message"] = dbResponse.Message;
                            TempData["MessageType"] = dbResponse.MessageType;
                            if (dbResponse.MessageType == "ERROR")
                            {
                                BindServiceCategory();
                                return View(ServiceList);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on UpdateServiceList() funcion in ServiceController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on UpdateServiceList() funcion in ServiceController.cs -->Web..");

                    throw ex;
                }
                return RedirectToAction("Index", "Service");
            }
            else
            {
                BindServiceCategory();
                return View(ServiceList);
            }
        }
        #endregion

        #region DeleteServiceList
        public ActionResult DeleteServiceList(int id)
        {
            fileLogger.Info("Inside DeleteServiceList() function in ServiceController.cs -->Web.");

            try
            {
                ServiceModel cObj = new ServiceModel();
                cObj.ServiceId = id;
                cObj.ModifiedBy = Convert.ToInt32(Session["UserId"]);

                DBResponse dbResponse = new DBResponse();
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.PostAsJsonAsync<ServiceModel>("Service/DeleteServiceList", cObj);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<DBResponse>();
                        readTask.Wait();
                        dbResponse = readTask.Result;
                    }
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {
                            return RedirectToAction("Index", "Service");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on DeleteServiceList() funcion in ServiceController.cs -->Web..");
                databaseLogger.Error(ex, "Error on DeleteServiceList() funcion in ServiceController.cs -->Web..");

                throw ex;
            }
            return RedirectToAction("Index", "Service");
        }
        #endregion

        #region GetServiceListServerSide
        public ActionResult GetServiceListServerSide()
        {
            fileLogger.Info("Inside GetServiceListServerSide() function in ServiceController.cs -->Web.");
            DataTableServerSideResult<ServiceModel> services = new DataTableServerSideResult<ServiceModel>();
            try
            {
                services = GetServicesServerSide();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(services, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Get Services ServerSide
        public DataTableServerSideResult<ServiceModel> GetServicesServerSide()
        {
            fileLogger.Info("Inside GetServicesServerSide() function in ServiceController.cs -->Web.");
            DataTableServerSideResult<ServiceModel> DTresult = new DataTableServerSideResult<ServiceModel>();
            DataTableServerSideSettings DTtablesettings = new DataTableServerSideSettings();

            //Datatable parameter
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            //paging parameter
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //sorting parameter
            DTtablesettings.sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            DTtablesettings.sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            //full search value
            DTtablesettings.searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

            //individual column search
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[0][name]").FirstOrDefault(), Request.Form.GetValues("columns[0][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[1][name]").FirstOrDefault(), Request.Form.GetValues("columns[1][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[2][name]").FirstOrDefault(), Request.Form.GetValues("columns[2][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[3][name]").FirstOrDefault(), Request.Form.GetValues("columns[3][search][value]").FirstOrDefault());

            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[4][name]").FirstOrDefault(), Request.Form.GetValues("columns[4][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[5][name]").FirstOrDefault(), Request.Form.GetValues("columns[5][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[6][name]").FirstOrDefault(), Request.Form.GetValues("columns[6][search][value]").FirstOrDefault());
            DTtablesettings.ColumnSearch.Add(Request.Form.GetValues("columns[7][name]").FirstOrDefault(), Request.Form.GetValues("columns[7][search][value]").FirstOrDefault());

            DTtablesettings.pageSize = length != null ? Convert.ToInt32(length) : 0;
            DTtablesettings.skip = start != null ? Convert.ToInt32(start) : 0;
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                 
                        var responseTask = hObj.PostAsJsonAsync<DataTableServerSideSettings>("Service/GetServicesServerSide", DTtablesettings);
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DataTableServerSideResult<ServiceModel>>();
                            readTask.Wait();
                            DTresult = readTask.Result;
                        }
                    
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on GetServicesServerSide() funcion in ServiceController.cs -->Web..");
                databaseLogger.Error(ex, "Error on  GetServicesServerSide() funcion in ServiceController.cs -->Web..");
                throw ex;
            }
            return DTresult;
        }
        #endregion

        #region AddSubService
        [HttpGet]
        public ActionResult AddSubService()
        {
            fileLogger.Info("Inside AddSubService() function in ServiceController.cs -->Web.");
            return View();
        }
        #endregion

        #region AddSubService
        [HttpPost]
        public JsonResult AddSubService(Models.SubServiceModel subservice)
        {
            fileLogger.Info("Inside AddSubService() function in ServiceController.cs -->Web.");
            DBResponse dbResponse = new DBResponse();
            if (ModelState.IsValid)
            {
               
                try
                {

                  
                    subservice.CreatedBy = Convert.ToInt32(Session["UserId"]);
                    using (HttpClient hObj = pObj.Client())
                    {
                        SubServiceList sub = new SubServiceList();
                        sub.ParentServiceId = subservice.ParentServiceId;
                        sub.Option = subservice.Option;
                        sub.ServiceId = subservice.ServiceId;
                        sub.Sort = subservice.Sort;
                        sub.CreatedBy= Convert.ToInt32(Session["UserId"]);
                        var responseTask = hObj.PostAsJsonAsync<SubServiceModel>("Service/AddSubService", subservice);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on AddSubService() funcion in ServiceController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on AddSubService() funcion in ServiceController.cs -->Web..");

                    throw ex;
                }
                return Json(dbResponse);
            }
            else
            {
                dbResponse.Message = "Failed to add SubServce";
                dbResponse.MessageType = "ERROR";
                return Json(dbResponse);
            }
        }
        #endregion

        #region LoadSubService
        [HttpGet]
        public JsonResult LoadSubService(int id)
        {
            fileLogger.Info("Inside LoadSubService() function in ServiceController.cs -->Web.");

            List<SubServiceModel> services = new List<SubServiceModel>();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("Service/LoadSubService?ID=" + id);

                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<SubServiceModel>>();
                        readTask.Wait();
                        services = readTask.Result;
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on LoadSubService() funcion in ServiceController.cs -->Web..");
                databaseLogger.Error(ex, "Error on LoadSubService() funcion in ServiceController.cs -->Web..");

                throw ex;
            }
            return Json(services, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region UpdateSubService
        [HttpPost]
        public JsonResult UpdateSubService(Models.SubServiceModel subservice)
        {
            fileLogger.Info("Inside UpdateSubService() function in ServiceController.cs -->Web.");
            DBResponse dbResponse = new DBResponse();
            if (!string.IsNullOrEmpty(subservice.Option))
            {

                try
                {


                    subservice.CreatedBy = Convert.ToInt32(Session["UserId"]);
                    using (HttpClient hObj = pObj.Client())
                    {
                        SubServiceList sub = new SubServiceList();
                        sub.ParentServiceId = subservice.ParentServiceId;
                        sub.Option = subservice.Option;
                        sub.ServiceId = subservice.ServiceId;
                        sub.Sort = subservice.Sort;
                        sub.CreatedBy = Convert.ToInt32(Session["UserId"]);
                        var responseTask = hObj.PostAsJsonAsync<SubServiceModel>("Service/UpdateSubService", subservice);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                    }

                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on UpdateSubService() funcion in ServiceController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on UpdateSubService() funcion in ServiceController.cs -->Web..");

                    throw ex;
                }
                return Json(dbResponse);
            }
            else
            {
                dbResponse.Message = "Please select an Option";
                dbResponse.MessageType = "ERROR";
                return Json(dbResponse);
            }
        }
        #endregion

        #region DeleteSubService
        [HttpPost]
        public JsonResult DeleteSubService(Models.SubServiceModel subservice)
        {
            fileLogger.Info("Inside DeleteSubService() function in ServiceController.cs -->Web.");
            DBResponse dbResponse = new DBResponse();
            if (ModelState.IsValid)
            {

                try
                {
                    subservice.CreatedBy = Convert.ToInt32(Session["UserId"]);
                    using (HttpClient hObj = pObj.Client())
                    {
                       
                        var responseTask = hObj.PostAsJsonAsync<SubServiceModel>("Service/DeleteSubService", subservice);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                    }

                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on DeleteSubService() funcion in ServiceController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on DeleteSubService() funcion in ServiceController.cs -->Web..");

                    throw ex;
                }
                return Json(dbResponse);
            }
            else
            {
                dbResponse.Message = "Failed to delete SubServce";
                dbResponse.MessageType = "ERROR";
                return Json(dbResponse);
            }
        }
        #endregion

    }
}