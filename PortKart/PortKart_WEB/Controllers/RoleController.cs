﻿#region Headers
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Configuration;
using Repository;
using Domain;
using PortKart_WEB.Security;
using PortKart_WEB.Util;
using NLog;
#endregion Headers

namespace PortKart_WEB.Controllers
{
    [AuthorizationFilter]
    [RollCheck]
    public class RoleController : Controller
    {
        //Role

        #region Global Variables
        CommonMethods pObj = new CommonMethods();
        private static readonly Logger databaseLogger = LogManager.GetLogger("databaseLogger");
        private static readonly Logger fileLogger = LogManager.GetLogger("fileLogger");
        #endregion

        #region Index
        public ActionResult Index()
        {
            fileLogger.Info("Inside Index() function in RoleController.cs -->Web.");
            return View();
        }
        #endregion

        #region GetRoleList
        public ActionResult GetRoleList()
        {

            fileLogger.Info("Inside GetRoleList() function in RoleController.cs -->Web.");
            List<Role> roles = new List<Role>();
            try
            {
                roles = GetRoles();
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on GetRoleList() funcion in RoleController.cs -->Web..");
                databaseLogger.Error(ex, "Error on  GetRoleList() funcion in RoleController.cs -->Web..");
                throw ex;
            }
            return Json(roles, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Get roles
        public List<Role> GetRoles()
        {
            fileLogger.Info("Inside GetRoles() function in RoleController.cs -->Web.");

            List<Role> cobj = new List<Role>();
            try
            {

                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("Role/GetRoles");
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<Role>>();
                        readTask.Wait();
                        cobj = readTask.Result;
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on GetRoles() funcion in RoleController.cs -->Web..");
                databaseLogger.Error(ex, "Error on  GetRoles() funcion in RoleController.cs -->Web..");
                throw ex;
            }
            return cobj;
        }
        #endregion

        #region AddRole
        [HttpGet]
        public ActionResult AddRole()
        {
            fileLogger.Info("Inside get AddRole() function in RoleController.cs -->Web.");
            return View();
        }
        #endregion

        #region AddRole
        [HttpPost]
        public ActionResult AddRole(Domain.Role Role)
        {
            fileLogger.Info("Inside post AddRole() function in RoleController.cs -->Web.");
            if (ModelState.IsValid)
            {
                try
                {
                    DBResponse dbResponse = new DBResponse();
                    Role.CreatedBy = Convert.ToInt32(Session["UserId"]);
                    using (HttpClient hObj = pObj.Client())
                    {
                        var responseTask = hObj.PostAsJsonAsync<Role>("Role/AddRole", Role);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                    }
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {
                            return View();
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on post AddRole() funcion in RoleController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on  post AddRole() funcion in RoleController.cs -->Web..");
                    throw ex;
                }
                return RedirectToAction("Index", "Role");
            }
            else
            {
                return View();
            }
        }
        #endregion

        #region UpdateRole
        public ActionResult UpdateRole(int id)
        {
            fileLogger.Info("Inside get UpdateRole() function in RoleController.cs -->Web.");
            Role cobj = new Role();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("Role/GetRoleById/" + id);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<Role>();
                        readTask.Wait();
                        cobj = readTask.Result;
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get UpdateRole() funcion in RoleController.cs -->Web..");
                databaseLogger.Error(ex, "Error on  get UpdateRole() funcion in RoleController.cs -->Web..");
                throw ex;
            }
            return View(cobj);
        }
        #endregion

        #region UpdateRole
        [HttpPost]
        public ActionResult UpdateRole(Domain.Role Role)
        {
            fileLogger.Info("Inside post UpdateRole() function in RoleController.cs -->Web.");
            if (ModelState.IsValid)
            {
                try
                {
                    DBResponse dbResponse = new DBResponse();
                    Role.ModifiedBy = Convert.ToInt32(Session["UserId"]);
                    using (HttpClient hObj = pObj.Client())
                    {
                        var responseTask = hObj.PostAsJsonAsync<Role>("Role/UpdateRole", Role);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                        if (dbResponse != null)
                        {
                            TempData["Message"] = dbResponse.Message;
                            TempData["MessageType"] = dbResponse.MessageType;
                            if (dbResponse.MessageType == "ERROR")
                            {
                                return View();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on post UpdateRole() funcion in RoleController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on  post UpdateRole() funcion in RoleController.cs -->Web..");
                    throw ex;
                }
                return RedirectToAction("Index", "Role");
            }
            else
            {
                return View();
            }
        }
        #endregion

        #region DeleteRole
        public ActionResult DeleteRole(int id)
        {
            fileLogger.Info("Inside post DeleteRole() function in RoleController.cs -->Web.");
            try
            {
                Role cObj = new Role();
                cObj.RoleId = id;
                cObj.ModifiedBy = Convert.ToInt32(Session["UserId"]);

                DBResponse dbResponse = new DBResponse();
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.PostAsJsonAsync<Role>("Role/DeleteRole", cObj);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<DBResponse>();
                        readTask.Wait();
                        dbResponse = readTask.Result;
                    }
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {
                            return View();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on post DeleteRole() funcion in RoleController.cs -->Web..");
                databaseLogger.Error(ex, "Error on  post DeleteRole() funcion in RoleController.cs -->Web..");
                throw ex;
            }
            return RedirectToAction("Index", "Role");
        }
        #endregion

    }
}
