﻿using Domain;
using NLog;
using PortKart_WEB.Models;
using PortKart_WEB.Security;
using PortKart_WEB.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace PortKart_WEB.Controllers
{
    [AuthorizationFilter]
    [AuthoriseAction(Group = "Agency")]

    public class TemplateController : Controller
    {
        #region GLOBAL VARIABLE
        public CommonMethods pObj = new CommonMethods();
        private static readonly Logger databaseLogger = LogManager.GetLogger("databaseLogger");
        private static readonly Logger fileLogger = LogManager.GetLogger("fileLogger");
        #endregion
        // GET: Template


        public ActionResult Index()
        {
            BindCurrency();
            BindService();
            BindTemplateList();

            BindOwner();
            
            BindTemplate();
            
            return View();

        }

        #region BindCurrency
        public void BindCurrency()
        {
            fileLogger.Info("Inside BindCurrency() function in TemplateController.cs -->Web.");
            List<CurrencyDomain> currencylist = new List<CurrencyDomain>();
            using (HttpClient hObj = pObj.Client())
            {
                var responseTask = hObj.GetAsync("Currency/GetCurrencyList");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<CurrencyDomain>>();
                    readTask.Wait();

                    currencylist = readTask.Result;
                }

            }
            ViewBag.CurrencyList = currencylist;
        }
        #endregion

        #region BindService
        public List<ServiceList> BindService()
        {
            fileLogger.Info("Inside BindService() function in TemplateController.cs -->Web.");
            List<ServiceList> list = new List<ServiceList>();
            using (HttpClient hObj = pObj.Client())
            {
                var responseTask = hObj.GetAsync("Template/GetServiceList");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<ServiceList>>();
                    readTask.Wait();

                    list = readTask.Result;
                }

            }
            return ViewBag.ServiceList = list;
        }

        #endregion

        #region BindTemplateList
        public List<Template> BindTemplateList()
        {
            fileLogger.Info("Inside BindTemplateList() function in TemplateController.cs -->Web.");
            List<Template> list = new List<Template>();
            using (HttpClient hObj = pObj.Client())
            {
                var responseTask = hObj.GetAsync("Template/GetTemplateList");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<Template>>();
                    readTask.Wait();

                    list = readTask.Result;
                }

            }
            return ViewBag.Template = list;
        }

        #endregion


        #region GetTemplateList
        [HttpGet]
        public JsonResult GetTemplateList(string search)
        {
            fileLogger.Info("Inside GetTemplateList() function in TemplateController.cs -->Web.");
            List<Template> ServiceList = new List<Template>();
            List<Select2Model> ResultList = new List<Select2Model>();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("Template/GetTemplateList?search=" + search);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<Template>>();
                        readTask.Wait();

                        ServiceList = readTask.Result;
                    }

                }
                //ViewBag.PortList = ServiceList.Take(50);
                if (ServiceList.Count > 0)
                    ResultList = ServiceList.Select(x => new Select2Model { id = x.TemplateId, text = x.TemplateName }).ToList();

                var jsonresult = Json(ResultList, JsonRequestBehavior.AllowGet);
                jsonresult.MaxJsonLength = int.MaxValue;
                return jsonresult;
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get GetTemplateList() funcion in TemplateController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get GetTemplateList() funcion in TemplateController.cs -->Web..");

                throw ex;
            }


        }
        #endregion

        #region LoadTemplateService
        [HttpGet]
        public JsonResult LoadTemplateService(int id)
        {
            fileLogger.Info("Inside LoadTemplateService() function in TemplateController.cs -->Web.");
            List<TemplateDetails> ServiceList = new List<TemplateDetails>();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var AgencyId = Convert.ToInt32(Session["UserId"]);

                    var responseTask = hObj.GetAsync("Template/LoadTemplateService?TemplateId=" + id + "&AgencyId=" + AgencyId);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<TemplateDetails>>();
                        readTask.Wait();

                        ServiceList = readTask.Result;
                    }

                }

                var jsonresult = Json(ServiceList, JsonRequestBehavior.AllowGet);
                jsonresult.MaxJsonLength = int.MaxValue;
                return jsonresult;
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get LoadTemplateService() funcion in TemplateController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get LoadTemplateService() funcion in TemplateController.cs -->Web..");
                throw ex;
            }

        }
        #endregion

        #region LoadServicebyId
        [HttpPost]
        public JsonResult LoadServicebyId(int id)
        {
            fileLogger.Info("Inside LoadServicebyId() function in TemplateController.cs -->Web.");
            List<AgencyServiceDetails> ServiceList = new List<AgencyServiceDetails>();
            List<TemplateDetails> ServicetemplateList = new List<TemplateDetails>();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("Template/LoadServicebyId/" + id);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<AgencyServiceDetails>>();
                        readTask.Wait();

                        ServiceList = readTask.Result;
                    }

                }
                foreach (AgencyServiceDetails Tlist in ServiceList)
                {
                    TemplateDetails temp = new TemplateDetails();
                    temp.ServiceId = Tlist.ServiceId;
                    temp.ParentId = Tlist.ParentId;
                    temp.ModifiedBy = Tlist.ModifiedBy;
                    temp.ModifiedDate = Tlist.ModifiedDate;
                    temp.CreatedBy = Tlist.CreatedBy;
                    temp.CreatedDate = Tlist.CreatedDate;
                    temp.IsParent = Tlist.IsParent;
                    temp.CurrencyId = Tlist.CurrencyId;
                    temp.CostType = Tlist.CostType;
                    temp.Option = Tlist.Option;
                    temp.ServiceName = Tlist.ServiceName;
                    temp.ServiceCategoryName = Tlist.ServiceCategoryName;

                    ServicetemplateList.Add(temp);
                }

                var jsonresult = Json(ServicetemplateList, JsonRequestBehavior.AllowGet);
                jsonresult.MaxJsonLength = int.MaxValue;
                return jsonresult;
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get LoadServicebyId() funcion in AgencyController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get LoadServicebyId() funcion in AgencyController.cs -->Web..");

                throw ex;
            }

        }
        #endregion


        #region SaveAgencyServiceTemplate
        [HttpPost]
        public JsonResult SaveAgencyServiceTemplate(Template obj)
        {
            fileLogger.Info("Inside SaveAgencyServiceTemplate() function in TemplateController.cs -->Web.");
            DBResponse dbResponse = new DBResponse();

            try
            {

                obj.AgencyId = Convert.ToInt32(Session["UserId"]);
                obj.CreatedBy = Convert.ToInt32(Session["UserId"]);
                using (HttpClient hObj = pObj.Client())
                {

                    var responseTask = hObj.PostAsJsonAsync<Template>("Template/SaveAgencyServiceTemplate", obj);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<DBResponse>();
                        readTask.Wait();
                        dbResponse = readTask.Result;
                    }
                    else
                    {
                        fileLogger.Error(result.StatusCode.ToString(), "Connection Error on SaveAgencyServiceTemplate() funcion in TemplateController.cs -->Web..");
                        databaseLogger.Error(result.StatusCode.ToString(), "Connection Error on SaveAgencyServiceTemplate() funcion in TemplateController.cs -->Web..");
                        dbResponse.Message = "Failed to save agency service details";
                        dbResponse.MessageType = "ERROR";
                        return Json(dbResponse);
                    }
                }

            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on SaveAgencyServiceTemplate() funcion in TemplateController.cs -->Web..");
                databaseLogger.Error(ex, "Error on SaveAgencyServiceTemplate() funcion in TemplateController.cs -->Web..");

                throw ex;
            }
            return Json(dbResponse);

        }
        #endregion

        #region Addownertemplate
        public ActionResult AddOwnerTemplate()
        {
            fileLogger.Info("Inside AddOwnerTemplate() function in TemplateController.cs -->Web.");
            BindOwner();
            BindVesselPort();
            BindTemplate();
            return View();
        }
        #endregion

        #region AddOwnerTemplate
        [HttpPost]
        public ActionResult AddOwnerTemplate(OwnerTemplateModel owner)
        {
            fileLogger.Info("Inside AddOwnerTemplate() function in templateController.cs -->Web.");
            if (ModelState.IsValid)
            {
                try
                {
                    DBResponse dbResponse = new DBResponse();
                    owner.CreatedBy = Convert.ToInt32(Session["UserId"]);
                    using (HttpClient hObj = pObj.Client())
                    {
                        var responseTask = hObj.PostAsJsonAsync<OwnerTemplateModel>("Template/AddOwnerTemplate", owner);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                    }
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {
                            BindOwner();
                            BindVesselPort();
                            BindTemplate();
                            return View();
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on AddOwnerTemplate() funcion in templateController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on on AddOwnerTemplate() funcion in templateController.cs -->Web..");
                    throw ex;
                }
                return RedirectToAction("Index", "Template");
            }
            else
            {
                BindOwner();
                BindVesselPort();
                BindTemplate();
                return View();
            }
        }
        #endregion

        #region Update OwnerTemplate
        [HttpGet]
        public ActionResult UpdateOwnerTemplate(int id)
        {
            fileLogger.Info("Inside UpdateOwnerTemplate() function in templateController.cs -->Web.");
            OwnerTemplateModel cobj = new OwnerTemplateModel();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("Template/GetOwnerTemplateById/" + id );
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<OwnerTemplateModel>();
                        readTask.Wait();
                        cobj = readTask.Result;
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on UpdateOwnerTemplate() funcion in templateController.cs -->Web..");
                databaseLogger.Error(ex, "Error on UpdateOwnerTemplate() funcion in templateController.cs -->Web..");

                throw ex;
            }
            BindOwner();
            BindVesselPort();
            BindTemplate();
            return View(cobj);
        }
        #endregion


        #region Update OwnerTemplate
        [HttpPost]
        public ActionResult UpdateOwnerTemplate(OwnerTemplateModel ownerTemplate)
        {
            fileLogger.Info("Inside UpdateOwnerTemplate() function in templateController.cs -->Web.");
            if (ModelState.IsValid)
            {
                try
                {
                    DBResponse dbResponse = new DBResponse();
                    //ServiceList.ModifiedBy = Convert.ToInt32(Session["UserId"]);
                    using (HttpClient hObj = pObj.Client())
                    {
                        var responseTask = hObj.PostAsJsonAsync<OwnerTemplateModel>("Template/UpdateOwnerTemplate", ownerTemplate);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                        if (dbResponse != null)
                        {
                            TempData["Message"] = dbResponse.Message;
                            TempData["MessageType"] = dbResponse.MessageType;
                            if (dbResponse.MessageType == "ERROR")
                            {
                                BindOwner();
                                BindVesselPort();
                                BindTemplate();
                                return View();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on UpdateOwnerTemplate() funcion in templateController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on UpdateOwnerTemplate() funcion in templateController.cs -->Web..");



                    throw ex;
                }
                return RedirectToAction("Index", "Template");
            }
            else
            {
                BindOwner();
                BindVesselPort();
                BindTemplate();
                return View();
            }
        }
        #endregion

        #region DeleteOwnerTemplate

    
        public ActionResult DeleteOwnerTemplate(int id)
        {
            fileLogger.Info("Inside DeleteOwnerTemplate() function in templateController.cs -->Web.");
          
            try
            {
                DBResponse dbResponse = new DBResponse();
                OwnerTemplate temp = new OwnerTemplate();
                
                temp.OwnerTemplateId = id;
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.PostAsJsonAsync<OwnerTemplate>("Template/DeleteOwnerTemplate", temp);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<DBResponse>();
                        readTask.Wait();
                        dbResponse = readTask.Result;
                    }
                }
                if (dbResponse != null)
                {
                    TempData["Message"] = dbResponse.Message;
                    TempData["MessageType"] = dbResponse.MessageType;
                    if (dbResponse.MessageType == "ERROR")
                    {
                        BindOwner();
                        BindTemplate();
                        return View();
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on DeleteOwnerTemplate() funcion in templateController.cs -->Web..");
                databaseLogger.Error(ex, "Error on on DeleteOwnerTemplate() funcion in templateController.cs -->Web..");
                throw ex;
            }
            BindOwner();
            BindTemplate();
            return RedirectToAction("Index", "Template");
        }
        #endregion


        #region BindOwner
        public void BindOwner()
        {
            fileLogger.Info("Inside  BindOwner() function in templateController.cs -->Web.");
            List<User> owner = new List<User>();
            owner = pObj.BindOwner();

          
            ViewBag.Owner = new SelectList((from s in owner
                                            select new
                                            {
                                                UserId = s.UserId,
                                                UserName = s.FirstName + " " + s.LastName + " (" + s.EmailId + ")"
                                            }), "UserId", "UserName", null);
        }
        #endregion

        #region BindTemplate
        public void BindTemplate()
        {
            fileLogger.Info("Inside BindTemplate() function in templateController.cs -->Web.");
            List<Template> template = new List<Template>();
            template = pObj.BindTemplate();
            ViewBag.TemplateName = new SelectList(template, "TemplateId", "TemplateName");
        }
        #endregion

        #region GetOwnerTemplateList
        public ActionResult GetOwnerTemplateList()
        {
            fileLogger.Info("Inside GetOwnerTemplateList() function in OwnerTemplateController.cs -->Web.");
            List<OwnerTemplateModel> ownertemplate = new List<OwnerTemplateModel>();
            try
            {
                ownertemplate = GetAllOwnerTemplateList();
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on GetOwnerTemplateList() funcion in TemplateController.cs -->Web..");
                databaseLogger.Error(ex, "Error on on GetOwnerTemplateList() funcion in TemplateController.cs -->Web..");
                throw ex;
            }
            return Json(ownertemplate, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region GetAllOwnerTemplateList
        List<OwnerTemplateModel> GetAllOwnerTemplateList()
        {
            fileLogger.Info("Inside GetAllOwnerTemplateList() function in OwnerTemplateController.cs -->Web.");
            List<OwnerTemplateModel> sObj = new List<OwnerTemplateModel>();
            using (HttpClient hObj = pObj.Client())
            {
                var responseTask = hObj.GetAsync("Template/GetAllOwnerTemplateList");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<OwnerTemplateModel>>();
                    readTask.Wait();
                    sObj = readTask.Result;
                }
            }
            return sObj;
        }
        #endregion

        #region BindVesselPort
        public void BindVesselPort()
        {
            fileLogger.Info("Inside BindVesselPort() function in templateController.cs -->Web.");
            List<VesselPort> vesselPort = new List<VesselPort>();
            vesselPort = pObj.BindVesselPort();
            ViewBag.Port = new SelectList(vesselPort, "PortId", "Port");
        }
        #endregion

        #region GetTemplateNameList
   
        public JsonResult GetTemplateNameList(string search)
        {
            fileLogger.Info("Inside GetTemplateNameList() function in TemplateController.cs -->Web.");
            //List<Template> sObj = new List<Template>();
            List<Template> ServiceList = new List<Template>();
            List<Select2Model> ResultList = new List<Select2Model>();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("Template/GetTemplateNameList?search=" + search);

                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<Template>>();
                        readTask.Wait();

                        ServiceList = readTask.Result;
                    }

                }
                //ViewBag.PortList = ServiceList.Take(50);
                if (ServiceList.Count > 0)
                    ResultList = ServiceList.Select(x => new Select2Model { id = x.TemplateId, text = x.TemplateName }).ToList();

                var jsonresult = Json(ResultList, JsonRequestBehavior.AllowGet);
                jsonresult.MaxJsonLength = int.MaxValue;
                return jsonresult;
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get GetVesselPortList() funcion in OwnerController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get GetVesselPortList() funcion in OwnerController.cs -->Web..");

                throw ex;
            }

        }
        #endregion


        #region UpdateServiceTemplate
        [HttpPost]
        public JsonResult UpdateServiceTemplate(Template obj)
        {
            fileLogger.Info("Inside UpdateServiceTemplate() function in TemplateController.cs -->Web.");
            DBResponse dbResponse = new DBResponse();
            try
            {
                
                obj.AgencyId = Convert.ToInt32(Session["UserId"]);
                obj.CreatedBy = Convert.ToInt32(Session["UserId"]);
                
                using (HttpClient hObj = pObj.Client())
                {
                  
                    var responseTask = hObj.PostAsJsonAsync<Template>("Template/UpdateServiceTemplate", obj);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<DBResponse>();
                        readTask.Wait();
                        dbResponse = readTask.Result;
                    }
                    else
                    {
                        fileLogger.Error(result.StatusCode.ToString(), "Connection Error on UpdateServiceTemplate() funcion in TemplateController.cs -->Web..");
                        databaseLogger.Error(result.StatusCode.ToString(), "Connection Error on UpdateServiceTemplate() funcion in TemplateController.cs -->Web..");
                        dbResponse.Message = "Failed to update  service template details";
                        dbResponse.MessageType = "ERROR";
                        return Json(dbResponse);
                    }
                }

            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on UpdateServiceTemplate() funcion in TemplateController.cs -->Web..");
                databaseLogger.Error(ex, "Error on UpdateServiceTemplate() funcion in TemplateController.cs -->Web..");

                throw ex;
            }
            return Json(dbResponse);

        }
        #endregion

   #region GetVesselPortList
        [HttpGet]
        public JsonResult GetVesselPortList(string search)
        {
            fileLogger.Info("Inside GetVesselPortList() function in OwnerController.cs -->Web.");
            List<VesselPort> ServiceList = new List<VesselPort>();
            List<Select2Model> ResultList = new List<Select2Model>();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("Template/GetVesselPortList?search=" + search);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<List<VesselPort>>();
                        readTask.Wait();

                        ServiceList = readTask.Result;
                    }

                }
                //ViewBag.PortList = ServiceList.Take(50);
                if (ServiceList.Count > 0)
                    ResultList = ServiceList.Select(x => new Select2Model { id = x.PortId, text = x.Port }).ToList();

                var jsonresult = Json(ResultList, JsonRequestBehavior.AllowGet);
                jsonresult.MaxJsonLength = int.MaxValue;
                return jsonresult;
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on get GetVesselPortList() funcion in OwnerController.cs -->Web..");
                databaseLogger.Error(ex, "Error on get GetVesselPortList() funcion in OwnerController.cs -->Web..");

                throw ex;
            }



        }
        #endregion





    }
}