﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain;
using Repository;
using Dapper;
using System.Net.Http;
using PortKart_WEB.Util;

using System.Configuration;
using NLog;

namespace PortKart_WEB.Controllers
{
    public class LoginController : Controller
    {
        public CommonMethods pObj = new CommonMethods();
        private static readonly Logger databaseLogger = LogManager.GetLogger("databaseLogger");
        private static readonly Logger fileLogger = LogManager.GetLogger("fileLogger");

        // GET: Login
        #region Index GET
        [HttpGet]
        public ActionResult Index()
        {
            fileLogger.Info("Inside Index() function in LoginController.cs -->Web.");

            if (Session["UserId"] != null)
            {
                if (Session["UserRoleName"].ToString() == "SuperAdmin" || Session["UserRoleName"].ToString() == "Admin")
                {
                    return RedirectToAction("Index", "Dashboard");
                }
                else if (Session["UserGroup"].ToString() == "Owner")
                {
                    return RedirectToAction("Index", "Owner");
                }
                else if (Session["UserGroup"].ToString() == "Agency")
                {
                    return RedirectToAction("Index", "Agency");
                }
                else
                {
                    return RedirectToAction("PreviewIndex", "UserProfile");
                }
            }
            return View();
        }
        #endregion

        #region Index POST
        [HttpPost]
        public ActionResult Index(string emailId, string Password)
        {
            ViewBag.ResendEmail = false;
            fileLogger.Info("Inside Index() function in LoginController.cs -->Web.");
            if (ModelState.IsValid)
            {
                try
                {
                    List<UserRole> uObj = new List<UserRole>();
                    Login lObj = new Login();
                    lObj.emailId = emailId;
                    lObj.Password = pObj.EncMD5(Password);
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseURL"]);
                        var responseTask = client.PostAsJsonAsync<Login>("Login/GetUsers", lObj);
                        responseTask.Wait();

                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<List<UserRole>>();
                            readTask.Wait();

                            uObj = readTask.Result;
                        }
                    }
                    if (uObj.Count == 0)
                    {
                        ViewBag.Message = "Invalid Username or Password";
                    }
                    else
                    {
                        var companyid = 0;
                        foreach (var val in uObj)
                        {
                            if (val.RoleId == 0 || val.RoleName == null)
                            {
                                ViewBag.Message = "Roles are not assigned to this user.";
                                return View();
                            }
                            if (!val.EmailVerified)
                            {
                                ViewBag.Message = "Email address not verfied";
                                ViewBag.ResendEmail = true;
                                return View();
                            }
                            Session["UserId"] = val.UserId;
                            Session["RoleId"] = val.RoleId;
                            Session["UserFullName"] = val.FirstName + ' ' + val.LastName;
                            Session["UserRoleName"] = val.RoleName;
                            Session["LoginObj"] = new Login { emailId = emailId, Password = Password };
                            Session["UserGroup"] = val.UserGroup;
                            companyid = val.CompanyId;
                        }
                        if (Session["UserRoleName"].ToString() == "SuperAdmin" || Session["UserRoleName"].ToString() == "Admin")
                        {
                            return RedirectToAction("Index", "Dashboard");
                        }
                        else if (Session["UserGroup"].ToString() == "Owner")
                        {
                            if (companyid > 0)
                                return RedirectToAction("Index", "Owner");
                            else
                                return RedirectToAction("PreviewIndex", "UserProfile");
                        }
                        else if (Session["UserGroup"].ToString() == "Agency")
                        {
                            if (companyid > 0)
                                return RedirectToAction("Index", "Agency");
                            else
                                return RedirectToAction("PreviewIndex", "UserProfile");
                        }
                        else
                            return RedirectToAction("PreviewIndex", "UserProfile");
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on Index() funcion in LoginController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on  Index() funcion in LoginController.cs -->Web..");
                    throw ex;
                }
            }
            return View();
        }
        #endregion
        #region Logout
        [HttpGet]
        public ActionResult Logout()
        {
            fileLogger.Info("Inside Logout() function in LoginController.cs -->Web.");
            Session["UserId"] = null;
            Session["RoleId"] = null;
            Session["UserFullName"] = null;
            Session["UserRoleName"] = null;
            Session["LoginObj"] = null;
            Session["AccessToken"] = null;
            Session["UserGroup"] = null;
            Session.Abandon();
            return RedirectToAction("Index", "Login");
        }
        #endregion

        #region Register GET
        [HttpGet]
        public ActionResult Register()
        {
            fileLogger.Info("Inside Register() function in LoginController.cs -->Web.");
            return View();
        }
        #endregion

        #region Register POST
        [HttpPost]
        public ActionResult Register(Models.UserModel obj)
        {
            fileLogger.Info("Inside Register() function in LoginController.cs -->Web.");
            if (ModelState.IsValid)
            {
                try
                {
                    obj.Password = pObj.EncMD5(obj.Password);
                    DBResponse dbresponse = new DBResponse();
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseURL"]);
                        var responseTask = client.PostAsJsonAsync<Models.UserModel>("Login/Register", obj);
                        responseTask.Wait();

                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();

                            dbresponse = readTask.Result;
                        }
                        if (dbresponse != null)
                        {
                            TempData["Message"] = dbresponse.Message;
                            TempData["MessageType"] = dbresponse.MessageType;
                            if (dbresponse.MessageType == "ERROR")
                            {

                                return View();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on Register() funcion in LoginController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on  Register() funcion in LoginController.cs -->Web..");
                    throw ex;
                }
            }
            else
            {
                return View();
            }
            return RedirectToAction("Register", "Login");
        }
        #endregion

        #region ForgotPassword
        [HttpGet]
        public ActionResult ForgotPassword()
        {
            fileLogger.Info("Inside ForgotPassword() function in LoginController.cs -->Web.");
            return View();
        }
        #endregion
        #region ForgotPassword
        [HttpPost]
        public ActionResult ForgotPassword(string EmailId)
        {
            fileLogger.Info("Inside ForgotPassword() function in LoginController.cs -->Web.");

            try
            {
                DBResponse dbresponse = new DBResponse();
                using (var client = new HttpClient())
                {
                    User uobj = new User();
                    uobj.EmailId = EmailId;
                    uobj.CreatedBy = 0;
                    client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseURL"]);
                    var responseTask = client.PostAsJsonAsync<User>("Login/ForgotPassword", uobj);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<DBResponse>();
                        readTask.Wait();

                        dbresponse = readTask.Result;
                    }
                    else
                    {
                        dbresponse.Message = "Unable to reset password";
                        dbresponse.MessageType = "ERROR";
                    }
                    if (dbresponse != null)
                    {
                        TempData["Message"] = dbresponse.Message;
                        TempData["MessageType"] = dbresponse.MessageType;
                        if (dbresponse.MessageType == "ERROR")
                        {

                            return View();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on Register() funcion in LoginController.cs -->Web..");
                databaseLogger.Error(ex, "Error on  Register() funcion in LoginController.cs -->Web..");
                throw ex;
            }
            return View();
        }
        #endregion

        #region ResendVerify
        [HttpGet]
        public ActionResult ResendVerify()
        {
            fileLogger.Info("Inside ResendVerify() function in LoginController.cs -->Web.");
            return View();
        }
        #endregion
        #region ResendVerify
        [HttpPost]
        public ActionResult ResendVerify(string EmailId)
        {
            fileLogger.Info("Inside ResendVerify() function in LoginController.cs -->Web.");

            try
            {
                DBResponse dbresponse = new DBResponse();
                using (var client = new HttpClient())
                {
                    User uobj = new User();
                    uobj.EmailId = EmailId;
                    uobj.CreatedBy = 0;
                    client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseURL"]);
                    var responseTask = client.PostAsJsonAsync<User>("Login/ResendVerify", uobj);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<DBResponse>();
                        readTask.Wait();

                        dbresponse = readTask.Result;
                    }
                    else
                    {
                        dbresponse.Message = "There was an unexpected error and the verification mail was not sent";
                        dbresponse.MessageType = "ERROR";
                    }
                    if (dbresponse != null)
                    {
                        TempData["Message"] = dbresponse.Message;
                        TempData["MessageType"] = dbresponse.MessageType;
                        if (dbresponse.MessageType == "ERROR")
                        {

                            return View();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on Register() funcion in LoginController.cs -->Web..");
                databaseLogger.Error(ex, "Error on  Register() funcion in LoginController.cs -->Web..");
                throw ex;
            }
            return View();
        }
        #endregion

    }
}