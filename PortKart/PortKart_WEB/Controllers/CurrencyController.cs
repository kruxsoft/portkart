﻿using Domain;
using NLog;
using PortKart_WEB.Models;
using PortKart_WEB.Security;
using PortKart_WEB.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace PortKart_WEB.Controllers
{
    [AuthorizationFilter]
    [RollCheck]
    public class CurrencyController : Controller
    {
        #region GLOBAL VARIABLE
        public CommonMethods pObj = new CommonMethods();
        private static readonly Logger databaseLogger = LogManager.GetLogger("databaseLogger");
        private static readonly Logger fileLogger = LogManager.GetLogger("fileLogger");
        #endregion

        #region Index
        public ActionResult Index()
        {
            fileLogger.Info("Inside Index() function in CurrencyController.cs -->Web.");
            return View();
        }
        #endregion

        #region GetCurrencyList
        public ActionResult GetCurrencyList()
        {
            fileLogger.Info("Inside GetCurrencyList() function in CurrencyController.cs -->Web.");

            List<CurrencyModel> currency = new List<CurrencyModel>();
            try
            {
                currency = GetAllCurrencies();
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on GetCurrencyList() funcion in CurrencyController.cs -->Web..");
                databaseLogger.Error(ex, "Error on on GetCurrencyList() funcion in CurrencyController.cs -->Web..");
                throw ex;
            }
            return Json(currency, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region GetAllCurrencies
        List<CurrencyModel> GetAllCurrencies()
        {
            fileLogger.Info("Inside GetAllCurrencies() function in CurrencyController.cs -->Web.");

            List<CurrencyModel> sObj = new List<CurrencyModel>();
            using (HttpClient hObj = pObj.Client())
            {
                var responseTask = hObj.GetAsync("Currency/GetAllCurrencies");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<CurrencyModel>>();
                    readTask.Wait();
                    sObj = readTask.Result;
                }
            }
            return sObj;
        }
        #endregion

        #region AddCurrency
        [HttpGet]
        public ActionResult AddCurrency()
        {
            fileLogger.Info("Inside AddCurrency() function in CurrencyController.cs -->Web.");
            return View();
        }
        #endregion

        #region AddCurrency
        [HttpPost]
        public ActionResult AddCurrency(CurrencyModel obj)
        {
            fileLogger.Info("Inside AddCurrency() function in CurrencyController.cs -->Web.");
            if (ModelState.IsValid)
            {
                try
                {
                    DBResponse dbResponse = new DBResponse();
                    obj.CreatedBy = Convert.ToInt32(Session["UserId"]);
                    obj.ModifiedBy = Convert.ToInt32(Session["UserId"]);

                    using (HttpClient hObj = pObj.Client())
                    {
                        var responseTask = hObj.PostAsJsonAsync<CurrencyModel>("Currency/AddCurrency", obj);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                    }
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {
                            return View();
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on AddCurrency() funcion in CurrencyController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on on AddCurrency() funcion in CurrencyController.cs -->Web..");
                    throw ex;
                }
                return RedirectToAction("Index", "Currency");
            }
            else
            {
                return View();
            }
        }
        #endregion

        #region UpdateCurrency
        [HttpGet]
        public ActionResult UpdateCurrency(int id)
        {
            fileLogger.Info("Inside UpdateCurrency() function in CurrencyController.cs -->Web.");
            CurrencyModel cobj = new CurrencyModel();
            try
            {
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.GetAsync("Currency/GetCurrencyById/" + id);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<CurrencyModel>();
                        readTask.Wait();
                        cobj = readTask.Result;
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on GetUserRolesList() funcion in CurrencyController.cs -->Web..");
                databaseLogger.Error(ex, "Error on on GetUserRolesList() funcion in CurrencyController.cs -->Web..");
                throw ex;
            }
            return View(cobj);
        }
        #endregion

        #region UpdateCurrency
        [HttpPost]
        public ActionResult UpdateCurrency(CurrencyModel obj)
        {
            fileLogger.Info("Inside UpdateCurrency() function in CurrencyController.cs -->Web.");

            if (ModelState.IsValid)
            {
                try
                {
                    DBResponse dbResponse = new DBResponse();
                    obj.ModifiedBy = Convert.ToInt32(Session["UserId"]);
                    using (HttpClient hObj = pObj.Client())
                    {
                        var responseTask = hObj.PostAsJsonAsync<CurrencyModel>("Currency/UpdateCurrency", obj);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                        if (dbResponse != null)
                        {
                            TempData["Message"] = dbResponse.Message;
                            TempData["MessageType"] = dbResponse.MessageType;
                            if (dbResponse.MessageType == "ERROR")
                            {
                                return View();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileLogger.Error(ex, "Error on UpdateCurrency() funcion in CurrencyController.cs -->Web..");
                    databaseLogger.Error(ex, "Error on on UpdateCurrency() funcion in CurrencyController.cs -->Web..");
                    throw ex;
                }
                return RedirectToAction("Index", "Currency");
            }
            else
            {
                return View();
            }
        }
        #endregion

        #region DeleteCurrency
        public ActionResult DeleteCurrency(int id)
        {
            fileLogger.Info("Inside DeleteCurrency() function in CurrencyController.cs -->Web.");
            DBResponse dbResponse = new DBResponse();
            try
            {
                CurrencyModel currency = new CurrencyModel();
                currency.CurrencyId = id;
                currency.ModifiedBy = id;
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.PostAsJsonAsync<CurrencyModel>("Currency/DeleteCurrency", currency);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<DBResponse>();
                        readTask.Wait();
                        dbResponse = readTask.Result;
                    }
                }
                if (dbResponse != null)
                {
                    TempData["Message"] = dbResponse.Message;
                    TempData["MessageType"] = dbResponse.MessageType;
                    if (dbResponse.MessageType == "ERROR")
                    {
                        return RedirectToAction("Index", "Currency");
                    }
                }
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on DeleteCurrency() funcion in CurrencyController.cs -->Web..");
                databaseLogger.Error(ex, "Error on on DeleteCurrency() funcion in CurrencyController.cs -->Web..");
                throw ex;
            }
            return RedirectToAction("Index", "Currency");
        }
        #endregion

    }
}