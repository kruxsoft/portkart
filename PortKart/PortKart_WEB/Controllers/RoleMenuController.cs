﻿#region Header
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Repository;
using Domain;
using PortKart_WEB.Security;
using System.Net.Http;
using System.Configuration;
using PortKart_WEB.Util;
using NLog;
#endregion
namespace PortKart_WEB.Controllers
{
   [AuthorizationFilter]
   
    public class RoleMenuController : Controller
    {
        #region GLOBAL VARIABLE
        public CommonMethods pObj = new CommonMethods();
        private static readonly Logger databaseLogger = LogManager.GetLogger("databaseLogger");
        private static readonly Logger fileLogger = LogManager.GetLogger("fileLogger");

        #endregion

        #region Index
        [RollCheck]
        public ActionResult Index()
        {
            fileLogger.Info("Inside Index() function in RoleMenuController.cs -->Web.");
            return View();
        }
        #endregion

        #region GetRoleMenuList
        public ActionResult GetRoleMenuList()
        {
            fileLogger.Info("Inside GetRoleMenuList() function in RoleMenuController.cs -->Web.");
            List<RoleMenu> roleMenu = new List<RoleMenu>();
            try
            {
                roleMenu = GetAllRoleMenus();
            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on GetRoleMenuList() funcion in RoleMenuController.cs -->Web..");
                databaseLogger.Error(ex, "Error on  GetRoleMenuList() funcion in RoleMenuController.cs -->Web..");
                throw ex;
            }            
            return Json(roleMenu, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region MenuLayout        
        public ActionResult MenuLayout()
        {
            fileLogger.Info("Inside MenuLayout() function in RoleMenuController.cs -->Web.");
            List<RoleMenu> rObj = new List<RoleMenu>();
            using (HttpClient hObj = pObj.Client())
            {
                int id = Convert.ToInt32(Session["RoleId"]);
              
                var responseTask = hObj.GetAsync("RoleMenu/GetRoleMenus/" + id);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<RoleMenu>>();
                    readTask.Wait();

                    rObj = readTask.Result;
                }

            }         
             ViewBag.MenuList = rObj;
            return PartialView("_MenuLayout");
        }
        #endregion

        #region GetAllRoleMenus
        List<RoleMenu> GetAllRoleMenus()
        {
            fileLogger.Info("Inside GetAllRoleMenus() function in RoleMenuController.cs -->Web.");
            List<RoleMenu> rObj = new List<RoleMenu>();
            using (HttpClient hObj = pObj.Client())
            {
                var responseTask = hObj.GetAsync("RoleMenu/GetAllRoleMenus/");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<RoleMenu>>();
                    readTask.Wait();
                    rObj = readTask.Result;
                }
            }
            return rObj;
        }
        #endregion

        #region BindRole
        public void BindRole()
        {
            fileLogger.Info("Inside BindRole() function in RoleMenuController.cs -->Web.");
            List<Role> role = new List<Role>();
            role = pObj.BindRole();
            ViewBag.Role = new SelectList(role, "RoleId", "RoleName");
        }
        #endregion

        #region BindMenu
        public void BindMenu()
        {
            fileLogger.Info("Inside BindMenu() function in RoleMenuController.cs -->Web.");
            List<RoleMenu> roleMenus = new List<RoleMenu>();
            roleMenus = pObj.BindMenu();
            ViewBag.Menu = new SelectList(roleMenus, "MenuId", "MenuName");
        }
        #endregion

        #region AddRoleMenu
        [RollCheck]
        [HttpGet]
        public ActionResult AddRoleMenu()
        {
            fileLogger.Info("Inside get AddRoleMenu() function in RoleMenuController.cs -->Web.");
            BindRole();
            BindMenu();
            return View();
        }
        [HttpPost]
        [RollCheck]
        public ActionResult AddRoleMenu(RoleMenu cs)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    DBResponse dbResponse = new DBResponse();
                    using (HttpClient hObj = pObj.Client())
                    {
                        cs.CreatedBy = Convert.ToInt32(Session["UserId"]);
                        var responseTask = hObj.PostAsJsonAsync<RoleMenu>("RoleMenu/AddRoleMenu", cs);
                        responseTask.Wait();
                        var result = responseTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<DBResponse>();
                            readTask.Wait();
                            dbResponse = readTask.Result;
                        }
                    }
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {
                            BindRole();
                            BindMenu();
                            return View();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return RedirectToAction("Index", "RoleMenu");
            }
            else
            {
                BindRole();
                BindMenu();
                return View();
            }
        }
        #endregion

        #region DeleteRolemenu
        [RollCheck]
        public ActionResult DeleteRolemenu(int MenuId, int RoleId)
        {
            fileLogger.Info("Inside post DeleteRolemenu() function in RoleMenuController.cs -->Web.");
            try
            {
                DBResponse dbResponse = new DBResponse();
                RoleMenu roleMenu = new RoleMenu();
                roleMenu.MenuId = MenuId;
                roleMenu.RoleId = RoleId;
                using (HttpClient hObj = pObj.Client())
                {
                    var responseTask = hObj.PostAsJsonAsync<RoleMenu>("RoleMenu/DeleteRolemenu/", roleMenu);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<DBResponse>();
                        readTask.Wait();
                        dbResponse = readTask.Result;
                    }
                    if (dbResponse != null)
                    {
                        TempData["Message"] = dbResponse.Message;
                        TempData["MessageType"] = dbResponse.MessageType;
                        if (dbResponse.MessageType == "ERROR")
                        {
                            return View();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                fileLogger.Error(ex, "Error on post DeleteRolemenu() funcion in RoleMenuController.cs -->Web..");
                databaseLogger.Error(ex, "Error on  post DeleteRolemenu() funcion in RoleMenuController.cs -->Web..");
                throw ex;
            }
            return RedirectToAction("Index", "RoleMenu");
        }
        #endregion

        #region MenuLayout2        
        public ActionResult MenuLayout2()
        {
            fileLogger.Info("Inside MenuLayout2() function in RoleMenuController.cs -->Web.");
            List<RoleMenu> rObj = new List<RoleMenu>();
            using (HttpClient hObj = pObj.Client())
            {
                int id = Convert.ToInt32(Session["RoleId"]);

                var responseTask = hObj.GetAsync("RoleMenu/GetRoleMenus/" + id);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<RoleMenu>>();
                    readTask.Wait();

                    rObj = readTask.Result;
                }

            }
            ViewBag.MenuList = rObj;
            return PartialView("MenuLayout2");
        }
        #endregion
    }
}