﻿using Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PortKart_WEB.Models
{
    public class UserProfileModel
    {
        public int UserId { get; set; }
        public int CompanyId { get; set; }

        [Display(Name = "Company Name")]
        [Required(ErrorMessage = "Company name required")]
        public string CompanyName { get; set; }

        [Display(Name = "Company RegNo")]
        [Required(ErrorMessage = "RegNo required")]
        [RegularExpression(@"\d{3}", ErrorMessage = "Please Enter 3 Digit Company RegNo.")]        
        public string CompanyRegNo { get; set; }

        [Display(Name = "Country")]
        public int? CountryId { get; set; }
        public string CountryName { get; set; }
        [Display(Name = "Industry Type")]
        
        public int? IndustryTypeId { get; set; }
        public string IndustryTypeName { get; set; }
        [Display(Name = "Expiry Date")]
        [Required(ErrorMessage = "Expiry Date required")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        public DateTime? ExpiryDate { get; set; }
        public string City { get; set; }
        public string Website { get; set; }

        [Display(Name = "Email Id")]
        [Required(ErrorMessage = "EmailID required")]
        [StringLength(254, ErrorMessage = "Max length of Email Address field should be less than or equal to 254 characters.")]
        [EmailAddress(ErrorMessage = "Invalid Email Address.")]
        public string EmailID { get; set; }

        public string Address { get; set; }
        [Display(Name = "Country Code")]
        [StringLength(10, ErrorMessage = "Max length of Country Code field should be less than or equal to 10 characters.")]
        [RegularExpression(@"^(\+?\d{1,3}|\d{1,4})$", ErrorMessage = "Invalid Country Code.")]
        public string CountryCode { get; set; }
        [Display(Name = "Contact Number")]
        [Required(ErrorMessage = "Contact Number required")]
        [StringLength(15, ErrorMessage = "Max length of Mobile Number field should be less than or equal to 15 characters.")]
        [RegularExpression(@"^([0-9]{0,20})$", ErrorMessage = "Please Enter Valid Mobile Number.")]
        public string Phone { get; set; }
        public string LogoPath { get; set; }
        public string AttachmentPath { get; set; }
        public string About { get; set; }

        public byte[] Logo { get; set; }

        public byte[] Attachment { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

        private List<ProfileSocialMedia> _SocialMediaList = new List<ProfileSocialMedia>();
        public List<ProfileSocialMedia> SocialMediaList { get { return _SocialMediaList; } set { _SocialMediaList = value; } }

        private List<ContactPerson> _ContactPersonList = new List<ContactPerson>();
        public List<ContactPerson> ContactPersonList { get { return _ContactPersonList; } set { _ContactPersonList = value; } }

        private List<CompanyBranch> _CompanyBranchList = new List<CompanyBranch>();
        public List<CompanyBranch> CompanyBranchList { get { return _CompanyBranchList; } set { _CompanyBranchList = value; } }

        public string LogoBase64 { get; set; }
        public string LogoContentType{ get; set; }
        public string LogoFilename { get; set; }
        public string LogoFormat{ get; set; }


        public string AttachBase64 { get; set; }
        public string AttachContentType { get; set; }
        public string AttachFilename { get; set; }
        public string AttachFormat { get; set; }
    }
}