﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PortKart_WEB.Models
{
    public class VesselPortModel
    {

        public int PortId { get; set; }

        [Required(ErrorMessage = "Code Required.")]
        public string Code { get; set; }

        [Required(ErrorMessage = "Port Required.")]
        public string Port { get; set; }

       
        public string Country { get; set; }

       
        public string Airport { get; set; }

        [Display(Name = "UU/LCODE")]
        
        public string Un_Locode { get; set; }

        [Display(Name = "Time Zone")]
        [RegularExpression(@"^(?:Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])$", ErrorMessage = "Please Enter Valid Time Zone.")]
        public string TimeZone { get; set; }

       
        public decimal? Latitude { get; set; }

        [Display(Name = "Latitude Degree")]
      
        public decimal? LatitudeDegree { get; set; }

        [Display(Name = "Latitude Minutes")]
        
        public decimal? LatitudeMinutes { get; set; }

        [Display(Name = "Latitude Direction")]
    
        public string LatitudeDirection { get; set; }

      
        public decimal? Longitude { get; set; }

        [Display(Name = "Longitude Degree")]
    
        public decimal? LongitudeDegree { get; set; }

        [Display(Name = "Longitude Minutes")]
   
        public decimal? LongitudeMinutes { get; set; }

        [Display(Name = "Longitude Direction")]
        //[Required(ErrorMessage = "Longitude Direction Required.")]
        public string LongitudeDirection { get; set; }
       
        public int CreatedBy { get; set; }
       
        public DateTime CreatedDate { get; set; }
      
        public int ModifiedBy { get; set; }
      
        public DateTime ModifiedDate { get; set; }

        [Display(Name = "Status")]
        public bool IsActive { get; set; }

        [Required(ErrorMessage = "Please Select a country")]
        [Display(Name = "Country")]
        public int CountryId { get; set; }

        public string CountryName { get; set; }
        //for datatable
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
    }
}