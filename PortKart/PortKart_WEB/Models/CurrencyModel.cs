﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PortKart_WEB.Models
{
    public class CurrencyModel
    {
        public int CurrencyId { get; set; }

        [Display(Name = "Currency Code")]
        [Required(ErrorMessage = "Please enter Currency Code.")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Currency Code accept only alphabets")]
        public string Code { get; set; }

        [Display(Name = "Currency Name")]
        [Required(ErrorMessage = "Please enter Currency Name.")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Currency Name accepts only characters")]
        public string Currency { get; set; }

        [Display(Name = "Status")]
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedDateAsString { get; set; }
    }
}