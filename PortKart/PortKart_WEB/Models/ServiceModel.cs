﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PortKart_WEB.Models
{
    public class ServiceModel
    {
        public int ServiceId { get; set; }

        [Display(Name = "Service Code")]
        [Required(ErrorMessage = "Please enter Service Code")]
        [RegularExpression(@"\d{3}", ErrorMessage = "Please Enter 3 Digit Service Code.")]
        public string ServiceCode { get; set; }

        [Display(Name = "Service Name")]
        [Required(ErrorMessage = "Please enter Service Name")]
        public string ServiceName { get; set; }

        [Display(Name = "Status")]
        public bool IsActive { get; set; }


        public List<SelectListItem> ServiceCategoryIds { get; set; }

        [Display(Name = "Service Category")]
        [Required(ErrorMessage = "Please enter Service Category")]
        public int[] ServiceCategoryId
        {
            get; set;
        }

        public string ServiceCategoryName { get; set; }

        [Display(Name = "Groups")]
        public bool IsGroup { get; set; }

        [Display(Name = "Cost Type")]
        public string CostType { get; set; }

        public string Description { get; set; }
        public int CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public int ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }

        //for datatable
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }

        public string ModifiedDateAsString { get; set; }
        public int SubServiceCount { get; set; }

    }
}