﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PortKart_WEB.Models
{
    public class CountryModel
    {
        public int CountryId { get; set; }

        [Display(Name = "Country Name")]
        [Required(ErrorMessage = "Please enter Country.")]
        [RegularExpression(@"^[a-zA-Z ]+$", ErrorMessage = "Country accepts only characters")]
        public string CountryName { get; set; }
        public int CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public int ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }

        [Display(Name = "Status")]
        public bool IsActive { get; set; }

    }
}
