﻿using Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PortKart_WEB.Models
{
    public class OwnerTemplateModel
    {
        public int OwnerTemplateId { get; set; }

        [Required(ErrorMessage = "Please Select Owner")]
        [Display(Name = "Owner")]
        public int UserId { get; set; }

        public string Owner { get; set; }
        public int OwnerId { get; set; }

        [Required(ErrorMessage = "Please Select Template")]

        [Display(Name = "Template")]
        public int TemplateId { get; set; }

        public string TemplateName { get; set; }

       

       
        [Display(Name = "Port")]
        public string[] PortId { get; set; }

        public string Port { get; set; }

        [Display(Name = "Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        public DateTime StartDate { get; set; }



        [Display(Name = "End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        public DateTime? EndDate { get; set; }

        public string StartDateAsString { get; set; }
        public string EndDateAsString { get; set; }

        [Display(Name = "Status")]
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

        private List<VesselPort> _PortList = new List<VesselPort>();
        public List<VesselPort> PortList { get { return _PortList; } set { _PortList = value; } }
    }
}