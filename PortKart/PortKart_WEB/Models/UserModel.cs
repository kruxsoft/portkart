﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PortKart_WEB.Models
{
    public class UserModel
    {
        public int UserId { get; set; }


        [Display(Name = "First Name")]
        [Required(ErrorMessage = "Please enter First Name.")]
        [StringLength(50, ErrorMessage = "Max length of First Name field should be less than or equal to 50 characters.")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Please enter Last Name.")]
        [StringLength(50, ErrorMessage = "Max length of Last Name field should be less than or equal to 50 characters.")]
        public string LastName { get; set; }

        public string UserGroup { get; set; }

        [Display(Name = "Password")]
        [Required(ErrorMessage = "Please enter Password.")]
        [RegularExpression(@"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?!.* )(?=.*?[#?!@('@')$%^&*-]).{8,}$", ErrorMessage = "Invalid password")]
        public string Password { get; set; }


        [Display(Name = "Confirm Password")]
        [Required(ErrorMessage = "Please enter Confirm Password.")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Email Id")]
        [Required(ErrorMessage = "Please enter Email ID.")]
        [StringLength(254, ErrorMessage = "Max length of Email Address field should be less than or equal to 254 characters.")]
        [EmailAddress(ErrorMessage = "Invalid Email Address.")]
        public string EmailId { get; set; }

        [Display(Name = "Date of Inception")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        public DateTime? DateOfBirth { get; set; }

        [Display(Name = "Company Reg. No.")]
        [RegularExpression(@"\d{3}", ErrorMessage = "Please enter 3 digit Company Reg. No.")]
        public string CompanyRegNo { get; set; }

        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Display(Name = "Contact Number")]
        [StringLength(15, ErrorMessage = "Max length of Mobile Number field should be less than or equal to 15 characters.")]
        [RegularExpression(@"^([0-9]{0,15})$", ErrorMessage = "Please Enter Valid Mobile Number.")]
        public string MobileNumber { get; set; }

        [Display(Name = "Country Code")]
        [StringLength(10, ErrorMessage = "Max length of Country Code field should be less than or equal to 10 characters.")]
        [RegularExpression(@"^(\+?\d{1,3}|\d{1,4})$", ErrorMessage = "Invalid Country Code.")]
        public string CountryCode { get; set; }

      
        public int ModifiedBy { get; set; }
        public string RoleName { get; set; }
        public int CreatedBy { get; set; }
        [Display(Name = "Status")]
        public int UserStatusId { get; set; }
        public string UserStatus { get; set; }

        //for datatable
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
    }
}