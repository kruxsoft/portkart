﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PortKart_WEB.Models
{
    public class SubServiceModel
    {
        public int ParentServiceId { get; set; }

        [Display(Name = "Option")]
        public string Option { get; set; }

        [Display(Name = "Service")]
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public int Sort { get; set; }
      
        public int CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public int ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }
      

      
    }
}