﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PortKart_WEB.Models
{
    public class TemplateModel
    {
        public int TemplateId { get; set; }
        [Required(ErrorMessage = "Invalid Template Name.")]
        public string TemplateName { get; set; }
        public int AgencyId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

        private List<TemplateDetailsModel> _TemplateDetail = new List<TemplateDetailsModel>();
        public List<TemplateDetailsModel> TemplateDetail { get { return _TemplateDetail; } set { _TemplateDetail = value; } }

    }
}