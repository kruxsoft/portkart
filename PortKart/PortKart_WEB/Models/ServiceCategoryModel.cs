﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PortKart_WEB.Models
{
    public class ServiceCategoryModel
    {
        public int ServiceCategoryId { get; set; }

        [Display(Name = "Service Category Code")]
        [Required(ErrorMessage = "Service Category Code.")]
        [RegularExpression(@"\d{3}", ErrorMessage = "Please Enter 3 Digit Service Category Code.")]
        public string ServiceCategoryCode { get; set; }

        [Display(Name = "Service Category Name")]
        [Required(ErrorMessage = "Invalid Service Category Name.")]
        public string ServiceCategoryName { get; set; }

        [Display(Name = "Status")]
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedDateAsString { get; set; }

        //for datatable
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
    }
}