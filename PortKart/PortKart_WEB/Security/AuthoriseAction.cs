﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PortKart_WEB.Security
{
    public class AuthoriseAction : AuthorizeAttribute, IAuthorizationFilter
    {
        #region Global Variables
        public string Group { get; set; }
        public string Role { get; set; }
        #endregion

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true)
                || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true))
            {
                // Don't check for authorization as AllowAnonymous filter is applied to the action or controller
                return;
            }

            // Check for authorization
            if (HttpContext.Current.Session["UserGroup"] != null)
            {
                if(!string.IsNullOrEmpty(Group))
                {
                    //var Grouplist= Group.Split
                    if (HttpContext.Current.Session["UserGroup"].ToString() != Group)
                    {
                        filterContext.Result = new RedirectResult("~/Forbidden/Index");
                    }
                }
                

            }
            else
            {
                filterContext.Result = new RedirectResult("~/Login/Index");
            }
        }
    }

}