﻿#region Headers
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Domain;
#endregion

namespace PortKart_WEB.Util
{
    public class CommonMethods
    {

        #region BindRole
        public List<Role> BindRole()
        {
            List<Role> roles = new List<Role>();
            using (HttpClient apiClient = Client())
            {
                var responseTask = apiClient.GetAsync("Role/GetRolesList");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<Role>>();
                    readTask.Wait();
                    roles = readTask.Result;
                }
            }
            return roles;
        }
        #endregion

        #region BindUser
        public List<User> BindUser()
        {
            List<User> users = new List<User>();
            using (HttpClient apiClient = Client())
            {
                var responseTask = apiClient.GetAsync("User/GetUsersList");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<User>>();
                    readTask.Wait();
                    users = readTask.Result;
                }
            }
            return users;
        }
        #endregion

        #region BindMenu

        public List<RoleMenu> BindMenu()
        {
            List<RoleMenu> roleMenus = new List<RoleMenu>();
            using (HttpClient apiClient = Client())
            {
                var responseTask = apiClient.GetAsync("RoleMenu/GetMenuList");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<RoleMenu>>();
                    readTask.Wait();
                    roleMenus = readTask.Result;
                }
            }
            return roleMenus;
        }

        #endregion

        #region BindState
        public List<State> BindState(int? countryId)
        {
            List<State> states = new List<State>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseURL"]);
                var responseTask = client.GetAsync("Company/GetState/" + countryId);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<State>>();
                    readTask.Wait();

                    states = readTask.Result;
                }

            }
            return states;
        }
        #endregion

        #region BindCountry
        public List<Country> BindCountry()
        {
            List<Country> country = new List<Country>();
            using (HttpClient hObj = Client())
            {
                var responseTask = hObj.GetAsync("Country/GetAllCountries");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<Country>>();
                    readTask.Wait();

                    country = readTask.Result;
                }

            }
            return country;
        }
        #endregion

        #region BaseAddress
        public string ClientBaseAddress()
        {
           string _baseAddress = "";
           return  _baseAddress= ConfigurationManager.AppSettings ["BaseURL"];
        }
        #endregion

        #region GetRoleNameByRoleId
        public string GetRoleNameByRoleId(int roleId)
        {
            string roleName ="";
            using (HttpClient hObj = Client())
            {
                var responseTask = hObj.GetAsync("Role/GetRoleNameByRoleId/" + roleId);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    roleName = readTask.Result;
                }
            }
            switch (roleName)
            {
                case "Admin":
                    roleName = "AD";
                    break;
                case "SuperAdmin":
                    roleName = "SA";
                    break;
            }
            return roleName;
        }
        #endregion

        #region Client
        public HttpClient Client()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseURL"]);
            return client;
        }
        #endregion

        #region EncMD5

        public string EncMD5(string password)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            UTF8Encoding encoder = new UTF8Encoding();
            Byte[] originalBytes = encoder.GetBytes(password);
            Byte[] encodedBytes = md5.ComputeHash(originalBytes);
            password = BitConverter.ToString(encodedBytes).Replace("-", "");
            var result = password.ToLower();
            return result;
        }

        #endregion


        #region GetRoleNameByRoleId
        public string GenerateAccessToken(string emailid, string password)
        {
            string token = "";
            try
            {
                using (HttpClient hObj = Client())
                {
                    AccessToken Tobj = new AccessToken();
                    hObj.DefaultRequestHeaders.Add("EmailId", $"{emailid}");
                    hObj.DefaultRequestHeaders.Add("Password", $"{password}");
                    var responseTask = hObj.GetAsync("/api/auth");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<AccessToken>();
                        readTask.Wait();
                        Tobj = readTask.Result;
                        token = Tobj.access_token;
                    }
                }
                return token;
            }
            catch(Exception ex)
            {
                return token;
            }
           
        }
        #endregion


        #region BindServiceCategory
        public List<ServiceCategory> BindServiceCategory()
        {
            List<ServiceCategory> sc = new List<ServiceCategory>();
            using (HttpClient apiClient = Client())
            {
                var responseTask = apiClient.GetAsync("ServiceCategory/GetServiceCategoryList");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<ServiceCategory>>();
                    readTask.Wait();
                    sc = readTask.Result;
                }
            }
            return sc;
        }
        #endregion

        #region BindOwner
        public List<User> BindOwner()
        {
            List<User> ownerTemplate = new List<User>();
            using (HttpClient apiClient = Client())
            {
                var responseTask = apiClient.GetAsync("Template/GetUserOwnerList");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<User>>();
                    readTask.Wait();
                    ownerTemplate = readTask.Result;
                }
            }
            return ownerTemplate;
        }
        #endregion



        #region BindTemplate
        public List<Template> BindTemplate()
        {
            List<Template> ownerTemplate = new List<Template>();
            using (HttpClient apiClient = Client())
            {
                var responseTask = apiClient.GetAsync("Template/GetOwnerTemplateList");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<Template>>();
                    readTask.Wait();
                    ownerTemplate = readTask.Result;
                }
            }
            return ownerTemplate;
        }
        #endregion


        #region BindVesselPort
        public List<VesselPort> BindVesselPort()
        {
            List<VesselPort> vesselPort = new List<VesselPort>();
            using (HttpClient apiClient = Client())
            {
                var responseTask = apiClient.GetAsync("Template/GetPortInOwnerTemplate");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<VesselPort>>();
                    readTask.Wait();
                    vesselPort = readTask.Result;
                }
            }
            return vesselPort;
        }
        #endregion





    }
}