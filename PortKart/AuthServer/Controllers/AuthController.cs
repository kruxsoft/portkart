﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Repository;

namespace AuthServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private IOptions<Audience> _settings;
        private IUserRepository _userRepository;
        public AuthController(IOptions<Audience> settings,IUserRepository userRepository)
        {
            _settings = settings;
            _userRepository = userRepository;
        }
        [HttpGet]
        public IActionResult Get()
        {
            List<UserRole> userlist = new List<UserRole>();
            try
            {
                var Password = EncMD5(Request.Headers["Password"]);
                userlist = _userRepository.GetUsers(Request.Headers["EmailId"], Password).ToList();
                if (userlist.Count > 0)
                {
                    var val = userlist[0];
                    if (val.RoleId == 0 || val.RoleName == null)
                    {
                        return new JsonResult("No roles assigned to the user");
                    }
                    if (val.UserStatusID == 2 )
                    {
                        return new JsonResult("User is inactive");
                    }
                    if (val.UserStatusID == 3)
                    {
                        return new JsonResult("User is suspended");
                    }
                    var now = DateTime.UtcNow;

                    var claims = new Claim[]
                    {
                    new Claim(JwtRegisteredClaimNames.Sub, Request.Headers["EmailId"]),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.Iat, now.ToUniversalTime().ToString(), ClaimValueTypes.Integer64)
                    };

                    var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_settings.Value.Secret));
                    var tokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = signingKey,
                        ValidateIssuer = true,
                        ValidIssuer = _settings.Value.Iss,
                        ValidateAudience = true,
                        ValidAudience = _settings.Value.Aud,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero,
                        RequireExpirationTime = true,

                    };

                    var jwt = new JwtSecurityToken(
                        issuer: _settings.Value.Iss,
                        audience: _settings.Value.Aud,
                        claims: claims,
                        notBefore: now,
                        expires: now.Add(TimeSpan.FromMinutes(_settings.Value.ExpireinMin)),
                        signingCredentials: new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256)
                    );
                    var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
                    var responseJson = new
                    {
                        access_token = encodedJwt,
                        expires_in = (int)TimeSpan.FromMinutes(_settings.Value.ExpireinMin).TotalSeconds
                    };

                    return new JsonResult(responseJson);

                }
                else
                {
                    return new JsonResult("Provided emailid and password is incorrect");
                   
                }

            }
            catch (Exception ex)
            {
                return new JsonResult("Unable to retrieve user data");
               
            }
          
        }
        #region EncMD5

        private string EncMD5(string password)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            UTF8Encoding encoder = new UTF8Encoding();
            Byte[] originalBytes = encoder.GetBytes(password);
            Byte[] encodedBytes = md5.ComputeHash(originalBytes);
            password = BitConverter.ToString(encodedBytes).Replace("-", "");
            var result = password.ToLower();
            return result;
        }

        #endregion

    }
    public class Audience
    {
        public string Secret { get; set; }
        public string Iss { get; set; }
        public string Aud { get; set; }
        public int ExpireinMin { get; set; }
    }
}