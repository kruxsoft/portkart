﻿DECLARE @CreatedBy smallint
		,	@ParentMenuId smallint

IF NOT EXISTS	(	SELECT	1
					FROM dbo.[tbl_Menu])
BEGIN

	
	--SELECT	@CreatedBy = UserId
	--FROM	dbo.tbl_User 
	--WHERE	USERNAME = 'sa'

	INSERT INTO [dbo].[tbl_Menu]
			   ([MenuName]
			   ,[MenuURL]
			   ,[ParentId]
			   ,[IsParent]
			   ,[HasChildren]
			   ,[MenuOrder]
			   ,[CreatedBy]
			   ,[CreatedDate]
			   ,[IsActive])
	VALUES		('User Data Management', null, null, 1, 1, 1, null, GETDATE(), 1),
				('Master Data Management', null, null, 1, 1, 2, null, GETDATE(), 1),
				('Role & Menu Management', null, null, 1, 1, 3, null, GETDATE(), 1)
				
				
	SELECT	@ParentMenuId = MenuId
	FROM	[dbo].[tbl_Menu]
	WHERE	MenuName = 'User Data Management'
	
	INSERT INTO [dbo].[tbl_Menu]
				([MenuName]
				,[MenuURL]
				,[ParentId]
				,[IsParent]
			    ,[MenuOrder]
				,[CreatedBy]
				,[CreatedDate]
				,[IsActive])
	VALUES		('User Management', 'User/Index', @ParentMenuId, 0, 4, null, GETDATE(), 1), 
				('Subscription Management', 'SubscriptionManagement/Index', @ParentMenuId, 0, 5, null, GETDATE(), 1)
				
				SELECT	@ParentMenuId = MenuId
	FROM	[dbo].[tbl_Menu]
	WHERE	MenuName = 'Role & Menu Management'
	
	INSERT INTO [dbo].[tbl_Menu]
				([MenuName]
				,[MenuURL]
				,[ParentId]
				,[IsParent]
			    ,[MenuOrder]
				,[CreatedBy]
				,[CreatedDate]
				,[IsActive])
	VALUES		('Role', 'Role/Index', @ParentMenuId, 0, 6, null, GETDATE(), 1),
				('User Role', 'UserRole/Index', @ParentMenuId, 0, 7, null, GETDATE(), 1),
                ('Role Menu', 'RoleMenu/Index',  @ParentMenuId, 0, 8, null, GETDATE(), 1)
               
	
	SELECT	@ParentMenuId = MenuId
	FROM	[dbo].[tbl_Menu]
	WHERE	MenuName = 'Master Data Management'
	
	INSERT INTO [dbo].[tbl_Menu]
				([MenuName]
				,[MenuURL]
				,[ParentId]
				,[IsParent]
			    ,[MenuOrder]
				,[CreatedBy]
				,[CreatedDate]
				,[IsActive])
	VALUES		('Service Category', 'ServiceCategory/Index', @ParentMenuId, 0, 9, null, GETDATE(), 1), 
				('Service', 'Service/Index', @ParentMenuId, 0, 10, null, GETDATE(), 1),
				('Vessel Port', 'VesselPort/Index', @ParentMenuId, 0, 11, null, GETDATE(), 1),
                ('Currency', 'Currency/Index',  @ParentMenuId, 0, 12, null, GETDATE(), 1),
				 ('Country', 'Country/Index',  @ParentMenuId, 0, 13, null, GETDATE(), 1)
END
if NOT EXISTS (Select 1 from tbl_Menu where MenuName = 'Country') 
begin



SELECT	@ParentMenuId = MenuId
	FROM	[dbo].[tbl_Menu]
	WHERE	MenuName = 'Master Data Management'
	INSERT INTO [dbo].[tbl_Menu]
				([MenuName]
				,[MenuURL]
				,[ParentId]
				,[IsParent]
			    ,[MenuOrder]
				,[CreatedBy]
				,[CreatedDate]
				,[IsActive])
	VALUES		
				 ('Country', 'Country/Index',  @ParentMenuId, 0, 13, null, GETDATE(), 1)
END
GO

Update tbl_Menu Set icon='fa fa-home' where [MenuName]='User Data Management'
Update tbl_Menu Set icon='fa fa-plug' where [MenuName]='Master Data Management'
Update tbl_Menu Set icon='fa fa-user' where [MenuName]='Role & Menu Management'
Update tbl_Menu Set icon='fa fa-address-book-o fa-ico' where [MenuName]='User Management'
Update tbl_Menu Set icon='fa fa-users fa-ico' where [MenuName]='Subscription Management'
Update tbl_Menu Set icon='fa fa-graduation-cap fa-ico' where [MenuName]='Role'
Update tbl_Menu Set icon='fa fa-puzzle-piece fa-ico' where [MenuName]='User Role'
Update tbl_Menu Set icon='fa fa-arrows fa-ico' where [MenuName]='Role Menu'
Update tbl_Menu Set icon='fa fa-server fa-ico' where [MenuName]='Service Category'
Update tbl_Menu Set icon='fa fa-cog fa-ico' where [MenuName]='Service'
Update tbl_Menu Set icon='fa fa-ship fa-ico' where [MenuName]='Vessel Port'
Update tbl_Menu Set icon='fa fa-money fa-ico' where [MenuName]='Currency'
Update tbl_Menu Set icon='fa fa-flag fa-ico' where [MenuName]='Country'