﻿

MERGE	dbo.[tbl_UserStatus] AS TARGET
USING	(	SELECT	'Active' AS [UserStatus]
			,	1 AS [UserStatusID]
			UNION
			SELECT	'Inactive' AS [UserStatus]
			,	2 AS [UserStatusID]
			UNION
			SELECT	'Suspended' AS [UserStatus]
			,	3 AS [UserStatusID]
			UNION
			SELECT	'Confirmation Pending' AS [UserStatus]
			,	4 AS [UserStatusID]
			) AS SOURCE
ON		(lower(TARGET.[UserStatus]) = lower(SOURCE.[UserStatus]))
WHEN NOT MATCHED BY TARGET
		THEN	INSERT ([UserStatus]
				   ,	[UserStatusID])
		 VALUES		(	SOURCE.[UserStatus]
		           ,	SOURCE.[UserStatusID]);
GO