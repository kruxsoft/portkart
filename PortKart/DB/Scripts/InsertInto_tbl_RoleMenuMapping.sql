﻿DECLARE @RoleID smallint
	,	@User_ID smallint
	,	@CreatedBy smallint

--SELECT	@CreatedBy = UserId
--FROM	dbo.tbl_User 
--WHERE	UserName = 'Owner'

SELECT	@RoleID = RoleId
FROM	dbo.tbl_Role 
WHERE	RoleName = 'SuperAdmin'

MERGE	dbo.tbl_RoleMenuMapping AS TARGET
USING	(	SELECT	@RoleID AS [RoleId]
			,	MenuID AS [MenuId]
		--	,	@CreatedBy AS [CreatedBy]
			,	GETDATE() AS [CreatedDate]
			FROM	dbo.tbl_Menu) AS SOURCE
ON		(	TARGET.[RoleId] = SOURCE.[RoleId]
		AND	TARGET.[MenuId] = SOURCE.[MenuId])
WHEN NOT MATCHED BY TARGET
		THEN	INSERT ([RoleId]
				   ,	[MenuId]
				--   ,	[CreatedBy]
				   ,	[CreatedDate])
		 VALUES		(	SOURCE.[RoleId]
					,	SOURCE.[MenuId]
				--	,	SOURCE.[CreatedBy]
					,	SOURCE.[CreatedDate]);

SELECT	@RoleID = RoleId
FROM	dbo.tbl_Role 
WHERE	RoleName = 'Admin'

MERGE	dbo.tbl_RoleMenuMapping AS TARGET
USING	(	SELECT	@RoleID AS [RoleId]
			,	MenuID AS [MenuId]
		--	,	@CreatedBy AS [CreatedBy]
			,	GETDATE() AS [CreatedDate]
			FROM	dbo.tbl_Menu) AS SOURCE
ON		(	TARGET.[RoleId] = SOURCE.[RoleId]
		AND	TARGET.[MenuId] = SOURCE.[MenuId])
WHEN NOT MATCHED BY TARGET
		THEN	INSERT ([RoleId]
				   ,	[MenuId]
				--   ,	[CreatedBy]
				   ,	[CreatedDate])
		 VALUES		(	SOURCE.[RoleId]
					,	SOURCE.[MenuId]
					--,	SOURCE.[CreatedBy]
					,	SOURCE.[CreatedDate]);
GO