﻿MERGE	dbo.[tbl_User] AS TARGET
USING	(	SELECT	
				'Super' AS [FirstName]
			,	'Administrator' AS [LastName]
			,	'1e0a55d2ab93ff0fec2dc379284f05b3' AS [Password]
			,   'superadmin@cetacean.com' AS [EmailId]
			,   'Owner' AS [UserGroup]
			,	1 as [EmailVerified]
			,	0 AS [CreatedBy]
			,	GETDATE() AS [CreatedDate]
			,	1 AS [UserStatusID]
			UNION
			SELECT	
				'Administrator' AS [FirstName]
			,	'' AS [LastName]
			,	'e6e061838856bf47e1de730719fb2609' AS [Password]
			,   'admin@cetacean.com' AS [EmailId]
			,   'Owner' AS [UserGroup]
			,	1 as [EmailVerified]
			,	0 AS [CreatedBy]
			,	GETDATE() AS [CreatedDate]
			,	1 AS [UserStatusID]			
			) AS SOURCE
ON		(lower(TARGET.[EmailId]) = lower(SOURCE.[EmailId]))
WHEN	MATCHED	AND (TARGET.[FirstName] <> SOURCE.[FirstName]
				OR	TARGET.[LastName] <> SOURCE.[LastName]
				OR	TARGET.[UserStatusID] <> SOURCE.[UserStatusID])
		THEN	UPDATE	
				SET		TARGET.[FirstName] = SOURCE.[FirstName]
					,	TARGET.[LastName] = SOURCE.[LastName]	
					,	TARGET.[UserStatusID] = SOURCE.[UserStatusID]
WHEN NOT MATCHED BY TARGET
		THEN	INSERT ([FirstName]
				   ,	[LastName]
				   ,	[Password]
				   ,	[EmailId]
				   ,	[UserGroup]
				   ,	[EmailVerified]
				   ,	[CreatedBy]
				   ,	[CreatedDate]
				   ,	[UserStatusID])
		 VALUES		(	SOURCE.[FirstName]
					,	SOURCE.[LastName]
					,	SOURCE.[Password]
					,	SOURCE.[EmailId]
					,	SOURCE.[UserGroup]
					,	SOURCE.[EmailVerified]
					,	SOURCE.[CreatedBy]
					,	SOURCE.[CreatedDate]
					,	SOURCE.[UserStatusID]);
GO