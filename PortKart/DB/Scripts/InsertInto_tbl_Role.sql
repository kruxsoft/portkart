﻿DECLARE @CreatedBy smallint

--SELECT	@CreatedBy = UserId
--FROM	dbo.tbl_User 
--WHERE	USERNAME = 'sa'

MERGE	dbo.[tbl_Role] AS TARGET
USING	(	SELECT	'Owner' AS [RoleName]
			--,	@CreatedBy AS [CreatedBy]
			,	GETDATE() AS [CreatedDate]
			,	1 AS [IsActive]
			UNION
			SELECT	'Agency' AS [RoleName]
			--,	@CreatedBy AS [CreatedBy]
			,	GETDATE() AS [CreatedDate]
			,	1 AS [IsActive]
			UNION
			SELECT	'SuperAdmin' AS [RoleName]
			--,	@CreatedBy AS [CreatedBy]
			,	GETDATE() AS [CreatedDate]
			,	1 AS [IsActive]
			UNION
			SELECT	'Admin' AS [RoleName]
			--,	@CreatedBy AS [CreatedBy]
			,	GETDATE() AS [CreatedDate]
			,	1 AS [IsActive]
			) AS SOURCE
ON		(lower(TARGET.[RoleName]) = lower(SOURCE.[RoleName]))
WHEN NOT MATCHED BY TARGET
		THEN	INSERT ([RoleName]
			--	   ,	[CreatedBy]
				   ,	[CreatedDate]
				   ,	[IsActive])
		 VALUES		(	SOURCE.[RoleName]
				--	,	SOURCE.[CreatedBy]
					,	SOURCE.[CreatedDate]
					,	SOURCE.[IsActive]);
GO