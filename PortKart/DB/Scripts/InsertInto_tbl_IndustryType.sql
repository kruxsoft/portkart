﻿

MERGE	dbo.[tbl_IndustryType] AS TARGET
USING	(	SELECT	'Maritime Industry' AS [IndustryTypeName]
			,Getdate() as createddate,1 as IsActive
			UNION
			SELECT	'Port Agency' AS [IndustryTypeName]
			,Getdate() as createddate,1 as IsActive
			UNION
			SELECT	'Logistics Services' AS [IndustryTypeName]
			,Getdate() as createddate,1 as IsActive
			UNION
			SELECT	'Shipping Business' AS [IndustryTypeName]
			,Getdate() as createddate,1 as IsActive
			UNION
			SELECT	'Freight' AS [IndustryTypeName]
			,Getdate() as createddate,1 as IsActive
			UNION
			SELECT	'Supply Chain' AS [IndustryTypeName]
			,Getdate() as createddate,1 as IsActive
			UNION
			SELECT	'Chartering ' AS [IndustryTypeName]
			,Getdate() as createddate,1 as IsActive
			) AS SOURCE
ON		(lower(TARGET.[IndustryTypeName]) = lower(SOURCE.[IndustryTypeName]))
WHEN NOT MATCHED BY TARGET
		THEN	INSERT ([IndustryTypeName],createddate,IsActive)
		 VALUES		(	SOURCE.[IndustryTypeName],SOURCE.createddate,SOURCE.IsActive);
GO