﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
:r .\InsertInto_tbl_UserStatus.sql
:r .\InsertInto_tbl_User.sql
:r .\InsertInto_tbl_Menu.sql
:r .\InsertInto_tbl_Role.sql
:r .\InsertInto_tbl_RoleMenuMapping.sql
:r .\InsertInto_tbl_UserRoleMapping.sql
:r .\InsertInto_tbl_IndustryType.sql