﻿IF NOT EXISTS	(	SELECT	1
					FROM dbo.[tbl_UserRoleMapping])
BEGIN
	DECLARE @UserID smallint
		,	@CreatedBy smallint

	SELECT	@CreatedBy = 0

	SELECT	@UserID = UserId
	FROM	dbo.tbl_User 
	WHERE	lower(FirstName) = 'Super'
	AND     lower(LastName)= 'Administrator'

	INSERT INTO dbo.tbl_UserRoleMapping
	SELECT	@UserID, RoleId, @CreatedBy, GETDATE()
	FROM	dbo.tbl_Role 
	WHERE	RoleName = 'SuperAdmin'

	SELECT	@UserID = UserId
	FROM	dbo.tbl_User 
	WHERE	lower(FirstName) = 'Administrator'
	AND     lower(LastName)= ''

	INSERT INTO dbo.tbl_UserRoleMapping
	SELECT	@UserID, RoleId, @CreatedBy, GETDATE()
	FROM	dbo.tbl_Role 
	WHERE	RoleName = 'Admin'
END
GO