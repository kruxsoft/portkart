﻿CREATE PROCEDURE [dbo].[procSelectTemplateList]
@search varchar(500)
	AS
BEGIN
Declare @limit varchar(100)
SET @limit='';
IF(Select LEN(isnull(@search,'')))<2
BEGIN
SET @limit='Top 100'
END
	EXEC('SELECT '+@limit+'	T.TemplateId
			,T.TemplateName
	
			,T.IsActive
			,T.CreatedBy
			,T.CreatedDate
			,T.ModifiedBy
			,T.ModifiedDate
	FROM tbl_ServiceTemplate AS T
	
	Where T.IsActive=1
	AND (T.TemplateName like ''%'+@search+'%''  OR isnull('''+@search+''','''')='''')')

	END