﻿CREATE PROCEDURE [dbo].[procSelectUsersByRole]
	@roleName varchar(50)
AS
	SELECT	U.UserId
	,		U.FirstName
	,		U.LastName
	,		U.MobileNumber
	,		U.EmailId
	,		U.UserStatusId
	FROM tbl_User U 
    LEFT OUTER JOIN tbl_UserRoleMapping UR
		ON UR.UserId=U.UserId
	LEFT OUTER JOIN tbl_Role R 
		ON R.RoleId=UR.RoleId
	WHERE RoleName=@roleName
	AND U.UserStatusId=1