﻿CREATE PROCEDURE [dbo].[procUpdateNewPassword]
@EmailId NVARCHAR(254),
@Password nvarchar(max),
@userid int,
@UserEmailId int
AS
BEGIN
IF EXISTS(SELECT 1 FROM tbl_UserEmail WHERE UserEmailId=@UserEmailId and IsExpire=1)
BEGIN
SELECT	'ERROR' AS [MessageType], 'Link Expired' AS [Message]
return
END
	Update tbl_User SET Password=@Password WHERE UserId=@userid

	Update tbl_UserEmail SET IsExpire=1 where UserEmailId=@UserEmailId

	IF @@ROWCOUNT > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Password resetted successfully.' AS [Message]
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
		END
END
