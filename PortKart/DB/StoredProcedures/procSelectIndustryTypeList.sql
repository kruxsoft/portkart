﻿CREATE PROCEDURE [dbo].[procSelectIndustryTypeList]
AS
BEGIN
	SELECT IndustryTypeId,IndustryTypeName
	FROM tbl_IndustryType
	WHERE IsActive=1
END
