﻿CREATE PROCEDURE [dbo].[procInsertUserProfileCompanyBranch]
	@CompanyId int,
	@BranchId int,
	@CountryId int,
	@BranchName nvarchar(255),
	@Address nvarchar(1000),
	@CountryCode varchar(15),
	@Phone  varchar(18),
	@ContactPerson nvarchar(255),
	@CP_CountryCode varchar(15),
	@CP_Phone varchar(18),
	@Email nvarchar(255),
	@Website nvarchar(255),
	@IsPreview bit
	,@outBranchId int output
AS
BEGIN
DECLARE @RowCount int
IF isnull(@BranchId,0)=0
BEGIN
INSERT INTO tbl_CompanyBranches(CompanyId
            ,BranchName
			,CountryCode
			,Phone
			,CountryId
			,Address
			,ContactPerson
			,CP_CountryCode
			,CP_Phone
			,Email
			,Website
			,IsPreview)
VALUES (@CompanyId
            ,@BranchName
			,@CountryCode
			,@Phone
			,@CountryId
			,@Address
			,@ContactPerson
			,@CP_CountryCode
			,@CP_Phone
			,@Email
			,@Website
			,@IsPreview)
SET @RowCount=@@ROWCOUNT
SET @outBranchId=@@IDENTITY
EnD
ELSE
BEGIN
UPDATE  tbl_CompanyBranches 
SET		     CompanyId=@CompanyId
            ,BranchName=@BranchName
			,CountryCode=@CountryCode
			,Phone=@Phone
			,CountryId=@CountryId
			,Address=@Address
			,ContactPerson=@ContactPerson
			,CP_CountryCode=@CP_CountryCode
			,CP_Phone=@CP_Phone
			,Email=@Email
			,Website=@Website	
WHERE   BranchId=@BranchId
SET @RowCount=@@ROWCOUNT
SET @outBranchId=@BranchId
END

IF @RowCount > 0
	BEGIN
		SELECT	'SUCCESS' AS [MessageType], 'Branch details saved successfully.' AS [Message]			
	END
	ELSE
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
	END
END