﻿
CREATE PROCEDURE [dbo].[procSelectCurrencyList]
    AS
BEGIN
    SET NOCOUNT ON;
    SELECT    CurrencyId
    ,        Code
    ,        Currency
    ,        ModifiedBy
    ,        IsActive
    FROM    tbl_Currency
    WHERE IsActive=1
END