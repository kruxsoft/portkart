﻿CREATE PROCEDURE [dbo].[procSelectAllRoleMenu]
AS
BEGIN
	SET NOCOUNT ON;
	SELECT	RMM.RoleId
		,	RMM.MenuId
		,	RMM.CreatedBy
		,	RMM.CreatedDate
		,	M.MenuName
		,	R.RoleName
	FROM	dbo.tbl_RoleMenuMapping AS RMM
	INNER JOIN dbo.tbl_Menu AS M
		ON	RMM.MenuId = M.MenuId
	INNER JOIN dbo.tbl_Role AS R
		ON	RMM.RoleId = R.RoleId
END
