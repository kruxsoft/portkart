﻿CREATE PROCEDURE [dbo].[procSelectSubServiceById]
	@serviceId int
	AS
BEGIN
	SET NOCOUNT ON;
	SELECT
	RMM.ServiceId
		,	RMM.ServiceCode
		,	RMM.ServiceName
		,	RMM.IsGroup
		,	RMM.CostType
		,	RMM.Description
		,	RMM.CreatedBy
		,	RMM.CreatedDate
		
		,   RMM.IsActive
		
	FROM	dbo.tbl_Service AS RMM
	 WHERE serviceId <> @serviceId
	 AND IsGroup<>1
	
END