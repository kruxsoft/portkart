﻿CREATE PROCEDURE [dbo].[procSelectUserbyEmailCode]
@EmailCode nvarchar(500),
@EmailType char(1)
AS
BEGIN
DECLARE @UserEmailId int
SELECT @UserEmailId=UserEmailID FROM tbl_UserEmail WHERE EmailCode=@EmailCode and EmailType=@EmailType and IsExpire<>1

	SELECT E.UserEmailID,E.UserId,U.EmailId
	FROM tbl_UserEmail E
	INNER JOIN tbl_user U
	ON	U.UserId=E.UserId
	WHERE E.UserEmailId=@UserEmailId
END
