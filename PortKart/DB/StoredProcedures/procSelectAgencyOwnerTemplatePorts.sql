﻿CREATE PROCEDURE [dbo].[procSelectAgencyOwnerTemplatePorts]
	@OwnerTemplateId int
AS
BEGIN

SET NOCOUNT ON;
SELECT PortId 
FROM tbl_Port_OwnerMapping
WHERE OwnerTemplateId=@OwnerTemplateId

END