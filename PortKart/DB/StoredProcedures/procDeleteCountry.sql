﻿CREATE PROCEDURE [dbo].[procDeleteCountry]

        
(
  @countryId int
  ,@modifiedBy int
)
AS
BEGIN
IF EXISTS    (SELECT    1
                 FROM    tbl_VesselPort
                 Where  CountryId= @countryId and IsActive = 1)
             
    BEGIN
        SELECT    'ERROR' AS MessageType, 'Cannot deactivate this Country' AS Message;
    END
    ELSE    
    BEGIN
    UPDATE  tbl_Country 
    SET      IsActive = '0'     
		    ,ModifiedBy = @modifiedBy
		    ,ModifiedDate = GETDATE()
    WHERE    CountryId= @countryId

 

    IF @@ROWCOUNT > 0
    BEGIN
        SELECT    'SUCCESS' AS [MessageType], 'Country deactivated successfully.' AS [Message]
    END
    ELSE
    BEGIN
        SELECT    'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
    END
    END
END
