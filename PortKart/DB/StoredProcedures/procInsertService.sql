﻿CREATE PROCEDURE [dbo].[procInsertService]
(

	@serviceCode     nvarchar(20),
	@serviceName	 nvarchar(20),
	@serviceCategoryId		xml,
	@isGroup bit,
	@costType varchar(50),
	@description nvarchar(2000),
	@createdBy		smallint	
 )
AS
BEGIN	
declare @serviceId int
declare @hdoc int 
exec sp_xml_preparedocument @hdoc output, @serviceCategoryId


	IF EXISTS	(SELECT	1
				 FROM	dbo.tbl_Service
				 WHERE	serviceCode = @serviceCode 
				   
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'Service Code already exists.' AS [Message]
		return
	END
	IF EXISTS	(SELECT	1
				 FROM	dbo.tbl_Service
				 WHERE	(ServiceName = @serviceName ) 
				   
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'Service Name already exists.' AS [Message]
	END
	ELSE
	BEGIN
		INSERT INTO tbl_Service(
				ServiceCode
			,	ServiceName
			,	ServiceCategoryId
			,   IsGroup		
			,	CostType
			,	Description
			,	CreatedBy
			,   IsActive 
			,	CreatedDate)
			VALUES	(
				@serviceCode
			,	@serviceName
			,   NULL
			,   @isGroup
			,	@costType
			,	@description
			,	@createdBy
			,   1
			,	GETDATE())

			SET @serviceId = @@IDENTITY

			Insert into tbl_Service_SCategoryMapping(ServiceId,ServiceCategoryId,CreatedBy,CreatedDate)
			Select @ServiceId,SC.ServiceCategoryId,@CreatedBy,GETDATE() from OpenXML(@hdoc, 'Servicategorylist/ServiceCategoryId')
			WITH (ServiceCategoryId int '.') SC
			--WHERE ISNULL(SC.ServiceCategoryId,0) <> 0

		IF @@ROWCOUNT > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Service created successfully.' AS [Message]
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
		END
	END
END