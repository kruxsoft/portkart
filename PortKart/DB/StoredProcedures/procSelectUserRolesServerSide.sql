﻿CREATE PROCEDURE [dbo].[procSelectUserRolesServerSide]
(
@pageSize int,
@skip int,
@sortColumn varchar(50),
@sortColumnDir varchar(10),
@searchValue varchar(100),
@FirstName varchar(100),
@LastName varchar(100),
@EmailId varchar(100),
@RoleName varchar(100)
)
AS
BEGIN

DECLARE @recordsTotal int
DECLARE @recordsFiltered int
DECLARE @sqlquery varchar(5000)
---------------getting total count-------------------------------------------------
	SELECT     @recordsTotal=Count(*)
	FROM	dbo.tbl_UserRoleMapping AS URM 
	INNER JOIN dbo.tbl_User AS U 
		ON	URM.UserId = U.UserId
	INNER JOIN dbo.tbl_Role AS R 
		ON	URM.RoleId = R.RoleId	
	
------------------------------------------------------------------------------------------------------------------------
IF isnull(@searchValue,'')=''
BEGIN
    SELECT	   @recordsFiltered=COUNT(*)
	FROM	   dbo.tbl_UserRoleMapping AS URM 
	INNER JOIN dbo.tbl_User AS U 
	ON		   URM.UserId = U.UserId
	INNER JOIN dbo.tbl_Role AS R 
	ON		   URM.RoleId = R.RoleId	
	WHERE     (U.FirstName like '%'+@FirstName+'%' OR isnull(@FirstName,'')='')
	AND		  (U.LastName like '%'+@LastName+'%' OR isnull(@LastName,'')='')
	AND		  (U.EmailId like '%'+@EmailId+'%' OR isnull(@EmailId,'')='')
	AND       (R.RoleName like '%'+@RoleName+'%' OR isnull(@RoleName,'')='')
	

	SET @sqlquery='SELECT	 U.FirstName, U.LastName, U.EmailId
		,	U.MobileNumber, R.RoleName, URM.RoleId
		,	R.RoleId, URM.UserId, U.UserId
		,   '+CONVERT(VARCHAR,@recordsTotal)+' as recordsTotal
		,   '+CONVERT(VARCHAR,@recordsFiltered)+' as recordsFiltered
	FROM	dbo.tbl_UserRoleMapping AS URM 
	INNER JOIN dbo.tbl_User AS U 
		ON	URM.UserId = U.UserId
	INNER JOIN dbo.tbl_Role AS R 
		ON	URM.RoleId = R.RoleId	
	WHERE       (U.FirstName like''%'+CONVERT(VARCHAR,@FirstName)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@FirstName,''))+''','''')='''')
	AND       (U.LastName like ''%'+CONVERT(VARCHAR,@LastName)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@LastName,''))+''','''')='''')
	AND       (U.EmailId like ''%'+CONVERT(VARCHAR,@EmailId)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@EmailId,''))+''','''')='''')
	AND       (R.RoleName like ''%'+CONVERT(VARCHAR,@RoleName)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@RoleName,''))+''','''')='''')
	ORDER BY  (CASE '''+CONVERT(VARCHAR,@sortColumn)+''' 
	                           WHEN ''FirstName'' THEN U.FirstName 
							   WHEN ''LastName'' THEN U.LastName 
							   WHEN ''EmailId'' THEN U.EmailId 
							   WHEN ''RoleName'' THEN R.RoleName							
							   ELSE  U.FirstName  END) '+ @sortColumnDir +'
	OFFSET     '+CONVERT(varchar,@skip)+' ROWS
	FETCH NEXT '+CONVERT(varchar,@pagesize)+' ROWS ONLY OPTION (RECOMPILE)'

			
exec (@sqlquery)
END
ELSE
BEGIN
	SELECT	   @recordsFiltered=COUNT(*)
	FROM	   dbo.tbl_UserRoleMapping AS URM 
	INNER JOIN dbo.tbl_User AS U 
	ON		   URM.UserId = U.UserId
	INNER JOIN dbo.tbl_Role AS R 
	ON		   URM.RoleId = R.RoleId	
	WHERE     (U.FirstName like '%'+@searchValue+'%' 
	OR		  U.LastName like '%'+@searchValue+'%' 
	OR		  U.EmailId like '%'+@searchValue+'%' 
	OR        R.RoleName like '%'+@searchValue+'%')
	AND       (U.FirstName like '%'+@FirstName+'%' OR isnull(@FirstName,'')='')
	AND		  (U.LastName like '%'+@LastName+'%' OR isnull(@LastName,'')='')
	AND		  (U.EmailId like '%'+@EmailId+'%' OR isnull(@EmailId,'')='')
	AND       (R.RoleName like '%'+@RoleName+'%' OR isnull(@RoleName,'')='')
	


		SET @sqlquery='SELECT	 U.FirstName, U.LastName, U.EmailId
		,	U.MobileNumber, R.RoleName, URM.RoleId
		,	R.RoleId, URM.UserId, U.UserId
		,   '+CONVERT(VARCHAR,@recordsTotal)+' as recordsTotal
		,   '+CONVERT(VARCHAR,@recordsFiltered)+' as recordsFiltered
	FROM	dbo.tbl_UserRoleMapping AS URM 
	INNER JOIN dbo.tbl_User AS U 
		ON	URM.UserId = U.UserId
	INNER JOIN dbo.tbl_Role AS R 
		ON	URM.RoleId = R.RoleId	
	 WHERE     (U.FirstName like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
	 OR         U.LastName like ''%'+CONVERT(VARCHAR,@searchValue)+'%''
	 OR         U.EmailId like ''%'+CONVERT(VARCHAR,@searchValue)+'%''
	 OR         R.RoleName like ''%'+CONVERT(VARCHAR,@searchValue)+'%'') 
	AND        (U.FirstName like''%'+CONVERT(VARCHAR,@FirstName)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@FirstName,''))+''','''')='''')
	AND        (U.LastName like ''%'+CONVERT(VARCHAR,@LastName)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@LastName,''))+''','''')='''')
	AND        (U.EmailId like ''%'+CONVERT(VARCHAR,@EmailId)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@EmailId,''))+''','''')='''')
	AND        (R.RoleName like ''%'+CONVERT(VARCHAR,@RoleName)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@RoleName,''))+''','''')='''')
	ORDER BY  (CASE '''+CONVERT(VARCHAR,@sortColumn)+''' 
	                           WHEN ''FirstName'' THEN U.FirstName 
							   WHEN ''LastName'' THEN U.LastName 
							   WHEN ''EmailId'' THEN U.EmailId 
							   WHEN ''RoleName'' THEN R.RoleName							
							   ELSE  U.FirstName  END) '+ @sortColumnDir +'
	OFFSET     '+CONVERT(varchar,@skip)+' ROWS
	FETCH NEXT '+CONVERT(varchar,@pagesize)+' ROWS ONLY OPTION (RECOMPILE)'
	exec (@sqlquery)
END
END