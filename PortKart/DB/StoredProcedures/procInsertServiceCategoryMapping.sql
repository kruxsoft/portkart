﻿CREATE TYPE [dbo].[ServiceMapping] AS TABLE(
	[ServiceId] [int] NOT NULL,
	[ServiceCategoryId] [int] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NOT NULL
)
GO


CREATE procedure [dbo].[procInsertServiceCategoryMapping](@servicemapping ServiceMapping readonly)  
as  
begin  
   insert into tbl_Service_SCategoryMapping select [ServiceId],[ServiceCategoryId],[CreatedBy],[CreatedDate] from @servicemapping  
end  