﻿CREATE PROCEDURE [dbo].[procSelectServiceCategoryList]
    AS
BEGIN
    SET NOCOUNT ON;
    SELECT    ServiceCategoryId
    ,        ServiceCategoryCode
    ,        ServiceCategoryName
    ,        ModifiedBy
    ,        IsActive
    FROM    tbl_ServiceCategory
    WHERE IsActive=1
END