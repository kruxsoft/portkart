﻿CREATE PROCEDURE [dbo].[procSelectSubServiceListForOwner]
AS
BEGIN
	SET NOCOUNT ON;
	SELECT	SS.ServiceId
		,	SS.ParentServiceId
		,	S.ServiceName
		,	SS.[Option] as [Option]
		,	SS.Sort
		,	SC.ServiceCategoryName
		,   S.IsActive
	FROM	dbo.tbl_SubService AS SS
	INNER JOIN dbo.tbl_Service AS S
		ON	SS.ServiceId = S.ServiceId
	INNER JOIN dbo.tbl_ServiceCategory AS SC
		ON	SC.ServiceCategoryId = S.ServiceCategoryId
	Order by SS.ParentServiceId,SS.Sort
END
