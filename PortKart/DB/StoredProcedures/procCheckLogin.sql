/****** Object:  StoredProcedure [dbo].[procCheckLogin]    Script Date: 25-10-2019 12:13:03 ******/
-- ===========================================================================
--	Author:		Anurag P.G
--	Create date: 08-12-2020
--	Description:	To check user exists
-- ===========================================================================
CREATE  PROCEDURE [dbo].[procCheckLogin]
    @emailid VARCHAR(50),
    @password varchar(50)
AS
BEGIN
SET NOCOUNT ON
DECLARE @UserId smallint
SET @UserId = (	SELECT	UserId 
				FROM	[dbo].[tbl_User] AS U 
				WHERE	U.EmailId = @emailid 
					AND U.Password = @Password
					--AND [UserStatusID] not in (2,3)
			  )
IF (@UserId >= 1)
    SELECT	UR.RoleId, RoleName, U.UserId, U.EmailId
		,	U.Password, U.FirstName, U.LastName,U.UserGroup,U.UserStatusId,US.UserStatus,U.EmailVerified,M.CompanyId
	FROM	[dbo].[tbl_User] AS U 
	INNER JOIN [dbo].[tbl_UserStatus] US
		ON	US.UserStatusId=U.UserStatusId
	LEFT OUTER JOIN [dbo].[tbl_UserRoleMapping] AS UR
		ON	U.UserId = UR.UserId
	LEFT OUTER JOIN [dbo].[tbl_Role] AS R
		ON	R.RoleId = UR.RoleId
		LEFT OUTER JOIN [dbo].[tbl_UserCompanyMapping] AS M
		ON	M.UserId = U.UserId
	WHERE U.UserId = @UserId
		--AND U.[UserStatusID] not in (2,3)
ELSE
    RETURN '0'
END