﻿create PROCEDURE [dbo].[procSelectVesselPortListMapping] 
@search varchar(500)
	AS
BEGIN
Declare @limit varchar(100)
SET @limit='';
IF(Select LEN(isnull(@search,'')))<2
BEGIN
SET @limit='Top 100'
END
	EXEC('SELECT '+@limit+'	VP.PortId
			,VP.Code
			,VP.Port 
			,VP.Airport 
			,VP.Un_Locode 
			,VP.TimeZone 
			,VP.Latitude 
			,VP.LatitudeDegree 
			,VP.LatitudeMinutes 
			,VP.LatitudeDirection 
			,VP.Longitude
			,VP.LongitudeDegree 
			,VP.LongitudeMinutes 
			,VP.LongitudeDirection 
			,VP.IsActive
			,VP.CreatedBy
			,VP.CreatedDate
			,VP.ModifiedBy
			,VP.ModifiedDate
			
	FROM tbl_VesselPort AS VP
	 
		
	Where VP.IsActive=1
	AND (VP.Port like ''%'+@search+'%''  OR isnull('''+@search+''','''')='''')')

	
END
