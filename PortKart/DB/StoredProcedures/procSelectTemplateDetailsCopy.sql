﻿CREATE PROCEDURE [dbo].[procSelectTemplateDetailsCopy]
	

@AgencyId int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT DISTINCT	AC.AgencyDetailId
		,	AC.AgencyServiceId
		,	AC.ServiceId
		,	AC.IsParent
		,	AC.CurrencyId
		,	AC.Amount
		,	AC.ActualAmount
		,	AC.ParentId
		,	AC.CurrencyId
		,	C.Currency
		,	C.Code
		,	S.ServiceCode
		,	S.ServiceName
		,	S.CostType
		,	AC.Description
		,	isnull(AC.Tat,'')Tat
		,	SS.[Option]
		,   SC.ServiceCategoryName
	FROM	dbo.tbl_AgencyServiceDetails AS AC
	INNER JOIN dbo.tbl_Service AS S
		ON	S.ServiceId = AC.ServiceId
	INNER JOIN (SELECT STUFF((SELECT ',' + ServiceCategoryName
								FROM tbl_Service EE
								INNER JOIN tbl_Service_SCategoryMapping M
								ON M.ServiceId=EE.ServiceId
								Inner join tbl_ServiceCategory C
								ON C.ServiceCategoryId=M.ServiceCategoryId
								WHERE EE.ServiceId=E.ServiceId								
								FOR XML PATH('')), 1, 1, '') AS ServiceCategoryName,E.ServiceId
				FROM tbl_Service E
				INNER JOIN tbl_Service_SCategoryMapping EM
				ON EM.ServiceId=E.ServiceId
				Inner join tbl_ServiceCategory EC
				ON EC.ServiceCategoryId=EM.ServiceCategoryId				
				GROUP BY E.ServiceId )SC
	ON AC.ServiceId=Sc.ServiceId
	Left JOIN dbo.tbl_SubService AS SS
		ON	SS.ServiceId = AC.ServiceId
		AND SS.ParentServiceId=AC.ParentId
		AND (AC.IsParent<>1 AND isnull(AC.ParentId,0)>0)
	INNER JOIN dbo.tbl_Currency AS C
		ON	C.CurrencyId = AC.CurrencyId
	WHERE AC.AgencyServiceId in (SELECT DISTINCT AgencyServiceId FROM tbl_AgencyService WHERE AgencyId=@AgencyId)

END