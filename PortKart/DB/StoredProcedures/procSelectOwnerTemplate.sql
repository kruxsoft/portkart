﻿create PROCEDURE [dbo].[procSelectOwnerTemplate]
AS
BEGIN
	SELECT	TemplateId
	,		TemplateName 
	,       IsActive
	FROM tbl_ServiceTemplate
	where IsActive = 1
END