﻿CREATE PROCEDURE [dbo].[procDeleteBranch]
	@UserId int = 0,
	@BranchId int
AS
Begin
	Delete CP
		FROM tbl_CompanyBranches CP
		INNER JOIN tbl_UserCompanyMapping M
	    ON CP.CompanyId=M.CompanyId
	    WHERE M.UserId=@UserId
		AND CP.BranchId =@BranchId

 IF @@ROWCOUNT > 0
    BEGIN
        SELECT    'SUCCESS' AS [MessageType], 'Branch deleted successfully.' AS [Message]
    END
    ELSE
    BEGIN
        SELECT    'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
    END
END
