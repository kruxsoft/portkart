-- =============================================
-- Author:		Anurag P.G
-- Create date: 08-12-2020
-- Description:	To add new user
-- =============================================
CREATE PROCEDURE [dbo].[procInsertUser]
(
	@firstName		NVARCHAR(50),
	@lastName		NVARCHAR(50),
	@password		VARCHAR(MAX),
	@emailId		NVARCHAR(254),
	@mobileNumber	VARCHAR(20),
	@companyname	NVARCHAR(150),
	@companyregno	VARCHAR(50),
	@UserGroup	    VARCHAR(20),
	@countrycode    VARCHAR(10),
	@DateOfBirth    datetime,
	@createdBy		INT,
	@UserStatusID	TinyInt
 )

AS
BEGIN	
	DECLARE @userId INT,@RowCount INT,@CompanyId int

	IF isnull(@companyname,'')=''  AND isnull(@companyregno,'')<>''
	BEGIN
	SELECT	'ERROR' AS [MessageType], 'Company name cannot be empty when Company Reg. No. is given' AS [Message]
	return
	END

	IF EXISTS	(SELECT	1
				 FROM	dbo.tbl_Company
				 WHERE	lower(CompanyName) = lower(@companyname) 
				 
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'Company already exists.' AS [Message]
		return
	END

	IF EXISTS	(SELECT	1
				 FROM	dbo.tbl_User
				 WHERE	lower(emailid) = lower(@emailId)
				 --OR EmailId = @emailId OR MobileNumber = @mobileNumber  
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'User with that EmailID  already exists.' AS [Message]
	END
	ELSE
	BEGIN

		INSERT INTO tbl_User	(
						FirstName
					,	LastName
					,	Password
					,	EmailId
					,	MobileNumber
					,	CountryCode
					,	CompanyName
					,	CompanyRegNo
					,	UserGroup
					,	DateOfBirth
					, 	CreatedBy
					,	CreatedDate
					,	UserStatusID	
					,	EmailVerified)
					VALUES	(	
						@firstName
					,	@lastName	
					,	@password
					,	@emailId
					,	@mobileNumber
					,	@countrycode
					,	@companyname
					,	@companyregno
					,	@UserGroup
					,	@DateOfBirth
					,	@createdBy
					,	GETDATE()
					,	4	
					,	0)

		SET @userId=@@IDENTITY

		SET @RowCount=@@ROWCOUNT

		
		INSERT INTO dbo.tbl_UserRoleMapping
		SELECT	@userId, RoleId, null, GETDATE()
		FROM	dbo.tbl_Role 
		WHERE	lower(RoleName) = lower(@UserGroup)

		IF isnull(@companyname,'')<>''
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM tbl_Company where CompanyName=@companyname )
		BEGIN
			INSERT INTO tbl_Company(CompanyName
								,	CompanyRegNo
									,Phone
									,CountryCode
								,	CreatedBy
								,	CreatedDate
								,	IsActive)
			VALUES				   (@companyname
								,	@companyregno
								,	@mobileNumber
								,	@countrycode
								,	@createdBy
								,	GETDATE()
								,	1)
			SET @CompanyId=@@IDENTITY

			INSERT INTO tbl_UserCompanyMapping(UserId,CompanyId,CreatedBy,CreatedDate)
			SELECT @userId,@CompanyId,@createdBy,GETDATE()
		END
	
	END
	
	IF @RowCount > 0
	BEGIN
		SELECT	'SUCCESS' AS [MessageType], 'User registered successfully.' AS [Message]			
	END
	ELSE
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
	END
	END
END
