﻿CREATE PROCEDURE [dbo].[procInsertAgencyServiceTemplateDetails](
	@TemplateId int,
	@ServiceId int,
	@CurrencyId int,
	@Offer int,
	@IsParent bit,
	@ParentId int,
	@CreatedBy int,
	@Type nvarchar(100)
	)
AS
BEGIN
	
	INSERT INTO tbl_ServiceTemplateDetails(TemplateId
										,ServiceId
										,CurrencyId
										,Offer
										,IsParent
										,ParentID
										,CreatedBy
										,CreatedDate
										,Type
										)
							VALUES (@TemplateId
							,@ServiceId
							,@CurrencyId
							,@Offer
							,@IsParent
							,@ParentId
							,@CreatedBy
							,getdate()
							,@Type)
	IF @@ROWCOUNT > 0
	BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Agency service template saved successfully.' AS [Message]			
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the service template details were not saved.' AS [Message]
	END
END