-- =============================================
-- Author:		Simi Simon
-- Create date: 29-10-2019
-- Description:	To flag a Role Menu Mapping
-- =============================================
CREATE PROCEDURE [dbo].[procDeleteRolemenu]
(
	@RoleId smallint
,	@MenuId smallint
)
AS
BEGIN	
	DELETE FROM  tbl_RoleMenuMapping 
	WHERE	RoleId = @RoleId
		AND MenuId = @MenuId
 IF (@@ROWCOUNT > 0)
	BEGIN
		SELECT	'SUCCESS' AS [MessageType], 'Role Menu deleted successfully.' AS [Message]
	END
	ELSE
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
	END
END
