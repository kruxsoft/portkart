﻿CREATE PROCEDURE [dbo].[procDeleteSubService]
(
	@parentServiceID int,
    @serviceId        int,    
    @CreatedBy        smallint
 )
AS
BEGIN    

If EXISTS (SELECT 1 from tbl_AgencyServiceDetails  where ServiceId=@serviceId AND ParentId=@parentServiceID)
BEGIN
    SELECT	'ERROR' AS [MessageType], 'Cannot remove this sub service, it is added against a port' AS [Message]
    return
END
 
 DELETE tbl_SubService
 WHERE (ServiceId=@serviceId AND ParentServiceId=@parentServiceID)

 UPDATE tbl_Service
 SET ModifiedBy=@createdBy,
  ModifiedDate=GETDATE()
  WHERE ServiceId=@parentServiceID

 

        IF @@ROWCOUNT > 0
        BEGIN
            SELECT    'SUCCESS' AS [MessageType], 'Sub Service deleted successfully.' AS [Message]
        END
        ELSE
        BEGIN
            SELECT    'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
        END
    
END