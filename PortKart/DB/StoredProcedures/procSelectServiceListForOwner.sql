﻿CREATE PROCEDURE [dbo].[procSelectServiceListForOwner]
@ServiceCategoryId varchar(1000),
@ServiceId varchar(1000),
@PortId int
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @ServiceCategoryCond varchar(1000)
	DECLARE @ServiceCond varchar(1000)
	DECLARE @ServiceCategoryCond2 varchar(1000)=''
	DECLARE @ServiceCategoryCond3 varchar(1000)=''
	DECLARE @ServiceCategoryCond4 varchar(1000)=''
	DECLARE @ServiceCategoryCond5 varchar(1000)=''
	DECLARE @ServiceCategoryCond6 varchar(1000)=''
	SET @ServiceCategoryCond=''
	IF isnull(@ServiceCategoryId,'')<>''
	BEGIN
	SET @ServiceCategoryCond=' AND S.ServiceCategoryId in ('+@ServiceCategoryId+')'
	SET @ServiceCategoryCond2=' AND C.ServiceCategoryId in ('+@ServiceCategoryId+')'
	SET @ServiceCategoryCond3=' WHERE EC.ServiceCategoryId in ('+@ServiceCategoryId+')'
	SET @ServiceCategoryCond4=' AND SSC.ServiceCategoryId in ('+@ServiceCategoryId+')'
	SET @ServiceCategoryCond5=' WHERE SEC.ServiceCategoryId in ('+@ServiceCategoryId+')'
	SET @ServiceCategoryCond6=' AND FSEC.ServiceCategoryId in ('+@ServiceCategoryId+')'
	END

	SET @ServiceCond=''
	IF isnull(@ServiceCond,'')<>''
	BEGIN
	SET @ServiceCond=' AND RMM.ServiceId in ('+@ServiceId+')'
	END
	
	EXEC('SELECT	RMM.ServiceId
		,	S.ServiceName
		,	S.IsGroup
		,	S.CostType
		,   RMM.IsParent
		,   RMM.ParentId
		,   SB.[Option]
		,	SB.Sort
	FROM	dbo.tbl_AgencyServiceDetails AS RMM
	INNER JOIN dbo.tbl_Service AS S
		ON	RMM.ServiceId = S.ServiceId
	Left JOIN (SELECT  STUFF((SELECT  '','' + ServiceCategoryName
								FROM tbl_Service EE
								INNER JOIN tbl_Service_SCategoryMapping M
								ON    M.ServiceId=EE.ServiceId
								Inner join tbl_ServiceCategory C
								ON C.ServiceCategoryId=M.ServiceCategoryId
								WHERE   EE.ServiceId=E.ServiceId'+
								@ServiceCategoryCond2+
								' FOR XML PATH('''')), 1, 1, '''') AS ServiceCategoryName,E.ServiceId
				FROM tbl_Service E
				INNER JOIN tbl_Service_SCategoryMapping EM
				ON    EM.ServiceId=E.ServiceId
				Inner join tbl_ServiceCategory EC
				ON EC.ServiceCategoryId=EM.ServiceCategoryId'+
				@ServiceCategoryCond3+
				' GROUP BY E.ServiceId )SC
	ON RMM.ServiceId=Sc.ServiceId
	AND (RMM.IsParent=1 OR isnull(RMM.ParentId,0)=0)
	Left JOIN (SELECT  STUFF((SELECT  '','' + ServiceCategoryName
								FROM tbl_Service SEE
								INNER JOIN tbl_Service_SCategoryMapping SM
								ON    SM.ServiceId=SEE.ServiceId
								Inner join tbl_ServiceCategory SSC
								ON SSC.ServiceCategoryId=SM.ServiceCategoryId
								WHERE   SEE.ServiceId=SE.ServiceId'+
								@ServiceCategoryCond4+
								' FOR XML PATH('''')), 1, 1, '''') AS SubCategory,SE.ServiceId
				FROM tbl_Service SE
				INNER JOIN tbl_Service_SCategoryMapping SEM
				ON    SEM.ServiceId=SE.ServiceId
				Inner join tbl_ServiceCategory SEC
				ON SEC.ServiceCategoryId=SEM.ServiceCategoryId'+
								@ServiceCategoryCond5+
				' GROUP BY SE.ServiceId )SCS
	ON RMM.ServiceId=SCS.ServiceId
	AND (RMM.IsParent=0 AND isnull(RMM.ParentId,0)>0)
	LEFT JOIN dbo.tbl_SubService AS SB
		ON	RMM.ServiceId = SB.ServiceId
		AND (RMM.IsParent<>1 OR Isnull(RMM.ParentId,0)<>0)
	WHERE RMM.AgencyServiceId in (SELECT AgencyServiceId from tbl_AgencyService where PortId ='+@PortId+' ) '+@ServiceCond+' '+
	' AND (isnull(SC.ServiceCategoryName,'''')<>'''' OR isnull(SCS.SubCategory,'''')<>'''' OR RMM.ServiceId in (SELECT distinct FSE.ParentId from tbl_AgencyServiceDetails FSE
				INNER JOIN tbl_Service_SCategoryMapping FSEM
				ON    FSEM.ServiceId=FSE.ServiceId
				Inner join tbl_ServiceCategory FSEC
				ON FSEC.ServiceCategoryId=FSEM.ServiceCategoryId
				WHERE  FSE.ParentId<>0'+
				@ServiceCategoryCond6+
			
			') OR (RMM.ParentId in (SELECT distinct FSE.ParentId from tbl_AgencyServiceDetails FSE
				INNER JOIN tbl_Service_SCategoryMapping FSEM
				ON    FSEM.ServiceId=FSE.ServiceId
				Inner join tbl_ServiceCategory FSEC
				ON FSEC.ServiceCategoryId=FSEM.ServiceCategoryId
				WHERE  FSE.ParentId<>0 '+
				@ServiceCategoryCond6+')  AND SB.[Option]=''D'') 
				
				OR (RMM.ParentId in (SELECT distinct FSE.ServiceId from tbl_AgencyServiceDetails FSE
				INNER JOIN tbl_Service_SCategoryMapping FSEM
				ON    FSEM.ServiceId=FSE.ServiceId
				Inner join tbl_ServiceCategory FSEC
				ON FSEC.ServiceCategoryId=FSEM.ServiceCategoryId
				WHERE  FSE.IsParent=1 '+
				@ServiceCategoryCond6+
				' ))) '+
	' Group By RMM.ServiceId,S.ServiceName,S.IsGroup,S.CostType,SC.ServiceCategoryName,SC.ServiceCategoryName,scs.SubCategory,
			  RMM.IsParent,RMM.ParentId,SB.[Option],SB.Sort')
END
