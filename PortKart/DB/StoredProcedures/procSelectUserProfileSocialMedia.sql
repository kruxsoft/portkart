﻿CREATE PROCEDURE [dbo].[procSelectUserProfileSocialMedia]
@UserId int
AS
BEGIN
IF  EXISTS(SELECT 1 FROM tbl_ProfileSocialMedia S INNER JOIN tbl_UserCompanyMapping M ON S.CompanyId=M.CompanyId	WHERE M.UserId=@UserId)
BEGIN
	SELECT S.Link,S.SocialMediaId,S.Type,S.CompanyId
	FROM tbl_ProfileSocialMedia S
	INNER JOIN tbl_UserCompanyMapping M
	ON S.CompanyId=M.CompanyId
	WHERE M.UserId=@UserId
END
ELSE
BEGIN
DECLARE @CompanyId int
SELECT @CompanyId=CompanyId FROM tbl_UserCompanyMapping WHERE UserId=@UserId

	SELECT '' as Link,0 as SocialMediaId,'Facebook' as Type,isnull(@CompanyId,0)CompanyId
	UNION
	SELECT '' as Link,0 as SocialMediaId,'Twitter' as Type,isnull(@CompanyId,0)CompanyId
	UNION
	SELECT '' as Link,0 as SocialMediaId,'Blog' as Type,isnull(@CompanyId,0)CompanyId
	UNION
	SELECT '' as Link,0 as SocialMediaId,'LinkedIn' as Type,isnull(@CompanyId,0)CompanyId
	UNION
	SELECT '' as Link,0 as SocialMediaId,'Youtube' as Type,isnull(@CompanyId,0)CompanyId

END
END
