﻿CREATE PROCEDURE [dbo].[procInsertTemplate]
(

	@templateName	 nvarchar(50),
	@agencyId		int,
	@createdBy		smallint
 )
AS
BEGIN	
declare @templateId int

	IF EXISTS	(SELECT	1
				 FROM	dbo.tbl_ServiceTemplate
				 WHERE	TemplateName = @templateName 
				   
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'Template name  already exists.' AS [Message]
		return
	END
	
	ELSE
	BEGIN
		INSERT INTO tbl_ServiceTemplate(
			  TemplateName
			,   AgencyId
			,	CreatedBy
			,	CreatedDate
			,   IsActive 
			)
			VALUES	(
			@templateName
			,	@agencyId
			,	@createdBy
			,	getdate()
			,   1
			)

		IF @@ROWCOUNT > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Template created successfully.' AS [Message]
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
		END
	END
END