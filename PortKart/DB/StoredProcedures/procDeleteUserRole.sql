-- =============================================
-- Author:		Prageeth Rajan
-- Create date: 05-11-2019
-- Description:	To flag a Role
-- =============================================
CREATE PROCEDURE [dbo].[procDeleteUserRole]
(
	@UserId int
,	@RoleId smallint
)
AS
BEGIN	
	DELETE FROM  tbl_UserRoleMapping 
	WHERE	UserId = @UserId
		AND RoleId = @RoleId

   IF (@@ROWCOUNT > 0)
	BEGIN
		SELECT	'SUCCESS' AS [MessageType], 'User Role deleted successfully.' AS [Message]
	END
	ELSE
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
	END
END
