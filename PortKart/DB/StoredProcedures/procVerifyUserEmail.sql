﻿CREATE PROCEDURE [dbo].[procVerifyUserEmail]
	@EmailCode nvarchar(500)

AS
BEGIN
	DECLARE @UserID int, @UserEmailId int
	
	SELECT @UserID=UserId,@UserEmailId=UserEmailId
	FROM   tbl_UserEmail
	WHERE  EmailCode=@EmailCode
	AND    EmailType='R'
	AND    IsExpire=0

	Update tbl_User set EmailVerified=1,UserStatusId=1 where UserId=@UserID
	Update tbl_UserEmail set IsExpire=1 where UserEmailId=@UserEmailId

	IF @@ROWCOUNT > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'EmailID verified successfully.' AS [Message]
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
		END
END
