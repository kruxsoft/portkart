﻿

CREATE PROCEDURE [dbo].[procInsertCurrency]
	(
	@currency	nvarchar(255),
	@currencyCode	nvarchar(255),
	@createdBy				int,
	@modifiedBy				int	
 )
AS
BEGIN
	IF EXISTS	(SELECT	1
				 FROM	dbo.tbl_Currency
				 WHERE	Currency = @currency
				 OR Code= @currencyCode
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'Currency already exists.' AS [Message]
	END
	ELSE
	BEGIN

		INSERT INTO tbl_Currency(
			Currency
			,Code
			,IsActive
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			
			)
		VALUES	(
			@currency
		,	@currencyCode
		,	1
		,	@createdBy
		,	GETDATE()
		,	@modifiedBy
		
			)

		IF @@ROWCOUNT > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Currency created successfully.' AS [Message]
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
		END
	END
END
GO
