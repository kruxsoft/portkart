﻿CREATE PROCEDURE [dbo].[procInsertUserProfileContactPerson]
	@CompanyId int,
	@Name nvarchar(255),
	@ContactPersonId int,
	@CountryCode varchar(15),
	@PhoneNo  varchar(18),
	@IsPreview bit,
	@OutContactPersonId int output
AS
BEGIN
DECLARE @RowCount int
IF isnull(@ContactPersonId,0)=0
BEGIN
INSERT INTO tbl_ContactPerson(CompanyId,Name,CountryCode,PhoneNo,IsPreview)
VALUES (@CompanyId,isnull(@Name,''),isnull(@CountryCode,''),isnull(@PhoneNo,''),@IsPreview)
SET @RowCount=@@ROWCOUNT
SET @OutContactPersonId=@@IDENTITY
EnD
ELSE
BEGIN
UPDATE  tbl_ContactPerson 
SET		CompanyId=@CompanyId,
		Name=isnull(@Name,''),
		CountryCode=isnull(@CountryCode,''),
		PhoneNo=isnull(@PhoneNo,''),
		IsPreview=@IsPreview
WHERE   ContactPersonId=@ContactPersonId
SET @RowCount=@@ROWCOUNT
SET @OutContactPersonId=@ContactPersonId
END

IF @RowCount > 0
	BEGIN
		SELECT	'SUCCESS' AS [MessageType], 'Contact Person saved successfully.' AS [Message]			
	END
	ELSE
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
	END
END
