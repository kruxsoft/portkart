﻿CREATE PROCEDURE [dbo].[procSelectUserRoles]
	AS
BEGIN	
	SET NOCOUNT ON;
	SELECT	 U.FirstName, U.LastName, U.EmailId
		,	U.MobileNumber, R.RoleName, URM.RoleId
		,	R.RoleId, URM.UserId, U.UserId
	FROM	dbo.tbl_UserRoleMapping AS URM 
	INNER JOIN dbo.tbl_User AS U 
		ON	URM.UserId = U.UserId
	INNER JOIN dbo.tbl_Role AS R 
		ON	URM.RoleId = R.RoleId	
END