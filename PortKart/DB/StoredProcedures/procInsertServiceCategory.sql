﻿CREATE PROCEDURE [dbo].[procInsertServiceCategory]
	(
	@serviceCategoryName	varchar(100),
	@serviceCategroyCode	varchar(20),
	@createdBy				int,
	@modifiedBy				int	
 )
AS
BEGIN
	IF EXISTS	(SELECT	1
				 FROM	dbo.tbl_ServiceCategory
				 WHERE	ServiceCategoryName = @serviceCategoryName
				 OR ServiceCategoryCode= @serviceCategroyCode
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'Service Category with that name or categorycode already exists.' AS [Message]
	END
	ELSE
	BEGIN

		INSERT INTO tbl_ServiceCategory(
			ServiceCategoryName
			,ServiceCategoryCode
			,IsActive
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate)
		VALUES	(
			@serviceCategoryName
		,	@serviceCategroyCode
		,	1
		,	@createdBy
		,	GETDATE()
		,	@modifiedBy
		,	GETDATE()
			)

		IF @@ROWCOUNT > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Service Category created successfully.' AS [Message]
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
		END
	END
END
GO
