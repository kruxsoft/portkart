﻿create PROCEDURE [dbo].[procDeleteOwnerTemplate]
(
	@OwnerTemplateId int

)
AS
BEGIN	
	update tbl_OwnerTemplateMapping 
	set IsActive = '0',
	ModifiedDate = getdate()
	WHERE	OwnerTemplateId = @OwnerTemplateId
		
 IF (@@ROWCOUNT > 0)
	BEGIN
		SELECT	'SUCCESS' AS [MessageType], 'Owner Template deleted successfully.' AS [Message]
	END
	ELSE
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
	END
END
