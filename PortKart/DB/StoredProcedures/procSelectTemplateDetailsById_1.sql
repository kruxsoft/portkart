﻿Create PROCEDURE [dbo].[procSelectTemplateDetailsById]
@TemplateId int,
@AgencyId int
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT DISTINCT	
			C.Currency
		,	C.Code
		,	S.ServiceCode
		,	S.ServiceName
		,	S.CostType
		,	SS.[Option]
		,	SS.Sort
		,   SC.ServiceCategoryName
		,	ST.TemplateId
		,	ST.TemplateName
		,	STD.Offer
		,	STD.Type
		,   STD.ServiceId
		,	STD.TemplateDetailId
		,	STD.IsParent
		,	STD.CurrencyId
		,	STD.ParentID
	FROM	dbo.tbl_ServiceTemplateDetails AS STD
	INNER JOIN dbo.tbl_ServiceTemplate ST
		ON ST.TemplateId = STD.TemplateId
	INNER JOIN dbo.tbl_Service AS S
		ON	S.ServiceId = STD.ServiceId
	INNER JOIN (SELECT STUFF((SELECT ',' + ServiceCategoryName
								FROM tbl_Service EE
								INNER JOIN tbl_Service_SCategoryMapping M
								ON M.ServiceId=EE.ServiceId
								Inner join tbl_ServiceCategory C
								ON C.ServiceCategoryId=M.ServiceCategoryId
								WHERE EE.ServiceId=E.ServiceId								
								FOR XML PATH('')), 1, 1, '') AS ServiceCategoryName,E.ServiceId
				FROM tbl_Service E
				INNER JOIN tbl_Service_SCategoryMapping EM
				ON EM.ServiceId=E.ServiceId
				Inner join tbl_ServiceCategory EC
				ON EC.ServiceCategoryId=EM.ServiceCategoryId				
				GROUP BY E.ServiceId )SC
	ON STD.ServiceId=Sc.ServiceId
	Left JOIN dbo.tbl_SubService AS SS
		ON	SS.ServiceId = STD.ServiceId
		AND SS.ParentServiceId=STD.ParentId
		AND (STD.IsParent<>1 AND isnull(STD.ParentId,0)>0)
	INNER JOIN dbo.tbl_Currency AS C
		ON	C.CurrencyId = STD.CurrencyId
	
	WHERE ST.TemplateId = @TemplateId 

	END