﻿CREATE PROCEDURE [dbo].[procInsertVesselPort]
	(
	@code nvarchar (255),
	@port	varchar(100),
	@airport nvarchar (255),
    @un_Locode nvarchar (255),
    @timeZone nvarchar (255),
    @latitude decimal,
    @latitudeDegree decimal,
    @latitudeMinutes decimal,
    @latitudeDirection varchar(255),
    @longitude decimal,
    @longitudeDegree decimal,
    @longitudeMinutes decimal,
    @longitudeDirection varchar(255),
    @createdBy int,
    @modifiedBy int,
	@countryId int)
AS
BEGIN
	IF EXISTS	(SELECT 1 
				from  tbl_VesselPort
				WHERE Code = @code and Port = @port
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'Vessel Port with that Name or Code already exists.' AS [Message]
	END
	ELSE
	BEGIN

		INSERT INTO tbl_VesselPort(
			Code
			,Port
			,Airport 
			,Un_Locode 
			,TimeZone 
			,Latitude 
			,LatitudeDegree 
			,LatitudeMinutes 
			,LatitudeDirection 
			,Longitude
			,LongitudeDegree 
			,LongitudeMinutes 
			,LongitudeDirection 
			,IsActive
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,CountryId)
		VALUES	(
			@code
		,	@port
		,	@airport
		,	@un_Locode
		,	@timeZone
		,	@latitude
		,	@latitudeDegree
		,	@latitudeMinutes
		,	@latitudeDirection
		,	@longitude
		,	@longitudeDegree
		,	@longitudeMinutes
		,	@longitudeDirection
		,	1 
		,	@createdBy
		,	GETDATE()
		,	@modifiedBy
		,	GETDATE()
		,	@countryId
			)

		IF @@ROWCOUNT > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Vessel Port created successfully.' AS [Message]
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
		END
	END
END
GO
