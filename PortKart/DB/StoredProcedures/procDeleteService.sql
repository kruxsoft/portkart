﻿CREATE PROCEDURE [dbo].[procDeleteService]
(
	
	@serviceId		int,
	@modifiedby		int	
)
AS
BEGIN	
If EXISTS (SELECT 1 from tbl_AgencyServiceDetails  where ServiceId=@serviceId)
BEGIN
SELECT	'ERROR' AS [MessageType], 'Cannot deactivate this service, it is added against a port' AS [Message]
return
END


UPDATE  tbl_Service 
	SET		IsActive = '0' 
		,	ModifiedBy = @modifiedby
		,	ModifiedDate = GETDATE()
	WHERE	ServiceId = @serviceId

	IF @@ROWCOUNT > 0
	BEGIN
		SELECT	'SUCCESS' AS [MessageType], 'Service deactivated successfully.' AS [Message]
	END
	ELSE
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
	END
END



