﻿
    
create PROCEDURE [dbo].[procUpdateOwnerTemplate]
    (
    @OwnerTemplateId int,
    @OwnerId int,
    @templateId int,
    @portId xml,
    @startdate datetime,
    @enddate datetime,
    @isActive bit,
    
    @modifiedBy int
)
AS
BEGIN    

DECLARE @RowCount int



declare @hdoc int 
exec sp_xml_preparedocument @hdoc output, @portId





    IF EXISTS    (SELECT    1
                 FROM    dbo.tbl_OwnerTemplateMapping M
                 WHERE CONVERT(date, @startdate)>=M.StartDate
                 AND CONVERT(date, @startdate)<=M.EndDate
                 AND M.IsActive=1
                 AND Ownerid=@OwnerId
                 AND OwnerTemplateId <> @OwnerTemplateId
                   
                )
BEGIN
        SELECT    'ERROR' AS [MessageType], 'A template mapping with an overlapping date range already exists for this Owner.' AS [Message]
        return
    END
    IF EXISTS    (SELECT    1
                 FROM    dbo.tbl_OwnerTemplateMapping
                 WHERE    OwnerId = @ownerId and TemplateId = @templateId 
                 AND OwnerTemplateId <> @OwnerTemplateId
                   
                )
    BEGIN
        SELECT    'ERROR' AS [MessageType], 'Owner Template Mapping already exists.' AS [Message]
        return
    END
    
 



        UPDATE  tbl_OwnerTemplateMapping 
        SET        
            
                StartDate = @startdate
            ,    EndDate = @enddate
            ,    ModifiedBy = @modifiedBy
            ,    IsActive = @isActive
            ,    ModifiedDate = GETDATE()
        WHERE    OwnerTemplateId = @OwnerTemplateId 

        SET @RowCount=@@ROWCOUNT
        Delete tbl_Port_OwnerMapping Where OwnerTemplateId=@OwnerTemplateId
        
            Insert into tbl_Port_OwnerMapping(OwnerTemplateId,PortId,CreatedBy,CreatedDate)
            Select @OwnerTemplateId,SC.PortId,@modifiedBy,GETDATE() from OpenXML(@hdoc, 'Portlist/PortId')
            WITH (PortId int '.') SC
            WHERE IsNull (SC.PortId,0) <> 0





        IF @RowCount > 0
        BEGIN
            SELECT    'SUCCESS' AS [MessageType], 'Owner updated successfully.' AS [Message]
        END
        ELSE
        BEGIN
            SELECT    'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
        END
    
END









