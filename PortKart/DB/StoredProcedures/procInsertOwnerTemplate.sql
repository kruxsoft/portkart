﻿CREATE PROCEDURE [dbo].[procInsertOwnerTemplate]
(
	
	@ownerId		int,
	@templateId	 int,
	@portId xml,
	@startdate datetime,
	@enddate datetime,
	@createdBy		smallint
	
 )
AS
BEGIN	
declare @ownerTemplateId int
declare @hdoc int 
declare @rowcount int
exec sp_xml_preparedocument @hdoc output, @portId
	IF EXISTS	(SELECT	1
				 FROM	dbo.tbl_OwnerTemplateMapping M
				 WHERE CONVERT(date, @startdate)>=M.StartDate
				 AND CONVERT(date, @startdate)<=M.EndDate
				 AND M.IsActive=1
				 AND OwnerID=@ownerId
				   
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'A template mapping with an overlapping date range already exists for this Owner.' AS [Message]
		return
	END
	IF EXISTS	(SELECT	1
				 FROM	dbo.tbl_OwnerTemplateMapping
				 WHERE	OwnerId = @ownerId and TemplateId = @templateId 
				   
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'Owner Template Mapping already exists.' AS [Message]
		return
	END
	
	ELSE
	BEGIN
		INSERT INTO tbl_OwnerTemplateMapping(
				OwnerId
			,   TemplateId
				
			,	StartDate
			,	EndDate
			,	CreatedBy
			,	CreatedDate
			,   IsActive 
			)
			VALUES	(
				@ownerId
			,	@templateId	
			
			,	@startdate 
			,	@enddate 
			,	@createdBy
			,	getdate()
			,   1
			)
			
			SET @rowcount=@@ROWCOUNT

			SET @ownerTemplateId = @@IDENTITY
			Insert into tbl_Port_OwnerMapping(OwnerTemplateId,PortId,CreatedBy,CreatedDate)
			Select @OwnerTemplateId,SC.PortId,@CreatedBy,GETDATE() from OpenXML(@hdoc, 'Portlist/PortId')
			WITH (PortId int '.') SC
			WHERE IsNull (SC.PortId,0) <> 0
	

		IF @rowcount > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Owner created successfully.' AS [Message]
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
		END
	END
END
