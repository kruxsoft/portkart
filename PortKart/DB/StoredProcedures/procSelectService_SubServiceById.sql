﻿CREATE PROCEDURE [dbo].[procSelectService_SubServiceById]
(
    @serviceID    int,
	@parentServiceID int
)
AS
BEGIN
    SELECT   
            M.ServiceName
        ,    RMM.[Option]           
        ,    RMM.Sort
    FROM    dbo.tbl_SubService AS RMM
    INNER JOIN dbo.tbl_Service AS M
        ON    RMM.ParentServiceId = M.ServiceId
    WHERE     RMM.ParentServiceId = @parentServiceID and RMM.ServiceId=@serviceID
    ORDER BY RMM.Sort ASC
END