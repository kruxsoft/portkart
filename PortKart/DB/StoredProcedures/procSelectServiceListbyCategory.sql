﻿CREATE PROCEDURE [dbo].[procSelectServiceListbyCategory]
@ServiceCategoryId varchar(500)
AS
BEGIN
DECLARE @cond varchar(500)
SET @cond=''
IF isnull(@ServiceCategoryId,'')<>''
BEGIN
SET @cond=' AND M.ServiceCategoryId in ('+@ServiceCategoryId+')'
END
	exec ('SELECT distinct  S.ServiceId
			,S.ServiceName
			,S.ServiceCode
	FROM	 tbl_Service S
	INNER JOIN tbl_Service_SCategoryMapping M
	on M.ServiceId=S.ServiceId
	WHERE	 IsActive=1 '+@cond)
END