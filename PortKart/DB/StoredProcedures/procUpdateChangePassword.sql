﻿CREATE PROCEDURE [dbo].[procUpdateChangePassword]
(	@userid		smallint,
	@password	varchar(max),
	@modifiedBy		int
)
AS
BEGIN

	UPDATE  tbl_User 
	SET		    [Password] = @password
	        ,	ModifiedBy=@modifiedBy
	WHERE	UserId = @userid

	IF @@ROWCOUNT > 0
	BEGIN
		SELECT	'SUCCESS' AS [MessageType], 'User Password updated successfully.' AS [Message]
	END
	ELSE
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
	END

END