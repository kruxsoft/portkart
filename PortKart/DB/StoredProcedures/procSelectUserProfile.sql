﻿CREATE PROCEDURE [dbo].[procSelectUserProfile]
	@UserId int
AS
BEGIN
	SELECT   M.UserId
			,M.CompanyId
			,p.CountryId
			,p.City
			,p.IndustryTypeId
			,p.ExpiryDate
			,p.Website
			,p.EmailID
			,p.Address
			,p.CountryCode
			,p.Phone
			,p.LogoPath
			,p.AttachmentPath
			,p.Logo
			,p.Attachment
			,p.CreatedBy
			,p.CreatedDate
			,p.ModifiedBy
			,p.ModifiedDate
			,P.CompanyName
			,P.CompanyRegNo
			,C.CountryName
			,I.IndustryTypeName
			,P.About
	FROM   tbl_Company P
	INNER JOIN tbl_UserCompanyMapping M
	ON M.CompanyId=P.CompanyId
	LEFT JOIN tbl_Country C
	ON P.CountryId=C.CountryId
	LEFT JOIN tbl_IndustryType I
	ON P.IndustryTypeId=I.IndustryTypeId
	WHERE M.UserId=@UserId
END
