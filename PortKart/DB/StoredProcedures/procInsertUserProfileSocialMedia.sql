﻿CREATE PROCEDURE [dbo].[procInsertUserProfileSocialMedia]
	@CompanyId int,
	@Link nvarchar(max),
	@SocialMediaId int,
	@Type nvarchar(100)
AS
BEGIN
DECLARE @RowCount int
IF isnull(@SocialMediaId,0)=0
BEGIN
INSERT INTO tbl_ProfileSocialMedia(CompanyId,Link,Type)
VALUES (@CompanyId,isnull(@Link,''),@Type)
SET @RowCount=@@ROWCOUNT
EnD
ELSE
BEGIN
UPDATE  tbl_ProfileSocialMedia 
SET		CompanyId=@CompanyId,
		link=isnull(@Link,''),
		Type=@Type
WHERE   SocialMediaId=@SocialMediaId
SET @RowCount=@@ROWCOUNT
END

IF @RowCount > 0
	BEGIN
		SELECT	'SUCCESS' AS [MessageType], 'Profile saved successfully.' AS [Message]			
	END
	ELSE
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
	END
END
