-- =============================================
-- Author:		Simi Simon
-- Create date: 28-10-2019
-- Description:	To flag a Role
-- =============================================
CREATE PROCEDURE [dbo].[procDeleteRole]
(
	@RoleId int
,	@modifiedBy smallint
)
AS
BEGIN	
	UPDATE  tbl_Role 
	SET		IsActive = '0' 
		,	ModifiedBy = @modifiedBy
		,	ModifiedDate = GETDATE()
	WHERE	RoleId = @RoleId

	IF @@ROWCOUNT > 0
	BEGIN
		SELECT	'SUCCESS' AS [MessageType], 'Role deactivated successfully.' AS [Message]
	END
	ELSE
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
	END
END