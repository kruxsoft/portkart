﻿CREATE PROCEDURE [dbo].[procDeleteAgencyServiceDetail]
	@AgencyDetailId int 
AS
BEGIN
	DECLARE @ROWcount int
	DECLARE @AgencyServiceId int
	DECLARE @ServiceId int
	
	SELECT @AgencyServiceId=AgencyserviceId,@ServiceId=ServiceId FROM tbl_AgencyServiceDetails WHERE AgencyDetailId=@AgencyDetailId
	
	DELETE tbl_AgencyServiceDetails WHERE AgencyDetailId=@AgencyDetailId
	SET @ROWcount=@@ROWCOUNT
	
	DELETE tbl_AgencyServiceDetails WHERE ParentId=@ServiceId AND AgencyServiceId=@AgencyServiceId
	

IF @ROWcount > 0
	BEGIN
		SELECT	'SUCCESS' AS [MessageType], 'Service deleted successfully.' AS [Message]
	END
	ELSE
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'Failed to delete service details' AS [Message]
	END
END