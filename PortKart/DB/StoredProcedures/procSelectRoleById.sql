﻿-- =============================================
-- Author:		Prageeth Rajan
-- Create date: 02-12-2019
-- Description:	To get roles by id
CREATE PROCEDURE [dbo].[procSelectRoleById]
(
	@roleid varchar(50)
)
AS
BEGIN
	SELECT	RoleId
	,		RoleName
	,		IsActive
	FROM tbl_Role 
	WHERE RoleId=@roleid
END
