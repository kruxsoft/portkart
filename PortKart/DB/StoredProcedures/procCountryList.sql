﻿CREATE PROCEDURE [dbo].[procCountryList]
	 AS
BEGIN
    SET NOCOUNT ON;
    SELECT    CountryId
    ,        CountryName
    ,        IsActive
    FROM    tbl_Country
    WHERE IsActive=1
END