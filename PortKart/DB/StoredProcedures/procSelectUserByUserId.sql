﻿CREATE PROCEDURE [dbo].[procSelectUserByUserId]
	(@userId int)
AS
BEGIN
   SET NOCOUNT ON;
	SELECT	U.UserId
		,	U.FirstName
		,	U.LastName
		,	U.Password
		,	U.EmailId
		,	U.MobileNumber
		,	U.CountryCode
		,	CO.CompanyName
		,	CO.CompanyRegNo
		,	U.UserGroup
		,	U.DateOfBirth
		, 	U.CreatedBy
		,	U.CreatedDate
		,	U.UserStatusID	
		,	U.EmailVerified
		,	US.UserStatus
	FROM tbl_User U 
	INNER JOIN [dbo].[tbl_UserStatus] US
		ON	US.UserStatusId=U.UserStatusId
		LEFT JOIN tbl_UserCompanyMapping UC ON UC.UserId=U.UserId 
		LEFT JOIN tbl_Company CO ON CO.CompanyId=UC.CompanyId
	WHERE U.UserId = @userId
END
