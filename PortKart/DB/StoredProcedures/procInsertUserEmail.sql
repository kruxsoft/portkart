﻿CREATE PROCEDURE [dbo].[procInsertUserEmail]
	@EmailId NVARCHAR(254),
	@EmailBody nvarchar(max),
	@EmailType char(1),
	@EmailCode nvarchar(500),
	@SentStatus BIT,
	@CreatedBy INT
AS
BEGIN
	DECLARE @UserId int
	SELECT @UserId=UserId FROM tbl_user Where lower(EmailId)=lower(@EmailId)
	IF EXISTS(SELECT 1 FROM tbl_UserEmail WHERE userid=@UserId and EmailType=@EmailType AND isnull(IsExpire,0)=0)
	BEGIN
		UPDATE tbl_UserEmail 
		SET    IsExpire=1
		WHERE  userid=@UserId 
		AND    EmailType=@EmailType 
		AND    isnull(IsExpire,0)=0
	END

	INSERT INTO tbl_UserEmail(UserId
						     ,Email
							 ,EmailBody
							 ,EmailType
							 ,SentStatus
							 ,IsExpire
							 ,EmailCode
							 ,CreatedBy
							 ,CreatedDate)
                      VALUES(@UserId
					        ,@EmailId
							,@EmailBody
							,@EmailType
							,@SentStatus
							,0
							,@EmailCode
							,@CreatedBy
							,GETDATE())
	
	IF @@ROWCOUNT > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType],
			case @EmailType When 'R' THEN 'A verification link has been sent to the registered email address'
			                When 'P' THEN 'A password reset link has been sent to the registered email address'
							END 	AS [Message]			
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the UserEmail details were not saved.' AS [Message]
		END
END