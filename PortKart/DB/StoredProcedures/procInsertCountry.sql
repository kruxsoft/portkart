-- =============================================
-- Author:		Gopika Vipin
-- Create date: 21-10-2019
-- Description:	To add new county
-- =============================================
CREATE PROCEDURE [dbo].[procInsertCountry]
(
	@countryName varchar(100),
	  @createdBy int,
    @modifiedBy int
)
AS
BEGIN
IF EXISTS	(SELECT	1
				 FROM	dbo.tbl_Country
				 WHERE	CountryName = @countryName
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'Country with that name already exists.' AS [Message]
	END
	ELSE
	BEGIN

	INSERT INTO tbl_Country	(
	CountryName
	,IsActive
	,CreatedBy
	,CreatedDate
	,ModifiedBy
	,ModifiedDate) 
	VALUES(
	@countryName,
	1
	,@createdBy
	,GETDATE()
	,@modifiedBy
	,GETDATE())

		IF @@ROWCOUNT > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Country created successfully.' AS [Message]
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
		END
	END
END
GO