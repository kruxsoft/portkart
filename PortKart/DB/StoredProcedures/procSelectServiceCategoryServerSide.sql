﻿CREATE PROCEDURE [dbo].[procSelectServiceCategoryServerSide]
(
@pageSize int,
@skip int,
@sortColumn varchar(50),
@sortColumnDir varchar(10),
@searchValue varchar(100),
@ServiceCategoryCode varchar(100),
@ServiceCategoryName varchar(100),
@ModifiedDate varchar(100),
@IsActive varchar(100)
)
AS
BEGIN

DECLARE @recordsTotal int
DECLARE @recordsFiltered int
DECLARE @sqlquery varchar(5000)
DECLARE @orderbyquery varchar(5000)
---------------getting total count-------------------------------------------------
	SELECT     @recordsTotal=Count(*)
	FROM	   tbl_ServiceCategory
------------------------------------------------------------------------------------------------------------------------
set @orderbyquery=(CASE CONVERT(VARCHAR,@sortColumn)
	                           WHEN 'ServiceCategoryCode' THEN 'ServiceCategoryCode'
							   WHEN 'ServiceCategoryName' THEN 'ServiceCategoryName'
							   WHEN 'ModifiedDate' THEN  'ModifiedDate'
							   WHEN 'IsActive' THEN 'case IsActive when 1 then ''Active'' else ''Inactive'' end'
							   ELSE  'ServiceCategoryCode'  END)
IF isnull(@searchValue,'')=''
BEGIN
    SELECT	   @recordsFiltered=COUNT(*)
	FROM	   tbl_ServiceCategory
	WHERE     (ServiceCategoryCode like '%'+@ServiceCategoryCode+'%' OR isnull(@ServiceCategoryCode,'')='')
	AND       (ServiceCategoryName like '%'+@ServiceCategoryName+'%' OR isnull(@ServiceCategoryName,'')='')
	AND       (CONVERT(varchar, ModifiedDate,105) like '%'+@ModifiedDate+'%' OR isnull(@ModifiedDate,'')='')
	AND       (case IsActive when 1 then 'Active' else 'Inactive' end like '%'+@IsActive+'%' OR isnull(@IsActive,'')='')


	SET @sqlquery='SELECT	ServiceCategoryId
		,		ServiceCategoryCode
		,		ServiceCategoryName
		,		ModifiedDate 
		,		CONVERT(varchar, ModifiedDate,105) As ModifiedDateAsString
		,       IsActive
		,    '+CONVERT(VARCHAR,@recordsTotal)+' as recordsTotal
		,    '+CONVERT(VARCHAR,@recordsFiltered)+' as recordsFiltered
	FROM      tbl_ServiceCategory
	WHERE     (ServiceCategoryCode like''%'+CONVERT(VARCHAR,@ServiceCategoryCode)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@ServiceCategoryCode,''))+''','''')='''')
	AND       (ServiceCategoryName like ''%'+CONVERT(VARCHAR,@ServiceCategoryName)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@ServiceCategoryName,''))+''','''')='''')
	AND       (CONVERT(varchar, ModifiedDate,105) like''%'+CONVERT(VARCHAR,@ModifiedDate)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@ModifiedDate,''))+''','''')='''')
	AND       (case IsActive when 1 then ''Active'' else ''Inactive'' end like ''%'+CONVERT(VARCHAR,@IsActive)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@IsActive,''))+''','''')='''')
	ORDER BY  '+@OrderByQuery+' '+ @sortColumnDir +'
	OFFSET     '+CONVERT(varchar,@skip)+' ROWS
	FETCH NEXT '+CONVERT(varchar,@pagesize)+' ROWS ONLY OPTION (RECOMPILE)'

			
exec (@sqlquery)
END
ELSE
BEGIN
    SELECT	   @recordsFiltered=COUNT(*)
	FROM	   tbl_ServiceCategory
	WHERE     (ServiceCategoryCode like '%'+@searchValue+'%' 
	OR         ServiceCategoryName like '%'+@searchValue+'%'
	OR         CONVERT(varchar, ModifiedDate,105) like '%'+@searchValue+'%'
	OR         case IsActive when 1 then 'Active' else 'Inactive' end like '%'+@searchValue+'%' )
	AND       (ServiceCategoryCode like '%'+@ServiceCategoryCode+'%' OR isnull(@ServiceCategoryCode,'')='')
	AND       (ServiceCategoryName like '%'+@ServiceCategoryName+'%' OR isnull(@ServiceCategoryName,'')='')
	AND       (CONVERT(varchar, ModifiedDate,105) like '%'+@ModifiedDate+'%' OR isnull(@ModifiedDate,'')='')
	AND       (case IsActive when 1 then 'Active' else 'Inactive' end like '%'+@IsActive+'%' OR isnull(@IsActive,'')='')



		SET @sqlquery='SELECT	ServiceCategoryId
		,		ServiceCategoryCode
		,		ServiceCategoryName
		,		ModifiedDate 
		,		CONVERT(varchar, ModifiedDate,105) As ModifiedDateAsString
		,       IsActive
		,      '+CONVERT(VARCHAR,@recordsTotal)+' as recordsTotal
		,      '+CONVERT(VARCHAR,@recordsFiltered)+' as recordsFiltered
	 FROM       tbl_ServiceCategory
	 WHERE     (ServiceCategoryCode like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
	 OR         ServiceCategoryName like ''%'+CONVERT(VARCHAR,@searchValue)+'%''
	 OR         CONVERT(varchar, ModifiedDate,105) like ''%'+CONVERT(VARCHAR,@searchValue)+'%''
	 OR         CASE IsActive when 1 then ''Active'' else ''Inactive'' end like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' )
     AND       (ServiceCategoryCode like''%'+CONVERT(VARCHAR,@ServiceCategoryCode)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@ServiceCategoryCode,''))+''','''')='''')
	 AND       (ServiceCategoryName like ''%'+CONVERT(VARCHAR,@ServiceCategoryName)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@ServiceCategoryName,''))+''','''')='''')
	 AND       (CONVERT(varchar, ModifiedDate,105) like''%'+CONVERT(VARCHAR,@ModifiedDate)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@ModifiedDate,''))+''','''')='''')
 	 AND       (case IsActive when 1 then ''Active'' else ''Inactive'' end like ''%'+CONVERT(VARCHAR,@IsActive)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@IsActive,''))+''','''')='''')
	ORDER BY  '+@OrderByQuery+' '+ @sortColumnDir +'
	OFFSET     '+CONVERT(varchar,@skip)+' ROWS
	FETCH NEXT '+CONVERT(varchar,@pagesize)+' ROWS ONLY OPTION (RECOMPILE)'
	
	exec (@sqlquery)
END
END	
	