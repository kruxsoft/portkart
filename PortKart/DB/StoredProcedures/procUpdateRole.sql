-- =============================================
-- Author:		Simi SImon
-- Create date: 28-10-2019
-- Description:	update new role
-- =============================================
CREATE PROCEDURE [dbo].[procUpdateRole]
(
	@roleName	varchar(50),
	@modifiedBy smallint,
	@isActive	bit,
	@roleId		int
 )
AS
BEGIN

	IF EXISTS	(SELECT	1
				 FROM	dbo.tbl_Role
				 WHERE	Rolename = @roleName
				 AND RoleId<>@roleId
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'Role with that name already exists.' AS [Message]
	END
	ELSE
	BEGIN

		UPDATE  tbl_Role 
		SET 
			RoleName=@roleName,
			ModifiedBy=@modifiedBy,
			IsActive=@isActive,
			ModifiedDate=GETDATE()
		WHERE RoleId=@roleId

		IF @@ROWCOUNT > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Role details updated successfully.' AS [Message]
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
		END
	END
END
