﻿CREATE PROCEDURE [dbo].[procUpdateAgencyServiceDetails](
	@AgencyDetailId int,
	@AgencyServiceId int,
	@ServiceId int,
	@CurrencyId int,
	@Amount decimal (30,5),
	@Tat int,
	@Description nvarchar(2000),
	@IsParent bit,
	@IsActive bit,
	@TatDescription nvarchar(500),
	@CostType varchar(50),
	@Option char(1),
	@ParentId int,
	@CreatedBy int)
AS
BEGIN
	DECLARE @ROWCOUNT int
	IF isnull(@AgencyDetailId,0)>0
	BEGIN
	UPDATE tbl_AgencyServiceDetails SET AgencyServiceId=@AgencyServiceId
										,ServiceId=@ServiceId
										,CurrencyId=@CurrencyId
										,Amount=@Amount
										,ActualAmount=@Amount
										,ModifiedBy=@CreatedBy
										,ModifiedDate=Getdate()
										,IsParent=@IsParent
										,IsActive=@IsActive
										,TatDescription=@TatDescription
										,CostType=@CostType
										,[Option]=@Option
										,ParentId=@ParentId
										,Tat=@Tat
										,Description=@Description
	WHERE AgencyDetailId=@AgencyDetailId
							
	SET @ROWCOUNT=@@ROWCOUNT
	END
	ELSE
	BEGIN
	INSERT INTO tbl_AgencyServiceDetails(AgencyServiceId
										,ServiceId
										,CurrencyId
										,Amount
										,ActualAmount
										,CreatedBy
										,CreatedDate
										,IsParent
										,IsActive
										,TatDescription
										,CostType
										,[Option]
										,ParentId
										,Tat
										,Description)
							VALUES (@AgencyServiceId
							,@ServiceId
							,@CurrencyId
							,@Amount
							,@Amount
							,@CreatedBy
							,GETDATE()
							,@IsParent
							,@IsActive
							,@TatDescription
							,@CostType
							,@Option
							,@ParentId
							,@Tat
							,@Description)
	
	SET @ROWCOUNT=@@ROWCOUNT
	END
	
	IF @@ROWCOUNT > 0
	BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Agency service updated successfully.' AS [Message]			
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the service details were not updated.' AS [Message]
	END
END