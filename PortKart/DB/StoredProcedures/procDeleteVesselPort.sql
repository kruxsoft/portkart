﻿CREATE PROCEDURE [dbo].[procDeleteVesselPort]
	(
		@portId    int
		,@modifiedBy int
)
AS
BEGIN	
	UPDATE  [dbo].[tbl_VesselPort] 
	SET		IsActive = '0' 
		,	ModifiedBy = @modifiedBy
		,	ModifiedDate = GETDATE()
	WHERE	PortId = @portId

	IF @@ROWCOUNT > 0
	BEGIN
		SELECT	'SUCCESS' AS [MessageType], 'Vessel Port deactivated successfully.' AS [Message]
	END
	ELSE
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
	END
END
