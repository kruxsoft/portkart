﻿CREATE PROCEDURE [dbo].[procUpdateServiceCategory]
	(
	@serviceCategoryName	varchar(100),
	@serviceCategroyCode	varchar(20),
	@modifiedBy				int,	
	@serviceCategoryId		int,
	@isActive           bit
)
AS
BEGIN
IF EXISTS    (SELECT    1
                 FROM    tbl_Service
                 Where ServiceCategoryId = @servicecategoryId and IsActive = 1 and @isActive=0)
             
    BEGIN
        SELECT    'ERROR' AS MessageType, 'Cannot deactivate this service category' AS Message;
		return
    END
	IF EXISTS	(SELECT	1
				 FROM	dbo.tbl_ServiceCategory
				 WHERE	(ServiceCategoryCode = @serviceCategroyCode
							OR	ServiceCategoryName = @serviceCategoryName)
					AND	ServiceCategoryId <> @serviceCategoryId
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'Service Category with that name or category code already exists.' AS [Message]
	END
	ELSE
	BEGIN
		UPDATE  tbl_ServiceCategory 
		SET		ServiceCategoryName = @serviceCategoryName
			,	ServiceCategoryCode = @serviceCategroyCode
			,	ModifiedBy = @modifiedBy
			,	IsActive = @isActive
			,	ModifiedDate = GETDATE()
		WHERE	ServiceCategoryId = @serviceCategoryId

		IF @@ROWCOUNT > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Service Category details updated successfully.' AS [Message]
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
		END
	END
END

