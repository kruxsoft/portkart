﻿CREATE PROCEDURE [dbo].[procSelectAgencyOwnerTemplate]
	@OwnerId int,
	@AgencyId int
AS
BEGIN
SET NOCOUNT ON;
	SELECT top(1) M.OwnerId,M.TemplateId,M.StartDate,M.EndDate,T.TemplateName,OwnerTemplateId
	FROM tbl_OwnerTemplateMapping M
	INNER JOIN	tbl_ServiceTemplate T
	ON T.TemplateId=M.TemplateId
	WHERE M.OwnerId=@OwnerId
	AND   T.AgencyId=@AgencyId
	AND CONVERT(date, getdate())>=M.StartDate
	AND (CONVERT(date, getdate())<=M.EndDate or isnull(M.EndDate,'')='')
	AND M.IsActive=1

END