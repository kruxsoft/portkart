-- =============================================
-- Author:		Gopika Vipin
-- Create date: 21-10-2019
-- Description:	To get all state
-- =============================================
CREATE PROCEDURE [dbo].[procSelectState]
(
 @countryId int
 )
AS
BEGIN
	SELECT	StateID
	,		StateName
	,		s.CountryID
	,		CountryName 
	FROM tbl_State s 
	INNER JOIN tbl_Country c 
		ON s.CountryID=c.CountryID 
		WHERE s.CountryID=@countryId
END
