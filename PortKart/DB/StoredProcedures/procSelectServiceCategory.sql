﻿CREATE PROCEDURE [dbo].[procSelectServiceCategory]
	AS
BEGIN
	SELECT	ServiceCategoryId
	,		ServiceCategoryCode
	,		ServiceCategoryName
	,		ModifiedDate 
	,		CONVERT(varchar, ModifiedDate,105) As ModifiedDateAsString
	,       IsActive
	FROM tbl_ServiceCategory
END
