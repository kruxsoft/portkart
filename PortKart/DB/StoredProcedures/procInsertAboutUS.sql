﻿CREATE PROCEDURE [dbo].[procInsertAboutUS]
	@CompanyID int,
	@aboutUS nvarchar(1000)
AS
BEGIN
	DECLARE @RowCount int
	Update tbl_Company set About=@aboutUS where CompanyId=@CompanyID
	SET @RowCount=@@ROWCOUNT

	IF @RowCount > 0
	BEGIN
		SELECT	'SUCCESS' AS [MessageType], 'Saved successfully.' AS [Message]			
	END
	ELSE
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
	END
	END
	
RETURN 0
