﻿CREATE PROCEDURE [dbo].[procSelectVesselPortById]
(
@portId int

)

AS
BEGIN
	SELECT	VP.Code
			,VP.Port
			,VP.PortId
			 
			,VP.Airport 
			,VP.Un_Locode 
			,VP.TimeZone 
			,VP.Latitude 
			,VP.LatitudeDegree 
			,VP.LatitudeMinutes 
			,VP.LatitudeDirection 
			,VP.Longitude
			,VP.LongitudeDegree 
			,VP.LongitudeMinutes 
			,VP.LongitudeDirection 
			,VP.IsActive
			,VP.CreatedBy
			,VP.CreatedDate
			,VP.ModifiedBy
			,VP.ModifiedDate
			,C.CountryName
			,C.CountryId
	FROM	dbo.tbl_VesselPort AS VP
	INNER JOIN dbo.tbl_Country AS C
		ON	VP.CountryId = C.CountryId 
		WHERE PortId = @portId
	
END

