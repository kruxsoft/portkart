﻿CREATE PROCEDURE [dbo].[procUpdateTemplateDetails]
(
	@TemplateId int,
	@TemplateDetailId int,
	@ServiceId int,
	@CurrencyId int,
	@Offer decimal,
	@Type nvarchar(100),
	@CreatedBy int,
	@Isparent bit,
	@ParentId int
)

AS
BEGIN
IF EXISTS    (SELECT    1
                 FROM    tbl_ServiceTemplateDetails
                 Where TemplateDetailId = @TemplateDetailId)
    BEGIN
	UPDATE  tbl_ServiceTemplateDetails 
		SET		Offer = @Offer,
				CurrencyId = @CurrencyId,
				Type = @Type,
				ModifiedDate = GETDATE()
		WHERE	 TemplateDetailId = @TemplateDetailId
        SELECT    'SUCCESS' AS MessageType, 'Updated this template' AS Message;
		return
    END
ELSE

BEGIN
		Insert into tbl_ServiceTemplateDetails(
			TemplateId
		,	ServiceId
		,	CurrencyId	
		,	Type
		,	Offer
		,	CreatedBy
		,	CreatedDate
		,	IsParent
		,	ParentID
		)values(
		@TemplateId,
		@ServiceId,
		@CurrencyId,
		@Type,
		@Offer,
		@CreatedBy,
		GETDATE(),
		@IsParent,
		@ParentId
		)
END
IF @@ROWCOUNT > 0
	BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Agency service template details saved successfully.' AS [Message]			
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the service template details were not saved.' AS [Message]
	END
END