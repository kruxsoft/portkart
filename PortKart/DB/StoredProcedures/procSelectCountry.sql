﻿-- =============================================
-- Author:		Gopika Vipin
-- Create date: 21-10-2019
-- Description:	To get all country
-- =============================================
CREATE PROCEDURE [dbo].[procSelectCountry]
AS
BEGIN
	SELECT	CountryID
	,		CountryName 
	,       IsActive
	FROM tbl_Country
	order by countryname
END
