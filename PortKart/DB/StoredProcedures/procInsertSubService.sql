﻿CREATE PROCEDURE [dbo].[procInsertSubService]
    (
    @parentServiceID int,
    @option     char(1),
    @sort    int,
    @serviceId        int,    
    @createdBy        smallint,
    @createdDate datetime
 )
AS
BEGIN    


 if exists (select 1 from tbl_SubService where (serviceid=@serviceId and ParentServiceId=@parentServiceID) )
 begin
  SELECT    'ERROR' AS [MessageType], 'Selected Sub service  already exists under this service' AS [Message]
  return
 end
  if exists (select 1 from tbl_SubService where (Sort=@sort and ParentServiceId=@parentServiceID) )
 begin
  SELECT    'ERROR' AS [MessageType], 'Sort number already exists' AS [Message]
  return
 end

 

        INSERT INTO tbl_SubService(
        ParentServiceId,
             [Option]
            ,Sort
            ,ServiceId    
            ,CreatedBy
            ,CreatedDate
            )
        VALUES    (
        @parentServiceID,
            @option
        ,    @sort
        ,   @serviceId            
        ,    @createdBy
        ,    GETDATE()        
            )

 

 

 

        IF @@ROWCOUNT > 0
        BEGIN
            SELECT    'SUCCESS' AS [MessageType], 'Sub Service created successfully.' AS [Message]
        END
        ELSE
        BEGIN
            SELECT    'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
        END
    
END