﻿CREATE PROCEDURE [dbo].[procSelectService_SubService]
(
    @serviceID    int
)
AS
BEGIN
    SELECT   RMM.ServiceId,
    RMM.ParentServiceId,
            M.ServiceName
        ,    RMM.[Option]           
        ,    RMM.Sort
    FROM    dbo.tbl_SubService AS RMM
    INNER JOIN dbo.tbl_Service AS M
    ON    RMM.ServiceId = M.ServiceId
    WHERE     RMM.ParentServiceId = @serviceID
    ORDER BY RMM.Sort ASC
END