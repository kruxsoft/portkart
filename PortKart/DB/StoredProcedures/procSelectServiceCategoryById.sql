﻿CREATE PROCEDURE [dbo].[procSelectServiceCategoryById]
	(
@serviceCategoryId int

)

AS
BEGIN
	SELECT	ServiceCategoryId
			,ServiceCategoryCode
			,ServiceCategoryName
			,IsActive
	FROM	dbo.tbl_ServiceCategory 
	
	WHERE ServiceCategoryId = @serviceCategoryId
END
