﻿CREATE PROCEDURE [dbo].[procSelectServicebyIdForAgency]
@ServiceId varchar(1000)
AS
BEGIN
	DECLARE @CurrencyId int
	DECLARE @sqlquery varchar(8000)
	IF EXISTS (SELECT 1 FROM tbl_Currency WHERE Code='USD')
	BEGIN
		SELECT @CurrencyId=CurrencyId FROM tbl_Currency WHERE Code='USD'
	END
	ELSE
	BEGIN
		SELECT @CurrencyId=min(CurrencyId) FROM tbl_Currency
	END
	SET @CurrencyId=isnull(@CurrencyId,0)
	
	DECLARE @cond varchar(1000)
	DECLARE @cond2 varchar(1000)
	SET @cond=''
	SET @cond2=''
	IF isnull(@ServiceId,'')<>''
	BEGIN
		SET @cond=' WHERE S.ServiceId in ('+@ServiceId+')'
		SET @cond2=' WHERE SS.ParentServiceId in ('+@ServiceId+')'
	END
	
	SET @sqlquery= 'SELECT DISTINCT S.ServiceId
		,	isnull(S.IsGroup,0) as IsParent
		,	0 as ParentId
		,	S.ServiceCode
		,	S.ServiceName
		,	S.CostType
		,	''''as [Option]
		,   SC.ServiceCategoryName
		,	' +CONVERT(VARCHAR(100),@CurrencyId)+ ' as CurrencyId
	FROM	dbo.tbl_Service AS S
	INNER JOIN (SELECT STUFF((SELECT '','' + ServiceCategoryName
								FROM tbl_Service EE
								INNER JOIN tbl_Service_SCategoryMapping M
								ON M.ServiceId=EE.ServiceId
								Inner join tbl_ServiceCategory C
								ON C.ServiceCategoryId=M.ServiceCategoryId
								WHERE EE.ServiceId=E.ServiceId								
								FOR XML PATH('''')), 1, 1, '''') AS ServiceCategoryName,E.ServiceId
				FROM tbl_Service E
				INNER JOIN tbl_Service_SCategoryMapping EM
				ON EM.ServiceId=E.ServiceId
				Inner join tbl_ServiceCategory EC
				ON EC.ServiceCategoryId=EM.ServiceCategoryId				
				GROUP BY E.ServiceId )SC
	ON S.ServiceId=Sc.ServiceId
	'+CONVERT(varchar(1000),@cond)+'
	UNION
	SELECT DISTINCT S.ServiceId
		,	0 as IsParent
		,	SS.ParentServiceId as ParentId
		,	S.ServiceCode
		,	S.ServiceName
		,	S.CostType
		,	SS.[Option]
		,   SC.ServiceCategoryName
		,	' +CONVERT(VARCHAR(100),@CurrencyId)+ ' as CurrencyId
	FROM	dbo.tbl_Service AS S
	INNER JOIN (SELECT STUFF((SELECT '','' + ServiceCategoryName
								FROM tbl_Service EE
								INNER JOIN tbl_Service_SCategoryMapping M
								ON M.ServiceId=EE.ServiceId
								Inner join tbl_ServiceCategory C
								ON C.ServiceCategoryId=M.ServiceCategoryId
								WHERE EE.ServiceId=E.ServiceId								
								FOR XML PATH('''')), 1, 1, '''') AS ServiceCategoryName,E.ServiceId
				FROM tbl_Service E
				INNER JOIN tbl_Service_SCategoryMapping EM
				ON EM.ServiceId=E.ServiceId
				Inner join tbl_ServiceCategory EC
				ON EC.ServiceCategoryId=EM.ServiceCategoryId				
				GROUP BY E.ServiceId )SC
	ON S.ServiceId=Sc.ServiceId
	INNER JOIN	tbl_SubService SS
	ON SS.ServiceId=S.ServiceId
	'+CONVERT(varchar(1000),@cond2)

	exec (@sqlquery)
END