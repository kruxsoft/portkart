﻿CREATE PROCEDURE [dbo].[procSelectPortsByOwnerTemplateId]
	(
@OwnerTemplateId int
)

AS
BEGIN
	SELECT
			M.PortId,P.Port
			
	FROM	dbo.tbl_Port_OwnerMapping M
	INNER JOIN tbl_VesselPort P
	ON P.PortId=M.PortId
	WHERE OwnerTemplateId = @OwnerTemplateId
END


