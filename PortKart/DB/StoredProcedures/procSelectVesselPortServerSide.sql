﻿CREATE PROCEDURE [dbo].[procSelectVesselPortServerSide]
(
@pageSize int,
@skip int,
@sortColumn varchar(50),
@sortColumnDir varchar(10),
@searchValue varchar(100),
@Code varchar(100),
@Port varchar(100),
@CountryName varchar(100),
@Airport varchar(100),
@Un_Locode varchar(100),
@TimeZone varchar(100),
@Latitude varchar(100),
@LatitudeDegree varchar(100),
@LatitudeMinutes varchar(100),
@LatitudeDirection varchar(100),
@Longitude varchar(100),
@LongitudeDegree varchar(100),
@LongitudeMinutes varchar(100),
@LongitudeDirection varchar(100),
@IsActive varchar(100)
)
AS
BEGIN

DECLARE @recordsTotal int
DECLARE @recordsFiltered int
DECLARE @sqlquery varchar(5000)
DECLARE @orderbyquery varchar(5000)
---------------getting total count-------------------------------------------------
	SELECT      @recordsTotal=Count(*)
	FROM		tbl_VesselPort AS VP
	INNER JOIN  dbo.tbl_Country AS C
	ON			VP.CountryId = C.CountryId
------------------------------------------------------------------------------------------------------------------------
set @orderbyquery=(CASE CONVERT(VARCHAR,@sortColumn)
	                           WHEN 'Code' THEN 'Code'
							   WHEN 'Port' THEN 'Port'
							   WHEN 'CountryName' THEN  'C.CountryName'
							   WHEN 'Airport' THEN  'Airport'
							   WHEN 'Un_Locode' THEN  'Un_Locode'
							   WHEN 'TimeZone' THEN  'TimeZone'
							   WHEN 'Latitude' THEN  'Latitude'
							   WHEN 'LatitudeDegree' THEN  'LatitudeDegree'
							   WHEN 'LatitudeMinutes' THEN  'LatitudeMinutes'
							WHEN 'LatitudeDirection' THEN  'LatitudeDirection'
							WHEN 'Longitude' THEN  'Longitude'
							WHEN 'LongitudeDegree' THEN  'LongitudeDegree'
							WHEN 'LongitudeMinutes' THEN  'LongitudeMinutes'
							WHEN 'LongitudeDirection' THEN  'LongitudeDirection'
							   WHEN 'IsActive' THEN 'case VP.IsActive when 1 then ''Active'' else ''Inactive'' end'
							   ELSE  'ServiceCategoryCode'  END)
IF isnull(@searchValue,'')=''
BEGIN
    SELECT	   @recordsFiltered=COUNT(*)
	FROM		tbl_VesselPort AS VP
	INNER JOIN  dbo.tbl_Country AS C
	ON			VP.CountryId = C.CountryId
	WHERE     (Code like '%'+@Code+'%' OR isnull(@Code,'')='')
	AND       (Port like '%'+@Port+'%' OR isnull(@Port,'')='')
	AND       (Airport like '%'+@Airport+'%' OR isnull(@Airport,'')='')
	AND       (Un_Locode like '%'+@Un_Locode+'%' OR isnull(@Un_Locode,'')='')
	AND       (TimeZone like '%'+@TimeZone+'%' OR isnull(@TimeZone,'')='')
	AND       (Latitude like '%'+@Latitude+'%' OR isnull(@Latitude,'')='')
	AND       (LatitudeDegree like '%'+@LatitudeDegree+'%' OR isnull(@LatitudeDegree,'')='')
	AND       (LatitudeMinutes like '%'+@LatitudeMinutes+'%' OR isnull(@LatitudeMinutes,'')='')
	AND       (LatitudeDirection like '%'+@LatitudeDirection+'%' OR isnull(@LatitudeDirection,'')='')
	AND       (Longitude like '%'+@Longitude+'%' OR isnull(@Longitude,'')='')
	AND       (LongitudeDegree like '%'+@LongitudeDegree+'%' OR isnull(@LongitudeDegree,'')='')
	AND       (LongitudeMinutes like '%'+@LongitudeMinutes+'%' OR isnull(@LongitudeMinutes,'')='')
	AND       (LongitudeDirection like '%'+@LongitudeDirection+'%' OR isnull(@LongitudeDirection,'')='')
	AND       (C.CountryName like '%'+@CountryName+'%' OR isnull(@CountryName,'')='')
	AND       (case VP.IsActive when 1 then 'Active' else 'Inactive' end like '%'+@IsActive+'%' OR isnull(@IsActive,'')='')


	SET @sqlquery='SELECT	VP.PortId
			,VP.Code
			,VP.Port 
			,VP.Airport 
			,VP.Un_Locode 
			,VP.TimeZone 
			,VP.Latitude 
			,VP.LatitudeDegree 
			,VP.LatitudeMinutes 
			,VP.LatitudeDirection 
			,VP.Longitude
			,VP.LongitudeDegree 
			,VP.LongitudeMinutes 
			,VP.LongitudeDirection 
			,VP.IsActive
			,VP.CreatedBy
			,VP.CreatedDate
			,VP.ModifiedBy
			,VP.ModifiedDate
			,C.CountryName
		,    '+CONVERT(VARCHAR,@recordsTotal)+' as recordsTotal
		,    '+CONVERT(VARCHAR,@recordsFiltered)+' as recordsFiltered
	FROM tbl_VesselPort AS VP
	INNER JOIN dbo.tbl_Country AS C
		ON	VP.CountryId = C.CountryId
	WHERE     (Code like''%'+CONVERT(VARCHAR,@Code)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@Code,''))+''','''')='''')
	AND       (Port like ''%'+CONVERT(VARCHAR,@Port)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@Port,''))+''','''')='''')
	AND       (Airport like ''%'+CONVERT(VARCHAR,@Airport)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@Airport,''))+''','''')='''')
	AND       (Un_Locode like ''%'+CONVERT(VARCHAR,@Un_Locode)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@Un_Locode,''))+''','''')='''')
	AND       (TimeZone like ''%'+CONVERT(VARCHAR,@TimeZone)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@TimeZone,''))+''','''')='''')
	AND       (Latitude like ''%'+CONVERT(VARCHAR,@Latitude)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@Latitude,''))+''','''')='''')
	AND       (LatitudeDegree like ''%'+CONVERT(VARCHAR,@LatitudeDegree)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@LatitudeDegree,''))+''','''')='''')
	AND       (LatitudeMinutes like ''%'+CONVERT(VARCHAR,@LatitudeMinutes)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@LatitudeMinutes,''))+''','''')='''')
	AND       (LatitudeDirection like ''%'+CONVERT(VARCHAR,@LatitudeDirection)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@LatitudeDirection,''))+''','''')='''')
	AND       (Longitude like ''%'+CONVERT(VARCHAR,@Longitude)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@Longitude,''))+''','''')='''')
	AND       (LongitudeDegree like ''%'+CONVERT(VARCHAR,@LongitudeDegree)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@LongitudeDegree,''))+''','''')='''')
	AND       (LongitudeMinutes like ''%'+CONVERT(VARCHAR,@LongitudeMinutes)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@LongitudeMinutes,''))+''','''')='''')
	AND       (LongitudeDirection like ''%'+CONVERT(VARCHAR,@LongitudeDirection)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@LongitudeDirection,''))+''','''')='''')
	AND       (C.CountryName like ''%'+CONVERT(VARCHAR,@CountryName)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@CountryName,''))+''','''')='''')
	AND       (case VP.IsActive when 1 then ''Active'' else ''Inactive'' end like ''%'+CONVERT(VARCHAR,@IsActive)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@IsActive,''))+''','''')='''')
	ORDER BY  '+@OrderByQuery+' '+ @sortColumnDir +'
	OFFSET     '+CONVERT(varchar,@skip)+' ROWS
	FETCH NEXT '+CONVERT(varchar,@pagesize)+' ROWS ONLY OPTION (RECOMPILE)'

			
exec (@sqlquery)
END
ELSE
BEGIN
    SELECT	   @recordsFiltered=COUNT(*)
	FROM		tbl_VesselPort AS VP
	INNER JOIN  dbo.tbl_Country AS C
	ON			VP.CountryId = C.CountryId
	WHERE     (Code like '%'+@searchValue+'%' 
	OR         Port like '%'+@searchValue+'%'
	OR         Airport like '%'+@searchValue+'%'
	OR         Un_Locode like '%'+@searchValue+'%'
	OR         TimeZone like '%'+@searchValue+'%'
	OR         Latitude like '%'+@searchValue+'%'
	OR         LatitudeDegree like '%'+@searchValue+'%'
	OR         LatitudeMinutes like '%'+@searchValue+'%'
	OR         LatitudeDirection like '%'+@searchValue+'%'
	OR         Longitude like '%'+@searchValue+'%'
	OR         LongitudeDegree like '%'+@searchValue+'%'
	OR         LongitudeMinutes like '%'+@searchValue+'%'
	OR         LongitudeDirection like '%'+@searchValue+'%'
	OR         C.CountryName  like '%'+@searchValue+'%'
	OR         case VP.IsActive when 1 then 'Active' else 'Inactive' end like '%'+@searchValue+'%' )
	AND       (Code like '%'+@Code+'%' OR isnull(@Code,'')='')
	AND       (Port like '%'+@Port+'%' OR isnull(@Port,'')='')
	AND       (Airport like '%'+@Airport+'%' OR isnull(@Airport,'')='')
	AND       (Un_Locode like '%'+@Un_Locode+'%' OR isnull(@Un_Locode,'')='')
	AND       (TimeZone like '%'+@TimeZone+'%' OR isnull(@TimeZone,'')='')
	AND       (Latitude like '%'+@Latitude+'%' OR isnull(@Latitude,'')='')
	AND       (LatitudeDegree like '%'+@LatitudeDegree+'%' OR isnull(@LatitudeDegree,'')='')
	AND       (LatitudeMinutes like '%'+@LatitudeMinutes+'%' OR isnull(@LatitudeMinutes,'')='')
	AND       (LatitudeDirection like '%'+@LatitudeDirection+'%' OR isnull(@LatitudeDirection,'')='')
	AND       (Longitude like '%'+@Longitude+'%' OR isnull(@Longitude,'')='')
	AND       (LongitudeDegree like '%'+@LongitudeDegree+'%' OR isnull(@LongitudeDegree,'')='')
	AND       (LongitudeMinutes like '%'+@LongitudeMinutes+'%' OR isnull(@LongitudeMinutes,'')='')
	AND       (LongitudeDirection like '%'+@LongitudeDirection+'%' OR isnull(@LongitudeDirection,'')='')
	AND       (C.CountryName like '%'+@CountryName+'%' OR isnull(@CountryName,'')='')
	AND       (case VP.IsActive when 1 then 'Active' else 'Inactive' end like '%'+@IsActive+'%' OR isnull(@IsActive,'')='')



		SET @sqlquery='SELECT	VP.PortId
			,VP.Code
			,VP.Port 
			,VP.Airport 
			,VP.Un_Locode 
			,VP.TimeZone 
			,VP.Latitude 
			,VP.LatitudeDegree 
			,VP.LatitudeMinutes 
			,VP.LatitudeDirection 
			,VP.Longitude
			,VP.LongitudeDegree 
			,VP.LongitudeMinutes 
			,VP.LongitudeDirection 
			,VP.IsActive
			,VP.CreatedBy
			,VP.CreatedDate
			,VP.ModifiedBy
			,VP.ModifiedDate
			,C.CountryName
		,    '+CONVERT(VARCHAR,@recordsTotal)+' as recordsTotal
		,    '+CONVERT(VARCHAR,@recordsFiltered)+' as recordsFiltered
	FROM tbl_VesselPort AS VP
	INNER JOIN dbo.tbl_Country AS C
		ON	VP.CountryId = C.CountryId
	 WHERE     (Code like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
	OR         Port like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
	OR         Airport like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
	OR         Un_Locode like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
	OR         TimeZone like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
	OR         Latitude like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
	OR         LatitudeDegree like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
	OR         LatitudeMinutes like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
	OR         LatitudeDirection like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
	OR         Longitude like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
	OR         LongitudeDegree like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
	OR         LongitudeMinutes like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
	OR         LongitudeDirection like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
	OR         C.CountryName  like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
	 OR         CASE VP.IsActive when 1 then ''Active'' else ''Inactive'' end like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' )
     AND       (Code like''%'+CONVERT(VARCHAR,@Code)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@Code,''))+''','''')='''')
	AND       (Port like ''%'+CONVERT(VARCHAR,@Port)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@Port,''))+''','''')='''')
	AND       (Airport like ''%'+CONVERT(VARCHAR,@Airport)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@Airport,''))+''','''')='''')
	AND       (Un_Locode like ''%'+CONVERT(VARCHAR,@Un_Locode)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@Un_Locode,''))+''','''')='''')
	AND       (TimeZone like ''%'+CONVERT(VARCHAR,@TimeZone)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@TimeZone,''))+''','''')='''')
	AND       (Latitude like ''%'+CONVERT(VARCHAR,@Latitude)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@Latitude,''))+''','''')='''')
	AND       (LatitudeDegree like ''%'+CONVERT(VARCHAR,@LatitudeDegree)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@LatitudeDegree,''))+''','''')='''')
	AND       (LatitudeMinutes like ''%'+CONVERT(VARCHAR,@LatitudeMinutes)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@LatitudeMinutes,''))+''','''')='''')
	AND       (LatitudeDirection like ''%'+CONVERT(VARCHAR,@LatitudeDirection)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@LatitudeDirection,''))+''','''')='''')
	AND       (Longitude like ''%'+CONVERT(VARCHAR,@Longitude)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@Longitude,''))+''','''')='''')
	AND       (LongitudeDegree like ''%'+CONVERT(VARCHAR,@LongitudeDegree)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@LongitudeDegree,''))+''','''')='''')
	AND       (LongitudeMinutes like ''%'+CONVERT(VARCHAR,@LongitudeMinutes)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@LongitudeMinutes,''))+''','''')='''')
	AND       (LongitudeDirection like ''%'+CONVERT(VARCHAR,@LongitudeDirection)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@LongitudeDirection,''))+''','''')='''')
	AND       (C.CountryName like ''%'+CONVERT(VARCHAR,@CountryName)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@CountryName,''))+''','''')='''')
	AND       (case VP.IsActive when 1 then ''Active'' else ''Inactive'' end like ''%'+CONVERT(VARCHAR,@IsActive)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@IsActive,''))+''','''')='''')
	ORDER BY  '+@OrderByQuery+' '+ @sortColumnDir +'
	OFFSET     '+CONVERT(varchar,@skip)+' ROWS
	FETCH NEXT '+CONVERT(varchar,@pagesize)+' ROWS ONLY OPTION (RECOMPILE)'
	
	exec (@sqlquery)
END
END	
