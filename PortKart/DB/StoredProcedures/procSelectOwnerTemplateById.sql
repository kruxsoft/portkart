﻿create PROCEDURE [dbo].[procSelectOwnerTemplateById]
	(
@OwnerTemplateId int
)

AS
BEGIN
	SELECT	RMM.OwnerTemplateId
		,	RMM.OwnerId
		,	RMM.TemplateId
		
		,	RMM.StartDate
		,	RMM.EndDate
		,   RMM.IsActive
		,	CONVERT(varchar, StartDate,105) As StartDateAsString
		,	CONVERT(varchar, EndDate,105) As EndDateAsString
		,	RMM.CreatedBy
		,	RMM.CreatedDate
		,	M.TemplateName
		,	R.UserGroup
		

		,	R.FirstName +  '' + R.LastName as Owner
	FROM	dbo.tbl_OwnerTemplateMapping AS RMM
	INNER JOIN dbo.tbl_ServiceTemplate AS M
		ON	RMM.TemplateId = M.TemplateId
	
	INNER JOIN dbo.tbl_User AS R
		ON	RMM.OwnerId = R.UserId 	
	WHERE RMM.OwnerTemplateId = @OwnerTemplateId 
END