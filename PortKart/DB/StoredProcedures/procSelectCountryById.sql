﻿CREATE PROCEDURE [dbo].[procSelectCountryById]
	(
@countryId int

)

AS
BEGIN
	SELECT
			CountryId
			,CountryName
			,IsActive
	FROM	dbo.tbl_Country 
	
	WHERE CountryId = @countryId
END