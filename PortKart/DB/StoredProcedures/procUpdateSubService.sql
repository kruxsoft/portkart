﻿CREATE PROCEDURE [dbo].[procUpdateSubService]
  (
	@parentServiceID int,
    @option     char(1),
    @sort    int,
    @serviceId        int,    
    @createdBy        smallint,
    @createdDate datetime
 )
AS
BEGIN    

 
 UPDATE tbl_SubService
 SET Sort=@sort,
 [option]=@option,
 CreatedBy=@createdBy,
 CreatedDate=GETDATE()
 WHERE (ServiceId=@serviceId AND ParentServiceId=@parentServiceID)

 UPDATE tbl_Service
 SET ModifiedBy=@createdBy,
  ModifiedDate=GETDATE()
  WHERE ServiceId=@parentServiceID

 

        IF @@ROWCOUNT > 0
        BEGIN
            SELECT    'SUCCESS' AS [MessageType], 'Sub Service updated successfully.' AS [Message]
        END
        ELSE
        BEGIN
            SELECT    'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
        END
    
END