﻿


CREATE PROCEDURE [dbo].[procUpdateCurrency]
	(
	@currency	varchar(100),
	@code	varchar(20),
	@modifiedBy			int,	
	@currencyId		int,
	@isActive           bit
)
AS
BEGIN
IF EXISTS    (SELECT    1
                 FROM    tbl_Currency
                 Where CurrencyId = @currencyId and IsActive = 1 and @isActive=0)
             
    BEGIN
        SELECT    'ERROR' AS MessageType, 'Cannot deactivate this currency' AS Message;
		return
    END
	IF EXISTS	(SELECT	1
				 FROM	dbo.tbl_Currency
				 WHERE	(Code = @code
							OR	Currency = @currency)
					AND	CurrencyId <> @currencyId
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'Currency with that name or code already exists.' AS [Message]
	END
	ELSE
	BEGIN
		UPDATE  tbl_Currency 
		SET		Currency = @currency
			,	Code = @code
			,	ModifiedBy = @modifiedBy
			,	IsActive = @isActive
			,	ModifiedDate = GETDATE()
		WHERE	CurrencyId = @currencyId

		IF @@ROWCOUNT > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Currency updated successfully.' AS [Message]
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
		END
	END
END


