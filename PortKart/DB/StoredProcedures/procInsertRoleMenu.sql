-- =============================================
-- Author:		Gopika Vipin
-- Create date: 21-10-2019
-- Description:	To map a role to menu
-- =============================================
CREATE PROCEDURE [dbo].[procInsertRoleMenu]
(
	@menuId			smallint,
	@roleId			smallint,
	@createdBy		smallint	
 )
AS
BEGIN	
	IF EXISTS	(SELECT	1
				 FROM	dbo.tbl_RoleMenuMapping
				 WHERE	RoleId = @roleId
				    AND MenuId = @menuId
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'Role Menu mapping already exists.' AS [Message]
	END
	ELSE
	BEGIN
		INSERT INTO tbl_RoleMenuMapping	(
				RoleId
			,	MenuId
			,	CreatedBy
			,	CreatedDate)
			VALUES	(
				@roleId
			,	@menuId
			,	@createdBy
			,	GETDATE())

		IF @@ROWCOUNT > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Role menu created successfully.' AS [Message]
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
		END
	END
END