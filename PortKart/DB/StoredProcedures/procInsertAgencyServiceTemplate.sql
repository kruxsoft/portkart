﻿CREATE PROCEDURE [dbo].[procInsertAgencyServiceTemplate](
	
@AgencyId int,
@CreatedBy int,
@TemplateName nvarchar(100),
@OutTemplateId int output
)
AS
BEGIN
	DECLARE @TemplateId  int
	DECLARE @RowCount int

	IF EXISTS	(SELECT	1
				 FROM	dbo.tbl_ServiceTemplate
				 WHERE	TemplateName = @TemplateName
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'Template with that name already exists.' AS [Message]
	END

	ELSE

	INSERT INTO tbl_ServiceTemplate(AgencyId
							     ,CreatedBy
								 ,TemplateName
								 ,CreatedDate
								 ,IsActive)
				VALUES	(@AgencyId
				,@CreatedBy
				,@TemplateName
				,GETDATE()
				,1)

		SET @OutTemplateId=@@IDENTITY
		SET @RowCount=@@ROWCOUNT
		IF @RowCount > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Agency service template saved successfully.' AS [Message]			
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the service template details were not saved.' AS [Message]
	END
END
