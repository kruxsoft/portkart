﻿CREATE PROCEDURE [dbo].[procSelectOwnerListForAgency]
@search varchar(500)
	AS
BEGIN
Declare @limit varchar(100)
SET @limit='';
IF(Select LEN(isnull(@search,'')))<2
BEGIN
SET @limit='Top 100'
END
	EXEC('SELECT '+@limit+'	U.UserId
		,	U.FirstName
		,	U.LastName
		,	U.Password
		,	U.EmailId
		,	U.MobileNumber
		,	U.CountryCode
		,	U.CompanyName
		,	U.CompanyRegNo
		,	U.UserGroup
		,	U.DateOfBirth
		, 	U.CreatedBy
		,	U.CreatedDate
		,	U.UserStatusID	
		,	U.EmailVerified
		,	US.UserStatus
	FROM tbl_User U 
	INNER JOIN [dbo].[tbl_UserStatus] US
		ON	US.UserStatusId=U.UserStatusId
	WHERE U.UserStatusId=1
	AND  U.UserGroup=''Owner''
	AND (U.FirstName like ''%'+@search+'%'' OR U.LastName like ''%'+@search+'%'' OR U.EmailId like ''%'+@search+'%''  OR isnull('''+@search+''','''')='''')')

	
END