﻿CREATE PROCEDURE [dbo].[procSelectAgencyServiceDetailsForOwner]
AS
BEGIN
	SET NOCOUNT ON;
	SELECT	AC.AgencyDetailId
		,	AC.AgencyServiceId
		,	AC.ServiceId
		,	AC.IsParent
		,	AC.CurrencyId
		,	AC.Amount
		,	AC.ActualAmount
		,	AC.ParentId
		,	C.Currency
		,	C.Code
		,	S.ServiceCode
		,	S.ServiceName
		,	S.CostType
		,	S.Description
		,	SS.[Option]
	FROM	dbo.tbl_AgencyServiceDetails AS AC
	INNER JOIN dbo.tbl_Service AS S
		ON	S.ServiceId = AC.ServiceId
	INNER JOIN dbo.tbl_ServiceCategory AS SC
	ON	SC.ServiceCategoryId = S.ServiceCategoryID
	Left JOIN dbo.tbl_SubService AS SS
		ON	SS.ServiceId = AC.ServiceId
		AND AC.IsParent<>1
	INNER JOIN dbo.tbl_Currency AS C
		ON	C.CurrencyId = AC.CurrencyId

END
