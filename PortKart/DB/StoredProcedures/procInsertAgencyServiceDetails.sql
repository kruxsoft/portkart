﻿CREATE PROCEDURE [dbo].[procInsertAgencyServiceDetails](
	@AgencyServiceId int,
	@ServiceId int,
	@CurrencyId int,
	@Amount decimal (30,2),
	@Tat int,
	@Description nvarchar(2000),
	@IsParent bit,
	@IsActive bit,
	@TatDescription nvarchar(500),
	@CostType varchar(50),
	@Option char(1),
	@ParentId int,
	@CreatedBy int)
AS
BEGIN
	
	INSERT INTO tbl_AgencyServiceDetails(AgencyServiceId
										,ServiceId
										,CurrencyId
										,Amount
										,ActualAmount
										,CreatedBy
										,CreatedDate
										,IsParent
										,IsActive
										,TatDescription
										,CostType
										,[Option]
										,ParentId
										,Tat
										,Description)
							VALUES (@AgencyServiceId
							,@ServiceId
							,@CurrencyId
							,@Amount
							,@Amount
							,@CreatedBy
							,GETDATE()
							,@IsParent
							,@IsActive
							,@TatDescription
							,@CostType
							,@Option
							,@ParentId
							,@Tat
							,@Description)
	IF @@ROWCOUNT > 0
	BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Agency service saved successfully.' AS [Message]			
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the service details were not saved.' AS [Message]
	END
END