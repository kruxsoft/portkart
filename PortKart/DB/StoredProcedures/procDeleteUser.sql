-- =============================================
-- Author:		Prageeth Rajan
-- Create date: 28-10-2019
-- Description:	To delete a user
-- =============================================
CREATE PROCEDURE [dbo].[procDeleteUser]
(
	@UserId int
,	@ModifiedBy smallint
)
AS
BEGIN	
	UPDATE  tbl_User 
	SET		UserStatusId = 2
		,	ModifiedBy = @ModifiedBy
		,	ModifiedDate = GETDATE()
	WHERE	UserId = @UserId
	IF @@ROWCOUNT > 0
	BEGIN
		SELECT	'SUCCESS' AS [MessageType], 'User deactivated successfully.' AS [Message]
	END
	ELSE
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
	END
END
