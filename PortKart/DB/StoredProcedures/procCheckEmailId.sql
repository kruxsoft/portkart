﻿CREATE PROCEDURE [dbo].[procCheckEmailId]
	@EmailId nvarchar(255)
AS
BEGIN 
IF EXISTS (SELECT 1 from tbl_user where EmailId=@EmailId)
	BEGIN
		SELECT	'SUCCESS' AS [MessageType], 'User Exists' AS [Message]			
	END
	ELSE
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'User with given email address does not exist' AS [Message]
	END
END