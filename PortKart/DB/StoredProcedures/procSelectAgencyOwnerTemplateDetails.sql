﻿CREATE PROCEDURE [dbo].[procSelectAgencyOwnerTemplateDetails]
	@TemplateId int
AS
BEGIN
	SELECT  TemplateDetailId
			,TemplateId
			,ServiceId
			,CurrencyId
			,Type
			,Offer
			,IsParent
			,ParentID
	FROM tbl_ServiceTemplateDetails 
	WHERE TemplateId=@TemplateId
	

END