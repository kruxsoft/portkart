﻿CREATE PROCEDURE [dbo].[procInsertAgencyService](
@PortId int,
@AgencyId int,
@CreatedBy int,
@OutAgencyServiceId int output
)
AS
BEGIN
	DECLARE @AgencyServiceId  int
	DECLARE @RowCount int

	INSERT INTO tbl_AgencyService(AgencyId
							     ,PortId
								 ,CreatedBy
								 ,CreatedDate
								 ,IsActive)
				VALUES	(@AgencyId,@PortId,@CreatedBy,GETDATE(),1)

		SET @OutAgencyServiceId=@@IDENTITY
		SET @RowCount=@@ROWCOUNT
		IF @RowCount > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Agency service saved successfully.' AS [Message]			
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the service details were not saved.' AS [Message]
	END
END
