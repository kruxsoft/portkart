﻿CREATE PROCEDURE [dbo].[procSelectUsersServerSide]
@pageSize int,
@skip int,
@sortColumn varchar(50),
@sortColumnDir varchar(10),
@searchValue varchar(100),
@FirstName varchar(100),
@LastName varchar(100),
@EmailId varchar(100),
@UserGroup varchar(100),
@CompanyName varchar(100),
@UserStatus varchar(100)
AS
BEGIN

 

DECLARE @recordsTotal int
DECLARE @recordsFiltered int
DECLARE @sqlquery varchar(5000)
---------------getting total count-------------------------------------------------
    SELECT     @recordsTotal=Count(*)
    FROM       tbl_User U
    INNER JOIN tbl_UserStatus US
	ON	       US.UserStatusId=U.UserStatusId
 


------------------------------------------------------------------------------------------------------------------------
IF isnull(@searchValue,'')=''
BEGIN
   SELECT       @recordsFiltered=COUNT(*)
    FROM        tbl_User U
    INNER JOIN  tbl_UserStatus US
	ON	        US.UserStatusId=U.UserStatusId
    WHERE      (FirstName like '%'+@FirstName+'%' OR isnull(@FirstName,'')='')
    AND        (LastName like '%'+@LastName+'%' OR isnull(@LastName,'')='')
    AND        (EmailId like '%'+@EmailId+'%' OR isnull(@EmailId,'')='')
    AND        (UserGroup like '%'+@UserGroup+'%' OR isnull(@UserGroup,'')='')
    AND        (CompanyName like '%'+@CompanyName+'%' OR isnull(@CompanyName,'')='')
    AND        (US.UserStatus like '%'+@UserStatus+'%' OR isnull(@UserStatus,'')='')

 

    SET @sqlquery='SELECT    U.UserId
		,	U.FirstName
		,	U.LastName
		,	U.Password
		,	U.EmailId
		,	U.MobileNumber
		,	U.CountryCode
		,	U.CompanyName
		,	U.CompanyRegNo
		,	U.UserGroup
		,	U.DateOfBirth
		, 	U.CreatedBy
		,	U.CreatedDate
		,	U.UserStatusID	
		,	U.EmailVerified
		,	US.UserStatus
        ,   '+CONVERT(VARCHAR,@recordsTotal)+' as recordsTotal
        ,   '+CONVERT(VARCHAR,@recordsFiltered)+' as recordsFiltered
    FROM       tbl_User U 
	INNER JOIN [dbo].[tbl_UserStatus] US
	ON	       US.UserStatusId=U.UserStatusId
    WHERE     (FirstName like ''%'+CONVERT(VARCHAR,@FirstName)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@FirstName,''))+''','''')='''')
    AND       (LastName like ''%'+CONVERT(VARCHAR,@LastName)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@LastName,''))+''','''')='''')
    AND       (EmailId like ''%'+CONVERT(VARCHAR,@EmailId)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@EmailId,''))+''','''')='''')
    AND       (UserGroup like ''%'+CONVERT(VARCHAR,@UserGroup)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@UserGroup,''))+''','''')='''')
    AND       (CompanyName like ''%'+CONVERT(VARCHAR,@CompanyName)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@CompanyName,''))+''','''')='''')
    AND       (US.UserStatus like ''%'+CONVERT(VARCHAR,@UserStatus)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@UserStatus,''))+''','''')='''')
    ORDER BY  (CASE '''+CONVERT(VARCHAR,@sortColumn)+''' 
                               WHEN ''FirstName'' THEN FirstName
                               WHEN ''LastName'' THEN LastName
                               WHEN ''EmailId'' THEN EmailId
                               WHEN ''UserGroup'' THEN UserGroup
                               WHEN ''CompanyName'' THEN CompanyName
                               WHEN ''UserStatus'' THEN US.UserStatus
                               ELSE FirstName  END) '+ @sortColumnDir +'
    OFFSET     '+CONVERT(varchar,@skip)+' ROWS
    FETCH NEXT '+CONVERT(varchar,@pagesize)+' ROWS ONLY OPTION (RECOMPILE)'

 

            
exec (@sqlquery)
END
ELSE
BEGIN
    SELECT      @recordsFiltered=COUNT(*)
    FROM        tbl_User U
    INNER JOIN  tbl_UserStatus US
	ON	        US.UserStatusId=U.UserStatusId
    WHERE      (FirstName like '%'+@searchValue+'%'
    OR          LastName like '%'+@searchValue+'%'
    OR          EmailId like '%'+@searchValue+'%'
    OR          UserGroup like '%'+@searchValue+'%'
    OR          CompanyName  like '%'+@searchValue+'%'
    OR          US.UserStatus like '%'+@searchValue+'%')
    AND        (FirstName like '%'+@FirstName+'%' OR isnull(@FirstName,'')='')
    AND        (LastName like '%'+@LastName+'%' OR isnull(@LastName,'')='')
    AND        (EmailId like '%'+@EmailId+'%' OR isnull(@EmailId,'')='')
    AND        (UserGroup like '%'+@UserGroup+'%' OR isnull(@UserGroup,'')='')
    AND        (CompanyName like '%'+@CompanyName+'%' OR isnull(@CompanyName,'')='')
    AND        (US.UserStatus like '%'+@UserStatus+'%' OR isnull(@UserStatus,'')='')
 


    SET @sqlquery='SELECT    U.UserId
		,	U.FirstName
		,	U.LastName
		,	U.Password
		,	U.EmailId
		,	U.MobileNumber
		,	U.CountryCode
		,	U.CompanyName
		,	U.CompanyRegNo
		,	U.UserGroup
		,	U.DateOfBirth
		, 	U.CreatedBy
		,	U.CreatedDate
		,	U.UserStatusID	
		,	U.EmailVerified
		,	US.UserStatus
        ,   '+CONVERT(VARCHAR,@recordsTotal)+' as recordsTotal
        ,   '+CONVERT(VARCHAR,@recordsFiltered)+' as recordsFiltered
    FROM       tbl_User U 
	INNER JOIN [dbo].[tbl_UserStatus] US
	ON	       US.UserStatusId=U.UserStatusId
    WHERE     (FirstName like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
    OR         LastName like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
    OR         EmailId like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
    OR         UserGroup like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
    OR         CompanyName like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
    OR         US.UserStatus like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' )
    AND       (FirstName like ''%'+CONVERT(VARCHAR,@FirstName)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@FirstName,''))+''','''')='''')
    AND       (LastName like ''%'+CONVERT(VARCHAR,@LastName)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@LastName,''))+''','''')='''')
    AND       (EmailId like ''%'+CONVERT(VARCHAR,@EmailId)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@EmailId,''))+''','''')='''')
    AND       (UserGroup like ''%'+CONVERT(VARCHAR,@UserGroup)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@UserGroup,''))+''','''')='''')
    AND       (CompanyName like ''%'+CONVERT(VARCHAR,@CompanyName)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@CompanyName,''))+''','''')='''')
    AND       (US.UserStatus like ''%'+CONVERT(VARCHAR,@UserStatus)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@UserStatus,''))+''','''')='''')
    ORDER BY  (CASE '''+CONVERT(VARCHAR,@sortColumn)+''' 
                               WHEN ''FirstName'' THEN FirstName
                               WHEN ''LastName'' THEN LastName
                               WHEN ''EmailId'' THEN EmailId
                               WHEN ''UserGroup'' THEN UserGroup
                               WHEN ''CompanyName'' THEN CompanyName
                               WHEN ''UserStatus'' THEN US.UserStatus
                               ELSE FirstName  END) '+ @sortColumnDir +'
    OFFSET     '+CONVERT(varchar,@skip)+' ROWS
    FETCH NEXT '+CONVERT(varchar,@pagesize)+' ROWS ONLY OPTION (RECOMPILE)'

 

    exec (@sqlquery)
END
END
