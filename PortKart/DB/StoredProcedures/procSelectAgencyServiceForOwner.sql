﻿CREATE PROCEDURE [dbo].[procSelectAgencyServiceForOwner]
AS
BEGIN
	SET NOCOUNT ON;
	SELECT	AC.AgencyId
		,	AC.AgencyServiceId
		,	C.CompanyName
		,	C.About
		,	C.LogoPath
		,	AC.PortId
		,	P.Port
		,	R.Rating
	FROM	dbo.tbl_AgencyService AS AC
	INNER JOIN dbo.tbl_User AS A
		ON	A.UserId = AC.AgencyId
	INNER JOIN dbo.tbl_VesselPort AS P
		ON	P.PortId = AC.PortId
	INNER JOIN dbo.tbl_UserCompanyMapping AS M
	ON	M.UserId = AC.AgencyId
	INNER JOIN dbo.tbl_Company AS C
	ON	M.CompanyId = C.CompanyId
	Left JOIN dbo.tbl_AgencyRating AS R
	ON	R.CompanyId = C.CompanyId
	WHERE AC.IsActive=1
END
