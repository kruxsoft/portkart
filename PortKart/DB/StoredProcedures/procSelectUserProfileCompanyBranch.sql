﻿CREATE PROCEDURE [dbo].[procSelectUserProfileCompanyBranch]
@UserId int
AS
BEGIN
	SELECT   S.BranchName
			,S.BranchId
			,S.CountryCode
			,S.Phone
			,S.CompanyId
			,S.CountryId
			,S.Address
			,S.ContactPerson
			,S.CP_CountryCode
			,S.CP_Phone
			,S.Email
			,S.Website	
			,S.IsPreview
	FROM tbl_CompanyBranches S
	INNER JOIN tbl_UserCompanyMapping M
	ON S.CompanyId=M.CompanyId
	WHERE M.UserId=@UserId
END
