-- =============================================
-- Author:		Gopika Vipin
-- Create date: 21-10-2019
-- Description:	To add new state
-- =============================================
CREATE PROCEDURE [dbo].[procInsertState]
(
	@stateName nvarchar(50),
	@countryID tinyint
 )
AS
BEGIN
	INSERT INTO tbl_State	(
		StateName
	,	CountryID	) 
	VALUES	(
		@stateName
	,	@countryID)
END
