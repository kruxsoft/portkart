-- =============================================
-- Author:		Gopika Vipin
-- Create date: 21-10-2019
-- Description:	To add new role
-- =============================================
CREATE PROCEDURE [dbo].[procInsertRole]
(
	@roleName	varchar(50),
	@createdBy	int
)
AS
BEGIN
	
	IF EXISTS	(SELECT	1
				 FROM	dbo.tbl_Role
				 WHERE	RoleName = @roleName
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'Role with that name already exists.' AS [Message]
	END
	ELSE
	BEGIN

		INSERT INTO tbl_Role	(
			RoleName
		,	CreatedBy
		,	CreatedDate
		,	IsActive	)
		VALUES	(
			@roleName
		,	@createdBy
		,	GETDATE()
		,	'1')

		IF @@ROWCOUNT > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Role created successfully.' AS [Message]
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
		END
	END
END
