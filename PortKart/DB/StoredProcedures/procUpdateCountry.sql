﻿CREATE PROCEDURE [dbo].[procUpdateCountry]
	(
	@countryName	 nvarchar(20),
	@countryId int,
	@modifiedBy int,
	@isActive bit
 )
AS
BEGIN	
	IF EXISTS	(SELECT	1
				 FROM	dbo.tbl_Country
				 WHERE	CountryName = @countryName 
				 and CountryId <> @countryId
				   
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'Country already exists.' AS [Message]
		return
	END
	
	ELSE
	BEGIN
		UPDATE  tbl_Country 
		SET		CountryName = @countryName
				,ModifiedBy = @modifiedBy
				,ModifiedDate = GETDATE()
				,IsActive = @isActive
		WHERE	CountryId = @countryId
			

		IF @@ROWCOUNT > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Country updated successfully.' AS [Message]
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
		END
	END
END