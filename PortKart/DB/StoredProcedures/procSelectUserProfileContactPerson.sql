﻿CREATE PROCEDURE [dbo].[procSelectUserProfileContactPerson]
@UserId int
AS
BEGIN
	SELECT S.Name,S.ContactPersonId,S.CountryCode,S.PhoneNo,S.CompanyId,S.IsPreview
	FROM tbl_ContactPerson S
	INNER JOIN tbl_UserCompanyMapping M
	ON S.CompanyId=M.CompanyId
	WHERE M.UserId=@UserId
END
