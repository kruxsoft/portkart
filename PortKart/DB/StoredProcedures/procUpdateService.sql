﻿CREATE PROCEDURE [dbo].[procUpdateService]
(
	@serviceCode     nvarchar(20),
	@serviceName	 nvarchar(20),
	@serviceCategoryId		xml,
	@modifiedBy		int	,
	@serviceId int,
	@isActive bit,
	@isGroup bit,
	@costType varchar(50),
	@description nvarchar(2000)
 )
AS
BEGIN	
declare @rowcount int
declare @hdoc int 
declare @oldgroup bit 

	IF isnull(@isActive,0)=0
	BEGIN
		If EXISTS (SELECT 1 from tbl_AgencyServiceDetails  where ServiceId=@serviceId)
		BEGIN
		SELECT	'ERROR' AS [MessageType], 'Cannot deactivate this service, it is added against a port' AS [Message]
		return
		END
	END
	
	SELECT @oldgroup=Isgroup from tbl_Service where ServiceId=@serviceId
	
	IF isnull(@oldgroup,0)<>isnull(@isGroup,0)
	BEGIN
	If EXISTS (SELECT 1 from tbl_AgencyServiceDetails  where ServiceId=@serviceId)
		BEGIN
		SELECT	'ERROR' AS [MessageType], 'Cannot change group of this service, it is added against a port' AS [Message]
		return
		END
	END
exec sp_xml_preparedocument @hdoc output, @serviceCategoryId
	IF EXISTS	(SELECT	1
				 FROM	dbo.tbl_Service
				 WHERE	serviceCode = @serviceCode 
				 and ServiceId <> @serviceId
				   
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'Service Code already exists.' AS [Message]
		return
	END
	IF EXISTS	(SELECT	1
				 FROM	dbo.tbl_Service
				 WHERE	(ServiceName = @serviceName ) 
				 and ServiceId <> @serviceId
				   
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'Service Name already exists.' AS [Message]
	END
	ELSE
	BEGIN
	if isnull(@isGroup,0)=1
	begin
	if exists (select 1 from tbl_SubService where serviceid=@serviceId)
	begin
	SELECT	'ERROR' AS [MessageType], 'This service is a sub service, so it cannot be changed to Group Service' AS [Message]
	return
	end
	end
		UPDATE  tbl_Service 
		SET		ServiceName = @serviceName
			,	ServiceCode = @serviceCode
			,	IsGroup = @isGroup
			,	CostType = @costType
			,	Description = @description
			,	ModifiedBy = @modifiedBy
			,	IsActive = @isActive
			,	ModifiedDate = GETDATE()
		WHERE	ServiceId = @serviceId
		Delete tbl_Service_SCategoryMapping Where ServiceId=@serviceId
		
		Insert into tbl_Service_SCategoryMapping(ServiceId,ServiceCategoryId,CreatedBy,CreatedDate)
			Select @ServiceId,SC.ServiceCategoryId,@modifiedBy,GETDATE() from OpenXML(@hdoc, 'Servicategorylist/ServiceCategoryId')
			WITH (ServiceCategoryId int '.') SC

		IF @@ROWCOUNT > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Service updated successfully.' AS [Message]
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
		END
	END
END