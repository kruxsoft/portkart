﻿CREATE PROCEDURE [dbo].[procSelectServiceCategoryByServiceId]
	(
@serviceId int

)

AS
BEGIN
	SELECT	ServiceCategoryId
			
	FROM	tbl_Service_SCategoryMapping 
	
	WHERE ServiceId = @serviceId
END