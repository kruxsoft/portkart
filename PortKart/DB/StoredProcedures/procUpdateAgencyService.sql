﻿CREATE PROCEDURE [dbo].[procUpdateAgencyService](
@PortId int,
@AgencyId int,
@CreatedBy int,
@AgencyServiceId int 
)
AS
BEGIN
	DECLARE @RowCount int

	UPDATE tbl_AgencyService
	SET ModifiedBy=@CreatedBy,
		ModifiedDate=GETDATE()
	WHERE AgencyServiceId=@AgencyServiceId

	

		SET @RowCount=@@ROWCOUNT
		IF @RowCount > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Agency service saved successfully.' AS [Message]			
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the service details were not saved.' AS [Message]
	END
END
