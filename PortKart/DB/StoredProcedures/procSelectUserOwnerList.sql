﻿Create PROCEDURE [dbo].[procSelectUserOwnerList]
    AS
BEGIN
     SET NOCOUNT ON;
    SELECT    U.UserId,
    U.FirstName,
    U.LastName,
    U.EmailId,
    UST.UserStatus
               
    FROM tbl_User as U INNER JOIN tbl_UserStatus as UST
    ON U.UserStatusId = UST.UserStatusId
    where U.UserGroup='Owner' and UST.UserStatus='Active'
end   