﻿-- =============================================
-- Author:		Anurag P.G
-- Create date: 08-12-2020
-- Description:	To get all list of User
-- =============================================
CREATE PROCEDURE [dbo].[procSelectUserList]
	AS
BEGIN
     SET NOCOUNT ON;
	SELECT	UserId
	,		EmailId
	FROM tbl_User
	END