﻿create PROCEDURE [dbo].[procSelectAllOwnerTemplates]
AS
BEGIN
	SET NOCOUNT ON;
	SELECT	RMM.OwnerTemplateId
		,	RMM.OwnerId
		,	RMM.TemplateId
		
		,	RMM.StartDate
		,	RMM.EndDate
		,   RMM.IsActive
		,	CONVERT(varchar, StartDate,105) As StartDateAsString
		,	CONVERT(varchar, EndDate,105) As EndDateAsString
		,	RMM.CreatedBy
		,	RMM.CreatedDate
		,	M.TemplateName
		,	R.UserGroup
		,	isnull(VP.Port,'') Port
		,	R.FirstName +  '' + R.LastName as Owner
	FROM	dbo.tbl_OwnerTemplateMapping AS RMM
	INNER JOIN dbo.tbl_ServiceTemplate AS M
		ON	RMM.TemplateId = M.TemplateId
	Left JOIN (SELECT  STUFF((SELECT  ',' + Port
								FROM tbl_Port_OwnerMapping EE
								INNER JOIN tbl_VesselPort M
								ON    M.PortId=EE.PortId
								WHERE   EE.OwnerTemplateId=E.OwnerTemplateId
								FOR XML PATH('')), 1, 1, '') AS Port,E.OwnerTemplateId
				FROM tbl_Port_OwnerMapping E
				INNER JOIN tbl_VesselPort EM
				ON    EM.PortId=E.PortId
				GROUP BY E.OwnerTemplateId )VP
	ON VP.OwnerTemplateId=RMM.OwnerTemplateId
	INNER JOIN dbo.tbl_User AS R
	ON	RMM.OwnerId = R.UserId where R.UserGroup ='Owner'
END