﻿CREATE PROCEDURE [dbo].[procUpdateTemplate](
	@templateId int,
	@agencyId int
)
AS
BEGIN
IF EXISTS    (SELECT    1
                 FROM   tbl_OwnerTemplateMapping
                 Where TemplateId = @templateId and IsActive = 1 )
             
    BEGIN
        SELECT    'ERROR' AS MessageType, 'Cannot update this template' AS Message;
		return
    END
	
	ELSE
	BEGIN
		UPDATE  tbl_ServiceTemplate 
		SET		
				ModifiedDate = GETDATE()
		WHERE	TemplateId = @templateId

		IF @@ROWCOUNT > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Template  details updated successfully.' AS [Message]
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
		END
	END
END

