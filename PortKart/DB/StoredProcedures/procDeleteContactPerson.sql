﻿CREATE PROCEDURE [dbo].[procDeleteContactPerson]
	@UserId int ,
	@ContactPersonId int
AS
Begin
	Delete CP
		FROM tbl_ContactPerson CP
		INNER JOIN tbl_UserCompanyMapping M
	    ON CP.CompanyId=M.CompanyId
	    WHERE M.UserId=@UserId
		AND CP.ContactPersonId =@ContactPersonId

		 IF @@ROWCOUNT > 0
    BEGIN
        SELECT    'SUCCESS' AS [MessageType], 'Contact Person deleted successfully.' AS [Message]
    END
    ELSE
    BEGIN
        SELECT    'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
    END
    
END
