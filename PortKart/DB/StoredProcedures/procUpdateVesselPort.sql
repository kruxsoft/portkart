﻿CREATE PROCEDURE [dbo].[procUpdateVesselPort]
	(
	@portId int,
	@code nvarchar (255),
	@port	nvarchar (255),
	@airport nvarchar (255),
    @un_Locode nvarchar (255),
    @timeZone nvarchar (255),
    @latitude decimal,
    @latitudeDegree decimal,
    @latitudeMinutes decimal,
    @latitudeDirection varchar(255),
    @longitude decimal,
    @longitudeDegree decimal,
    @longitudeMinutes decimal,
    @longitudeDirection varchar(255),
    @modifiedBy int,
	@isActive bit,
	@countryId int
)
AS
BEGIN

	IF EXISTS	(SELECT	1
				 FROM	dbo.tbl_VesselPort
				 WHERE	([Port] = @port
							OR	Code = @code)
					AND	PortId <> @portId
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'Vessel port with that name or code code already exists.' AS [Message]
	END
	ELSE
	BEGIN
		UPDATE  tbl_VesselPort 
		SET		Code = @code,
				[Port] =	@port,
				Airport = @airport ,
				Un_Locode = @un_Locode ,
				TimeZone = @timeZone ,
				Latitude = @latitude ,
				LatitudeDegree = @latitudeDegree,
				LatitudeMinutes = @latitudeMinutes ,
				LatitudeDirection = @latitudeDirection,
				Longitude = @longitude ,
				LongitudeDegree = @longitudeDegree,
				LongitudeMinutes = @longitudeMinutes ,
				LongitudeDirection = @longitudeDirection,
				ModifiedBy = @modifiedBy,
				IsActive = @isActive,
				ModifiedDate = GETDATE(),
				CountryId = @countryId
		WHERE	PortId = @portId

		IF @@ROWCOUNT > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Vessel Port details updated successfully.' AS [Message]
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
		END
	END
END
