﻿
CREATE PROCEDURE [dbo].[procDeleteCurrency]
        
(
     @currencyId    int
,    @modifiedBy smallint
)
AS
BEGIN

    UPDATE   tbl_Currency
    SET        IsActive = '0' 
        ,    ModifiedBy = @modifiedby
        ,    ModifiedDate = GETDATE()
    WHERE    CurrencyId= @currencyId

 

    IF @@ROWCOUNT > 0
    BEGIN
        SELECT    'SUCCESS' AS [MessageType], 'Currency deactivated successfully.' AS [Message]
    END
    ELSE
    BEGIN
        SELECT    'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
    END
    END
