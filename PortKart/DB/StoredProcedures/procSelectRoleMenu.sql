-- =============================================
-- Author:		Simi Simon
-- Create date: 29-10-2019
-- Description:	To get all Rolemenus
-- =============================================
CREATE PROCEDURE [dbo].[procSelectRoleMenu]
(
	@roleID	int
)
AS
BEGIN
	SELECT	RMM.RoleId
		,	RMM.MenuId
		,	M.ParentId
		,	M.IsParent
		,	M.HasChildren
		,	M.MenuOrder
		,	RMM.CreatedBy
		,	RMM.CreatedDate
		,	M.MenuName
		,	M.MenuURL
		,	R.RoleName
		,	M.Icon
	FROM	dbo.tbl_RoleMenuMapping AS RMM
	INNER JOIN dbo.tbl_Menu AS M
		ON	RMM.MenuId = M.MenuId
	INNER JOIN dbo.tbl_Role AS R
		ON	RMM.RoleId = R.RoleId
	WHERE	RMM.RoleId = @roleID
	ORDER BY M.MenuOrder ASC
END
