﻿CREATE PROCEDURE [dbo].[procSelectTemplateDetails]

@TemplateId int,
@AgencyId int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT STD.ServiceId
            ,   STD.Offer
            ,   STD.ParentID
            ,   STD.IsParent
            ,   STD.Type
            
            ,   C.Code
            ,   ST.AgencyId
            ,   ST.TemplateId
            ,   ST.TemplateName

    FROM    dbo.tbl_ServiceTemplateDetails AS STD
    INNER JOIN dbo.tbl_ServiceTemplate ST
        ON    ST.TemplateId = STD.TemplateId
        INNER JOIN dbo.tbl_Currency AS C
        On STD.CurrencyId = C.CurrencyId
    WHERE     ST.TemplateId = @TemplateId and ST.AgencyId = @AgencyId

END