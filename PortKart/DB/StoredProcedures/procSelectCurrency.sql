﻿

CREATE PROCEDURE [dbo].[procSelectCurrency]
	AS
BEGIN
	SELECT	CurrencyId
	,		Code
	,		Currency
	,		ModifiedDate 
	,		CONVERT(varchar, ModifiedDate,105) As ModifiedDateAsString
	,       IsActive
	FROM tbl_Currency
END

