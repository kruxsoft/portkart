-- =============================================
-- Author:		Gopika Vipin
-- Create date: 21-10-2019
-- Description:	To add new role to user
-- =============================================
CREATE PROCEDURE [dbo].[procInsertUserRole]
(
	@userId		smallint,
	@roleId		smallint,
	@createdBy	int
)

AS
BEGIN	
	IF EXISTS	(SELECT	1
				 FROM	dbo.tbl_UserRoleMapping
				 WHERE	UserId = @userId
				    --AND RoleId = @roleId
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'This user already has a role assigned.' AS [Message]
	END
	ELSE
	BEGIN

		INSERT INTO tbl_UserRoleMapping	(
		UserId
	,	RoleId
	,	CreatedBy
	,	CreatedDate	)
	VALUES	(
		@userId
	,	@roleId
	,	@createdBy
	,	GETDATE()	)

		IF @@ROWCOUNT > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'User Role created successfully.' AS [Message]
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
		END
	END
END