﻿CREATE PROCEDURE [dbo].[procInsertUserProfile]
(
	@UserId int,
	@CompanyName nvarchar(255),
	@CompanyRegNo nvarchar(255),
	@CountryId int,
	@IndustryTypeId int,
	@ExpiryDate Datetime,
	@City nvarchar(255),
	@Website nvarchar(max),
	@EmailID nvarchar(max),
	@Address nvarchar(1000),
	@CountryCode varchar(15),
	@Phone varchar(18),
	@LogoPath nvarchar(255),
	@AttachmentPath nvarchar(255),
	@CreatedBy int,
	@About nvarchar(1000),
	@OutCompanyId int output
	)
AS
BEGIN

    DECLARE @CompanyId int
	DECLARE @RowCount int
		IF EXISTS  (SELECT 1 FROM tbl_Company where CompanyName=@companyname)
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'Company name already exists.' AS [Message]
			return
		END

	
			INSERT INTO tbl_Company(CompanyName
								,	CompanyRegNo
								,	CountryId
								,	IndustryTypeId
								,ExpiryDate
								,	City
								,	Website
								,EmailID
								,	Address
								,	CountryCode
								,	Phone
								,	LogoPath
								,AttachmentPath
								,	CreatedBy
								,	CreatedDate
								,	About
								,	IsActive)
			VALUES				   (@CompanyName
								,	@CompanyRegNo
								,	@CountryId
								,	@IndustryTypeId
								,@ExpiryDate
								,	@City
								,	@Website
								,@EmailID
								,	@Address
								,	@CountryCode
								,	@Phone
								,	@LogoPath
								,@AttachmentPath
								,	@CreatedBy
								,	GETDATE()
								,	@About
								,	1)
			SET @CompanyId=@@IDENTITY
			SET @OutCompanyId=@CompanyId
		
			INSERT INTO tbl_UserCompanyMapping(UserId,CompanyId,CreatedBy,CreatedDate)
			SELECT @userId,@CompanyId,@CreatedBy,GETDATE()

			SET @RowCount=@@ROWCOUNT

	IF @RowCount > 0
	BEGIN
		SELECT	'SUCCESS' AS [MessageType], 'Profile saved successfully.' AS [Message]			
	END
	ELSE
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
	END
END