-- =============================================
-- Author:		Simi Simon
-- Create date: 29-10-2019
-- Description:	To get all Menus
-- =============================================
CREATE PROCEDURE [dbo].[procSelectMenu]
AS
BEGIN
	SELECT	MenuName
	,		MenuURL
	,		CreatedBy
	,		CreatedDate
	FROM	tbl_Menu 
	WHERE	IsActive='1'
END


