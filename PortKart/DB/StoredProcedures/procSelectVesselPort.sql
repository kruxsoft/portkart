﻿CREATE PROCEDURE [dbo].[procSelectVesselPort]
	AS
BEGIN
	SELECT	VP.PortId
			,VP.Code
			,VP.Port 
			,VP.Airport 
			,VP.Un_Locode 
			,VP.TimeZone 
			,VP.Latitude 
			,VP.LatitudeDegree 
			,VP.LatitudeMinutes 
			,VP.LatitudeDirection 
			,VP.Longitude
			,VP.LongitudeDegree 
			,VP.LongitudeMinutes 
			,VP.LongitudeDirection 
			,VP.IsActive
			,VP.CreatedBy
			,VP.CreatedDate
			,VP.ModifiedBy
			,VP.ModifiedDate
			,C.CountryName
	FROM tbl_VesselPort AS VP
	INNER JOIN dbo.tbl_Country AS C
		ON	VP.CountryId = C.CountryId
	
END
