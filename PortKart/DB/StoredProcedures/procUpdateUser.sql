-- =============================================
-- Author:		Anurag P.G
-- Create date: 09-12-2020
-- Description:	To update a user
-- =============================================
CREATE PROCEDURE [dbo].[procUpdateUser]
(
	@firstName		NVARCHAR(50),
	@lastName		NVARCHAR(50),
	@password		VARCHAR(MAX),
	@emailId		NVARCHAR(254),
	@mobileNumber	VARCHAR(20),
	@companyname	NVARCHAR(150),
	@companyregno	VARCHAR(50),
	@UserGroup	    VARCHAR(20),
	@countrycode    VARCHAR(10),
	@DateOfBirth    datetime,
	@modifiedBy		INT,
	@UserStatusID	TinyInt,
	@userid         INT
)
AS
BEGIN
	DECLARE @OldStatusID TinyInt
	DECLARE @isEmailverified bit
	DECLARE @OldEmailID NVARCHAR(254)
	SELECT  @OldStatusID=UserStatusID,@OldEmailID=EmailID from tbl_User WHERE UserId=@userid

	
	IF @OldStatusID<>4  AND @UserStatusID=4
	BEGIN
	SELECT	'ERROR' AS [MessageType], 'Cannot change status to Confirmation Pending.' AS [Message]
	END
	ELSE IF EXISTS	(SELECT	1
				 FROM	dbo.tbl_User
			     WHERE	lower(emailid) = lower(@emailId)
				 --AND	(EmailId=@emailId OR MobileNumber=@mobileNumber) 
				 AND	UserId <> @userid
				)
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'User with that EmailID  already exists.' AS [Message]
	END
	ELSE
	BEGIN
		IF lower(@OldEmailID) <> lower(@emailId)
		BEGIN
		SET @UserStatusID=4
		SET @isEmailverified=0
		END
			UPDATE  tbl_User SET 
			FirstName=@firstname
		,	LastName=@lastname
		,	ModifiedBy=@modifiedBy
		,	EmailId=@emailid
		,	MobileNumber=@mobilenumber
		,	CountryCode=@countrycode
		,	CompanyName=@companyname
		,	CompanyRegNo=@companyregno
		,	UserGroup=@UserGroup
		,	DateOfBirth=@DateOfBirth
		,	ModifiedDate=GETDATE()
		,   UserStatusID = @UserStatusID
		,	EmailVerified=@isEmailverified
		WHERE UserId=@userid

		
		IF @@ROWCOUNT > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'User details updated successfully.' AS [Message]
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
		END
	END
END