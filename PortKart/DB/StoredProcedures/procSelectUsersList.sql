﻿-- =============================================
-- Author:		Anurag P.G
-- Create date: 08-12-2020
-- Description:	To select all users
-- =============================================
CREATE PROCEDURE [dbo].[procSelectUsersList]
AS
BEGIN
	SELECT	U.UserId
		,	U.FirstName
		,	U.LastName
		,	U.Password
		,	U.EmailId
		,	U.MobileNumber
		,	U.CountryCode
		,	U.CompanyName
		,	U.CompanyRegNo
		,	U.UserGroup
		,	U.DateOfBirth
		, 	U.CreatedBy
		,	U.CreatedDate
		,	U.UserStatusID	
		,	U.EmailVerified
		,	US.UserStatus
	FROM tbl_User U 
	INNER JOIN [dbo].[tbl_UserStatus] US
		ON	US.UserStatusId=U.UserStatusId
	WHERE U.UserStatusId=1
END
