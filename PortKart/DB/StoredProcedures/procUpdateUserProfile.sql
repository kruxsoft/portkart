﻿CREATE PROCEDURE [dbo].[procUpdateUserProfile]
(
	@UserId int,
	@CompanyName nvarchar(255),
	@CompanyRegNo nvarchar(255),
	@CountryId int,
	@IndustryTypeId int,
	@ExpiryDate DateTime,
	@City nvarchar(255),
	@Website nvarchar(max),
	@EmailID nvarchar(100),
	@Address nvarchar(1000),
	@CountryCode varchar(15),
	@Phone varchar(18),
	@LogoPath nvarchar(255),
	@AttachmentPath nvarchar(255),
	@ModifiedBy int,
	@About nvarchar(1000),
	@CompanyId int ,
	@ContactPersIds varchar(500),
	@BranchIds varchar(500)
	)
AS
BEGIN

	DECLARE @RowCount int
		
		IF EXISTS  (SELECT 1 FROM tbl_Company where CompanyName=@companyname and CompanyId<>@CompanyId)
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'Company name already exists.' AS [Message]
			return
		END

  UPDATE  tbl_Company
			SET     CompanyName=@CompanyName
				,	CompanyRegNo=@CompanyRegNo
				,	CountryId=@CountryId
				,	IndustryTypeId=@IndustryTypeId
				,ExpiryDate=@ExpiryDate
				,	City=@City
				,	Website=@Website
				,EmailID=@EmailID
				,	Address=@Address
				,	CountryCode=@CountryCode
				,	Phone=@Phone
				,	LogoPath=@LogoPath
				,	AttachmentPath=@AttachmentPath
				,	ModifiedBy=@ModifiedBy
				,	About=@About
				,	ModifiedDate=GETDATE()
				WHERE CompanyId=@CompanyId
			SET @RowCount=@@ROWCOUNT
		

		--Delete CP
		--FROM tbl_ContactPerson CP
		--INNER JOIN tbl_UserCompanyMapping M
	 --   ON CP.CompanyId=M.CompanyId
	 --   WHERE M.UserId=@UserId
		----AND CP.ContactPersonId not in (@ContactPersIds)

		--Delete CP
		--FROM tbl_CompanyBranches CP
		--INNER JOIN tbl_UserCompanyMapping M
	 --   ON CP.CompanyId=M.CompanyId
	 --   WHERE M.UserId=@UserId
		----AND CP.BranchId not in (@BranchIds)

	IF @RowCount > 0
	BEGIN
		SELECT	'SUCCESS' AS [MessageType], 'Profile updated successfully.' AS [Message]			
	END
	ELSE
	BEGIN
		SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
	END
END