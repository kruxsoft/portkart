﻿CREATE PROCEDURE [dbo].[procDeleteServiceCategory]
        
(
    @servicecategoryId    int
,    @modifiedBy smallint
)
AS
BEGIN
IF EXISTS    (SELECT    1
                 FROM    tbl_Service_SCategoryMapping M
                 INNER JOIN tbl_Service S
                 ON S.ServiceId=M.ServiceId
                 Where M.ServiceCategoryId = @servicecategoryId and IsActive = 1)
             
    BEGIN
        SELECT    'ERROR' AS MessageType, 'Cannot deactivate this service category' AS Message;
    END
    ELSE    
    BEGIN
    UPDATE  tbl_ServiceCategory 
    SET        IsActive = '0' 
        ,    ModifiedBy = @modifiedby
        ,    ModifiedDate = GETDATE()
    WHERE    ServiceCategoryId= @servicecategoryId

 

    IF @@ROWCOUNT > 0
    BEGIN
        SELECT    'SUCCESS' AS [MessageType], 'Service Category deactivated successfully.' AS [Message]
    END
    ELSE
    BEGIN
        SELECT    'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
    END
    END
END