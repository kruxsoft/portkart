﻿CREATE PROCEDURE [dbo].[procSelectRoleNameByRoleId]
	@roleId int
AS
BEGIN
	SELECT ROLENAME FROM tbl_Role AS R 
	WHERE RoleId=@roleId
END