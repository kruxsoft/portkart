﻿CREATE PROCEDURE [dbo].[procSelectMenuList]
AS
BEGIN
	SET NOCOUNT ON;
	SELECT	MenuId
		,	MenuName
	FROM	dbo.tbl_Menu
	ORDER BY MenuName ASC
END