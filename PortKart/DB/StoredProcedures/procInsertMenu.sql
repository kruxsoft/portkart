-- =============================================
-- Author:		Gopika Vipin
-- Create date: 21-10-2019
-- Description:	To add new  menu
-- =============================================
CREATE PROCEDURE [dbo].[procInsertMenu]
(
	@menuName		varchar(50),
	@menuURL		varchar(256),
	@createdBy		smallint,
	@isActive		bit
 )
AS
BEGIN
	INSERT INTO tbl_Menu (
		MenuName
	,	MenuURL
	,	CreatedBy
	,	CreatedDate
	,	IsActive	)
	VALUES (
		@menuName
	,	@menuURL
	,	@createdBy
	,	GETDATE()
	,	@isActive )
END
