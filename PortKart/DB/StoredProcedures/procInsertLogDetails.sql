﻿CREATE PROCEDURE [dbo].[procInsertLogDetails]
	@Message varchar(max),
	@Level   varchar(500), 
    @Logger VARCHAR(255) , 
    @Exception VARCHAR(MAX) , 
    @LongDate VARCHAR(255) 
AS
BEGIN
	INSERT INTO tbl_LogDetails(Message,Level,Logger,Exception,LongDate,CreatedDate)
	Values (@Message,@Level,@Logger,@Exception,@LongDate,Getdate())

	IF @@ROWCOUNT > 0
		BEGIN
			SELECT	'SUCCESS' AS [MessageType], 'Log Saved successfully.' AS [Message]
		END
		ELSE
		BEGIN
			SELECT	'ERROR' AS [MessageType], 'There was an unexpected error and the details were not saved.' AS [Message]
		END
END
