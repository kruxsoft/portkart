-- =============================================
-- Author:		Simi Simon
-- Create date: 28-10-2019
-- Description:	To get all Roles
-- =============================================
CREATE PROCEDURE [dbo].[procSelectRole]
AS
BEGIN
	SET NOCOUNT ON;
	SELECT	RoleId
	,		RoleName
	,		IsActive
	FROM	tbl_Role 
	ORDER BY RoleName ASC
END
