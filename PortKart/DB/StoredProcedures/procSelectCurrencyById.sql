﻿

CREATE PROCEDURE [dbo].[procSelectCurrencyById]
	(
@currencyId int

)

AS
BEGIN
	SELECT	CurrencyId
			,Code
			,Currency
			,IsActive
	FROM	dbo.tbl_Currency 
	
	WHERE CurrencyId = @currencyId
END
