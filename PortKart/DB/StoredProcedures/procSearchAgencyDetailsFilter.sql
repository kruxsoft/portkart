﻿CREATE PROCEDURE [dbo].[procSearchAgencyDetailsFilter]
@ServiceCategoryId varchar(1000),
@ServiceId varchar(1000),
@AgencyServiceId varchar(1000),
@FSingleService varchar(1000),
@FParentService varchar(1000),
@FSubService varchar(1000)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @AgencyServiceIdCond varchar(1000)
	DECLARE @ServiceCategoryCond varchar(1000)
	DECLARE @ServiceCategoryCond2 varchar(1000)=''
	DECLARE @ServiceCategoryCond3 varchar(1000)=''
	DECLARE @ServiceCategoryCond4 varchar(1000)=''
	DECLARE @ServiceCategoryCond5 varchar(1000)=''
	DECLARE @ServiceCategoryCond6 varchar(1000)=''
	DECLARE @FSingleServiceCond varchar(1000)=''
	DECLARE @FParentServiceCond varchar(1000)=''
	DECLARE @FSubServiceCond varchar(1000)=''
	DECLARE @FilterCond varchar(1000)=''
	DECLARE @ServiceCond varchar(1000)

	DECLARE @CheckFilter bit=0
	DECLARE @CheckFilter2 bit=0
	SET @ServiceCategoryCond=''
	IF isnull(@ServiceCategoryId,'')<>''
	BEGIN
	SET @ServiceCategoryCond=' AND S.ServiceCategoryId in ('+@ServiceCategoryId+')'
	SET @ServiceCategoryCond2=' AND C.ServiceCategoryId in ('+@ServiceCategoryId+')'
	SET @ServiceCategoryCond3=' WHERE EC.ServiceCategoryId in ('+@ServiceCategoryId+')'
	SET @ServiceCategoryCond4=' AND SSC.ServiceCategoryId in ('+@ServiceCategoryId+')'
	SET @ServiceCategoryCond5=' WHERE SEC.ServiceCategoryId in ('+@ServiceCategoryId+')'
	SET @ServiceCategoryCond6=' AND FSEC.ServiceCategoryId in ('+@ServiceCategoryId+')'
	END

	SET @ServiceCond=''
	IF isnull(@ServiceId,'')<>''
	BEGIN
	SET @ServiceCond=' AND (AC.ServiceId in ('+@ServiceId+') AND isnull(AC.ParentId,0)=0)'
	END

	SET @AgencyServiceIdCond=''
	IF isnull(@AgencyServiceId,'')<>''
	BEGIN
	SET @AgencyServiceIdCond='AC.AgencyServiceId in ('+@AgencyServiceId+')'
	END
-----------------------------------------------------------------------------------------------------
	SET @FSingleServiceCond=''
	IF isnull(@FSingleService,'')<>''
	BEGIN
	SET @FSingleServiceCond=' (AC.ServiceId in ('+@FSingleService+') AND isnull(AC.ParentId,0)=0 AND  isnull(AC.IsParent,0)=0) '
	SET @CheckFilter=1
	END

	SET @FParentServiceCond=''
	IF isnull(@FParentService,'')<>''
	BEGIN
	SET @FParentServiceCond=case when isnull(@FSingleServiceCond,'')<>'' then ' OR ' ELSE '' END +' ((AC.ServiceId in ('+@FParentService+') AND isnull(AC.IsParent,0)=1) OR (AC.ParentId in ('+@FParentService+'))) '
	SET @CheckFilter=1
	END

	IF isnull(@FSingleServiceCond,'')='' AND isnull(@FParentServiceCond,'')=''
	BEGIN
	SET @CheckFilter2=1
	END

	SET @FSubServiceCond=''
	IF isnull(@FSubService,'')<>''
	BEGIN
	SET @FSubServiceCond=case when isnull(@CheckFilter2,0)=0 then ' OR ' ELSE '' END +'  (AC.ServiceId in ('+@FSubService+') AND isnull(AC.ParentId,0) in ('+@FParentService+')) '
	SET @CheckFilter=1
	END

	
	IF isnull(@CheckFilter,0)=1
	BEGIN
	SET @FilterCond=' AND ( '+@FSingleServiceCond+' '+@FParentServiceCond+' '+@FSubServiceCond+' )'

	END
----------------------------------------------------------------------------------------------------------------
	EXEC ('SELECT distinct	FnAC.AgencyDetailId
		,	FnAC.AgencyServiceId
		,	FnAC.ServiceId
		,	FnAC.IsParent
		,	FnAC.CurrencyId
		,	FnAC.Amount
		,	FnAC.ActualAmount
		,	FnAC.ParentId
		,	FnAC.Tat
		,	FnC.Currency
		,	FnC.Code
		,	FnS.ServiceCode
		,	FnS.ServiceName
		,	FnAC.CostType
		,	FnAC.Description
		,	FnAC.[Option]
		,	FnSS.[Option] as OgOption
		,	FnSS.Sort
	FROM	dbo.tbl_AgencyServiceDetails AS FnAC
	INNER JOIN dbo.tbl_Service AS FnS
		ON	FnS.ServiceId = FnAC.ServiceId
	Left JOIN dbo.tbl_SubService AS FnSS
		ON	FnSS.ServiceId = FnAC.ServiceId
		AND FnAC.ParentId=FnSS.ParentServiceid
		AND (FnAC.IsParent<>1 OR Isnull(FnAC.ParentId,0)<>0)
	INNER JOIN dbo.tbl_Currency AS FnC
		ON	FnC.CurrencyId = FnAC.CurrencyId
		WHERE FnAC.AgencyServiceId in( SELECT	distinct AC.AgencyServiceId
	FROM	dbo.tbl_AgencyServiceDetails AS AC
	INNER JOIN dbo.tbl_Service AS S
		ON	S.ServiceId = AC.ServiceId
	Left JOIN (SELECT  STUFF((SELECT  '','' + ServiceCategoryName
								FROM tbl_Service EE
								INNER JOIN tbl_Service_SCategoryMapping M
								ON    M.ServiceId=EE.ServiceId
								Inner join tbl_ServiceCategory C
								ON C.ServiceCategoryId=M.ServiceCategoryId
								WHERE   EE.ServiceId=E.ServiceId'+
								@ServiceCategoryCond2+
								' FOR XML PATH('''')), 1, 1, '''') AS ServiceCategoryName,E.ServiceId
				FROM tbl_Service E
				INNER JOIN tbl_Service_SCategoryMapping EM
				ON    EM.ServiceId=E.ServiceId
				Inner join tbl_ServiceCategory EC
				ON EC.ServiceCategoryId=EM.ServiceCategoryId'+
				@ServiceCategoryCond3+
				' GROUP BY E.ServiceId )SC
	ON AC.ServiceId=Sc.ServiceId
	AND (AC.IsParent=1 OR isnull(AC.ParentId,0)=0)
	Left JOIN dbo.tbl_SubService AS SS
		ON	SS.ServiceId = AC.ServiceId
		AND (AC.IsParent<>1 OR Isnull(AC.ParentId,0)<>0)
	INNER JOIN dbo.tbl_Currency AS C
		ON	C.CurrencyId = AC.CurrencyId
		WHERE '+@AgencyServiceIdCond+' '+@ServiceCond+' '+
		' AND (isnull(SC.ServiceCategoryName,'''')<>''''
				
				OR (AC.ParentId in (SELECT distinct FSE.ServiceId from tbl_AgencyServiceDetails FSE
				INNER JOIN tbl_Service_SCategoryMapping FSEM
				ON    FSEM.ServiceId=FSE.ServiceId
				Inner join tbl_ServiceCategory FSEC
				ON FSEC.ServiceCategoryId=FSEM.ServiceCategoryId
				WHERE  FSE.IsParent=1 '+
				@ServiceCategoryCond6+
				' ))) '+@FilterCond+' ) ')
END