﻿CREATE PROCEDURE [dbo].[procSelectServiceServerSide]
@pageSize int,
@skip int,
@sortColumn varchar(50),
@sortColumnDir varchar(10),
@searchValue varchar(100),
@ServiceCode varchar(100),
@ServiceName varchar(100),
@ServiceCategoryName varchar(100),
@IsGroup varchar(100),
@CostType  varchar(50),
@Description nvarchar(2000),
@ModifiedDate varchar(100),
@IsActive varchar(100)
AS
BEGIN

 

DECLARE @recordsTotal int
DECLARE @recordsFiltered int
DECLARE @sqlquery varchar(5000)
DECLARE @orderbyquery varchar(5000)
---------------getting total count-------------------------------------------------
    SELECT     @recordsTotal=Count(*)
    FROM	   dbo.tbl_Service AS RMM
	
 set @orderbyquery=(CASE CONVERT(VARCHAR,@sortColumn)
	                         WHEN 'ServiceCode' THEN 'ServiceCode'
                               WHEN 'ServiceName' THEN 'ServiceName'
                               WHEN 'ServiceCategoryName' THEN 'SC.ServiceCategoryName'
							   WHEN 'IsGroup' THEN 'case RMM.IsGroup when 1 then ''Yes'' else ''No'' end'
							   WHEN 'CostType' THEN 'CostType'
                               WHEN 'Description' THEN 'Description'
							    WHEN 'ModifiedDate' THEN  'RMM.ModifiedDate'
                               WHEN 'IsActive' THEN 'case RMM.IsActive when 1 then ''Active'' else ''Inactive'' end'
                               ELSE 'ServiceCode'  END)


------------------------------------------------------------------------------------------------------------------------
IF isnull(@searchValue,'')=''
BEGIN
   SELECT       @recordsFiltered=COUNT(*)
    FROM	    dbo.tbl_Service AS RMM
	INNER JOIN (SELECT STUFF((SELECT ',' + ServiceCategoryName
								FROM tbl_Service EE
								INNER JOIN tbl_Service_SCategoryMapping M
								ON M.ServiceId=EE.ServiceId
								Inner join tbl_ServiceCategory C
								ON C.ServiceCategoryId=M.ServiceCategoryId
								WHERE EE.ServiceId=E.ServiceId								
								FOR XML PATH('')), 1, 1, '') AS ServiceCategoryName,E.ServiceId
				FROM tbl_Service E
				INNER JOIN tbl_Service_SCategoryMapping EM
				ON EM.ServiceId=E.ServiceId
				Inner join tbl_ServiceCategory EC
				ON EC.ServiceCategoryId=EM.ServiceCategoryId				
				GROUP BY E.ServiceId )SC
	ON RMM.ServiceId=Sc.ServiceId

    WHERE      (ServiceCode like '%'+@ServiceCode+'%' OR isnull(@ServiceCode,'')='')
    AND        (ServiceName like '%'+@ServiceName+'%' OR isnull(@ServiceName,'')='')
    AND        (ServiceCategoryName like '%'+@ServiceCategoryName+'%' OR isnull(@ServiceCategoryName,'')='')
	AND		   (case RMM.IsGroup when 1 then 'Yes' else 'No' end like '%'+@IsGroup+'%' OR isnull(@IsGroup,'')='')
    AND        (CostType like '%'+@CostType+'%' OR isnull(@CostType,'')='')
    AND        (Description like '%'+@Description+'%' OR isnull(@Description,'')='')
	AND       (CONVERT(varchar, ModifiedDate,105) like '%'+@ModifiedDate+'%' OR isnull(@ModifiedDate,'')='')
    AND        (case RMM.IsActive when 1 then 'Active' else 'Inactive' end like '%'+@IsActive+'%' OR isnull(@IsActive,'')='')

 

    SET @sqlquery='SELECT    RMM.ServiceId
		,	RMM.ServiceCode
		,	RMM.ServiceName
		,	RMM.IsGroup
		,	RMM.CostType
		,	RMM.Description
		,	RMM.CreatedBy
		,	RMM.CreatedDate
		,	RMM.ModifiedDate
		,	CONVERT(varchar, RMM.ModifiedDate,105) As ModifiedDateAsString
		,	SC.ServiceCategoryName
		,   RMM.IsActive
		,   isnull(SSB.SubCount,0)SubServiceCount
        ,   '+CONVERT(VARCHAR,@recordsTotal)+' as recordsTotal
        ,   '+CONVERT(VARCHAR,@recordsFiltered)+' as recordsFiltered
    FROM	   dbo.tbl_Service AS RMM
	INNER JOIN (SELECT STUFF((SELECT '','' + ServiceCategoryName
								FROM tbl_Service EE
								INNER JOIN tbl_Service_SCategoryMapping M
								ON M.ServiceId=EE.ServiceId
								Inner join tbl_ServiceCategory C
								ON C.ServiceCategoryId=M.ServiceCategoryId
								WHERE EE.ServiceId=E.ServiceId								
								FOR XML PATH('''')), 1, 1, '''') AS ServiceCategoryName,E.ServiceId
				FROM tbl_Service E
				INNER JOIN tbl_Service_SCategoryMapping EM
				ON EM.ServiceId=E.ServiceId
				Inner join tbl_ServiceCategory EC
				ON EC.ServiceCategoryId=EM.ServiceCategoryId				
				GROUP BY E.ServiceId )SC
	ON RMM.ServiceId=Sc.ServiceId
	LEFT JOIN (SELECT COUNT(SB.Serviceid)SubCount,SB.ParentServiceId FROM tbl_SubService SB GROUP BY SB.ParentServiceId)SSB
	ON        SSB.ParentServiceId=RMM.Serviceid
    WHERE     (ServiceCode like ''%'+CONVERT(VARCHAR,@ServiceCode)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@ServiceCode,''))+''','''')='''')
    AND       (ServiceName like ''%'+CONVERT(VARCHAR,@ServiceName)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@ServiceName,''))+''','''')='''')
    AND       (ServiceCategoryName like ''%'+CONVERT(VARCHAR,@ServiceCategoryName)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@ServiceCategoryName,''))+''','''')='''')
	AND       (CASE RMM.IsGroup when 1 then ''Yes'' else ''No'' end like ''%'+CONVERT(VARCHAR,@IsGroup)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@IsGroup,''))+''','''')='''')
	AND       (CostType like ''%'+CONVERT(VARCHAR,@CostType)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@CostType,''))+''','''')='''')
    AND       (Description like ''%'+CONVERT(VARCHAR,@Description)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@Description,''))+''','''')='''')
		AND       (CONVERT(varchar, ModifiedDate,105) like''%'+CONVERT(VARCHAR,@ModifiedDate)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@ModifiedDate,''))+''','''')='''')
    AND       (CASE RMM.IsActive when 1 then ''Active'' else ''Inactive'' end like ''%'+CONVERT(VARCHAR,@IsActive)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@IsActive,''))+''','''')='''')
    ORDER BY  '+@OrderByQuery+' '+ @sortColumnDir +'
    OFFSET     '+CONVERT(varchar,@skip)+' ROWS
    FETCH NEXT '+CONVERT(varchar,@pagesize)+' ROWS ONLY OPTION (RECOMPILE)'

 

            
exec (@sqlquery)
END
ELSE
BEGIN
    SELECT       @recordsFiltered=COUNT(*)
    FROM	     dbo.tbl_Service AS RMM
	INNER JOIN (SELECT STUFF((SELECT ',' + ServiceCategoryName
								FROM tbl_Service EE
								INNER JOIN tbl_Service_SCategoryMapping M
								ON M.ServiceId=EE.ServiceId
								Inner join tbl_ServiceCategory C
								ON C.ServiceCategoryId=M.ServiceCategoryId
								WHERE EE.ServiceId=E.ServiceId								
								FOR XML PATH('')), 1, 1, '') AS ServiceCategoryName,E.ServiceId
				FROM tbl_Service E
				INNER JOIN tbl_Service_SCategoryMapping EM
				ON EM.ServiceId=E.ServiceId
				Inner join tbl_ServiceCategory EC
				ON EC.ServiceCategoryId=EM.ServiceCategoryId				
				GROUP BY E.ServiceId )SC
	ON RMM.ServiceId=Sc.ServiceId
    WHERE       (ServiceCode like '%'+@searchValue+'%'
    OR           ServiceName like '%'+@searchValue+'%'
    OR           ServiceCategoryName like '%'+@searchValue+'%'
	OR           case RMM.IsGroup when 1 then 'Yes' else 'No' end  like '%'+@searchValue+'%'
	OR           CostType like '%'+@searchValue+'%'
    OR           Description like '%'+@searchValue+'%'
	OR         CONVERT(varchar, ModifiedDate,105) like '%'+@searchValue+'%'
    OR           case RMM.IsActive when 1 then 'Active' else 'Inactive' end  like '%'+@searchValue+'%')
    AND         (ServiceCode like '%'+@ServiceCode+'%' OR isnull(@ServiceCode,'')='')
    AND         (ServiceName like '%'+@ServiceName+'%' OR isnull(@ServiceName,'')='')
    AND         (ServiceCategoryName like '%'+@ServiceCategoryName+'%' OR isnull(@ServiceCategoryName,'')='')
	AND         (case RMM.IsGroup when 1 then 'Yes' else 'No' end like '%'+@IsGroup+'%' OR isnull(@IsGroup,'')='')
	 AND         (CostType like '%'+@CostType+'%' OR isnull(@CostType,'')='')
    AND         (Description like '%'+@Description+'%' OR isnull(@Description,'')='')
	AND       (CONVERT(varchar, ModifiedDate,105) like '%'+@ModifiedDate+'%' OR isnull(@ModifiedDate,'')='')
    AND         (case RMM.IsActive when 1 then 'Active' else 'Inactive' end like '%'+@IsActive+'%' OR isnull(@IsActive,'')='')

 


    SET @sqlquery='SELECT    RMM.ServiceId
		,	RMM.ServiceCode
		,	RMM.ServiceName
		,	RMM.IsGroup
		,	RMM.CostType
		,	RMM.Description
		,	RMM.CreatedBy
		,	RMM.CreatedDate
		,	RMM.ModifiedDate
		,   CONVERT(varchar, RMM.ModifiedDate,105) As ModifiedDateAsString
		,	SC.ServiceCategoryName
		,   RMM.IsActive
		,   isnull(SSB.SubCount,0)SubServiceCount
        ,   '+CONVERT(VARCHAR,@recordsTotal)+' as recordsTotal
        ,   '+CONVERT(VARCHAR,@recordsFiltered)+' as recordsFiltered
    FROM	   dbo.tbl_Service AS RMM
	INNER JOIN (SELECT STUFF((SELECT '','' + ServiceCategoryName
								FROM tbl_Service EE
								INNER JOIN tbl_Service_SCategoryMapping M
								ON M.ServiceId=EE.ServiceId
								Inner join tbl_ServiceCategory C
								ON C.ServiceCategoryId=M.ServiceCategoryId
								WHERE EE.ServiceId=E.ServiceId								
								FOR XML PATH('''')), 1, 1, '''') AS ServiceCategoryName,E.ServiceId
				FROM tbl_Service E
				INNER JOIN tbl_Service_SCategoryMapping EM
				ON EM.ServiceId=E.ServiceId
				Inner join tbl_ServiceCategory EC
				ON EC.ServiceCategoryId=EM.ServiceCategoryId				
				GROUP BY E.ServiceId )SC
	ON RMM.ServiceId=Sc.ServiceId
	LEFT JOIN (SELECT COUNT(SB.Serviceid)SubCount,SB.ParentServiceId FROM tbl_SubService SB GROUP BY SB.ParentServiceId)SSB
	ON        SSB.ParentServiceId=RMM.Serviceid
    WHERE     (ServiceCode like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
    OR         ServiceName like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
    OR         ServiceCategoryName like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
	OR         case RMM.IsGroup when 1 then ''Yes'' else ''No'' end like ''%'+CONVERT(VARCHAR,@searchValue)+'%''
	OR         CostType like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
    OR         Description like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' 
	  OR         CONVERT(varchar, ModifiedDate,105) like ''%'+CONVERT(VARCHAR,@searchValue)+'%''
    OR         case RMM.IsActive when 1 then ''Active'' else ''Inactive'' end like ''%'+CONVERT(VARCHAR,@searchValue)+'%'' )
    AND       (ServiceCode like ''%'+CONVERT(VARCHAR,@ServiceCode)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@ServiceCode,''))+''','''')='''')
    AND       (ServiceName like ''%'+CONVERT(VARCHAR,@ServiceName)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@ServiceName,''))+''','''')='''')
    AND       (ServiceCategoryName like ''%'+CONVERT(VARCHAR,@ServiceCategoryName)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@ServiceCategoryName,''))+''','''')='''')

	AND       (CASE RMM.IsGroup when 1 then ''Yes'' else ''No'' end like ''%'+CONVERT(VARCHAR,@IsGroup)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@IsGroup,''))+''','''')='''')
	AND       (CostType like ''%'+CONVERT(VARCHAR,@CostType)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@CostType,''))+''','''')='''')
    AND       (Description like ''%'+CONVERT(VARCHAR,@Description)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@Description,''))+''','''')='''')
	 AND       (CONVERT(varchar, ModifiedDate,105) like''%'+CONVERT(VARCHAR,@ModifiedDate)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@ModifiedDate,''))+''','''')='''')
    AND       (CASE RMM.IsActive when 1 then ''Active'' else ''Inactive'' end like ''%'+CONVERT(VARCHAR,@IsActive)+'%''  OR isnull('''+CONVERT(VARCHAR,isnull(@IsActive,''))+''','''')='''')
    ORDER BY  '+@OrderByQuery+' '+ @sortColumnDir +'
    OFFSET     '+CONVERT(varchar,@skip)+' ROWS
    FETCH NEXT '+CONVERT(varchar,@pagesize)+' ROWS ONLY OPTION (RECOMPILE)'

 

    exec (@sqlquery)
END
END