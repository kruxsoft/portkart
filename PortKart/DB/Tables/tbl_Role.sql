-- ============================================================================================================================
--	Author:      Simi and Gopika
--	Create date: 18-10-2019
--	Description: This table is created to capture the Role details
--	Edit Log Below:
--	DATE		Modified By			Modification details
--	01-11-2019	Madhu Poclassery	Modified the table structure based on the DB Design document.
-- ============================================================================================================================
CREATE TABLE [dbo].[tbl_Role](
	RoleId	smallint  NOT NULL  IDENTITY(1,1) ,
	RoleName	varchar(50)  NOT NULL ,
	CreatedBy	INT NULL ,
	CreatedDate	datetime2(2) NOT NULL ,
	ModifiedBy	INT NULL ,
	ModifiedDate	datetime2(2) NULL ,
	IsActive	BIT
 CONSTRAINT [PK_tbl_Role] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO