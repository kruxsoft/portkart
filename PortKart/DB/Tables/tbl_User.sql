-- ============================================================================================================================
--	Author:      Anurag P.G
--	Create date: 08-12-2020
--	Description: This table is created to capture the User Details

-- ============================================================================================================================
CREATE TABLE [dbo].[tbl_User](
	UserId	int NOT NULL  IDENTITY,
	FirstName	nvarchar(50) NOT NULL,
	LastName	nvarchar(50) NOT NULL,
	Password	varchar(MAX)  NOT NULL,
	EmailId	nvarchar(254) NOT NULL,
	CountryCode	varchar(10)  NULL,
	MobileNumber	varchar(20) ,
	CompanyName	nvarchar(150)  NULL,
	CompanyRegNo	varchar(50)  NULL,
	DateOfBirth	datetime2(2) NULL ,
	UserGroup	varchar(50) NOT NULL,
	CreatedBy	int  NULL ,
	CreatedDate	datetime2(2) NOT NULL ,
	ModifiedBy	int NULL ,
	ModifiedDate	datetime2(2) NULL ,
	EmailVerified  BIT,
	UserStatusId	tinyint
 CONSTRAINT [PK_tbl_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tbl_User]  WITH CHECK ADD  CONSTRAINT [FK_tbl_User_tbl_UserStatus] FOREIGN KEY([UserStatusId])
REFERENCES [dbo].[tbl_UserStatus] ([UserStatusId])
GO

ALTER TABLE [dbo].[tbl_User] CHECK CONSTRAINT [FK_tbl_User_tbl_UserStatus]
GO