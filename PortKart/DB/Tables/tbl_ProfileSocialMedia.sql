﻿CREATE TABLE [dbo].[tbl_ProfileSocialMedia]
(
	SocialMediaId	[int] IDENTITY(1,1) NOT NULL, 
CompanyId	int NOT NULL,
Link	nvarchar(max) NULL,
Type	nvarchar(100) NOT NULL,
 CONSTRAINT [PK_tbl_ProfileSocialMedia] PRIMARY KEY ([SocialMediaId])
)
GO

ALTER TABLE [dbo].[tbl_ProfileSocialMedia]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ProfileSocialMedia_tbl_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[tbl_Company] ([CompanyId])
GO

ALTER TABLE [dbo].[tbl_ProfileSocialMedia] CHECK CONSTRAINT [FK_tbl_ProfileSocialMedia_tbl_Company]
GO