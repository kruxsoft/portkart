﻿-- ============================================================================================================================
--	Author:      Prageeth Rajan
--	Create date: 01-01-2020
--	Description: This table is created to capture User Role Mapping audit details
--	Edit Log Below:
-- ============================================================================================================================
CREATE TABLE [dbo].[tbl_UserRoleMapping_Audit]
(
	UserRoleMappingAuditId bigint IDENTITY(1,1) NOT NULL, 
	UserId	int NOT NULL,
	RoleId	smallint NOT NULL,
	CreatedBy	int NULL,
	CreatedDate	datetime NOT NULL,
	OperationType char(1),
	OperationDate datetime2(2)
 CONSTRAINT [PK_tbl_UserRoleMapping_Audit] PRIMARY KEY CLUSTERED 
(
	[UserRoleMappingAuditId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO