﻿CREATE TABLE [dbo].[tbl_SubService]
(	
	[ParentServiceId] [int]  NOT NULL,
	[Option] char(1) NOT NULL,
	[ServiceId] [int] NOT NULL,
	[Sort] [int]  NULL,	
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime2](2) NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime2](2) NULL
 CONSTRAINT [PK_tbl_SubService] PRIMARY KEY CLUSTERED 
(
	[ParentServiceId],[ServiceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO