﻿-- ============================================================================================================================
--	Author:      Prageeth Rajan
--	Create date: 01-01-2020
--	Description: This table is created to capture Role Menu Mapping the Role audit details
--	Edit Log Below:
-- ============================================================================================================================
CREATE TABLE [dbo].[tbl_RoleMenuMapping_Audit]
(
	RoleMenuMappingAuditId bigint IDENTITY(1,1) NOT NULL,
	RoleId	smallint,
	MenuId	smallint,
	CreatedBy	INT,
	CreatedDate	datetime2(2),	
	OperationType char(1),
	OperationDate datetime2(2)
 CONSTRAINT [PK_tbl_RoleMenuMapping_Audit] PRIMARY KEY CLUSTERED 
(
	[RoleMenuMappingAuditId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO