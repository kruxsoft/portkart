﻿CREATE TABLE [dbo].[tbl_AgencyRating]
(
	[AgencyRatingId] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	CompanyId int not null,
	OneStar int,
	TwoStar int,
	ThreeStar int,
	FourStar int,
	FiveStar int,
	Rating decimal(18,1) 

)
