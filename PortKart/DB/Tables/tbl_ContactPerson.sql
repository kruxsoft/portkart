﻿CREATE TABLE [dbo].[tbl_ContactPerson]
(
	[ContactPersonId] INT  IDENTITY(1,1) NOT NULL PRIMARY KEY, 
	CompanyId	int NOT NULL,
    [Name] NVARCHAR(150) NOT NULL,
	CountryCode varchar(15),
	PhoneNo  varchar(18),
	IsPreview bit
)
GO

ALTER TABLE [dbo].[tbl_ContactPerson]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ContactPerson_tbl_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[tbl_Company] ([CompanyId])
GO

ALTER TABLE [dbo].[tbl_ContactPerson] CHECK CONSTRAINT [FK_tbl_ContactPerson_tbl_Company]
GO
