﻿-- ============================================================================================================================
--	Author:      Anurag P.G
--	Create date: 22-12-2020
--	Description: This table is created to capture the mapping between User and Company

-- ============================================================================================================================
CREATE TABLE [dbo].[tbl_UserCompanyMapping](
	UserId	int NOT NULL,
	CompanyId	int NOT NULL,
	CreatedBy	int NULL,
	CreatedDate	datetime NOT NULL
 CONSTRAINT [PK_tbl_UserCompanyMapping] PRIMARY KEY CLUSTERED 
(
	[UserId], [CompanyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tbl_UserCompanyMapping]  WITH CHECK ADD  CONSTRAINT [FK_tbl_UserCompanyMapping_tbl_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[tbl_User] ([UserId])
GO

ALTER TABLE [dbo].[tbl_UserCompanyMapping] CHECK CONSTRAINT [FK_tbl_UserCompanyMapping_tbl_User]
GO

ALTER TABLE [dbo].[tbl_UserCompanyMapping]  WITH CHECK ADD  CONSTRAINT [FK_tbl_UserCompanyMapping_tbl_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[tbl_Company] ([CompanyId])
GO

ALTER TABLE [dbo].[tbl_UserCompanyMapping] CHECK CONSTRAINT [FK_tbl_UserCompanyMapping_tbl_Company]
GO
