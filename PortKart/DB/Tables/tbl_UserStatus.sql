﻿-- ============================================================================================================================
--	Author:      Anurag P.G
--	Create date: 08-12-2020
--	Description: This table is created to capture the User Status Details

-- ============================================================================================================================
CREATE TABLE [dbo].[tbl_UserStatus](
	UserStatusId	tinyint NOT NULL  ,
	UserStatus      varchar(50) NOT NULL
 CONSTRAINT [PK_tbl_UserStatus] PRIMARY KEY CLUSTERED 
(
	[UserStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
