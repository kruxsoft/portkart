﻿-- ============================================================================================================================
--	Author:      Anurag P.G
--	Create date: 08-12-2020
--	Description: This table is created to capture User audit details
--	Edit Log Below:
-- ============================================================================================================================
CREATE TABLE [dbo].[tbl_User_Audit]
(
	UserAuditId bigint IDENTITY(1,1) NOT NULL, 
	UserId	int NOT NULL  ,
	FirstName	nvarchar(50) NOT NULL,
	LastName	nvarchar(50) NOT NULL,
	Password	varchar(MAX)  NOT NULL,
	EmailId	nvarchar(254) NOT NULL,
	CountryCode	varchar(10)  NULL,
	MobileNumber	varchar(20) ,
	CompanyName	nvarchar(150)  NULL,
	CompanyRegNo	varchar(50)  NULL,
	DateOfBirth	datetime2(2) NULL ,
	UserGroup	varchar(50) NOT NULL,
	CreatedBy	INT  NULL ,
	CreatedDate	datetime2(2) NOT NULL ,
	ModifiedBy	INT NULL ,
	ModifiedDate	datetime2(2) NULL ,
	EmailVerified  BIT,
	UserStatusId	tinyint,
	OperationType char(1),
	OperationDate datetime2(2)
 CONSTRAINT [PK_tbl_User_Audit] PRIMARY KEY CLUSTERED 
(
	[UserAuditId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO