﻿CREATE TABLE [dbo].[tbl_Service_SCategoryMapping](
	[ServiceId] [int] NOT NULL,
	[ServiceCategoryId] [int] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	
 CONSTRAINT [PK_tbl_Service_SCategoryMapping] PRIMARY KEY CLUSTERED 
(
	[ServiceId],[ServiceCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO