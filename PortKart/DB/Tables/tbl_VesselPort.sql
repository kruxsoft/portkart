﻿

CREATE table  [dbo].[tbl_VesselPort](
	[PortId] [int] NOT NULL IDENTITY(1,1) ,
	[Code] [nvarchar](255) NOT NULL,
	[Port] [nvarchar](255) NOT NULL,
	[Airport] [nvarchar](255) NULL,
	[Un_Locode] [nvarchar](255) NULL,
	[TimeZone] [nvarchar](255) NULL,
	[Latitude] [decimal](18, 5) NULL,
	[LatitudeDegree] [decimal](18, 5) NULL,
	[LatitudeMinutes] [decimal](18, 5) NULL,
	[LatitudeDirection] [varchar](255) NULL,
	[Longitude] [decimal](18, 5) NULL,
	[LongitudeDegree] [decimal](18, 5) NULL,
	[LongitudeMinutes] [decimal](18, 5) NULL,
	[LongitudeDirection] [varchar](255) NULL,
	CreatedBy	INT NULL ,
	CreatedDate	datetime2 NOT NULL ,
	ModifiedBy	INT NULL ,
	ModifiedDate	datetime2 NULL ,
	[IsActive] bit NOT NULL
CONSTRAINT [PK_tbl_VesselPort] PRIMARY KEY CLUSTERED 
(
	[PortId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY], 
    [CountryId] INT NOT NULL
) ON [PRIMARY]

GO


