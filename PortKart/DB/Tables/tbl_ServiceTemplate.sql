﻿CREATE TABLE [dbo].[tbl_ServiceTemplate]
(
TemplateId	int identity(1,1) NOT NULL,
TemplateName	NVARCHAR(100) NOT NULL,
AgencyId	int NOT NULL,
CreatedBy	int,
CreatedDate	datetime2(2),
ModifiedBy	int,
ModifiedDate	datetime2(2),
IsActive bit
CONSTRAINT [PK_tbl_ServiceTemplate] PRIMARY KEY (TemplateId) NULL
)
GO


ALTER TABLE [dbo].[tbl_ServiceTemplate]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ServiceTemplate_tbl_User] FOREIGN KEY([AgencyId])
REFERENCES [dbo].[tbl_User] ([UserId])
GO

ALTER TABLE [dbo].[tbl_ServiceTemplate] CHECK CONSTRAINT [FK_tbl_ServiceTemplate_tbl_User]
GO