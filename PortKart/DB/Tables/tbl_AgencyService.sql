﻿CREATE TABLE [dbo].[tbl_AgencyService]
(
AgencyServiceId	int identity(1,1) NOT NULL,
PortId	int NOT NULL,
AgencyId	int NOT NULL,
IsActive	bit NOT NULL,
CreatedBy	int NULL,
CreatedDate	datetime2 NULL,
ModifiedBy	int NULL,
ModifiedDate	datetime2 NULL
 CONSTRAINT [PK_tbl_AgencyService] PRIMARY KEY ([AgencyServiceId])
)
GO

ALTER TABLE [dbo].[tbl_AgencyService]  WITH CHECK ADD  CONSTRAINT [FK_tbl_AgencyService_tbl_User] FOREIGN KEY([AgencyId])
REFERENCES [dbo].[tbl_User] ([UserId])
GO

ALTER TABLE [dbo].[tbl_AgencyService] CHECK CONSTRAINT [FK_tbl_AgencyService_tbl_User]
GO

ALTER TABLE [dbo].[tbl_AgencyService]  WITH CHECK ADD  CONSTRAINT [FK_tbl_AgencyService_tbl_VesselPort] FOREIGN KEY([PortId])
REFERENCES [dbo].[tbl_VesselPort] ([PortId])
GO

ALTER TABLE [dbo].[tbl_AgencyService] CHECK CONSTRAINT [FK_tbl_AgencyService_tbl_VesselPort]
GO