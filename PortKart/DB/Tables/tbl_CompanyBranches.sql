﻿CREATE TABLE [dbo].[tbl_CompanyBranches]
(
	[BranchId] INT  IDENTITY(1,1) NOT NULL PRIMARY KEY, 
	CompanyId	int NOT NULL,
    [BranchName] NVARCHAR(255) NOT NULL,
	CountryId	int NULL,
	[Address]	nvarchar(1000),
	CountryCode	varchar(15),
	Phone	    varchar(20),
	Website		nvarchar(MAX),
	Email       NVARCHAR(255),
	ContactPerson  NVARCHAR(255),
	CP_CountryCode varchar(15),
	CP_Phone   varchar(20),
	IsPreview  bit
)
GO

ALTER TABLE [dbo].[tbl_CompanyBranches]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CompanyBranches_tbl_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[tbl_Company] ([CompanyId])
GO

ALTER TABLE [dbo].[tbl_CompanyBranches] CHECK CONSTRAINT [FK_tbl_CompanyBranches_tbl_Company]
GO

ALTER TABLE [dbo].[tbl_CompanyBranches]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CompanyBranches_tbl_Country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[tbl_Country] ([CountryId])
GO

ALTER TABLE [dbo].[tbl_CompanyBranches] CHECK CONSTRAINT [FK_tbl_CompanyBranches_tbl_Country]
GO
