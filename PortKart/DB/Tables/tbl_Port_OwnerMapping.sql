﻿CREATE TABLE [dbo].[tbl_Port_OwnerMapping](
	[OwnerTemplateId] [int] NOT NULL,
	[PortId] [int] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_PK_tbl_Port_OwnerMapping] PRIMARY KEY CLUSTERED 
(
	[OwnerTemplateId] ASC,
	[PortId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
