﻿CREATE TABLE [dbo].[tbl_Company]
(
	[CompanyId] [int] IDENTITY(1,1) NOT NULL, 
    [CompanyName] NVARCHAR(400) NOT NULL, 
	[CompanyRegNo] NVARCHAR(50) NULL,
	CountryId	int NULL,
	City	Nvarchar(255),
	IndustryTypeId	int,
	ExpiryDate DATETIME2,
	Website		nvarchar(MAX),
	EmailID nvarchar(100),
	[Address]	nvarchar(1000),
	CountryCode	varchar(15),
	Phone	    varchar(20),
	LogoPath	nvarchar(max),
	Logo	    varbinary,
	AttachmentPath	nvarchar(max),
	Attachment	    varbinary,
    CreatedBy	INT NULL ,
	CreatedDate	datetime2 NOT NULL ,
	ModifiedBy	INT NULL ,
	ModifiedDate	datetime2 NULL ,
    [IsActive] BIT NOT NULL, 
	[About]	nvarchar(1000)
    CONSTRAINT [PK_tbl_Company] PRIMARY KEY ([CompanyId])
)
GO

ALTER TABLE [dbo].[tbl_Company]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Company_tbl_Country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[tbl_Country] ([CountryId])
GO

ALTER TABLE [dbo].[tbl_Company] CHECK CONSTRAINT [FK_tbl_Company_tbl_Country]
GO