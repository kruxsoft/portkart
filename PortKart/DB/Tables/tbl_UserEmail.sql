﻿CREATE TABLE [dbo].[tbl_UserEmail]
(
	[UserEmailId] INT NOT NULL Identity,
	UserId      int NOT NULL,
	Email       nvarchar(50) NOT NULL,
	EmailBody   nvarchar(max) NOT NULL,
	SentStatus  bit NULL,
	IsExpire    bit,
	EmailType   char(1) NOT NULL,
	EmailCode       nvarchar(500) NOT NULL,
	CreatedBy	int  NULL ,
	CreatedDate	datetime2(2) NULL ,
	ModifiedBy	int NULL ,
	ModifiedDate	datetime2(2) NULL 
	 CONSTRAINT [PK_tbl_UserEmail] PRIMARY KEY CLUSTERED 
(
	[UserEmailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tbl_UserEmail]  WITH CHECK ADD  CONSTRAINT [FK_tbl_UserEmail_tbl_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[tbl_User] ([UserId])
GO

ALTER TABLE [dbo].[tbl_UserEmail] CHECK CONSTRAINT [FK_tbl_UserEmail_tbl_User]
GO