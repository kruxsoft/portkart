-- ============================================================================================================================
--	Author:      Simi and Gopika
--	Create date: 18-10-2019
--	Description: This table is created to capture the mapping between Role and Menu
--	Edit Log Below:
--	DATE		Modified By			Modification details
--	01-11-2019	Madhu Poclassery	Modified the table structure based on the DB Design document.
-- ============================================================================================================================
CREATE TABLE [dbo].[tbl_RoleMenuMapping](
	RoleId	smallint,
	MenuId	smallint,
	CreatedBy	INT,
	CreatedDate	datetime2(2)

 CONSTRAINT [PK_tbl_RoleMenuMapping] PRIMARY KEY CLUSTERED 
(
	[RoleId], [MenuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tbl_RoleMenuMapping]  WITH CHECK ADD  CONSTRAINT [FK_tbl_RoleMenuMapping_tbl_Menu] FOREIGN KEY([MenuId])
REFERENCES [dbo].[tbl_Menu] ([MenuId])
GO

ALTER TABLE [dbo].[tbl_RoleMenuMapping] CHECK CONSTRAINT [FK_tbl_RoleMenuMapping_tbl_Menu]
GO

ALTER TABLE [dbo].[tbl_RoleMenuMapping]  WITH CHECK ADD  CONSTRAINT [FK_tbl_RoleMenuMapping_tbl_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[tbl_Role] ([RoleId])
GO

ALTER TABLE [dbo].[tbl_RoleMenuMapping] CHECK CONSTRAINT [FK_tbl_RoleMenuMapping_tbl_Role]
GO
