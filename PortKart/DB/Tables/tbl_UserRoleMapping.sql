-- ============================================================================================================================
--	Author:      Simi and Gopika
--	Create date: 18-10-2019
--	Description: This table is created to capture the mapping between User and Role
--	Edit Log Below:
--	DATE		Modified By			Modification details
--	01-11-2019	Madhu Poclassery	Modified the table structure based on the DB Design document.
-- ============================================================================================================================
CREATE TABLE [dbo].[tbl_UserRoleMapping](
	UserId	int NOT NULL,
	RoleId	smallint NOT NULL,
	CreatedBy	int NULL,
	CreatedDate	datetime NOT NULL
 CONSTRAINT [PK_tbl_UserRoleMapping] PRIMARY KEY CLUSTERED 
(
	[UserId], [RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tbl_UserRoleMapping]  WITH CHECK ADD  CONSTRAINT [FK_tbl_UserRole_tbl_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[tbl_User] ([UserId])
GO

ALTER TABLE [dbo].[tbl_UserRoleMapping] CHECK CONSTRAINT [FK_tbl_UserRole_tbl_User]
GO

ALTER TABLE [dbo].[tbl_UserRoleMapping]  WITH CHECK ADD  CONSTRAINT [FK_tbl_UserRole_tbl_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[tbl_Role] ([RoleId])
GO

ALTER TABLE [dbo].[tbl_UserRoleMapping] CHECK CONSTRAINT [FK_tbl_UserRole_tbl_Role]
GO
