﻿CREATE TABLE [dbo].[tbl_AgencyServiceDetails]
(
AgencyDetailId	int identity(1,1) NOT NULL,
AgencyServiceId	int NOT NULL,
ServiceId	int NOT NULL,
CurrencyId	int NOT NULL,
Amount	decimal(30,2),
ActualAmount	decimal(30,2),
CreatedBy	int NULL,
CreatedDate	datetime2(2) NULL,
ModifiedBy	int NULL,
ModifiedDate	datetime2(2),
IsParent bit,
ParentId int,
Tat int,
TatDescription nvarchar(500),
[Description] nvarchar(2000),
IsActive bit NOT NULL,
[CostType] [varchar](50)  NULL,
[Option] char(1)  NULL
 CONSTRAINT [PK_tbl_AgencyServiceDetails] PRIMARY KEY (AgencyDetailId) 
)
GO

ALTER TABLE [dbo].[tbl_AgencyServiceDetails]  WITH CHECK ADD  CONSTRAINT [FK_tbl_AgencyServiceDetails_tbl_Service] FOREIGN KEY([ServiceId])
REFERENCES [dbo].[tbl_Service] ([ServiceId])
GO

ALTER TABLE [dbo].[tbl_AgencyServiceDetails] CHECK CONSTRAINT [FK_tbl_AgencyServiceDetails_tbl_Service]
GO


ALTER TABLE [dbo].[tbl_AgencyServiceDetails]  WITH CHECK ADD  CONSTRAINT [FK_tbl_AgencyServiceDetails_tbl_AgencyService] FOREIGN KEY([AgencyServiceId])
REFERENCES [dbo].[tbl_AgencyService] ([AgencyServiceId])
GO

ALTER TABLE [dbo].[tbl_AgencyServiceDetails] CHECK CONSTRAINT [FK_tbl_AgencyServiceDetails_tbl_AgencyService]
GO