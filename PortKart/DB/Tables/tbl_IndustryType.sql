﻿CREATE TABLE [dbo].[tbl_IndustryType]
(
	[IndustryTypeId] [int] IDENTITY(1,1) NOT NULL, 
    [IndustryTypeName] NVARCHAR(400) NOT NULL, 
	CreatedBy	INT NULL ,
	CreatedDate	datetime2 NOT NULL ,
	ModifiedBy	INT NULL ,
	ModifiedDate	datetime2 NULL ,
    [IsActive] BIT NOT NULL, 
    CONSTRAINT [PK_tbl_IndustryType] PRIMARY KEY ([IndustryTypeId])
)
GO

