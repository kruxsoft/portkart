﻿CREATE TABLE [dbo].[tbl_ServiceTemplateDetails]
(
TemplateDetailId int identity(1,1) Primary key,
[TemplateId]	int NOT NULL,
ServiceId	int NOT NULL,
CurrencyId	int NOT NULL,
Type	varchar(100),
Offer	decimal(18,5),
CreatedBy	int,
CreatedDate	datetime2(2),
ModifiedBy	int,
ModifiedDate	datetime2(2),
IsParent bit,
ParentID int,

)
