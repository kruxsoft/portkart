﻿CREATE TABLE [dbo].[tbl_UserProfile]
(
	UserProfileId [int] IDENTITY(1,1) NOT NULL, 
	UserId	int NOT NULL,
	CompanyId	int NOT NULL,
	CountryId	int NOT NULL,
	City	varchar(255),
	IndustryTypeId	int,
	ExpiryDate DateTime,
	Website		nvarchar(254),
	[Address]	varchar(1000),
	CountryCode	varchar(15),
	Phone	    varchar(20),
	LogoPath	varchar(max),
	Logo	    varbinary,
	AttachmentPath	varchar(max),
	Attachment	    varbinary,
	CreatedBy	int NULL,
	CreatedDate	datetime NOT NULL,
	ModifiedBy	INT NULL ,
	ModifiedDate	datetime2 NULL ,
 CONSTRAINT [PK_tbl_UserProfile] PRIMARY KEY CLUSTERED 
(
	[UserId], [CompanyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tbl_UserProfile]  WITH CHECK ADD  CONSTRAINT [FK_tbl_UserProfile_tbl_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[tbl_User] ([UserId])
GO

ALTER TABLE [dbo].[tbl_UserProfile] CHECK CONSTRAINT [FK_tbl_UserProfile_tbl_User]
GO

ALTER TABLE [dbo].[tbl_UserProfile]  WITH CHECK ADD  CONSTRAINT [FK_tbl_UserProfile_tbl_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[tbl_Company] ([CompanyId])
GO

ALTER TABLE [dbo].[tbl_UserProfile] CHECK CONSTRAINT [FK_tbl_UserProfile_tbl_Company]
GO
