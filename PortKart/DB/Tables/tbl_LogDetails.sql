﻿CREATE TABLE [dbo].[tbl_LogDetails]
(
	[LogId] INT NOT NULL identity(1,1) PRIMARY KEY,
	[Message] varchar(max),
	[Level]   varchar(500), 
    [Logger] VARCHAR(255) NULL, 
    [Exception] VARCHAR(MAX) NULL, 
    [LongDate] VARCHAR(255) NULL, 
    [CreatedDate] DATETIME NULL,

)
