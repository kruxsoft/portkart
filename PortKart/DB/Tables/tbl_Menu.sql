-- ============================================================================================================================
--	Author:      Simi and Gopika
--	Create date: 18-10-2019
--	Description: This table is created to capture the Menu details
--	Edit Log Below:
--	DATE		Modified By			Modification details
--	01-11-2019	Madhu Poclassery	Modified the table structure based on the DB Design document.
-- ============================================================================================================================
CREATE TABLE [dbo].[tbl_Menu](
	MenuId	smallint IDENTITY(1,1),
	MenuName	varchar(50) NOT NULL,
	MenuURL	varchar(256),
	ParentId smallint, 
	IsParent bit,
	HasChildren bit,
	MenuOrder smallint,
	CreatedBy	INT,
	CreatedDate	datetime2(2),
	IsActive	bit,
	Icon varchar(500)
 CONSTRAINT [PK_tbl_Menu] PRIMARY KEY CLUSTERED 
(
	[MenuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO