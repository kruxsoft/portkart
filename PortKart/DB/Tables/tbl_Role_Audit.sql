﻿-- ============================================================================================================================
--	Author:      Prageeth Rajan
--	Create date: 01-01-2020
--	Description: This table is created to capture the Role audit details
--	Edit Log Below:
-- ============================================================================================================================
CREATE TABLE [dbo].[tbl_Role_Audit]
(
	RoleAuditId bigint IDENTITY(1,1) NOT NULL,
	RoleId	smallint  NOT NULL,
	RoleName	varchar(50)  NOT NULL ,
	CreatedBy	INT NOT NULL ,
	CreatedDate	datetime2(2) NOT NULL ,
	ModifiedBy	INT NULL ,
	ModifiedDate	datetime2(2) NULL ,
	IsActive	BIT,
	OperationType char(1),
	OperationDate datetime2(2)
 CONSTRAINT [PK_tbl_Role_Audit] PRIMARY KEY CLUSTERED 
(
	[RoleAuditId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO