﻿-- ============================================================================================================================
--	Author:      Prageeth Rajan
--	Create date: 01-01-2020
--	Description: This table is created to capture the Menu audit details
--	Edit Log Below:
-- ============================================================================================================================
CREATE TABLE [dbo].[tbl_Menu_Audit]
(
	MenuAuditId bigint IDENTITY(1,1) NOT NULL,
	MenuId	smallint,
	MenuName	varchar(50) NOT NULL,
	MenuURL	varchar(256),
	ParentId smallint, 
	IsParent bit,
	HasChildren bit,
	MenuOrder smallint,
	CreatedBy	INT,
	CreatedDate	datetime2(2),
	IsActive	bit,
	OperationType char(1),
	OperationDate datetime2(2),
	Icon varchar(500)
 CONSTRAINT [PK_tbl_Menu_Audit] PRIMARY KEY CLUSTERED 
(
	[MenuAuditId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO