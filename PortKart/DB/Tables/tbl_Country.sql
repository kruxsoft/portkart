﻿CREATE TABLE [dbo].[tbl_Country]
(
	[CountryId] [int] IDENTITY(1,1) NOT NULL, 
    [CountryName] VARCHAR(100) NOT NULL, 
    CreatedBy	INT NULL ,
	CreatedDate	datetime2 NOT NULL ,
	ModifiedBy	INT NULL ,
	ModifiedDate	datetime2 NULL ,
    [IsActive] BIT NOT NULL, 
    CONSTRAINT [PK_tbl_Country] PRIMARY KEY ([CountryId])
)
