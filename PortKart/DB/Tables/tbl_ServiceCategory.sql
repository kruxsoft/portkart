﻿CREATE TABLE [dbo].[tbl_ServiceCategory]
(
	[ServiceCategoryId] [int] IDENTITY(1,1) NOT NULL,
	[ServiceCategoryCode] NVARCHAR(50) NOT NULL,
	[ServiceCategoryName] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime2](2) NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime2](2) NULL,
 CONSTRAINT [PK_tbl_ServiceCategory] PRIMARY KEY CLUSTERED 
(
	[ServiceCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO