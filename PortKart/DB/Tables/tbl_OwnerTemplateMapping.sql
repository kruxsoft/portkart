﻿CREATE TABLE [dbo].[tbl_OwnerTemplateMapping](
	[OwnerTemplateId] [int] Identity (1,1) NOT NULL,
	[OwnerId] [int] NOT NULL,
	[TemplateId] [int] NOT NULL,
	
	[StartDate] [datetime2](2) NULL,
	[EndDate] [datetime2](2) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime2](2) NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime2](2) NULL,
 CONSTRAINT [PK_tbl_OwnerTemplateMapping] PRIMARY KEY CLUSTERED 
(
	[OwnerTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[tbl_OwnerTemplateMapping]  WITH CHECK ADD  CONSTRAINT [FK_tbl_OwnerTemplateMapping_tbl_ServiceTemplate] FOREIGN KEY([TemplateId])
REFERENCES [dbo].[tbl_ServiceTemplate] ([TemplateId])
GO

ALTER TABLE [dbo].[tbl_OwnerTemplateMapping] CHECK CONSTRAINT [FK_tbl_OwnerTemplateMapping_tbl_ServiceTemplate]
GO

ALTER TABLE [dbo].[tbl_OwnerTemplateMapping]  WITH CHECK ADD  CONSTRAINT [FK_tbl_OwnerTemplateMapping_tbl_User] FOREIGN KEY([OwnerId])
REFERENCES [dbo].[tbl_User] ([UserId])
GO

ALTER TABLE [dbo].[tbl_OwnerTemplateMapping] CHECK CONSTRAINT [FK_tbl_OwnerTemplateMapping_tbl_User]
GO
