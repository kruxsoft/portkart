﻿CREATE TABLE [dbo].[tbl_Service](
	[ServiceId] [int] IDENTITY(1,1) NOT NULL,
	[ServiceCode] [nvarchar](20) NOT NULL,
	[ServiceName] [nvarchar](20) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[ServiceCategoryId] [int] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime2](2) NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime2](2) NULL,
	[IsGroup] [bit] NULL,
	[CostType] [varchar](50) NULL,
	[Description] [nvarchar](2000) NULL,
 CONSTRAINT [PK_tbl_Service] PRIMARY KEY CLUSTERED 
(
	[ServiceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO