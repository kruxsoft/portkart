﻿CREATE TRIGGER [tr_tbl_RoleMenuMapping_After]
ON	[dbo].[tbl_RoleMenuMapping]
AFTER  UPDATE, DELETE
AS
BEGIN
	DECLARE @OperationType CHAR(1)
	IF	EXISTS	(	SELECT	1
					FROM	INSERTED)
	BEGIN		
		IF EXISTS	(	SELECT	1
						FROM	DELETED	)
		BEGIN
			SET	@OperationType = 'U'
		END
		ELSE
		BEGIN
			SET	@OperationType = 'I'
		END
	END
	ELSE IF EXISTS	(	SELECT	1
						FROM	DELETED	)
	BEGIN
		SET	@OperationType = 'D'
	END
	
	
	IF	(@OperationType = 'U' OR @OperationType = 'D')
	BEGIN	
		INSERT INTO dbo.tbl_RoleMenuMapping_Audit	(
					RoleId
				,	MenuId	
				,	CreatedBy
				,	CreatedDate	
				,	OperationType
				,	OperationDate)
		SELECT		RoleId
				,	MenuId	
				,	CreatedBy
				,	CreatedDate	
				,	@OperationType
				,	GETDATE()
		FROM	DELETED
	END
	IF	(@OperationType = 'U')
	BEGIN	
		INSERT INTO dbo.tbl_RoleMenuMapping_Audit	(
					RoleId
				,	MenuId	
				,	CreatedBy
				,	CreatedDate	
				,	OperationType
				,	OperationDate)
		SELECT		RoleId
				,	MenuId	
				,	CreatedBy
				,	CreatedDate	
				,	@OperationType
				,	GETDATE()
		FROM	INSERTED
	END
END