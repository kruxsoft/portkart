﻿CREATE TRIGGER [tr_tbl_User_After]
ON	[dbo].[tbl_User]
AFTER  UPDATE, DELETE
AS
BEGIN
	DECLARE @OperationType CHAR(1)
	IF	EXISTS	(	SELECT	1
					FROM	INSERTED)
	BEGIN		
		IF EXISTS	(	SELECT	1
						FROM	DELETED	)
		BEGIN
			SET	@OperationType = 'U'
		END
		ELSE
		BEGIN
			SET	@OperationType = 'I'
		END
	END
	ELSE IF EXISTS	(	SELECT	1
						FROM	DELETED	)
	BEGIN
		SET	@OperationType = 'D'
	END
	
	
	IF	(@OperationType = 'U' OR @OperationType = 'D')
	BEGIN	
		INSERT INTO dbo.tbl_User_Audit	(
					UserId	
					,   FirstName
					,	LastName
					,	Password
					,	EmailId
					,	MobileNumber
					,	CountryCode
					,	CompanyName
					,	CompanyRegNo
					,	UserGroup
					,	DateOfBirth
					, 	CreatedBy
					,	CreatedDate
					,	[UserStatusID]	
					,	EmailVerified
				,	OperationType
				,	OperationDate)
		SELECT		UserId	
				,   FirstName
					,	LastName
					,	Password
					,	EmailId
					,	MobileNumber
					,	CountryCode
					,	CompanyName
					,	CompanyRegNo
					,	UserGroup
					,	DateOfBirth
					, 	CreatedBy
					,	CreatedDate
					,	[UserStatusID]	
					,	EmailVerified
				,	@OperationType
				,	GETDATE()
		FROM	DELETED
	END
	IF	(@OperationType = 'U')
	BEGIN	
		INSERT INTO dbo.tbl_User_Audit	(
					UserId	
				,   FirstName
					,	LastName
					,	Password
					,	EmailId
					,	MobileNumber
					,	CountryCode
					,	CompanyName
					,	CompanyRegNo
					,	UserGroup
					,	DateOfBirth
					, 	CreatedBy
					,	CreatedDate
					,	[UserStatusID]	
					,	EmailVerified
				,	OperationType
				,	OperationDate)
		SELECT		UserId	
				,   FirstName
					,	LastName
					,	Password
					,	EmailId
					,	MobileNumber
					,	CountryCode
					,	CompanyName
					,	CompanyRegNo
					,	UserGroup
					,	DateOfBirth
					, 	CreatedBy
					,	CreatedDate
					,	[UserStatusID]	
					,	EmailVerified
				,	@OperationType
				,	GETDATE()
		FROM	INSERTED
	END
END