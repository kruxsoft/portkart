﻿CREATE TRIGGER [dbo].[tr_tbl_Menu_After]
ON	[dbo].[tbl_Menu]
AFTER  UPDATE, DELETE
AS
BEGIN
	DECLARE @OperationType CHAR(1)
	IF	EXISTS	(	SELECT	1
					FROM	INSERTED)
	BEGIN		
		IF EXISTS	(	SELECT	1
						FROM	DELETED	)
		BEGIN
			SET	@OperationType = 'U'
		END
		ELSE
		BEGIN
			SET	@OperationType = 'I'
		END
	END
	ELSE IF EXISTS	(	SELECT	1
						FROM	DELETED	)
	BEGIN
		SET	@OperationType = 'D'
	END
	
	
	IF	(@OperationType = 'U' OR @OperationType = 'D')
	BEGIN	
		INSERT INTO dbo.tbl_Menu_Audit	(
					MenuId	
				,	MenuName	
				,	MenuURL	
				,	ParentId 
				,	IsParent 
				,	HasChildren 
				,	MenuOrder 
				,	CreatedBy	
				,	CreatedDate	
				,	IsActive
				,	OperationType
				,	OperationDate)
		SELECT		MenuId	
				,	MenuName	
				,	MenuURL	
				,	ParentId 
				,	IsParent 
				,	HasChildren 
				,	MenuOrder 
				,	CreatedBy	
				,	CreatedDate	
				,	IsActive
				,	@OperationType
				,	GETDATE()
		FROM	DELETED
	END
	IF	(@OperationType = 'U')
	BEGIN	
		INSERT INTO dbo.tbl_Menu_Audit	(
					MenuId	
				,	MenuName	
				,	MenuURL	
				,	ParentId 
				,	IsParent 
				,	HasChildren 
				,	MenuOrder 
				,	CreatedBy	
				,	CreatedDate	
				,	IsActive
				,	OperationType
				,	OperationDate)
		SELECT		MenuId	
				,	MenuName	
				,	MenuURL	
				,	ParentId 
				,	IsParent 
				,	HasChildren 
				,	MenuOrder 
				,	CreatedBy	
				,	CreatedDate	
				,	IsActive
				,	@OperationType
				,	GETDATE()
		FROM	INSERTED
	END
END