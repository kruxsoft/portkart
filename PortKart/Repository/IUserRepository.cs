﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public interface IUserRepository
    {
        List<User> GetAllUsers();
        IEnumerable<UserRole> GetUsers(string Username, string Password);
        DBResponse Register(User obj);
        DBResponse UpdateUser(User uObj);
        DBResponse DeleteUser(User cObj);
        User GetUserById(int userId);
        List<User> GetUsersList();
        DBResponse AddUserEmail(UserEmail UserEmail);
        DBResponse VerifyEmail(string EmailCode);
        PasswordReset GetUserbyEmailCode(string EmailCode);
        DBResponse NewPassword(PasswordReset lObj);
        DBResponse CheckEmailId(string EmailId);
     
        DataTableServerSideResult<User> GetAllUsersServerSide(DataTableServerSideSettings DTtablesettings);
    }
}
