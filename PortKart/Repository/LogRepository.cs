﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Domain;
using Microsoft.Extensions.Logging;

namespace Repository
{
    public class LogRepository: ILogRepository
    {
        #region GLOBAL VARIABLE
        private IDbConnection db;
        private readonly ILogger<LogRepository> _logger;
        public LogRepository(IConfiguration configuration, ILogger<LogRepository> logger)
        {
            var sqlConnectionString = configuration.GetConnectionString("SqlServerConnection");
            db = new SqlConnection(sqlConnectionString);
            _logger = logger;
        }
        #endregion

        #region SaveLog

        public DBResponse SaveLog(LogModel lm)
        {
            _logger.LogInformation("Inside SaveLog() function in LogRepository");
            DBResponse dbResponse = new DBResponse();
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@Message", lm.Message);
                param.Add("@Level", lm.Level);
                param.Add("@Logger", lm.Logger);
                param.Add("@Exception", lm.Exception);
                param.Add("@Longdate", lm.Longdate);
                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("procInsertLogDetails", param, commandType: CommandType.StoredProcedure);

            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dbResponse;
        }
        #endregion
    }
}
