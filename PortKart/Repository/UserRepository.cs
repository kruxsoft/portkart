﻿using Dapper;
using Domain;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class UserRepository : IUserRepository
    {
        private IDbConnection db;
        private readonly ILogger<UserRepository> _logger;
        public UserRepository(IConfiguration configuration, ILogger<UserRepository> logger)
        {
            var sqlConnectionString = configuration.GetConnectionString("SqlServerConnection");
            db = new SqlConnection(sqlConnectionString);
            _logger = logger;
        }
        #region GetAllUsers
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public List<User> GetAllUsers()
        {
            _logger.LogInformation("Inside GetAllUsers() function in UserRepository");
            List<User> user = new List<User>();
            try
            {
                db.Open();
                user = db.Query<User>("procSelectUsers", commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return user;
        }
        #endregion

        #region GetUsers
        public IEnumerable<UserRole> GetUsers(string emailid, string Password)
        {
            _logger.LogInformation("Inside GetUsers() function in UserRepository");
            List<UserRole> userlist = new List<UserRole>();
            try
            {

                DynamicParameters param = new DynamicParameters();
                param.Add("@emailid", emailid);
                param.Add("@password", Password);
                db.Open();

                userlist = db.Query<UserRole>("procCheckLogin", param, commandType: CommandType.StoredProcedure).ToList();
                db.Close();
                // Did Some Stuff and Returns Model;

            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
            }

            return userlist;
        }
        #endregion

        #region GetUsers
        public DBResponse Register(User obj)
        {
            _logger.LogInformation("Inside Register() function in UserRepository");
            DBResponse DBResponse = new DBResponse();
            try
            {

                DynamicParameters param = new DynamicParameters();
                param.Add("@firstName", obj.FirstName);
                param.Add("@lastName", obj.LastName);
                param.Add("@password", obj.Password);
                param.Add("@emailId", obj.EmailId);
                param.Add("@mobileNumber", obj.MobileNumber);
                param.Add("@companyname", obj.CompanyName);
                param.Add("@companyregno", obj.CompanyRegNo);
                param.Add("@UserGroup", obj.UserGroup);
                param.Add("@countrycode", obj.CountryCode);
                param.Add("@DateOfBirth", obj.DateOfBirth);
                param.Add("@createdBy", obj.CreatedBy);
                param.Add("@UserStatusID", obj.UserStatusId);
                db.Open();

                DBResponse = db.QuerySingle<DBResponse>("procInsertUser", param, commandType: CommandType.StoredProcedure);
                db.Close();
                // Did Some Stuff and Returns Model;

            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }

            return DBResponse;
        }
        #endregion

        #region UpdateUser
        /// <summary>
        /// Update a user
        /// </summary>
        /// <param name="cs"></param>
        /// <returns></returns>
        public DBResponse UpdateUser(User cs)
        {
            _logger.LogInformation("Inside UpdateUser() function in UserRepository");
            DBResponse dbResponse = new DBResponse();
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@firstName", cs.FirstName);
                param.Add("@lastName", cs.LastName);
                param.Add("@password", cs.Password);
                param.Add("@emailId", cs.EmailId);
                param.Add("@mobileNumber", cs.MobileNumber);
                param.Add("@companyname", cs.CompanyName);
                param.Add("@companyregno", cs.CompanyRegNo);
                param.Add("@UserGroup", cs.UserGroup);
                param.Add("@countrycode", cs.CountryCode);
                param.Add("@DateOfBirth", cs.DateOfBirth);
                param.Add("@UserStatusID", cs.UserStatusId);
                param.Add("@userid", cs.UserId);
                param.Add("@modifiedBy", cs.ModifiedBy);
             
                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("procUpdateUser", param, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dbResponse;
        }
        #endregion

        #region DeleteUser
        /// <summary>
        /// Flag a user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DBResponse DeleteUser(User cObj)
        {
            _logger.LogInformation("Inside DeleteUser() function in UserRepository");
            DBResponse dbResponse = new DBResponse();
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@userid", cObj.UserId);
                param.Add("@ModifiedBy", cObj.ModifiedBy);
                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("procDeleteUser", param, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dbResponse;
        }
        #endregion

        #region GetUserById
        public User GetUserById(int userId)
        {
            _logger.LogInformation("Inside GetUserById() function in UserRepository");
            User uObj = new User();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@userId", userId);
                uObj = db.Query<User>("procSelectUserByUserId", param, commandType: CommandType.StoredProcedure).SingleOrDefault();
                db.Close();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return uObj;
        }
        #endregion

        #region GetUsersList
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public List<User> GetUsersList()
        {
            _logger.LogInformation("Inside GetUsersList() function in UserRepository");
            List<User> user = new List<User>();
            try
            {
                db.Open();
                user = db.Query<User>("procSelectUsersList", commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return user;
        }
        #endregion

        #region AddUerEmail
        public DBResponse AddUserEmail(UserEmail Uobj)
        {
            _logger.LogInformation("Inside AddUserEmail() function in UserRepository");
            DBResponse userlist = new DBResponse();
            try
            {

                DynamicParameters param = new DynamicParameters();
                param.Add("@EmailId", Uobj.Email);
                param.Add("@EmailBody", Uobj.EmailBody);
                param.Add("@EmailType", Uobj.EmailType);
                param.Add("@EmailCode", Uobj.EmailCode);
                param.Add("@SentStatus", Uobj.SentStatus);
                param.Add("@CreatedBy", Uobj.CreatedBy);
                db.Open();

                userlist = db.QuerySingle<DBResponse>("procInsertUserEmail", param, commandType: CommandType.StoredProcedure);
                db.Close();
                // Did Some Stuff and Returns Model;

            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
            }

            return userlist;
        }
        #endregion

        #region Verify
        public DBResponse VerifyEmail(string EmailCode)
        {
            _logger.LogInformation("Inside VerifyEmail() function in UserRepository");
            DBResponse uObj = new DBResponse();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@EmailCode", EmailCode);
                uObj = db.Query<DBResponse>("procVerifyUserEmail", param, commandType: CommandType.StoredProcedure).SingleOrDefault();
                db.Close();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return uObj;
        }
        #endregion

        #region GetUserbyEmailCode
        public PasswordReset GetUserbyEmailCode(string EmailCode)
        {
            _logger.LogInformation("Inside GetUserbyEmailCode() function in UserRepository");
            PasswordReset uObj = new PasswordReset();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@EmailCode", EmailCode);
                param.Add("@EmailType", "P");
                uObj = db.Query<PasswordReset>("procSelectUserbyEmailCode", param, commandType: CommandType.StoredProcedure).SingleOrDefault();
                db.Close();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return uObj;
        }
        #endregion

        #region NewPassword
        /// <summary>
        /// Update password
        /// </summary>
        /// <param name="cs"></param>
        /// <returns></returns>
        public DBResponse NewPassword(PasswordReset lObj)
        {
            _logger.LogInformation("Inside NewPassword() function in UserRepository");
            DBResponse dbResponse = new DBResponse();
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@EmailId", lObj.EmailId);
                param.Add("@Password", lObj.EncPassword);
                param.Add("@userid", lObj.UserId);
                param.Add("@UserEmailId", lObj.UserEmailId);

                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("procUpdateNewPassword", param, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dbResponse;
        }
        #endregion

        #region GetAllUsersServerSide
        public DataTableServerSideResult<User> GetAllUsersServerSide(DataTableServerSideSettings DTSettings)
        {
            // fileLogger.Info("Inside GetAllUsersServerSide() function in UserRepository.");
            _logger.LogInformation("Inside GetAllUsersServerSide() function in UserRepository");
            List<User> users = new List<User>();
            DataTableServerSideResult<User> dtResult = new DataTableServerSideResult<User>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@pageSize", DTSettings.pageSize);
                param.Add("@skip", DTSettings.skip);
                param.Add("@sortColumn", DTSettings.sortColumn);
                param.Add("@sortColumnDir", DTSettings.sortColumnDir);
                param.Add("@searchValue", DTSettings.searchValue);
                param.Add("@FirstName", DTSettings.ColumnSearch.ContainsKey("FirstName") == true ? DTSettings.ColumnSearch["FirstName"] : "");
                param.Add("@LastName", DTSettings.ColumnSearch.ContainsKey("LastName") == true ? DTSettings.ColumnSearch["LastName"] : "");
                param.Add("@EmailId", DTSettings.ColumnSearch.ContainsKey("EmailId") == true ? DTSettings.ColumnSearch["EmailId"] : "");
                param.Add("@UserGroup", DTSettings.ColumnSearch.ContainsKey("UserGroup") == true ? DTSettings.ColumnSearch["UserGroup"] : "");
                param.Add("@CompanyName", DTSettings.ColumnSearch.ContainsKey("CompanyName") == true ? DTSettings.ColumnSearch["CompanyName"] : "");
                param.Add("@UserStatus", DTSettings.ColumnSearch.ContainsKey("UserStatus") == true ? DTSettings.ColumnSearch["UserStatus"] : "");

                users = db.Query<User>("procSelectUsersServerSide", param, commandType: CommandType.StoredProcedure).ToList();

                if (users.Count > 0)
                {
                    dtResult.recordsTotal = users[0].recordsTotal;
                    dtResult.recordsFiltered = users[0].recordsFiltered;
                }

                dtResult.data = users;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dtResult;
        }
        #endregion

        #region CheckEmailId
        public DBResponse CheckEmailId(string EmailId)
        {
            _logger.LogInformation("Inside CheckEmailId() function in UserRepository");
            DBResponse uObj = new DBResponse();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@EmailId", EmailId);
                uObj = db.QuerySingle<DBResponse>("procCheckEmailId", param, commandType: CommandType.StoredProcedure);
                db.Close();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            return uObj;
        }
        #endregion


     
    }
}
