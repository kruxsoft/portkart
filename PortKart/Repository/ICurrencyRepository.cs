﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public interface ICurrencyRepository
    {
        List<CurrencyDomain> GetAllCurrencies();
        DBResponse AddCurrency(CurrencyDomain oObj);
        CurrencyDomain GetCurrencyById(int CurrencyId);
        DBResponse UpdateCurrency(CurrencyDomain oObj);
        DBResponse DeleteCurrency(CurrencyDomain currency);

        List<CurrencyDomain> GetCurrencyList();
    }
}
