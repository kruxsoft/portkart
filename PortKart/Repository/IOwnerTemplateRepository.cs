﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public interface IOwnerTemplateRepository
    {
        List<OwnerTemplate> GetOwnerTemplateList();

        List<OwnerTemplate> GetAllOwnerTemplateList();
        DBResponse AddOwnerTemplate(OwnerTemplate TObj);    
        DBResponse UpdateOwnerTemplate(OwnerTemplate oObj);
        DBResponse DeleteOwnerTemplate(OwnerTemplate uObj);
    }
}
