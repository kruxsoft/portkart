﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Domain;
using Microsoft.Extensions.Configuration;

namespace Repository
{
   public class MenuRepository:IMenuRepository
    {
        private IDbConnection db;
        public MenuRepository(IConfiguration configuration)
        {
            var sqlConnectionString = configuration.GetConnectionString("SqlServerConnection");
            db = new SqlConnection(sqlConnectionString);
        }
        #region GetMenu
        /// <summary>
        /// Get Menus
        /// </summary>
        /// <returns></returns>
        public List<Menu> GetMenus()
        {
            List<Menu> menus = new List<Menu>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();

                menus = db.Query<Menu>("procSelectMenu", param, commandType: CommandType.StoredProcedure).ToList();
                db.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return menus;
        }
        #endregion
    }
}
