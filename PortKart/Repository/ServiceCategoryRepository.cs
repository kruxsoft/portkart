﻿using Dapper;
using Domain;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class ServiceCategoryRepository : IServiceCategoryRepository
    {
        #region GLOBAL VARIABLE
        private IDbConnection db;
        public ServiceCategoryRepository(IConfiguration configuration)
        {
            var sqlConnectionString = configuration.GetConnectionString("SqlServerConnection");
            db = new SqlConnection(sqlConnectionString);
        }
        #endregion

        #region GetAllServiceCategories
        public List<ServiceCategory> GetAllServiceCategories()
        {
            List<ServiceCategory> serviceCategory = new List<ServiceCategory>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                serviceCategory = db.Query<ServiceCategory>("procSelectServiceCategory", param, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return serviceCategory;
        }


        #endregion


        #region GetServiceCategoryList
        public List<ServiceCategory> GetServiceCategoryList()
        {
            List<ServiceCategory> serviceCategory = new List<ServiceCategory>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                serviceCategory = db.Query<ServiceCategory>("procSelectServiceCategoryList", param, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return serviceCategory;
        }


        #endregion

        #region AddServiceCategory

        public DBResponse AddServiceCategory(ServiceCategory sc)
        {

            DBResponse dbResponse = new DBResponse();
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@serviceCategroyCode", sc.ServiceCategoryCode);
                param.Add("@serviceCategoryName", sc.ServiceCategoryName);
                param.Add("@createdBy", sc.CreatedBy);
                param.Add("@modifiedBy", sc.ModifiedBy);
                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("procInsertServiceCategory", param, commandType: CommandType.StoredProcedure);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dbResponse;
        }
        #endregion

        #region GetServiceCategoryById
        public ServiceCategory GetServiceCategoryById(int id)
        {
            ServiceCategory serviceCategory = new ServiceCategory();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@serviceCategoryId", id);
                serviceCategory = db.Query<ServiceCategory>("procSelectServiceCategoryById", param, commandType: CommandType.StoredProcedure).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return serviceCategory;
        }
        #endregion

        #region UpdateIncident
        public DBResponse UpdateServiceCategory(ServiceCategory sc)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@serviceCategoryName", sc.ServiceCategoryName);
                param.Add("@serviceCategroyCode", sc.ServiceCategoryCode);
                param.Add("@modifiedBy", sc.ModifiedBy);
                param.Add("@serviceCategoryId", sc.ServiceCategoryId);
                param.Add("@isActive", sc.IsActive);

                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("procUpdateServiceCategory", param, commandType: CommandType.StoredProcedure);

                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region DeleteServiceCategory

        public DBResponse DeleteServiceCategory(ServiceCategory sObj)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@servicecategoryId", sObj.ServiceCategoryId);
                param.Add("@modifiedBy", sObj.ModifiedBy);
                db.Open();
                DBResponse dbResponse = db.QuerySingle<DBResponse>("procDeleteServiceCategory", param, commandType: CommandType.StoredProcedure);

                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region GetAllServiceCategoriesServerSide
        public DataTableServerSideResult<ServiceCategory> GetAllServiceCategoriesServerSide(DataTableServerSideSettings DTSettings)
        {
            // fileLogger.Info("Inside GetServicesServerSide() function in UserRepository.");

            List<ServiceCategory> ServiceList = new List<ServiceCategory>();
            DataTableServerSideResult<ServiceCategory> dtResult = new DataTableServerSideResult<ServiceCategory>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@pageSize", DTSettings.pageSize);
                param.Add("@skip", DTSettings.skip);
                param.Add("@sortColumn", DTSettings.sortColumn);
                param.Add("@sortColumnDir", DTSettings.sortColumnDir);
                param.Add("@searchValue", DTSettings.searchValue);
                param.Add("@ServiceCategoryCode", DTSettings.ColumnSearch.ContainsKey("ServiceCategoryCode") == true ? DTSettings.ColumnSearch["ServiceCategoryCode"] : "");
                param.Add("@ServiceCategoryName", DTSettings.ColumnSearch.ContainsKey("ServiceCategoryName") == true ? DTSettings.ColumnSearch["ServiceCategoryName"] : "");
                param.Add("@ModifiedDate", DTSettings.ColumnSearch.ContainsKey("ModifiedDate") == true ? DTSettings.ColumnSearch["ModifiedDate"] : "");
                param.Add("@IsActive", DTSettings.ColumnSearch.ContainsKey("IsActive") == true ? DTSettings.ColumnSearch["IsActive"] : "");


                ServiceList = db.Query<ServiceCategory>("procSelectServiceCategoryServerSide", param, commandType: CommandType.StoredProcedure).ToList();

                if (ServiceList.Count > 0)
                {
                    dtResult.recordsTotal = ServiceList[0].recordsTotal;
                    dtResult.recordsFiltered = ServiceList[0].recordsFiltered;
                }

                dtResult.data = ServiceList;
            }
            catch (Exception ex)
            {
                // fileLogger.Error(ex, "Error on GetServicesServerSide() funcion in UserRepository.");
                //databaseLogger.Error(ex, "Error on GetServicesServerSide() funcion in UserRepository.");
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dtResult;
        }
        #endregion

    }
}
