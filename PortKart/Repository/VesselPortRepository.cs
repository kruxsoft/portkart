﻿using Dapper;
using Domain;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class VesselPortRepository : IVesselPortRepository
    {
        #region GLOBAL VARIABLE
        private IDbConnection db;
        public VesselPortRepository(IConfiguration configuration)
        {
            var sqlConnectionString = configuration.GetConnectionString("SqlServerConnection");
            db = new SqlConnection(sqlConnectionString);
        }
        #endregion


        #region GetVesselPortList
        public List<VesselPort> GetVesselPortList()
        {
            List<VesselPort> vesselPorts = new List<VesselPort>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                vesselPorts = db.Query<VesselPort>("procSelectVesselPortList", param, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return vesselPorts;
        }
        #endregion

        #region GetAllServiceCategories
        public List<VesselPort> GetAllVesselPorts()
        {
            List<VesselPort> vesselPorts = new List<VesselPort>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                vesselPorts = db.Query<VesselPort>("procSelectVesselPort", param, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return vesselPorts;
        }

        #endregion

        #region AddVesselPort

        public DBResponse AddVesselPort(VesselPort vp)
        {

            DBResponse dbResponse = new DBResponse();
            try
            {
                DynamicParameters param = new DynamicParameters();
               // param.Add("@portId", vp.PortId);
                param.Add("@code", vp.Code);
                param.Add("@port", vp.Port);
                param.Add("@airport", vp.Airport);
                param.Add("@un_Locode", vp.Un_Locode);
                param.Add("@timeZone", vp.TimeZone);
                param.Add("@latitude", vp.Latitude);
                param.Add("@latitudeDegree", vp.LatitudeDegree);
                param.Add("@latitudeMinutes", vp.LatitudeMinutes);
                param.Add("@latitudeDirection", vp.LatitudeDirection);
                param.Add("@longitude", vp.Longitude);
                param.Add("@longitudeDegree", vp.LongitudeDegree);
                param.Add("@longitudeMinutes", vp.LongitudeMinutes);
                param.Add("@longitudeDirection", vp.LongitudeDirection);
                param.Add("@createdBy", vp.CreatedBy);
                param.Add("@modifiedBy", vp.ModifiedBy);
                param.Add("@countryId", vp.CountryId);
                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("procInsertVesselPort", param, commandType: CommandType.StoredProcedure);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dbResponse;
        }
        #endregion

        #region GetVesselPortById
        public VesselPort GetVesselPortById(int id)
        {
            VesselPort vesselPort = new VesselPort();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@portId", id);
                vesselPort = db.Query<VesselPort>("procSelectVesselPortById", param, commandType: CommandType.StoredProcedure).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return vesselPort;
        }
        #endregion


        #region UpdateVesselPort
        public DBResponse UpdateVesselPort(VesselPort vp)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@portId", vp.PortId);
                param.Add("@code", vp.Code);
                param.Add("@port", vp.Port);
                param.Add("@airport", vp.Airport);
                param.Add("@un_Locode", vp.Un_Locode);
                param.Add("@timeZone", vp.TimeZone);
                param.Add("@latitude", vp.Latitude);
                param.Add("@latitudeDegree", vp.LatitudeDegree);
                param.Add("@latitudeMinutes", vp.LatitudeMinutes);
                param.Add("@latitudeDirection", vp.LatitudeDirection);
                param.Add("@longitude", vp.Longitude);
                param.Add("@longitudeDegree", vp.LongitudeDegree);
                param.Add("@longitudeMinutes", vp.LongitudeMinutes);
                param.Add("@longitudeDirection", vp.LongitudeDirection);
                param.Add("@modifiedBy", vp.ModifiedBy);
                param.Add("@isActive", vp.IsActive);
                param.Add("@countryId", vp.CountryId);
                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("procUpdateVesselPort", param, commandType: CommandType.StoredProcedure);

                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region DeleteVesselPort

        public DBResponse DeleteVesselPort(VesselPort sObj)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                //  param.Add("@servicecategoryId", sObj.ServiceCategoryId);
                param.Add("@portId", sObj.PortId);
                param.Add("@modifiedBy", sObj.ModifiedBy);
                db.Open();
                DBResponse dbResponse = db.QuerySingle<DBResponse>("procDeleteVesselPort", param, commandType: CommandType.StoredProcedure);

                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region VesselPortExcellImport
        public DBResponse VesselPortExcellImport(VesselPort vp)
        {

            DBResponse dbResponse = new DBResponse();
            try
            {
                DynamicParameters param = new DynamicParameters();
                // param.Add("@portId", vp.PortId);
                param.Add("@code", vp.Code);
                param.Add("@port", vp.Port);
                param.Add("@airport", vp.Airport);
                param.Add("@un_Locode", vp.Un_Locode);
                param.Add("@timeZone", vp.TimeZone);
                param.Add("@latitude", vp.Latitude);
                param.Add("@latitudeDegree", vp.LatitudeDegree);
                param.Add("@latitudeMinutes", vp.LatitudeMinutes);
                param.Add("@latitudeDirection", vp.LatitudeDirection);
                param.Add("@longitude", vp.Longitude);
                param.Add("@longitudeDegree", vp.LongitudeDegree);
                param.Add("@longitudeMinutes", vp.LongitudeMinutes);
                param.Add("@longitudeDirection", vp.LongitudeDirection);
                param.Add("@createdBy", vp.CreatedBy);
                param.Add("@modifiedBy", vp.ModifiedBy);
                param.Add("@countryId", vp.CountryId);
                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("procInsertVesselPortExcellImport", param, commandType: CommandType.StoredProcedure);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dbResponse;
        }
        #endregion

        #region GetAllVesselPortsServerSide
        public DataTableServerSideResult<VesselPort> GetAllVesselPortsServerSide(DataTableServerSideSettings DTSettings)
        {
            // fileLogger.Info("Inside GetAllUsersServerSide() function in UserRepository.");

            List<VesselPort> users = new List<VesselPort>();
            DataTableServerSideResult<VesselPort> dtResult = new DataTableServerSideResult<VesselPort>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@pageSize", DTSettings.pageSize);
                param.Add("@skip", DTSettings.skip);
                param.Add("@sortColumn", DTSettings.sortColumn);
                param.Add("@sortColumnDir", DTSettings.sortColumnDir);
                param.Add("@searchValue", DTSettings.searchValue);
                param.Add("@Code", DTSettings.ColumnSearch.ContainsKey("Code") == true ? DTSettings.ColumnSearch["Code"] : "");
                param.Add("@Port", DTSettings.ColumnSearch.ContainsKey("Port") == true ? DTSettings.ColumnSearch["Port"] : "");
                param.Add("@CountryName", DTSettings.ColumnSearch.ContainsKey("CountryName") == true ? DTSettings.ColumnSearch["CountryName"] : "");
                param.Add("@Airport", DTSettings.ColumnSearch.ContainsKey("Airport") == true ? DTSettings.ColumnSearch["Airport"] : "");
                param.Add("@Un_Locode", DTSettings.ColumnSearch.ContainsKey("Un_Locode") == true ? DTSettings.ColumnSearch["Un_Locode"] : "");
                param.Add("@TimeZone", DTSettings.ColumnSearch.ContainsKey("TimeZone") == true ? DTSettings.ColumnSearch["TimeZone"] : "");
                param.Add("@Latitude", DTSettings.ColumnSearch.ContainsKey("Latitude") == true ? DTSettings.ColumnSearch["Latitude"] : "");
                param.Add("@LatitudeDegree", DTSettings.ColumnSearch.ContainsKey("LatitudeDegree") == true ? DTSettings.ColumnSearch["LatitudeDegree"] : "");
                param.Add("@LatitudeMinutes", DTSettings.ColumnSearch.ContainsKey("LatitudeMinutes") == true ? DTSettings.ColumnSearch["LatitudeMinutes"] : "");
                param.Add("@LatitudeDirection", DTSettings.ColumnSearch.ContainsKey("LatitudeDirection") == true ? DTSettings.ColumnSearch["LatitudeDirection"] : "");
                param.Add("@Longitude", DTSettings.ColumnSearch.ContainsKey("Longitude") == true ? DTSettings.ColumnSearch["Longitude"] : "");
                param.Add("@LongitudeDegree", DTSettings.ColumnSearch.ContainsKey("LongitudeDegree") == true ? DTSettings.ColumnSearch["LongitudeDegree"] : "");
                param.Add("@LongitudeMinutes", DTSettings.ColumnSearch.ContainsKey("LongitudeMinutes") == true ? DTSettings.ColumnSearch["LongitudeMinutes"] : "");
                param.Add("@LongitudeDirection", DTSettings.ColumnSearch.ContainsKey("LongitudeDirection") == true ? DTSettings.ColumnSearch["LongitudeDirection"] : "");
                param.Add("@IsActive", DTSettings.ColumnSearch.ContainsKey("IsActive") == true ? DTSettings.ColumnSearch["IsActive"] : "");

                users = db.Query<VesselPort>("procSelectVesselPortServerSide", param, commandType: CommandType.StoredProcedure).ToList();

                if (users.Count > 0)
                {
                    dtResult.recordsTotal = users[0].recordsTotal;
                    dtResult.recordsFiltered = users[0].recordsFiltered;
                }

                dtResult.data = users;
            }
            catch (Exception ex)
            {
                // fileLogger.Error(ex, "Error on GetAllUsersServerSide() funcion in UserRepository.");
                //databaseLogger.Error(ex, "Error on GetAllUsersServerSide() funcion in UserRepository.");
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dtResult;
        }
        #endregion
    }
}
