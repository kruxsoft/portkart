﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
   public interface ITemplateRepository
    {
        //DBResponse AddTemplate(Template TObj);
        List<ServiceList> GetServiceList();
        List<Template> GetTemplateList();

        //List<Template> GetTemplateList(string search);
        List<TemplateDetails> LoadTemplateService(int TemplateId, int AgencyId);
        List<AgencyServiceDetails> LoadServicebyId(int id);
        DBResponse SaveAgencyServiceTemplate(Domain.Template obj);


        List<OwnerTemplate> GetOwnerTemplateList();

        List<OwnerTemplate> GetAllOwnerTemplateList();
        DBResponse AddOwnerTemplate(OwnerTemplate TObj);
        DBResponse UpdateOwnerTemplate(OwnerTemplate oObj);
        DBResponse DeleteOwnerTemplate(OwnerTemplate uObj);
        OwnerTemplate GetOwnerTemplateById(int OwnerTemplateId);

        List<User> GetUserOwnerList();

        List<VesselPort> GetPortInOwnerTemplate();
        List<VesselPort> GetVesselPortList(string search);

  List<Template> GetTemplateNameList(string search);
        DBResponse UpdateServiceTemplate(Template rm);
    }
}
