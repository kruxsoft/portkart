﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public interface IServiceRepository
    {

        List<ServiceList> GetServices();
        List<ServiceList> LoadService(int id);

        List<SubServiceList> LoadSubService(int id);

        DBResponse AddSubService(SubServiceList oObj);
        DBResponse AddService(ServiceList oObj);
        ServiceList GetServiceListById(int ServiceCategoryId);
        List<SubServiceList> GetSubServiceListById(int parentServiceID, int serviceID);
        DBResponse UpdateServiceList(ServiceList oObj);
        DBResponse DeleteServiceList(ServiceList serviceCategory);

        DataTableServerSideResult<ServiceList> GetServicesServerSide(DataTableServerSideSettings DTtablesettings);
        DBResponse UpdateSubService(SubServiceList sc);

        DBResponse DeleteSubService(SubServiceList sc);
       // List<ServiceList> GetServiceForOwner();
    }
}
