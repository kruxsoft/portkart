﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public interface IOwnerRepository
    {
        List<AgencyService> GetAgencyServiceForOwner();
        List<VesselPort> GetVesselPortList(string search);
        List<ServiceCategory> GetServiceCategoryList();
        List<ServiceList> GetServiceList(string[] ids);

        AgencySearchList Search(AgencyFilter ids);
        List<AgencyServiceDetails> SearchService(AgencyFilter ids);
        AgencySearchList FilterAgency(AgencyFilter ids);

        AgencySearchList GetAgentsForCompare(AgencyFilter ids);

    }
}
