﻿using Dapper;
using Domain;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class RoleRepository : IRoleRepository
    {
        private IDbConnection db;
        public RoleRepository(IConfiguration configuration)
        {
            var sqlConnectionString = configuration.GetConnectionString("SqlServerConnection");
            db = new SqlConnection(sqlConnectionString);
        }

        #region AddRole
        /// <summary>
        /// To add new role
        /// </summary>
        /// <param name="rl">Role model</param>
        /// <returns>int</returns>
        public DBResponse AddRole(Role rl)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@roleName", rl.RoleName);
                param.Add("@createdBy", rl.CreatedBy);
                db.Open();
                DBResponse dbResponse = db.QuerySingle<DBResponse>("procInsertRole", param, commandType: CommandType.StoredProcedure);

                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region GetRoles
        /// <summary>
        /// Get Roles
        /// </summary>
        /// <returns></returns>
        public List<Role> GetRoles()
        {
            List<Role> roles = new List<Role>();
            try
            {
                db.Open();
                roles = db.Query<Role>("procSelectRole", commandType: CommandType.StoredProcedure).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return roles;
        }
        #endregion

        #region GetRolesList
        /// <summary>
        /// Get Roles
        /// </summary>
        /// <returns></returns>
        public List<Role> GetRolesList()
        {
            List<Role> roles = new List<Role>();
            try
            {
                db.Open();
                roles = db.Query<Role>("procSelectRoleList", commandType: CommandType.StoredProcedure).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return roles;
        }
        #endregion

        #region GetRoleById
        public Role GetRoleById(int id)
        {
            try
            {
                Role role = new Role();
                DynamicParameters param = new DynamicParameters();
                param.Add("@roleId", id);
                db.Open();
                role = db.Query<Role>("procSelectRoleById", param, commandType: CommandType.StoredProcedure).SingleOrDefault();

                return role;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region UpdateRole
        /// <summary>
        /// Update a role
        /// </summary>
        /// <param name="cs"></param>
        /// <returns></returns>
        public DBResponse UpdateRole(Role rl)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@roleName", rl.RoleName);
                param.Add("@modifiedBy", rl.ModifiedBy);
                param.Add("@isActive", rl.IsActive);
                param.Add("@roleId", rl.RoleId);
                db.Open();
                DBResponse dbResponse = db.QuerySingle<DBResponse>("procUpdateRole", param, commandType: CommandType.StoredProcedure);

                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region DeleteRole
        /// <summary>
        /// Flag a Role
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DBResponse DeleteRole(Role rObj)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@roleId", rObj.RoleId);
                param.Add("@modifiedBy", rObj.ModifiedBy);
                db.Open();
                DBResponse dbResponse = db.QuerySingle<DBResponse>("[procDeleteRole]", param, commandType: CommandType.StoredProcedure);

                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region GetRoleNameByRoleId
        public string GetRoleNameByRoleId(int roleId)
        {
            string RoleName = "";
            try
            {

                DynamicParameters param = new DynamicParameters();
                param.Add("@roleId", roleId);
                db.Open();
                RoleName = db.Query<string>("procSelectRoleNameByRoleId", param, commandType: CommandType.StoredProcedure).SingleOrDefault();
                return RoleName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion
    }
}
