﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public interface IVesselPortRepository
    {
        List<VesselPort> GetAllVesselPorts();
        DBResponse AddVesselPort(VesselPort oObj);
        VesselPort GetVesselPortById(int PortId);
        DBResponse UpdateVesselPort(VesselPort oObj);
        DBResponse DeleteVesselPort(VesselPort vesselPort);
        List<VesselPort> GetVesselPortList();
        DataTableServerSideResult<VesselPort> GetAllVesselPortsServerSide(DataTableServerSideSettings DTtablesettings);
    }
}
