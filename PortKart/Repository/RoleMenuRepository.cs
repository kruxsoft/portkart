﻿#region Header
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Domain;
using Microsoft.Extensions.Configuration;
#endregion

namespace Repository
{
   public class RoleMenuRepository: IRoleMenuRepository
    {
        #region GLOBAL VARIABLE
        private IDbConnection db;
        public RoleMenuRepository(IConfiguration configuration)
        {
            var sqlConnectionString = configuration.GetConnectionString("SqlServerConnection");
            db = new SqlConnection(sqlConnectionString);
        }
        #endregion

        #region AddRolemenu
        /// <summary>
        /// To add new Rolemenu
        /// </summary>
        /// <param name="rm">Rolemenu model</param>
        /// <returns>int</returns>
        public DBResponse AddRolemenu(RoleMenu rm)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@roleId", rm.RoleId);
                param.Add("@menuId", rm.MenuId);
                param.Add("@createdBy", rm.CreatedBy);               
                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("procInsertRoleMenu", param, commandType: CommandType.StoredProcedure);
                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region GetRoleMenus
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<RoleMenu> GetRolemenus(int roleId)
        {
            List<RoleMenu> rolemenu = new List<RoleMenu>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("roleId", roleId);
                rolemenu = db.Query<RoleMenu>("procSelectRoleMenu",param,commandType:CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return rolemenu;
        }
        #endregion

        #region GetAllRoleMenus
        /// <summary>
        /// 
        /// </summary>
        public List<RoleMenu> GetAllRoleMenus()
        /// <returns></returns>
        {
            List<RoleMenu> rolemenu = new List<RoleMenu>();
            try
            {   
                db.Open();
                DynamicParameters param = new DynamicParameters();
                rolemenu = db.Query<RoleMenu>("procSelectAllRoleMenu", param, commandType: CommandType.StoredProcedure).ToList();                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return rolemenu;
        }
        #endregion

        #region DeleteRolemenu
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DBResponse DeleteRolemenu(RoleMenu roleMenu)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@MenuId", roleMenu.MenuId);
                param.Add("@RoleId", roleMenu.RoleId);
                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("procDeleteRolemenu", param, commandType: CommandType.StoredProcedure);             
                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region GetMenuList
        public List<RoleMenu> GetMenuList()
        {
            List<RoleMenu> roleMenus = new List<RoleMenu>();
            try
            {
                DynamicParameters param = new DynamicParameters();
                db.Open();
                roleMenus = db.Query<RoleMenu>("procSelectMenuList", param, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return roleMenus;
        }
        #endregion
    }
}
