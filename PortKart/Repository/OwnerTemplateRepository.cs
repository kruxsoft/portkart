﻿using Dapper;
using Domain;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class OwnerTemplateRepository : IOwnerTemplateRepository
    {
        #region GLOBAL VARIABLE
        private IDbConnection db;
        private readonly ILogger<OwnerTemplateRepository> _logger;

        public OwnerTemplateRepository(IConfiguration configuration, ILogger<OwnerTemplateRepository> logger)
        {
            var sqlConnectionString = configuration.GetConnectionString("SqlServerConnection");
            db = new SqlConnection(sqlConnectionString);
            _logger = logger;
        }
        #endregion


        #region GetOwnerTemplate
        /// <summary>
        /// Get all Owner Template
        /// </summary>
        /// <returns></returns>
        public List<OwnerTemplate> GetOwnerTemplateList()
        {
            _logger.LogInformation("Inside GetOwnerTemplateList() function in OwnerTemplateRepository");
            List<OwnerTemplate> ownerTemplate = new List<OwnerTemplate>();
            try
            {
                db.Open();
                ownerTemplate = db.Query<OwnerTemplate>("procSelectOwnerTemplate", null, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return ownerTemplate;
        }
        #endregion

        #region GetAllOwnerTemplateList
        public List<OwnerTemplate> GetAllOwnerTemplateList()
        {
            List<OwnerTemplate> ownerTemplate = new List<OwnerTemplate>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                ownerTemplate = db.Query<OwnerTemplate>("procSelectAllOwnerTemplates", param, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return ownerTemplate;
        }


        #endregion


        #region AddOwnerTemplate
        public DBResponse AddOwnerTemplate(OwnerTemplate sc)
        {
            _logger.LogInformation("Inside AddOwnerTemplate() function in OwnerTemplateRepository");
            DBResponse dbResponse = new DBResponse();
            try
            {

                



                DynamicParameters param = new DynamicParameters();
                param.Add("@ownerId", sc.UserId);
                param.Add("@templateId", sc.TemplateId);
                param.Add("@startdate", sc.StartDate);
                param.Add("@enddate", sc.EndDate);
                param.Add("@createdBy", sc.CreatedBy);             
                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("procInsertOwnerTemplate", param, commandType: CommandType.StoredProcedure);

            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dbResponse;
        }
        #endregion


        #region GetOwnerTemplateById
        public Template GetOwnerTemplateById(int id)
        {
            Template currency = new Template();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@ownerId", id);
                currency = db.Query<Template>("procSelectOwnerTemplateById", param, commandType: CommandType.StoredProcedure).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return currency;
        }
        #endregion


        #region UpdateOwnerTemplate
        /// <summary>
        /// Update OwnerTemplate
        /// </summary>
        /// <param name="cs"></param>
        /// <returns></returns>
        public DBResponse UpdateOwnerTemplate(OwnerTemplate cs)
        {
            _logger.LogInformation("Inside UpdateOwnerTemplate() function in OwnerTemplateRepository");
            DBResponse dbResponse = new DBResponse();
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@ownerId", cs.UserId);
                param.Add("@templateId", cs.TemplateId);
                param.Add("@startdate", cs.StartDate);
                param.Add("@enddate", cs.EndDate);
                param.Add("@createdBy", cs.CreatedBy);            
                param.Add("@modifiedBy", cs.ModifiedBy);

                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("procUpdateOwnerTemplate", param, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dbResponse;
        }
        #endregion





        #region DeleteOwnerTemplate
        /// <summary>
        /// Delete owner template with id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DBResponse DeleteOwnerTemplate(OwnerTemplate sc)
        {
            _logger.LogInformation("Inside DeleteOwnerTemplate() function in OwnerTemplateRepository");
            DBResponse dbResponse = new DBResponse();
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@ownerId", sc.UserId);
                param.Add("@templateId", sc.TemplateId);
                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("[procDeleteOwnerTemplate]", param, commandType: CommandType.StoredProcedure);
                return dbResponse;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion
    }
}
