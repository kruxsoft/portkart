﻿using Dapper;
using Domain;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class UserStatusRepository: IUserStatusRepository
    {
        private IDbConnection db;
        private readonly ILogger<UserStatusRepository> _logger;
        public UserStatusRepository(IConfiguration configuration, ILogger<UserStatusRepository> logger)
        {
            var sqlConnectionString = configuration.GetConnectionString("SqlServerConnection");
            db = new SqlConnection(sqlConnectionString);
            _logger = logger;
        }

        #region GetUserStatus
        /// <summary>
        /// Get all userstatus
        /// </summary>
        /// <returns></returns>
        public List<UserStatuses> GetUserStatus()
        {
            _logger.LogInformation("Inside GetUserStatus() function in UserStatusRepository");
            List<UserStatuses> user = new List<UserStatuses>();
            try
            {
                db.Open();
                user = db.Query<UserStatuses>("procSelectUserStatus", commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return user;
        }
        #endregion
    }
}
