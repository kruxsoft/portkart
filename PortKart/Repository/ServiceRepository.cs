﻿using Dapper;
using Domain;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Repository
{
    public class ServiceRepository : IServiceRepository
    {
        #region GLOBAL VARIABLE
        private IDbConnection db;
        public ServiceRepository(IConfiguration configuration)
        {
            var sqlConnectionString = configuration.GetConnectionString("SqlServerConnection");
            db = new SqlConnection(sqlConnectionString);
        }
        #endregion


        #region GetServices
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<ServiceList> GetServices()
        {
            List<ServiceList> rolemenu = new List<ServiceList>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                rolemenu = db.Query<ServiceList>("procSelectService", param, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return rolemenu;
        }
        #endregion

        public DBResponse AddServiceMappingFunction(ServiceList rm)
        {
            DBResponse dbResponse = new DBResponse();
            List<int> status = new List<int>();
            try
            {

                string xml = "<Servicategorylist>";
                foreach (int id in rm.ServiceCategoryId)
                {
                    xml = xml + "<ServiceCategoryId>" + id.ToString() + "</ServiceCategoryId>";
                }

                xml = xml + "</Servicategorylist>";
                               
                var parameters = new DynamicParameters();
                parameters.Add("@ServiceId", rm.ServiceCode, DbType.String, ParameterDirection.Input);
                    
                    status = db.Query<int>("SELECT [ServiceId] FROM [tbl_Service] WHERE [ServiceCode] = @ServiceId", parameters, commandType: CommandType.Text).ToList();
                int serviceId = status.FirstOrDefault();

                DynamicParameters param1 = new DynamicParameters();

                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("ServiceId", typeof(int)));
                dt.Columns.Add(new DataColumn("ServiceCategoryId", typeof(int)));
                dt.Columns.Add(new DataColumn("CreatedBy", typeof(int)));
                dt.Columns.Add(new DataColumn("CreatedDate", typeof(DateTime)));

                param1.Add("servicemapping", dt.AsTableValuedParameter());
                foreach (int id in rm.ServiceCategoryId)
                {
                    dt.Rows.Add(serviceId, id, rm.CreatedBy, DateTime.Now);
                }
                dt.AcceptChanges();
                           
                dbResponse = db.QuerySingleOrDefault<DBResponse>("procInsertServiceCategoryMapping", param1, commandType: CommandType.StoredProcedure);

                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }



        #region AddService
        /// <summary>
        /// To add new Service
        /// </summary>
        /// <param name="rm">Service model</param>
        /// <returns>int</returns>
        public DBResponse AddService(ServiceList rm)
        {
            
            DBResponse dbResponse = new DBResponse();
            try
            {
                
                string xml = "<Servicategorylist>";
                foreach(int id in rm.ServiceCategoryId)
                {
                    xml = xml + "<ServiceCategoryId>" + id.ToString() + "</ServiceCategoryId>";
                }

                xml = xml + "</Servicategorylist>";

                DynamicParameters param = new DynamicParameters();              
                
                param.Add("@serviceCode", rm.ServiceCode);
                param.Add("@serviceName", rm.ServiceName);
                param.Add("@serviceCategoryId", xml);
                param.Add("@isGroup", rm.IsGroup);
                param.Add("@costtype", rm.CostType);
                param.Add("@description", rm.Description);
                param.Add("@createdBy", rm.CreatedBy);
                              
                db.Open();
              
                dbResponse = db.QuerySingle<DBResponse>("procInsertService", param, commandType: CommandType.StoredProcedure);
               // DBResponse dBResponse = (DBResponse)this.AddServiceMappingFunction(rm);

                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region GetServiceListById
        public ServiceList GetServiceListById(int id)
        {
            ServiceList serviceCategory = new ServiceList();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@serviceId", id);
                serviceCategory = db.Query<ServiceList>("procSelectServiceById", param, commandType: CommandType.StoredProcedure).SingleOrDefault();
                serviceCategory.ServiceCategoryId = db.Query<int>("procSelectServiceCategoryByServiceId", param, commandType: CommandType.StoredProcedure).ToArray();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return serviceCategory;
        }
        #endregion

        #region LoadService
        public List<ServiceList> LoadService(int id)
        {
            List<ServiceList> subServices = new List<ServiceList>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@serviceId", id);
                subServices = db.Query<ServiceList>("procSelectSubServiceById", param, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return subServices;
        }
        #endregion

        #region LoadSubService
        public List<SubServiceList> LoadSubService(int id)
        {
            List<SubServiceList> subServices = new List<SubServiceList>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@serviceId", id);
                subServices = db.Query<SubServiceList>("procSelectService_SubService", param, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return subServices;
        }
        #endregion

        #region GetSubServiceListById
        public List<SubServiceList> GetSubServiceListById(int parentServiceid, int serviceId)
        {
            List<SubServiceList> subServices = new List<SubServiceList>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@serviceId", serviceId);
                param.Add("@parentserviceId", parentServiceid);
                subServices = db.Query<SubServiceList>("procSelectService_SubServiceById", param, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return subServices;
        }
        #endregion

        #region UpdateServiceList
        /// <summary>
        /// To update new Service
        /// </summary>
        /// <param name="rm">Service model</param>
        /// <returns>int</returns>
        public DBResponse UpdateServiceList(ServiceList rm)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                string xml = "<Servicategorylist>";
                foreach (int id in rm.ServiceCategoryId)
                {
                    xml = xml + "<ServiceCategoryId>" + id.ToString() + "</ServiceCategoryId>";
                }

                xml = xml + "</Servicategorylist>";

                DynamicParameters param = new DynamicParameters();
                param.Add("@serviceCode", rm.ServiceCode);
                param.Add("@serviceName", rm.ServiceName);
                param.Add("@serviceCategoryId", xml);
                param.Add("@isGroup", rm.IsGroup);
                param.Add("@costtype", rm.CostType);
                param.Add("@description", rm.Description);
                param.Add("@modifiedBy", rm.ModifiedBy);
                param.Add("@serviceId", rm.ServiceId);
                param.Add("@isActive", rm.IsActive);
                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("procUpdateService", param, commandType: CommandType.StoredProcedure);
                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion


        #region DeleteService
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DBResponse DeleteServiceList(ServiceList serviceList)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@ServiceId", serviceList.ServiceId);
                param.Add("@ModifiedBy", serviceList.ModifiedBy);
                         
                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("procDeleteService", param, commandType: CommandType.StoredProcedure);
                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region GetServicesServerSide
        public DataTableServerSideResult<ServiceList> GetServicesServerSide(DataTableServerSideSettings DTSettings)
        {
            // fileLogger.Info("Inside GetServicesServerSide() function in UserRepository.");

            List<ServiceList> ServiceList = new List<ServiceList>();
            DataTableServerSideResult<ServiceList> dtResult = new DataTableServerSideResult<ServiceList>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@pageSize", DTSettings.pageSize);
                param.Add("@skip", DTSettings.skip);
                param.Add("@sortColumn", DTSettings.sortColumn);
                param.Add("@sortColumnDir", DTSettings.sortColumnDir);
                param.Add("@searchValue", DTSettings.searchValue);
                param.Add("@ServiceCode", DTSettings.ColumnSearch.ContainsKey("ServiceCode") == true ? DTSettings.ColumnSearch["ServiceCode"] : "");
                param.Add("@ServiceName", DTSettings.ColumnSearch.ContainsKey("ServiceName") == true ? DTSettings.ColumnSearch["ServiceName"] : "");
                param.Add("@ServiceCategoryName", DTSettings.ColumnSearch.ContainsKey("ServiceCategoryName") == true ? DTSettings.ColumnSearch["ServiceCategoryName"] : "");

                param.Add("@IsGroup", DTSettings.ColumnSearch.ContainsKey("IsGroup") == true ? DTSettings.ColumnSearch["IsGroup"] : "");
                param.Add("@CostType", DTSettings.ColumnSearch.ContainsKey("CostType") == true ? DTSettings.ColumnSearch["CostType"] : "");
                param.Add("@Description", DTSettings.ColumnSearch.ContainsKey("Description") == true ? DTSettings.ColumnSearch["Description"] : "");
                param.Add("@ModifiedDate", DTSettings.ColumnSearch.ContainsKey("ModifiedDate") == true ? DTSettings.ColumnSearch["ModifiedDate"] : "");
                param.Add("@IsActive", DTSettings.ColumnSearch.ContainsKey("IsActive") == true ? DTSettings.ColumnSearch["IsActive"] : "");
             

                ServiceList = db.Query<ServiceList>("procSelectServiceServerSide", param, commandType: CommandType.StoredProcedure).ToList();

                if (ServiceList.Count > 0)
                {
                    dtResult.recordsTotal = ServiceList[0].recordsTotal;
                    dtResult.recordsFiltered = ServiceList[0].recordsFiltered;
                }

                dtResult.data = ServiceList;
            }
            catch (Exception ex)
            {
                // fileLogger.Error(ex, "Error on GetServicesServerSide() funcion in UserRepository.");
                //databaseLogger.Error(ex, "Error on GetServicesServerSide() funcion in UserRepository.");
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dtResult;
        }
        #endregion

        #region AddSubService
        /// <summary>
        /// To add new AddSubService
        /// </summary>
        /// <param name="rm">Service model</param>
        /// <returns>int</returns>
        public DBResponse AddSubService(SubServiceList rm)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {



                DynamicParameters param = new DynamicParameters();


                param.Add("@parentServiceID", rm.ParentServiceId);
                param.Add("@option", rm.Option);
                param.Add("@serviceId", rm.ServiceId);
                param.Add("@sort", rm.Sort);
                param.Add("@createdBy", rm.CreatedBy);
                param.Add("@createdDate", DateTime.Now);



                db.Open();



                dbResponse = db.QuerySingle<DBResponse>("procInsertSubService", param, commandType: CommandType.StoredProcedure);




                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region UpdateSubService
        /// <summary>
        /// To add new AddSubService
        /// </summary>
        /// <param name="rm">Service model</param>
        /// <returns>int</returns>
        public DBResponse UpdateSubService(SubServiceList rm)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {



                DynamicParameters param = new DynamicParameters();


                param.Add("@parentServiceID", rm.ParentServiceId);
                param.Add("@option", rm.Option);
                param.Add("@serviceId", rm.ServiceId);
                param.Add("@sort", rm.Sort);
                param.Add("@createdBy", rm.CreatedBy);
                param.Add("@createdDate", DateTime.Now);



                db.Open();



                dbResponse = db.QuerySingle<DBResponse>("procUpdateSubService", param, commandType: CommandType.StoredProcedure);




                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region DeleteSubService
        /// <summary>
        /// To add new DeleteSubService
        /// </summary>
        /// <param name="rm">Service model</param>
        /// <returns>int</returns>
        public DBResponse DeleteSubService(SubServiceList rm)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {



                DynamicParameters param = new DynamicParameters();


                param.Add("@parentServiceID", rm.ParentServiceId);
                param.Add("@serviceId", rm.ServiceId);
                param.Add("@CreatedBy", rm.CreatedBy);

                db.Open();



                dbResponse = db.QuerySingle<DBResponse>("procDeleteSubService", param, commandType: CommandType.StoredProcedure);




                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        //#region GetServiceForOwner
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <returns></returns>
        //public List<ServiceList> GetServiceForOwner()
        //{
        //    List<ServiceList> ServiceList = new List<ServiceList>();
        //    List<SubServiceList> SubServiceList = new List<SubServiceList>();
        //    try
        //    {
        //        db.Open();
        //        DynamicParameters param = new DynamicParameters();
        //        ServiceList = db.Query<ServiceList>("procSelectServiceListForOwner", param, commandType: CommandType.StoredProcedure).ToList();
        //        SubServiceList = db.Query<SubServiceList>("procSelectSubServiceListForOwner",  commandType: CommandType.StoredProcedure).ToList();
        //        for(var i=0;i< ServiceList.Count;i++)
        //        {
        //            for(var j=0;j<SubServiceList.Count;j++)
        //            {
        //                if(ServiceList[i].ServiceId==SubServiceList[j].ParentServiceId)
        //                {
        //                    ServiceList[i].SubServiceList.Add(SubServiceList[j]);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        db.Close();
        //    }
        //    return ServiceList;
        //}
        //#endregion
    }

}
