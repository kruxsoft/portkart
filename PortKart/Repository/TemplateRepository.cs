﻿using Dapper;
using Domain;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class TemplateRepository : ITemplateRepository
    {
        #region GLOBAL VARIABLE
        private IDbConnection db;
        private readonly ILogger<TemplateRepository> _logger;
        public TemplateRepository(IConfiguration configuration, ILogger<TemplateRepository> logger)
        {
            var sqlConnectionString = configuration.GetConnectionString("SqlServerConnection");
            db = new SqlConnection(sqlConnectionString);
            _logger = logger;
        }

        #endregion

        //#region AddTemplate

        //public DBResponse AddTemplate(Template sc)
        //{

        //    DBResponse dbResponse = new DBResponse();
        //    try
        //    {
        //        DynamicParameters param = new DynamicParameters();
        //        param.Add("@templateName", sc.TemplateName);
        //        param.Add("@agencyId", sc.AgencyId);
        //        param.Add("@createdBy", sc.CreatedBy);

        //        db.Open();
        //        dbResponse = db.QuerySingle<DBResponse>("procInsertTemplate", param, commandType: CommandType.StoredProcedure);

        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //    finally
        //    {
        //        db.Close();
        //    }
        //    return dbResponse;
        //}
        //#endregion

        #region GetServiceList
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public List<ServiceList> GetServiceList()
        {
            _logger.LogInformation("Inside GetServiceList() function in OwnerRepository");
            List<ServiceList> ServiceList = new List<ServiceList>();
            string idstring = "";
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@ServiceCategoryId", idstring);
                ServiceList = db.Query<ServiceList>("procSelectServiceListbyCategory", param, commandType: CommandType.StoredProcedure).ToList();


            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return ServiceList;
        }
        #endregion
        #region GetTemplateList
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public List<Template> GetTemplateList()
        {
            _logger.LogInformation("Inside GetServiceList() function in OwnerRepository");
            List<Template> TemplateList = new List<Template>();

            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();

                TemplateList = db.Query<Template>("procSelectOwnerTemplate", param, commandType: CommandType.StoredProcedure).ToList();


            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return TemplateList;
        }
        #endregion

        //#region GetTemplateList
        ///// <summary>
        ///// Get all users
        ///// </summary>
        ///// <returns></returns>
        //public List<Template> GetTemplateList(string search)
        //{
        //    _logger.LogInformation("Inside GetTemplateList() function in AgencyRepository");
        //    List<Template> Template = new List<Template>();
        //    try
        //    {
        //        db.Open();
        //        DynamicParameters param = new DynamicParameters();
        //        param.Add("@search", search);
        //        Template = db.Query<Template>("procSelectTemplateList", param, commandType: CommandType.StoredProcedure).ToList();


        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Log(LogLevel.Error, ex, ex.Message, null);
        //        throw ex;
        //    }
        //    finally
        //    {
        //        db.Close();
        //    }
        //    return Template;
        //}
        //#endregion

        #region LoadTemplateService
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public List<TemplateDetails> LoadTemplateService(int TemplateId, int AgencyId)
        {
            _logger.LogInformation("Inside LoadTemplateService() function in TempplateRepository");
            List<TemplateDetails> TemplateDetails = new List<TemplateDetails>();
            try
            {
                db.Open();
                DynamicParameters Param = new DynamicParameters();

                Param.Add("@AgencyId", AgencyId);
                Param.Add("@TemplateId", TemplateId);
                //[procSelectTemplateDetailsCopy]
                //procSelectTemplateDetails
                TemplateDetails = db.Query<TemplateDetails>("procSelectTemplateDetailsById", Param, commandType: CommandType.StoredProcedure).ToList();

            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return TemplateDetails;
        }
        #endregion

        #region LoadServicebyId
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public List<AgencyServiceDetails> LoadServicebyId(int id)
        {
            _logger.LogInformation("Inside LoadServicebyId() function in TemplateRepository");
            List<AgencyServiceDetails> AgencyServiceDetails = new List<AgencyServiceDetails>();
            try
            {
                db.Open();
                DynamicParameters Param = new DynamicParameters();
                Param.Add("@ServiceId", id);
                AgencyServiceDetails = db.Query<AgencyServiceDetails>("procSelectServicebyIdForAgency", Param, commandType: CommandType.StoredProcedure).ToList();

            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return AgencyServiceDetails;
        }
        #endregion

        #region SaveAgencyService
        /// <summary>
        /// To add new SaveAgencyService
        /// </summary>
        /// <param name="rm">Service model</param>
        /// <returns>int</returns>
        public DBResponse SaveAgencyServiceTemplate(Template rm)
        {
            _logger.LogInformation("Inside SaveAgencyServiceTemplate() function in TemplateRepository");
            DBResponse dbResponse = new DBResponse();
            IDbTransaction tran = null;
            try
            {
                int TemplateId = 0;
                db.Open();
                tran = db.BeginTransaction();
                DynamicParameters param = new DynamicParameters();

                param.Add("@AgencyId", rm.AgencyId);
                param.Add("@Templatename", rm.TemplateName);
                param.Add("@CreatedBy", rm.CreatedBy);
                param.Add("@OutTemplateId", dbType: DbType.Int32, direction: ParameterDirection.Output);
                dbResponse = db.QuerySingle<DBResponse>("procInsertAgencyServiceTemplate", param, transaction: tran, commandType: CommandType.StoredProcedure);
                if (dbResponse.MessageType != "ERROR")
                {
                    TemplateId = param.Get<int>("@OutTemplateId");

                    if (TemplateId > 0)
                    {
                        DynamicParameters param1 = new DynamicParameters();
                        foreach (TemplateDetails det in rm.TemplateDetail)
                        {
                            param1 = new DynamicParameters();
                            param1.Add("@TemplateId", TemplateId);
                            param1.Add("@ServiceId", det.ServiceId);
                            param1.Add("@CurrencyId", det.CurrencyId);
                            param1.Add("@IsParent", det.IsParent);
                            param1.Add("@ParentId", det.ParentId);
                            param1.Add("@Offer", det.Offer);
                            param1.Add("@Type", det.Type);
                            param1.Add("@CreatedBy", rm.CreatedBy);
                            dbResponse = db.QuerySingle<DBResponse>("procInsertAgencyServiceTemplateDetails", param1, transaction: tran, commandType: CommandType.StoredProcedure);

                        }
                    }
                }
                tran.Commit();
            }
            catch (Exception ex)
            {
                if (tran != null)
                { tran.Rollback(); }
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dbResponse;

        }
        #endregion

        #region GetOwnerTemplate
        /// <summary>
        /// Get all Owner Template
        /// </summary>
        /// <returns></returns>
        public List<OwnerTemplate> GetOwnerTemplateList()
        {
            _logger.LogInformation("Inside GetOwnerTemplateList() function in OwnerTemplateRepository");
            List<OwnerTemplate> ownerTemplate = new List<OwnerTemplate>();
            try
            {
                db.Open();
                ownerTemplate = db.Query<OwnerTemplate>("procSelectOwnerTemplate", null, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return ownerTemplate;
        }
        #endregion

        #region GetAllOwnerTemplateList
        public List<OwnerTemplate> GetAllOwnerTemplateList()
        {
            List<OwnerTemplate> ownerTemplate = new List<OwnerTemplate>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                ownerTemplate = db.Query<OwnerTemplate>("procSelectAllOwnerTemplates", param, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return ownerTemplate;
        }


        #endregion


        #region AddOwnerTemplate
        public DBResponse AddOwnerTemplate(OwnerTemplate sc)
        {
            _logger.LogInformation("Inside AddOwnerTemplate() function in OwnerTemplateRepository");
            DBResponse dbResponse = new DBResponse();
            try
            {


                string xml = "<Portlist>";
                if (sc.PortId != null)
                {
                    foreach (string id in sc.PortId)
                    {
                        xml = xml + "<PortId>" + id.ToString() + "</PortId>";
                    }
                }
                xml = xml + "</Portlist>";


                DynamicParameters param = new DynamicParameters();
                param.Add("@ownerId", sc.UserId);
                param.Add("@templateId", sc.TemplateId);
                param.Add("@portId", xml);
                param.Add("@startdate", sc.StartDate);
                param.Add("@enddate", sc.EndDate);
                param.Add("@createdBy", sc.CreatedBy);
                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("procInsertOwnerTemplate", param, commandType: CommandType.StoredProcedure);

            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dbResponse;
        }
        #endregion


        #region GetOwnerTemplateById
        public OwnerTemplate GetOwnerTemplateById(int id)
        {
            OwnerTemplate ot = new OwnerTemplate();
            List<VesselPort> Portlist = new List<VesselPort>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@OwnerTemplateId", id);


                ot = db.Query<OwnerTemplate>("procSelectOwnerTemplateById", param, commandType: CommandType.StoredProcedure).SingleOrDefault();
                Portlist = db.Query<VesselPort>("procSelectPortsByOwnerTemplateId", param, commandType: CommandType.StoredProcedure).ToList();
                ot.PortList = Portlist;
                ot.PortId = Portlist.Select(x => x.PortId.ToString()).ToArray();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return ot;
        }
        #endregion


        #region UpdateOwnerTemplate
        /// <summary>
        /// Update OwnerTemplate
        /// </summary>
        /// <param name="cs"></param>
        /// <returns></returns>
        public DBResponse UpdateOwnerTemplate(OwnerTemplate cs)
        {
            _logger.LogInformation("Inside UpdateOwnerTemplate() function in OwnerTemplateRepository");
            DBResponse dbResponse = new DBResponse();
            try
            {
                string xml = "<Portlist>";
                if (cs.PortId != null) {
                    foreach (string id in cs.PortId)
                    {
                        xml = xml + "<PortId>" + id.ToString() + "</PortId>";
                    }
                }
               
                xml = xml + "</Portlist>";

                DynamicParameters param = new DynamicParameters();
                param.Add("@OwnerTemplateId", cs.OwnerTemplateId);
                param.Add("@OwnerId", cs.OwnerId);
                param.Add("@templateId", cs.TemplateId);

                param.Add("@portId", xml);
                param.Add("@startdate", cs.StartDate);
                param.Add("@enddate", cs.EndDate);
                param.Add("@isActive", cs.IsActive);

                param.Add("@modifiedBy", cs.ModifiedBy);

                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("procUpdateOwnerTemplate", param, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dbResponse;
        }
        #endregion



        #region DeleteOwnerTemplate
        /// <summary>
        /// Delete owner template with id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DBResponse DeleteOwnerTemplate(OwnerTemplate sc)
        {
            _logger.LogInformation("Inside DeleteOwnerTemplate() function in OwnerTemplateRepository");
            DBResponse dbResponse = new DBResponse();
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@OwnerTemplateId", sc.OwnerTemplateId);

                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("[procDeleteOwnerTemplate]", param, commandType: CommandType.StoredProcedure);
                return dbResponse;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion


        #region GetUserOwnerList
        /// <summary>
        /// Get all Owners
        /// </summary>
        /// <returns></returns>
        public List<User> GetUserOwnerList()
        {
            _logger.LogInformation("Inside GetUserOwnerList() function in UserRepository");
            List<User> user = new List<User>();
            try
            {
                db.Open();
                user = db.Query<User>("procSelectUserOwnerList", commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return user;
        }
        #endregion


        #region GetPortInOwnerTemplate
        /// <summary>
        /// Get all Port In OwnerTemplate
        /// </summary>
        /// <returns></returns>
        public List<VesselPort> GetPortInOwnerTemplate()
        {
            _logger.LogInformation("Inside GetOwnerTemplateList() function in OwnerTemplateRepository");
            List<VesselPort> vesselPort = new List<VesselPort>();
            try
            {
                db.Open();                           
                vesselPort = db.Query<VesselPort>("procSelectPortName", null, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return vesselPort;
        }
        #endregion



        #region GetTemplateNameList
        public List<Template> GetTemplateNameList(string search)
        {
            _logger.LogInformation("Inside GetTemplateNameList() function in TemplateRepository");
            List<Template> template = new List<Template>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@search", search);
                template = db.Query<Template>("procSelectTemplateNameList", param, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return template;
        }


        #endregion



        #region UpdateServiceTemplate
        /// <summary>
        /// To add new UpdateAgencyService
        /// </summary>
        /// <param name="rm">Service model</param>
        /// <returns>int</returns>
        public DBResponse UpdateServiceTemplate(Template rm)
        {
            _logger.LogInformation("Inside UpdateServiceTemplate() function in templateRepository");
            DBResponse dbResponse = new DBResponse();
            IDbTransaction tran = null;
            try
            {
                int TemplateId = rm.TemplateId;
                db.Open();
                tran = db.BeginTransaction();
                DynamicParameters param = new DynamicParameters();

                param.Add("@AgencyId", rm.AgencyId);
                //param.Add("@ModifiedBy", rm.AgencyId);
                param.Add("@templateId", rm.TemplateId);
                dbResponse = db.QuerySingle<DBResponse>("procUpdateTemplate", param, transaction: tran, commandType: CommandType.StoredProcedure);

                if (TemplateId > 0)
                {
                    DynamicParameters param1 = new DynamicParameters();
                    foreach (TemplateDetails det in rm.TemplateDetail)
                    {

                        param1 = new DynamicParameters();
                        param1.Add("@TemplateId", TemplateId);
                        param1.Add("@TemplateDetailId", det.TemplateDetailId);
                        param1.Add("@ServiceId", det.ServiceId);
                        param1.Add("@CurrencyId", det.CurrencyId);
                        param1.Add("@Type", det.Type);
                        param1.Add("@Offer", det.Offer);
                        param1.Add("@CreatedBy", det.CreatedBy);
                        param1.Add("@IsParent", det.IsParent);
                        param1.Add("@ParentId", det.ParentId);

                        dbResponse = db.QuerySingle<DBResponse>("procUpdateTemplateDetails", param1, transaction: tran, commandType: CommandType.StoredProcedure);

                    }
                }


                tran.Commit();
            }
            catch (Exception ex)
            {
                if (tran != null)
                { tran.Rollback(); }
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dbResponse;

        }
        #endregion 

        #region GetVesselPortList
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public List<VesselPort> GetVesselPortList(string search)
        {
            _logger.LogInformation("Inside GetVesselPortList() function in AgencyRepository");
            List<VesselPort> VesselPort = new List<VesselPort>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@search", search);
                VesselPort = db.Query<VesselPort>("procSelectVesselPortListMapping", param, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return VesselPort;
        }
        #endregion 
    }
}
