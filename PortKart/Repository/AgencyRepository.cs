﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Domain;
using Microsoft.Extensions.Configuration;

namespace Repository
{
    public class AgencyRepository: IAgencyRepository
    {
        private IDbConnection db;
        private readonly ILogger<AgencyRepository> _logger;
        public AgencyRepository(IConfiguration configuration, ILogger<AgencyRepository> logger)
        {
            var sqlConnectionString = configuration.GetConnectionString("SqlServerConnection");
            db = new SqlConnection(sqlConnectionString);
            _logger = logger;
        }

        #region LoadAgencyService
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public List<AgencyServiceDetails> LoadAgencyService(int PortId, int AgencyId)
        {
            _logger.LogInformation("Inside LoadAgencyService() function in AgencyRepository");
            List<AgencyServiceDetails> AgencyServiceDetails = new List<AgencyServiceDetails>();
            try
            {
                db.Open();
                DynamicParameters Param = new DynamicParameters();
                Param.Add("@PortId", PortId);
                Param.Add("@AgencyId", AgencyId);
                
                
        AgencyServiceDetails = db.Query<AgencyServiceDetails>("procSelectAgencyServiceDetails", Param, commandType: CommandType.StoredProcedure).ToList();
             
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return AgencyServiceDetails;
        }
        #endregion

        #region GetVesselPortList
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public List<VesselPort> GetVesselPortList(string search)
        {
            _logger.LogInformation("Inside GetVesselPortList() function in AgencyRepository");
            List<VesselPort> VesselPort = new List<VesselPort>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@search", search);
                VesselPort = db.Query<VesselPort>("procSelectVesselPortList", param, commandType: CommandType.StoredProcedure).ToList();


            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return VesselPort;
        }
        #endregion

        #region GetOwnerList
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public List<User> GetOwnerList(string search)
        {
            _logger.LogInformation("Inside GetOwnerList() function in AgencyRepository");
            List<User> ownerlist = new List<User>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@search", search);
                ownerlist = db.Query<User>("procSelectOwnerListForAgency", param, commandType: CommandType.StoredProcedure).ToList();


            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return ownerlist;
        }
        #endregion

        #region GetServiceList
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public List<ServiceList> GetServiceList()
        {
            _logger.LogInformation("Inside GetServiceList() function in OwnerRepository");
            List<ServiceList> ServiceList = new List<ServiceList>();
            string idstring = "";
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@ServiceCategoryId", idstring);
                ServiceList = db.Query<ServiceList>("procSelectServiceListbyCategory", param, commandType: CommandType.StoredProcedure).ToList();


            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return ServiceList;
        }
        #endregion

        #region LoadServicebyId
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public List<AgencyServiceDetails> LoadServicebyId(string[] ids)
        {
            _logger.LogInformation("Inside LoadServicebyId() function in AgencyRepository");
            List<AgencyServiceDetails> AgencyServiceDetails = new List<AgencyServiceDetails>();
            try
            {
                string idstring = "";
                idstring = string.Join(",", ids);
                db.Open();
                DynamicParameters Param = new DynamicParameters();
                Param.Add("@ServiceId ", idstring);
                AgencyServiceDetails = db.Query<AgencyServiceDetails>("procSelectServicebyIdForAgency", Param, commandType: CommandType.StoredProcedure).ToList();

            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return AgencyServiceDetails;
        }
        #endregion

        #region SaveAgencyService
        /// <summary>
        /// To add new SaveAgencyService
        /// </summary>
        /// <param name="rm">Service model</param>
        /// <returns>int</returns>
        public DBResponse SaveAgencyService(AgencyService rm)
        {
            _logger.LogInformation("Inside SaveAgencyService() function in AgencyRepository");
            DBResponse dbResponse = new DBResponse();
            IDbTransaction tran = null;
            try
            {
                int AgencyServiceId = 0;
                db.Open();
                tran = db.BeginTransaction();
                DynamicParameters param = new DynamicParameters();
                param.Add("@PortId", rm.PortId);
                param.Add("@AgencyId", rm.AgencyId);
                param.Add("@CreatedBy", rm.CreatedBy);
                param.Add("@OutAgencyServiceId", dbType: DbType.Int32, direction: ParameterDirection.Output);
                dbResponse = db.QuerySingle<DBResponse>("procInsertAgencyService", param, transaction: tran, commandType: CommandType.StoredProcedure);
                if (dbResponse.MessageType != "ERROR")
                    AgencyServiceId = param.Get<int>("@OutAgencyServiceId");

                if(AgencyServiceId>0)
                {
                    DynamicParameters param1 = new DynamicParameters();
                    foreach(AgencyServiceDetails det in rm.ServiceDetails)
                    {
                        param1 = new DynamicParameters();
                        param1.Add("@AgencyServiceId", AgencyServiceId);
                        param1.Add("@ServiceId", det.ServiceId);
                        param1.Add("@CurrencyId", det.CurrencyId);
                        param1.Add("@Amount", det.Amount);
                        param1.Add("@Tat", det.Tat);
                        param1.Add("@Description", det.Description);
                        param1.Add("@IsParent", det.IsParent);
                        param1.Add("@IsActive", det.IsActive);
                        param1.Add("@TatDescription", det.TatDescription);
                        param1.Add("@CostType", det.CostType);
                        param1.Add("@Option", det.Option);
                        param1.Add("@ParentId", det.ParentId);
                        param1.Add("@CreatedBy", rm.CreatedBy);
                        dbResponse = db.QuerySingle<DBResponse>("procInsertAgencyServiceDetails", param1, transaction: tran, commandType: CommandType.StoredProcedure);

                    }
                }


                tran.Commit();
            }
            catch (Exception ex)
            {
                if (tran != null)
                { tran.Rollback(); }
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dbResponse;

        }
        #endregion

        #region UpdateAgencyService
        /// <summary>
        /// To add new UpdateAgencyService
        /// </summary>
        /// <param name="rm">Service model</param>
        /// <returns>int</returns>
        public DBResponse UpdateAgencyService(AgencyService rm)
        {
            _logger.LogInformation("Inside UpdateAgencyService() function in AgencyRepository");
            DBResponse dbResponse = new DBResponse();
            IDbTransaction tran = null;
            try
            {
                int AgencyServiceId = rm.AgencyServiceId;
                db.Open();
                tran = db.BeginTransaction();
                DynamicParameters param = new DynamicParameters();
                param.Add("@PortId", rm.PortId);
                param.Add("@AgencyId", rm.AgencyId);
                param.Add("@CreatedBy", rm.CreatedBy);
                param.Add("@AgencyServiceId", rm.AgencyServiceId);
                dbResponse = db.QuerySingle<DBResponse>("procUpdateAgencyService", param, transaction: tran, commandType: CommandType.StoredProcedure);

                if (AgencyServiceId > 0)
                {
                    DynamicParameters param1 = new DynamicParameters();
                    foreach (AgencyServiceDetails det in rm.ServiceDetails)
                    {
                        param1 = new DynamicParameters();
                        param1.Add("@AgencyServiceId", AgencyServiceId);
                        param1.Add("@AgencyDetailId", det.AgencyDetailId);
                        param1.Add("@ServiceId", det.ServiceId);
                        param1.Add("@CurrencyId", det.CurrencyId);
                        param1.Add("@Amount", det.Amount);
                        param1.Add("@Tat", det.Tat);
                        param1.Add("@Description", det.Description);
                        param1.Add("@IsParent", det.IsParent);
                        param1.Add("@IsActive", det.IsActive);
                        param1.Add("@TatDescription", det.TatDescription);
                        param1.Add("@CostType", det.CostType);
                        param1.Add("@Option", det.Option);
                        param1.Add("@ParentId", det.ParentId);
                        param1.Add("@CreatedBy", rm.CreatedBy);
                        dbResponse = db.QuerySingle<DBResponse>("procUpdateAgencyServiceDetails", param1, transaction: tran, commandType: CommandType.StoredProcedure);

                    }
                }


                tran.Commit();
            }
            catch (Exception ex)
            {
                if (tran != null)
                { tran.Rollback(); }
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dbResponse;

        }
        #endregion

        #region GetServiceListByCategory
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public List<ServiceList> GetServiceListByCategory(string[] ids)
        {
            _logger.LogInformation("Inside GetServiceListByCategory() function in AgencyRepository");
            List<ServiceList> ServiceList = new List<ServiceList>();
            string idstring = "";
            idstring = string.Join(",", ids);
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@ServiceCategoryId", idstring);
                ServiceList = db.Query<ServiceList>("procSelectServiceListbyCategory", param, commandType: CommandType.StoredProcedure).ToList();


            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return ServiceList;
        }
        #endregion

        #region GetServiceCategoryList
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public List<ServiceCategory> GetServiceCategoryList()
        {
            _logger.LogInformation("Inside GetServiceCategoryList() function in AgencyRepository");
            List<ServiceCategory> ServiceCategory = new List<ServiceCategory>();
            try
            {
                db.Open();
                ServiceCategory = db.Query<ServiceCategory>("procSelectServiceCategoryList", commandType: CommandType.StoredProcedure).ToList();


            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return ServiceCategory;
        }
        #endregion

        #region LoadOwnerTemplate
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public OwnerTemplateMapping LoadOwnerTemplate(int OwnerId,int AgencyId)
        {
            _logger.LogInformation("Inside LoadOwnerTemplate() function in AgencyRepository");
            OwnerTemplateMapping template = new OwnerTemplateMapping();
            List<TemplateDetails> details = new List<TemplateDetails>();
            try
            {
               
                db.Open();
                DynamicParameters Param = new DynamicParameters();
                Param.Add("@OwnerId", OwnerId);
                Param.Add("@AgencyId", AgencyId);
                template = db.QuerySingleOrDefault<OwnerTemplateMapping>("procSelectAgencyOwnerTemplate", Param, commandType: CommandType.StoredProcedure);
                if(template!=null && template.TemplateId>0)
                {
                    DynamicParameters Param2 = new DynamicParameters();
                    Param2.Add("@TemplateId", template.TemplateId);
                    details = db.Query<TemplateDetails>("procSelectAgencyOwnerTemplateDetails", Param2, commandType: CommandType.StoredProcedure).ToList();
                   if(details!=null)
                    template.OffereDetails = details;
                }
                if(template==null)
                {
                    template=new OwnerTemplateMapping();
                }
                else
                {
                    DynamicParameters Param2 = new DynamicParameters();
                    Param2.Add("@OwnerTemplateId", template.OwnerTemplateId);
                    template.PortIds = db.Query<int>("procSelectAgencyOwnerTemplatePorts", Param2, commandType: CommandType.StoredProcedure).ToArray();
                }
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return template;
        }
        #endregion

        #region DeleteService
        /// <summary>
        /// To add new DeleteService
        /// </summary>
        /// <param name="rm">Service model</param>
        /// <returns>int</returns>
        public DBResponse DeleteService(int id)
        {
            _logger.LogInformation("Inside DeleteService() function in AgencyRepository");
            DBResponse dbResponse = new DBResponse();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@AgencyDetailId", id);
                dbResponse = db.QuerySingleOrDefault<DBResponse>("procDeleteAgencyServiceDetail", param, commandType: CommandType.StoredProcedure);

            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dbResponse;

        }
        #endregion  
    }
}
