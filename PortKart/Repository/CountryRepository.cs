﻿using Dapper;
using Domain;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class CountryRepository : ICountryRepository
    {
        #region GLOBAL VARIABLE
        private IDbConnection db;
        public CountryRepository(IConfiguration configuration)
        {
            var sqlConnectionString = configuration.GetConnectionString("SqlServerConnection");
            db = new SqlConnection(sqlConnectionString);
        }
        #endregion

        #region GetAllCountries
        public List<Country> GetAllCountries()
        {
            List<Country> country = new List<Country>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                country = db.Query<Country>("procSelectCountry", param, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return country;
        }


        #endregion


        #region GetCountryList
        public List<Country> GetCountryList()
        {
            List<Country> country = new List<Country>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                country = db.Query<Country>("procCountryList", param, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return country;
        }


        #endregion

        #region AddCountry

        public DBResponse AddCountry(Country sc)
        {

            DBResponse dbResponse = new DBResponse();
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@countryName", sc.CountryName);
                param.Add("@createdBy", sc.CreatedBy);
                param.Add("@modifiedBy", sc.ModifiedBy);

                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("procInsertCountry", param, commandType: CommandType.StoredProcedure);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dbResponse;
        }
        #endregion

        #region GetCountryById
        public Country GetCountryById(int id)
        {
            Country country = new Country();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@countryId", id);
                country = db.Query<Country>("procSelectCountryById", param, commandType: CommandType.StoredProcedure).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return country;
        }
        #endregion

        #region UpdateCountry
        public DBResponse UpdateCountry(Country sc)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@countryName", sc.CountryName);
                param.Add("@countryId", sc.CountryId);
                param.Add("@modifiedBy", sc.ModifiedBy);
                param.Add("@isActive", sc.IsActive);

                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("procUpdateCountry", param, commandType: CommandType.StoredProcedure);

                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region DeleteCountry

        public DBResponse DeleteCountry(Country sObj)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@countryId", sObj.CountryId);
                param.Add("@modifiedBy", sObj.ModifiedBy);
                db.Open();
                DBResponse dbResponse = db.QuerySingle<DBResponse>("procDeleteCountry", param, commandType: CommandType.StoredProcedure);

                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

    }
}
