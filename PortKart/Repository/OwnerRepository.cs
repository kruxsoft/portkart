﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Domain;
using Microsoft.Extensions.Configuration;

namespace Repository
{
    public class OwnerRepository : IOwnerRepository
    {
        private IDbConnection db;
        private readonly ILogger<OwnerRepository> _logger;
        public OwnerRepository(IConfiguration configuration, ILogger<OwnerRepository> logger)
        {
            var sqlConnectionString = configuration.GetConnectionString("SqlServerConnection");
            db = new SqlConnection(sqlConnectionString);
            _logger = logger;
        }

        #region GetAgencyServiceForOwner
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public List<AgencyService> GetAgencyServiceForOwner()
        {
            _logger.LogInformation("Inside GetAgencyServiceForOwner() function in OwnerRepository");
            List<AgencyService> AgencyService = new List<AgencyService>();
            List<AgencyServiceDetails> AgencyServiceDetails = new List<AgencyServiceDetails>();
            try
            {
                db.Open();
                AgencyService = db.Query<AgencyService>("procSelectAgencyServiceForOwner", commandType: CommandType.StoredProcedure).ToList();
                AgencyServiceDetails = db.Query<AgencyServiceDetails>("procSelectAgencyServiceDetailsForOwner", commandType: CommandType.StoredProcedure).ToList();
                for (var i = 0; i < AgencyService.Count; i++)
                {
                    for (var j = 0; j < AgencyServiceDetails.Count; j++)
                    {
                        if (AgencyService[i].AgencyServiceId == AgencyServiceDetails[j].AgencyServiceId)
                        {
                            AgencyService[i].ServiceDetails.Add(AgencyServiceDetails[j]);
                        }
                    }
                    AgencyService[i].ServiceCount = AgencyService[i].ServiceDetails.Where(x => x.IsParent == true).Count();

                }
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return AgencyService;
        }
        #endregion

        #region GetVesselPortList
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public List<VesselPort> GetVesselPortList(string search)
        {
            _logger.LogInformation("Inside GetVesselPortList() function in OwnerRepository");
            List<VesselPort> VesselPort = new List<VesselPort>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@search", search);
                VesselPort = db.Query<VesselPort>("procSelectVesselPortList", param, commandType: CommandType.StoredProcedure).ToList();


            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return VesselPort;
        }
        #endregion

        #region GetServiceCategoryList
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public List<ServiceCategory> GetServiceCategoryList()
        {
            _logger.LogInformation("Inside GetServiceCategoryList() function in OwnerRepository");
            List<ServiceCategory> ServiceCategory = new List<ServiceCategory>();
            try
            {
                db.Open();
                ServiceCategory = db.Query<ServiceCategory>("procSelectServiceCategoryList", commandType: CommandType.StoredProcedure).ToList();


            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return ServiceCategory;
        }
        #endregion

        #region GetServiceList
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public List<ServiceList> GetServiceList(string[] ids)
        {
            _logger.LogInformation("Inside GetServiceList() function in OwnerRepository");
            List<ServiceList> ServiceList = new List<ServiceList>();
            string idstring = "";
            idstring = string.Join(",", ids);
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@ServiceCategoryId", idstring);
                ServiceList = db.Query<ServiceList>("procSelectServiceListbyCategory", param, commandType: CommandType.StoredProcedure).ToList();


            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return ServiceList;
        }
        #endregion

        #region Search
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public AgencySearchList Search(AgencyFilter ids)
        {
            _logger.LogInformation("Inside Search() function in OwnerRepository");
            AgencySearchList AgencySearchList = new AgencySearchList();
            List<AgencyService> AgencyService = new List<AgencyService>();
            List<AgencyServiceDetails> AgencyServiceDetails = new List<AgencyServiceDetails>();
            string servicecategoryid = "", serviceid = "";
            if (ids.ServiceCategoryId != null)
                servicecategoryid = string.Join(",", ids.ServiceCategoryId);

            if (ids.ServiceId != null)
                serviceid = string.Join(",", ids.ServiceId);

            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();

                param.Add("@PortId", ids.PortId);
                param.Add("@ViewBy", ids.ViewBy);
                param.Add("@SortBy", ids.SortBy);
                AgencyService = db.Query<AgencyService>("procSearchAgency", param, commandType: CommandType.StoredProcedure).ToList();
                if (AgencyService.Count > 0)
                {
                    var AgencyServiceId = string.Join(",", AgencyService.Select(x => x.AgencyServiceId).ToArray());
                    DynamicParameters param2 = new DynamicParameters();
                    param2.Add("@ServiceCategoryId", servicecategoryid);
                    param2.Add("@ServiceId", serviceid);
                    param2.Add("@AgencyServiceId", AgencyServiceId);
                    param2.Add("@OwnerId", ids.OwnerId);
                    param2.Add("@PortId", ids.PortId);
                    AgencyServiceDetails = db.Query<AgencyServiceDetails>("procSearchAgencyDetails", param2, commandType: CommandType.StoredProcedure).ToList();
                    for (var i = 0; i < AgencyService.Count; i++)
                    {
                        for (var j = 0; j < AgencyServiceDetails.Count; j++)
                        {
                            if (AgencyService[i].AgencyServiceId == AgencyServiceDetails[j].AgencyServiceId)
                            {
                                AgencyService[i].ServiceDetails.Add(AgencyServiceDetails[j]);
                            }
                        }
                        AgencyService[i].ServiceCount = AgencyService[i].ServiceDetails.Where(x => x.IsParent == true || x.ParentId == 0).Count();
                    }
                }

                var query = (from t in AgencyServiceDetails
                             group t by new { t.ServiceId, t.ServiceName, t.IsGroup, t.IsParent, t.ParentId, t.Sort, t.OgOption }
                             into grp
                             select new AgencyServiceDetails
                             {
                                 ServiceId = grp.Key.ServiceId,
                                 ServiceName = grp.Key.ServiceName,
                                 IsGroup = grp.Key.IsGroup,
                                 IsParent = grp.Key.IsParent,
                                 ParentId = grp.Key.ParentId,
                                 Sort = grp.Key.Sort,
                                 OgOption = grp.Key.OgOption

                             }).ToList();

                AgencyService = AgencyService.Where(x => x.ServiceDetails.Count > 0).ToList();
                if (ids.ServiceId != null && ids.ServiceId.Length > 0)
                {
                    List<AgencyService> AgencyService2 = new List<AgencyService>();
                    for (var i = 0; i < AgencyService.Count; i++)
                    {
                        var svnofound = false;
                        for (var k = 0; k < ids.ServiceId.Length; k++)
                        {
                            var svcexsts = false;
                            for (var j = 0; j < AgencyService[i].ServiceDetails.Count; j++)
                            {
                                if (ids.ServiceId[k] == AgencyService[i].ServiceDetails[j].ServiceId
                                    && AgencyService[i].ServiceDetails[j].ParentId == 0)
                                {
                                    svcexsts = true;
                                }
                            }
                            if (!svcexsts) { svnofound = true; }
                        }
                        if (!svnofound)
                        {
                            AgencyService2.Add(AgencyService[i]);
                        }
                    }
                    AgencyService = AgencyService2;
                }
                var offrapplied = false;
                if (ids.ServiceId != null)
                    AgencySearchList.FServiceList = query.Where(x => (ids.ServiceId.Contains(x.ServiceId) && x.ParentId == 0) || (ids.ServiceId.Contains(x.ParentId))).ToList();
                else
                    AgencySearchList.FServiceList = query;

                if (ids.ViewBy == "A")
                {
                    AgencyService = AgencyService.OrderBy(x => x.CompanyName).ToList();
                }
                else
                {
                    if (ids.ServiceId != null && ids.ServiceId.Length == 1)
                    {
                        AgencyService = LoadAndApplyTemplateOffer(AgencyService, ids.OwnerId, ids.PortId);
                        offrapplied = true;
                        for (var i = 0; i < AgencyService.Count; i++)
                        {
                            if (ids.ServiceId != null && ids.ServiceId.Length == 1 && AgencyService[i].ServiceDetails.Count > 0)
                            {
                                AgencyService[i].SortAmount = AgencyService[i].ServiceDetails.FirstOrDefault(x => x.ServiceId == ids.ServiceId[0] && x.ParentId == 0).ActualAmount;
                            }
                        }
                        AgencyService = AgencyService.OrderBy(x => x.SortAmount).ThenByDescending(x => x.Rating).ThenBy(x => x.CompanyName).ToList();

                    }
                    else
                        AgencyService = AgencyService.OrderByDescending(x => x.Rating).ThenByDescending(x => x.ServiceCount).ThenBy(x => x.CompanyName).ToList();
                }
                AgencySearchList.totalcount = AgencyService.Count;
                AgencySearchList.AgencyServiceList = AgencyService.Skip(ids.skip).Take(ids.take).ToList();
                if (offrapplied != true)
                {
                    AgencySearchList.AgencyServiceList = LoadAndApplyTemplateOffer(AgencySearchList.AgencyServiceList, ids.OwnerId, ids.PortId);
                }
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return AgencySearchList;
        }


        #endregion

        #region SearchService
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public List<AgencyServiceDetails> SearchService(AgencyFilter ids)
        {
            _logger.LogInformation("Inside Search() function in OwnerRepository");
            List<AgencyServiceDetails> AgencyServiceDetails = new List<AgencyServiceDetails>();
            string servicecategoryid = "", serviceid = "";
            if (ids.ServiceCategoryId != null)
                servicecategoryid = string.Join(",", ids.ServiceCategoryId);

            if (ids.ServiceId != null)
                serviceid = string.Join(",", ids.ServiceId);

            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@ServiceCategoryId", servicecategoryid);
                param.Add("@ServiceId", serviceid);
                param.Add("@PortId", ids.PortId);

                AgencyServiceDetails = db.Query<AgencyServiceDetails>("procSelectServiceListForOwner", param, commandType: CommandType.StoredProcedure).ToList();


            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return AgencyServiceDetails;
        }
        #endregion

        #region FilterAgency
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public AgencySearchList FilterAgency(AgencyFilter ids)
        {
            _logger.LogInformation("Inside FilterAgency() function in OwnerRepository");
            AgencySearchList Finalresult = new AgencySearchList();
            List<AgencyService> AgencyService = new List<AgencyService>();
            List<AgencyServiceDetails> AgencyServiceDetails = new List<AgencyServiceDetails>();
            List<AgencyServiceDetails> FilteredServiceDetails = new List<AgencyServiceDetails>();
            List<FilterByService> FilterByList = new List<FilterByService>();
            string servicecategoryid = "", serviceid = "";


            try
            {
                if (ids.ServiceCategoryId != null)
                    servicecategoryid = string.Join(",", ids.ServiceCategoryId);

                if (ids.ServiceId != null)
                    serviceid = string.Join(",", ids.ServiceId);

                FilterByList = ids.FilterByService;
                if (FilterByList == null)
                {
                    FilterByList = new List<FilterByService>();
                }
                var FSingleServiceAr = FilterByList.Where(x => x.IsParent == false && x.ParentId == 0).Select(x => x.ServiceId).ToArray();
                var FParentServiceAr = FilterByList.Where(x => x.IsParent == true && x.ParentId == 0).Select(x => x.ServiceId).ToArray();
                var FSubServiceAr = FilterByList.Where(x => x.IsParent == false && x.ParentId > 0).Select(x => x.ServiceId).ToArray();
                string FSingleService = "", FParentService = "", FSubService = "";
                int selectedservicecount = 0;
                if (FSingleServiceAr != null)
                {
                    FSingleService = string.Join(",", FSingleServiceAr);
                    selectedservicecount = selectedservicecount + FSingleServiceAr.Length;
                }

                if (FParentServiceAr != null)
                {
                    FParentService = string.Join(",", FParentServiceAr);
                    selectedservicecount = selectedservicecount + FParentServiceAr.Length;
                }

                if (FSubServiceAr != null)
                    FSubService = string.Join(",", FSubServiceAr);
                db.Open();
                DynamicParameters param = new DynamicParameters();

                param.Add("@PortId", ids.PortId);
                param.Add("@ViewBy", ids.ViewBy);
                param.Add("@SortBy", ids.SortBy);
                AgencyService = db.Query<AgencyService>("procSearchAgency", param, commandType: CommandType.StoredProcedure).ToList();
                if (AgencyService.Count > 0)
                {
                    var AgencyServiceId = string.Join(",", AgencyService.Select(x => x.AgencyServiceId).ToArray());
                    DynamicParameters param2 = new DynamicParameters();
                    param2.Add("@ServiceCategoryId", servicecategoryid);
                    param2.Add("@ServiceId", serviceid);
                    param2.Add("@AgencyServiceId", AgencyServiceId);
                    param2.Add("@FSingleService", FSingleService);
                    param2.Add("@FParentService", FParentService);
                    param2.Add("@FSubService", FSubService);
                    AgencyServiceDetails = db.Query<AgencyServiceDetails>("procSearchAgencyDetailsFilter", param2, commandType: CommandType.StoredProcedure).ToList();

                    for (var i = 0; i < AgencyService.Count; i++)
                    {
                        for (var j = 0; j < AgencyServiceDetails.Count; j++)
                        {
                            if (AgencyService[i].AgencyServiceId == AgencyServiceDetails[j].AgencyServiceId)
                            {
                                AgencyService[i].ServiceDetails.Add(AgencyServiceDetails[j]);
                            }
                        }
                        AgencyService[i].ServiceCount = AgencyService[i].ServiceDetails.Where(x => x.IsParent == true || x.ParentId == 0).Count();
                    }
                }
                AgencyService = AgencyService.Where(x => x.ServiceDetails.Count > 0).ToList();
                List<int> filtrr = new List<int>();
                if (FilterByList.Count > 0)
                {
                    foreach (FilterByService fl in FilterByList)
                    {
                        if (fl.ParentId == 0)
                        {
                            filtrr.Add(fl.ServiceId);
                        }
                    }
                }
                if (ids.ServiceId != null && ids.ServiceId.Length > 0)
                {
                    List<AgencyService> AgencyService2 = new List<AgencyService>();
                    for (var i = 0; i < AgencyService.Count; i++)
                    {
                        var svnofound = false;
                        for (var k = 0; k < ids.ServiceId.Length; k++)
                        {
                            var svcexsts = false;
                            for (var j = 0; j < AgencyService[i].ServiceDetails.Count; j++)
                            {
                                if (ids.ServiceId[k] == AgencyService[i].ServiceDetails[j].ServiceId
                                    && AgencyService[i].ServiceDetails[j].ParentId == 0)
                                {
                                    svcexsts = true;
                                }
                            }
                            if (!svcexsts) { svnofound = true; }
                        }
                        if (!svnofound)
                        {
                            AgencyService2.Add(AgencyService[i]);
                        }
                    }
                    AgencyService = AgencyService2;
                }
                if (filtrr.Count > 0 && !ids.IsFirst && !ids.AllChecked)
                {
                    List<AgencyService> AgencyService2 = new List<AgencyService>();
                    for (var i = 0; i < AgencyService.Count; i++)
                    {
                        var svnofound = false;
                        for (var k = 0; k < filtrr.Count; k++)
                        {
                            var svcexsts = false;
                            for (var j = 0; j < AgencyService[i].ServiceDetails.Count; j++)
                            {
                                if (filtrr[k] == AgencyService[i].ServiceDetails[j].ServiceId
                                    && AgencyService[i].ServiceDetails[j].ParentId == 0)
                                {
                                    svcexsts = true;
                                }
                            }
                            if (!svcexsts) { svnofound = true; }
                        }
                        if (!svnofound)
                        {
                            AgencyService2.Add(AgencyService[i]);
                        }
                    }
                    AgencyService = AgencyService2;
                    AgencyService2 = new List<AgencyService>();
                    for (var i = 0; i < AgencyService.Count; i++)
                    {
                        var defsvc = false;
                        for (var j = 0; j < AgencyService[i].ServiceDetails.Count; j++)
                        {
                            if(AgencyService[i].ServiceDetails[j].IsParent)
                            {
                                var sublist = new List<AgencyServiceDetails>();
                                sublist = AgencyService[i].ServiceDetails.Where(x => x.ParentId == AgencyService[i].ServiceDetails[j].ServiceId).ToList();
                                var sublistF = new List<int>();
                                if (FilterByList.FirstOrDefault(x => x.ServiceId == AgencyService[i].ServiceDetails[j].ServiceId && x.IsParent == true) != null)
                                {
                                    sublistF = FilterByList.Where(x => x.ParentId == AgencyService[i].ServiceDetails[j].ServiceId).Select(x => x.ServiceId).ToList();
                                    var newlist = sublist.Where(x => !sublistF.Contains(x.ServiceId) && x.Option == "D").ToList();
                                    if (newlist.Count > 0)
                                    {
                                        defsvc = true;
                                    }
                                }
                            }
                        }

                        if (!defsvc)
                        {
                            AgencyService2.Add(AgencyService[i]);
                        }
                    }
                    AgencyService = AgencyService2;
                }
                var offrapplied = false;
                if (ids.ViewBy == "A")
                {
                    AgencyService = AgencyService.OrderBy(x => x.CompanyName).ToList();
                }
                else
                {
                    if ((ids.ServiceId != null && ids.ServiceId.Length == 1) || (selectedservicecount == 1))
                    {
                        AgencyService = LoadAndApplyTemplateOffer(AgencyService, ids.OwnerId, ids.PortId);
                        offrapplied = true;
                        for (var i = 0; i < AgencyService.Count; i++)
                        {
                            if (selectedservicecount == 0)
                                AgencyService[i].SortAmount = AgencyService[i].ServiceDetails.FirstOrDefault(x => x.ServiceId == ids.ServiceId[0] && x.ParentId == 0).ActualAmount;
                            else if (FParentServiceAr != null && FParentServiceAr.Length == 1)
                                AgencyService[i].SortAmount = AgencyService[i].ServiceDetails.FirstOrDefault(x => x.ServiceId == FParentServiceAr[0] && x.ParentId == 0).ActualAmount;
                            else if (FSingleServiceAr != null && FSingleServiceAr.Length == 1)
                                AgencyService[i].SortAmount = AgencyService[i].ServiceDetails.FirstOrDefault(x => x.ServiceId == FSingleServiceAr[0] && x.ParentId == 0).ActualAmount;

                        }
                        AgencyService = AgencyService.OrderBy(x => x.SortAmount).ThenByDescending(x => x.Rating).ThenBy(x => x.CompanyName).ToList();

                    }
                    else if (ids.SortBy == "A")
                    {
                        AgencyService = AgencyService.OrderBy(x => x.Rating).ToList();
                    }
                    else if (ids.SortBy == "D")
                    {
                        AgencyService = AgencyService.OrderByDescending(x => x.Rating).ToList();
                    }
                    else
                        AgencyService = AgencyService.OrderByDescending(x => x.Rating).ThenByDescending(x => x.ServiceCount).ThenBy(x => x.CompanyName).ToList();

                }

                Finalresult.totalcount = AgencyService.Count;
                Finalresult.AgencyServiceList = AgencyService.Skip(ids.skip).Take(ids.take).ToList();
                if (offrapplied != true)
                {
                    Finalresult.AgencyServiceList = LoadAndApplyTemplateOffer(Finalresult.AgencyServiceList, ids.OwnerId, ids.PortId);
                }
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return Finalresult;
        }
        #endregion

        #region LoadAndApplyTemplateOffer
        private List<AgencyService> LoadAndApplyTemplateOffer(List<AgencyService> agencyServiceList, int OwnerId, int PortId)
        {
            _logger.LogInformation("Inside LoadAndApplyTemplateOffer() function in OwnerRepository");

            try
            {
                for (int i = 0; i < agencyServiceList.Count; i++)
                {
                    OwnerTemplateMapping template = new OwnerTemplateMapping();
                    List<TemplateDetails> details = new List<TemplateDetails>();
                    DynamicParameters Param = new DynamicParameters();
                    Param.Add("@OwnerId", OwnerId);
                    Param.Add("@AgencyId", agencyServiceList[i].AgencyId);
                    template = db.QuerySingleOrDefault<OwnerTemplateMapping>("procSelectAgencyOwnerTemplate", Param, commandType: CommandType.StoredProcedure);
                    if (template != null)
                    {
                        DynamicParameters Param2 = new DynamicParameters();
                        Param2.Add("@OwnerTemplateId", template.OwnerTemplateId);
                        template.PortIds = db.Query<int>("procSelectAgencyOwnerTemplatePorts", Param2, commandType: CommandType.StoredProcedure).ToArray();

                    }
                    if (template != null && template.TemplateId > 0 && (template.PortIds.Contains(PortId) || template.PortIds.Length == 0))
                    {
                        DynamicParameters Param2 = new DynamicParameters();
                        Param2.Add("@TemplateId", template.TemplateId);
                        details = db.Query<TemplateDetails>("procSelectAgencyOwnerTemplateDetails", Param2, commandType: CommandType.StoredProcedure).ToList();
                        if (details != null)
                        {
                            template.OffereDetails = details;
                            agencyServiceList[i].ServiceDetails = CalculateOffer(agencyServiceList[i].ServiceDetails, template.OffereDetails);
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }

            return agencyServiceList;
        }

        private List<AgencyServiceDetails> CalculateOffer(List<AgencyServiceDetails> AgencyServiceList, List<TemplateDetails> OffereDetails)
        {
            if (OffereDetails.Count > 0 && AgencyServiceList.Count > 0)
            {
                for (var i = 0; i < OffereDetails.Count; i++)
                {
                    for (var j = 0; j < AgencyServiceList.Count; j++)
                    {
                        if (OffereDetails[i].ServiceId == AgencyServiceList[j].ServiceId && OffereDetails[i].ParentId == AgencyServiceList[j].ParentId
                            && OffereDetails[i].IsParent == AgencyServiceList[j].IsParent && AgencyServiceList[j].IsParent == false)
                        {
                            AgencyServiceList[j].Offer = OffereDetails[i].Offer;
                            AgencyServiceList[j].OfferType = OffereDetails[i].Type;
                            decimal actualamount = 0;
                            if (OffereDetails[i].Type == "A")
                            {
                                actualamount = (AgencyServiceList[j].Amount) - (OffereDetails[i].Offer);
                            }
                            else if (OffereDetails[i].Type == "P")
                            {
                                actualamount = (AgencyServiceList[j].Amount) - ((AgencyServiceList[j].Amount) * (OffereDetails[i].Offer) / 100);
                            }
                            if (actualamount > 0)
                            {
                                actualamount = Math.Round(actualamount, 2);
                            }
                            else
                            {
                                actualamount = 0;
                            }
                            AgencyServiceList[j].ActualAmount = actualamount;

                        }
                    }
                }
                for (var k = 0; k < AgencyServiceList.Count; k++)
                {
                    if (AgencyServiceList[k].CostType == "Cumulative" && AgencyServiceList[k].IsParent == true)
                    {
                        decimal totalamount = 0;
                        for (var i = 0; i < AgencyServiceList.Count; i++)
                        {
                            if (AgencyServiceList[i].ParentId == AgencyServiceList[k].ServiceId && AgencyServiceList[i].Option == "D")
                            {
                                totalamount = (totalamount) + (AgencyServiceList[i].ActualAmount);
                            }
                        }
                        totalamount = Math.Round(totalamount, 2);
                        AgencyServiceList[k].ActualAmount = totalamount;
                    }
                }
            }

            return AgencyServiceList;

        }
        #endregion

        #region GetAgentsForCompare
        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public AgencySearchList GetAgentsForCompare(AgencyFilter ids)
        {
            _logger.LogInformation("Inside GetAgentsForCompare() function in OwnerRepository");
            AgencySearchList Finalresult = new AgencySearchList();
            List<AgencyService> AgencyService = new List<AgencyService>();
            List<AgencyServiceDetails> AgencyServiceDetails = new List<AgencyServiceDetails>();
            List<AgencyServiceDetails> FilteredServiceDetails = new List<AgencyServiceDetails>();
            List<FilterByService> FilterByList = new List<FilterByService>();
            string servicecategoryid = "", serviceid = "";


            try
            {
                if (ids.ServiceCategoryId != null)
                    servicecategoryid = string.Join(",", ids.ServiceCategoryId);

                if (ids.ServiceId != null)
                    serviceid = string.Join(",", ids.ServiceId);

                FilterByList = ids.FilterByService;
                if (FilterByList == null)
                {
                    FilterByList = new List<FilterByService>();
                }
                var FSingleServiceAr = FilterByList.Where(x => x.IsParent == false && x.ParentId == 0).Select(x => x.ServiceId).ToArray();
                var FParentServiceAr = FilterByList.Where(x => x.IsParent == true && x.ParentId == 0).Select(x => x.ServiceId).ToArray();
                var FSubServiceAr = FilterByList.Where(x => x.IsParent == false && x.ParentId > 0).Select(x => x.ServiceId).ToArray();
                string FSingleService = "", FParentService = "", FSubService = "";
                int selectedservicecount = 0;
                if (FSingleServiceAr != null)
                {
                    FSingleService = string.Join(",", FSingleServiceAr);
                    selectedservicecount = selectedservicecount + FSingleServiceAr.Length;
                }

                if (FParentServiceAr != null)
                {
                    FParentService = string.Join(",", FParentServiceAr);
                    selectedservicecount = selectedservicecount + FParentServiceAr.Length;
                }

                if (FSubServiceAr != null)
                    FSubService = string.Join(",", FSubServiceAr);
                db.Open();
                DynamicParameters param = new DynamicParameters();

                param.Add("@PortId", ids.PortId);
                param.Add("@ViewBy", ids.ViewBy);
                param.Add("@SortBy", ids.SortBy);
                AgencyService = db.Query<AgencyService>("procSearchAgency", param, commandType: CommandType.StoredProcedure).ToList();
                if (AgencyService.Count > 0)
                {
                    var AgencyServiceId = string.Join(",", AgencyService.Select(x => x.AgencyServiceId).ToArray());
                    DynamicParameters param2 = new DynamicParameters();
                    param2.Add("@ServiceCategoryId", servicecategoryid);
                    param2.Add("@ServiceId", serviceid);
                    param2.Add("@AgencyServiceId", AgencyServiceId);
                    param2.Add("@FSingleService", FSingleService);
                    param2.Add("@FParentService", FParentService);
                    param2.Add("@FSubService", FSubService);
                    AgencyServiceDetails = db.Query<AgencyServiceDetails>("procSearchAgencyDetailsFilter", param2, commandType: CommandType.StoredProcedure).ToList();

                    for (var i = 0; i < AgencyService.Count; i++)
                    {
                        for (var j = 0; j < AgencyServiceDetails.Count; j++)
                        {
                            if (AgencyService[i].AgencyServiceId == AgencyServiceDetails[j].AgencyServiceId)
                            {
                                AgencyService[i].ServiceDetails.Add(AgencyServiceDetails[j]);
                            }
                        }
                        AgencyService[i].ServiceCount = AgencyService[i].ServiceDetails.Where(x => x.IsParent == true || x.ParentId == 0).Count();
                    }
                }
                AgencyService = AgencyService.Where(x => x.ServiceDetails.Count > 0).ToList();
                List<int> filtrr = new List<int>();
                if (FilterByList.Count > 0)
                {
                    foreach (FilterByService fl in FilterByList)
                    {
                        if (fl.ParentId == 0)
                        {
                            filtrr.Add(fl.ServiceId);
                        }
                    }
                }
                if (ids.ServiceId != null && ids.ServiceId.Length > 0 )
                {
                    List<AgencyService> AgencyService2 = new List<AgencyService>();
                    for (var i = 0; i < AgencyService.Count; i++)
                    {
                        var svnofound = false;
                        for (var k = 0; k < ids.ServiceId.Length; k++)
                        {
                            var svcexsts = false;
                            for (var j = 0; j < AgencyService[i].ServiceDetails.Count; j++)
                            {
                                if (ids.ServiceId[k] == AgencyService[i].ServiceDetails[j].ServiceId
                                    && AgencyService[i].ServiceDetails[j].ParentId == 0)
                                {
                                    svcexsts = true;
                                }
                            }
                            if (!svcexsts) { svnofound = true; }
                        }
                        if (!svnofound)
                        {
                            AgencyService2.Add(AgencyService[i]);
                        }
                    }
                    AgencyService = AgencyService2;
                }
                if (filtrr.Count > 0 && !ids.IsFirst && !ids.AllChecked)
                {
                    List<AgencyService> AgencyService2 = new List<AgencyService>();
                    for (var i = 0; i < AgencyService.Count; i++)
                    {
                        var svnofound = false;
                        for (var k = 0; k < filtrr.Count; k++)
                        {
                            var svcexsts = false;
                            for (var j = 0; j < AgencyService[i].ServiceDetails.Count; j++)
                            {
                                if (filtrr[k] == AgencyService[i].ServiceDetails[j].ServiceId
                                    && AgencyService[i].ServiceDetails[j].ParentId == 0)
                                {
                                    svcexsts = true;
                                }
                            }
                            if (!svcexsts) { svnofound = true; }
                        }
                        if (!svnofound)
                        {
                            AgencyService2.Add(AgencyService[i]);
                        }
                    }
                    AgencyService = AgencyService2;
                    AgencyService2 = new List<AgencyService>();
                    for (var i = 0; i < AgencyService.Count; i++)
                    {
                        var defsvc = false;
                        for (var j = 0; j < AgencyService[i].ServiceDetails.Count; j++)
                        {
                            if (AgencyService[i].ServiceDetails[j].IsParent)
                            {
                                var sublist = new List<AgencyServiceDetails>();
                                sublist = AgencyService[i].ServiceDetails.Where(x => x.ParentId == AgencyService[i].ServiceDetails[j].ServiceId).ToList();
                                var sublistF = new List<int>();
                                if (FilterByList.FirstOrDefault(x => x.ServiceId == AgencyService[i].ServiceDetails[j].ServiceId && x.IsParent == true) != null)
                                {
                                    sublistF = FilterByList.Where(x => x.ParentId == AgencyService[i].ServiceDetails[j].ServiceId).Select(x => x.ServiceId).ToList();
                                    var newlist = sublist.Where(x => !sublistF.Contains(x.ServiceId) && x.Option == "D").ToList();
                                    if (newlist.Count > 0)
                                    {
                                        defsvc = true;
                                    }
                                }
                            }
                        }

                        if (!defsvc)
                        {
                            AgencyService2.Add(AgencyService[i]);
                        }
                    }
                    AgencyService = AgencyService2;
                }
                var offrapplied = false;
                if (ids.ViewBy == "A")
                {
                    AgencyService = AgencyService.OrderBy(x => x.CompanyName).ToList();
                }
                else
                {
                    if ((ids.ServiceId != null && ids.ServiceId.Length == 1) || (selectedservicecount == 1))
                    {
                        AgencyService = LoadAndApplyTemplateOffer(AgencyService, ids.OwnerId, ids.PortId);
                        offrapplied = true;
                        for (var i = 0; i < AgencyService.Count; i++)
                        {
                            if (selectedservicecount == 0)
                                AgencyService[i].SortAmount = AgencyService[i].ServiceDetails.FirstOrDefault(x => x.ServiceId == ids.ServiceId[0] && x.ParentId == 0).ActualAmount;
                            else if (FParentServiceAr != null && FParentServiceAr.Length == 1)
                                AgencyService[i].SortAmount = AgencyService[i].ServiceDetails.FirstOrDefault(x => x.ServiceId == FParentServiceAr[0] && x.ParentId == 0).ActualAmount;
                            else if (FSingleServiceAr != null && FSingleServiceAr.Length == 1)
                                AgencyService[i].SortAmount = AgencyService[i].ServiceDetails.FirstOrDefault(x => x.ServiceId == FSingleServiceAr[0] && x.ParentId == 0).ActualAmount;

                        }
                        AgencyService = AgencyService.OrderBy(x => x.SortAmount).ThenByDescending(x => x.Rating).ThenBy(x => x.CompanyName).ToList();

                    }
                    else if (ids.SortBy == "A")
                    {
                        AgencyService = AgencyService.OrderBy(x => x.Rating).ToList();
                    }
                    else if (ids.SortBy == "D")
                    {
                        AgencyService = AgencyService.OrderByDescending(x => x.Rating).ToList();
                    }
                    else
                        AgencyService = AgencyService.OrderByDescending(x => x.Rating).ThenByDescending(x => x.ServiceCount).ThenBy(x => x.CompanyName).ToList();

                }

                Finalresult.totalcount = AgencyService.Count;
                Finalresult.AgencyServiceList = AgencyService;
                if (offrapplied != true)
                {
                    Finalresult.AgencyServiceList = LoadAndApplyTemplateOffer(Finalresult.AgencyServiceList, ids.OwnerId, ids.PortId);
                }
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return Finalresult;
        }
        #endregion
    }
}
