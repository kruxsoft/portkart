﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public interface IUserRoleRepository
    {
        List<UserRole> GetUserRoles();
        DBResponse AddUserRole(UserRole oObj);
        DBResponse DeleteUserRole(UserRole uObj);
        DataTableServerSideResult<UserRole> GetUserRolesListServerSide(DataTableServerSideSettings DTtablesettings);
    }
}
