﻿using Dapper;
using Domain;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
   public class UserRoleRepository : IUserRoleRepository
    {
        #region Global Variable
        private IDbConnection db;
        private readonly ILogger<UserRoleRepository> _logger;
        public UserRoleRepository(IConfiguration configuration, ILogger<UserRoleRepository> logger)
        {
            var sqlConnectionString = configuration.GetConnectionString("SqlServerConnection");
            db = new SqlConnection(sqlConnectionString);
            _logger = logger;
        }
        #endregion

        #region GetUserRoles
        /// <summary>
        /// Get all user roles
        /// </summary>
        /// <returns></returns>
        public List<UserRole> GetUserRoles()
        {
            _logger.LogInformation("Inside GetUserRoles() function in UserRoleRepository");
            List<UserRole> userRoles = new List<UserRole>();
            try
            {
                db.Open();
                userRoles = db.Query<UserRole>("procSelectUserRoles", null, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return userRoles;
        }
        #endregion


        #region AddUserRole
        /// <summary>
        /// To add new user roles
        /// </summary>
        /// <param name="cs">UserRole model</param>
        /// <returns>int</returns>
        public DBResponse AddUserRole(UserRole userRole)
        {
            _logger.LogInformation("Inside AddUserRole() function in UserRoleRepository");
            DBResponse dbResponse = new DBResponse();
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@userId", userRole.UserId);
                param.Add("@roleId", userRole.RoleId);
                param.Add("@createdBy", userRole.CreatedBy);
                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("procInsertUserRole", param, commandType: CommandType.StoredProcedure);
                return dbResponse;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion     

        #region DeleteUserRole
        /// <summary>
        /// Delete user roles with id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DBResponse DeleteUserRole(UserRole userRole)
        {
            _logger.LogInformation("Inside DeleteUserRole() function in UserRoleRepository");
            DBResponse dbResponse = new DBResponse();
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@UserId", userRole.UserId);
                param.Add("@RoleId", userRole.RoleId);
                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("[procDeleteUserRole]", param, commandType: CommandType.StoredProcedure);
                return dbResponse;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region GetUserRolesListServerSide
        public DataTableServerSideResult<UserRole> GetUserRolesListServerSide(DataTableServerSideSettings DTSettings)
        {
            _logger.LogInformation("Inside GetUserRolesListServerSide() function in UserRoleRepository");
            List<UserRole> userRoles = new List<UserRole>();
            DataTableServerSideResult<UserRole> dtResult = new DataTableServerSideResult<UserRole>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@pageSize", DTSettings.pageSize);
                param.Add("@skip", DTSettings.skip);
                param.Add("@sortColumn", DTSettings.sortColumn);
                param.Add("@sortColumnDir", DTSettings.sortColumnDir);
                param.Add("@searchValue", DTSettings.searchValue);
                param.Add("@FirstName", DTSettings.ColumnSearch.ContainsKey("FirstName") == true ? DTSettings.ColumnSearch["FirstName"] : "");
                param.Add("@LastName", DTSettings.ColumnSearch.ContainsKey("LastName") == true ? DTSettings.ColumnSearch["LastName"] : "");
                param.Add("@EmailId", DTSettings.ColumnSearch.ContainsKey("EmailId") == true ? DTSettings.ColumnSearch["EmailId"] : "");
                param.Add("@RoleName", DTSettings.ColumnSearch.ContainsKey("RoleName") == true ? DTSettings.ColumnSearch["RoleName"] : "");


                userRoles = db.Query<UserRole>("procSelectUserRolesServerSide", param, commandType: CommandType.StoredProcedure).ToList();

                if (userRoles.Count > 0)
                {
                    dtResult.recordsTotal = userRoles[0].recordsTotal;
                    dtResult.recordsFiltered = userRoles[0].recordsFiltered;
                }

                dtResult.data = userRoles;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dtResult;
        }
        #endregion


    }
}
