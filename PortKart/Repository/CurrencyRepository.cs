﻿using Dapper;
using Domain;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class CurrencyRepository : ICurrencyRepository
    {
        #region GLOBAL VARIABLE
        private IDbConnection db;
        public CurrencyRepository(IConfiguration configuration)
        {
            var sqlConnectionString = configuration.GetConnectionString("SqlServerConnection");
            db = new SqlConnection(sqlConnectionString);
        }
        #endregion

        #region GetAllCurrencies
        public List<CurrencyDomain> GetAllCurrencies()
        {
            List<CurrencyDomain> currency = new List<CurrencyDomain>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                currency = db.Query<CurrencyDomain>("procSelectCurrency", param, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return currency;
        }


        #endregion


        #region GetCurrencyList
        public List<CurrencyDomain> GetCurrencyList()
        {
            List<CurrencyDomain> currency = new List<CurrencyDomain>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                currency = db.Query<CurrencyDomain>("procSelectCurrencyList", param, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return currency;
        }


        #endregion

        #region AddCurrency

        public DBResponse AddCurrency(CurrencyDomain sc)
        {

            DBResponse dbResponse = new DBResponse();
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@currencyCode", sc.Code);
                param.Add("@currency", sc.Currency);
                param.Add("@createdBy", sc.CreatedBy);
                param.Add("@modifiedBy", sc.ModifiedBy);
                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("procInsertCurrency", param, commandType: CommandType.StoredProcedure);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                db.Close();
            }
            return dbResponse;
        }
        #endregion

        #region GetCurrencyById
        public CurrencyDomain GetCurrencyById(int id)
        {
            CurrencyDomain currency = new CurrencyDomain();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@currencyId", id);
                currency = db.Query<CurrencyDomain>("procSelectCurrencyById", param, commandType: CommandType.StoredProcedure).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return currency;
        }
        #endregion

        #region UpdateCurrency
        public DBResponse UpdateCurrency(CurrencyDomain sc)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@currency", sc.Currency);
                param.Add("@code", sc.Code);
                param.Add("@modifiedBy", sc.ModifiedBy);
                param.Add("@currencyId", sc.CurrencyId);
                param.Add("@isActive", sc.IsActive);

                db.Open();
                dbResponse = db.QuerySingle<DBResponse>("procUpdateCurrency", param, commandType: CommandType.StoredProcedure);

                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region DeleteServiceCategory

        public DBResponse DeleteCurrency(CurrencyDomain sObj)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@currencyId", sObj.CurrencyId);
                param.Add("@modifiedBy", sObj.ModifiedBy);
                db.Open();
                DBResponse dbResponse = db.QuerySingle<DBResponse>("procDeleteCurrency", param, commandType: CommandType.StoredProcedure);

                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion
    }
}
