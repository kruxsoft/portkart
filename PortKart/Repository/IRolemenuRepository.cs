﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;

namespace Repository
{
    public interface IRoleMenuRepository 

    {
        DBResponse AddRolemenu(RoleMenu RmObj);
        List<RoleMenu> GetRolemenus(int roleId);
        DBResponse DeleteRolemenu(RoleMenu roleMenu);
        List<RoleMenu> GetAllRoleMenus();
        List<RoleMenu> GetMenuList();
    }
}
