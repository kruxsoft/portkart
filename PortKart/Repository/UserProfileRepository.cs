﻿using Dapper;
using Domain;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Repository
{
    public class UserProfileRepository: IUserProfileRepository
    {
        #region GLOBAL VARIABLE
        private IDbConnection db;
        private readonly ILogger<UserProfileRepository> _logger;
        public UserProfileRepository(IConfiguration configuration, ILogger<UserProfileRepository> logger)
        {
            var sqlConnectionString = configuration.GetConnectionString("SqlServerConnection");
            db = new SqlConnection(sqlConnectionString);
            _logger = logger;
        }
        #endregion

        #region GetUserProfile
        public UserProfile GetUserProfile(int id)
        {
            _logger.LogInformation("Inside GetUserProfile() function in UserProfileRepository");
            UserProfile profile = new UserProfile();
            List<ProfileSocialMedia> SocialMediaList = new List<ProfileSocialMedia>();
            List<ContactPerson> ContactPersonList = new List<ContactPerson>();
            List<CompanyBranch> CompanyBranchList = new List<CompanyBranch>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                param.Add("@UserId", id);
                profile = db.Query<UserProfile>("procSelectUserProfile", param, commandType: CommandType.StoredProcedure).SingleOrDefault();
                if(profile==null)
                {
                    profile = new UserProfile();
                    profile.UserId = id;
                    profile.CompanyId = 0;
                    profile.CreatedBy = 0;
                    profile.ModifiedBy = 0;
                }
                SocialMediaList = db.Query<ProfileSocialMedia>("procSelectUserProfileSocialMedia", param, commandType: CommandType.StoredProcedure).ToList();
                ContactPersonList = db.Query<ContactPerson>("procSelectUserProfileContactPerson", param, commandType: CommandType.StoredProcedure).ToList();
                CompanyBranchList= db.Query<CompanyBranch>("procSelectUserProfileCompanyBranch", param, commandType: CommandType.StoredProcedure).ToList();
                if (SocialMediaList.Count>0)
                {
                    profile.SocialMediaList = SocialMediaList;
                }
                if (ContactPersonList.Count > 0)
                {
                    profile.ContactPersonList = ContactPersonList;
                }
                if (CompanyBranchList.Count > 0)
                {
                    profile.CompanyBranchList = CompanyBranchList;
                }
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return profile;
        }
        #endregion

        #region AddUserProfile
        public DBResponse AddUserProfile(UserProfile pobj)
        {
            _logger.LogInformation("Inside AddUserProfile() function in UserProfileRepository");
            DBResponse DBResponse = new DBResponse();
            IDbTransaction tran = null;
            try
            {
                int CompanyId = 0;
                db.Open();
                tran = db.BeginTransaction();
                DynamicParameters param = new DynamicParameters();
                param.Add("@UserId", pobj.UserId);
                param.Add("@CompanyName", pobj.CompanyName);
                param.Add("@CompanyRegNo", pobj.CompanyRegNo);
                param.Add("@CountryId", pobj.CountryId);
                param.Add("@IndustryTypeId", pobj.IndustryTypeId);
                param.Add("@ExpiryDate", pobj.ExpiryDate);
                param.Add("@City", pobj.City);
                param.Add("@Website", pobj.Website);
                param.Add("@EmailID", pobj.EmailID);
                param.Add("@Address", pobj.Address);
                param.Add("@About", pobj.About);
                param.Add("@CountryCode", pobj.CountryCode);
                param.Add("@Phone", pobj.Phone);
                param.Add("@LogoPath", pobj.LogoPath);
                param.Add("@CreatedBy", pobj.CreatedBy);
                param.Add("@OutCompanyId", dbType: DbType.Int32, direction: ParameterDirection.Output);
                DBResponse = db.QuerySingle<DBResponse>("procInsertUserProfile", param,transaction: tran, commandType: CommandType.StoredProcedure);
                if(DBResponse.MessageType!="ERROR")
                CompanyId = param.Get<int>("@OutCompanyId");

              if(CompanyId!=0)
                {
                    foreach (ProfileSocialMedia sm in pobj.SocialMediaList)
                    {
                        DynamicParameters param2 = new DynamicParameters();
                        param2.Add("@CompanyId", CompanyId);
                        param2.Add("@Link", sm.Link);
                        param2.Add("@SocialMediaId", sm.SocialMediaId);
                        param2.Add("@Type", sm.Type);
                        db.QuerySingle<DBResponse>("procInsertUserProfileSocialMedia", param2, transaction: tran, commandType: CommandType.StoredProcedure);
                    }
                    foreach (ContactPerson sm in pobj.ContactPersonList)
                    {
                        DynamicParameters param3 = new DynamicParameters();
                        param3.Add("@CompanyId", CompanyId);
                        param3.Add("@ContactPersonId", sm.ContactPersonId);
                        param3.Add("@Name", sm.Name);
                        param3.Add("@CountryCode", sm.CountryCode);
                        param3.Add("@PhoneNo", sm.PhoneNo);
                        param3.Add("@IsPreview", sm.IsPreview);
                        db.QuerySingle<DBResponse>("procInsertUserProfileContactPerson", param3, transaction: tran, commandType: CommandType.StoredProcedure);
                    }
                    foreach (CompanyBranch sm in pobj.CompanyBranchList)
                    {
                        DynamicParameters param4 = new DynamicParameters();
                        param4.Add("@CompanyId", CompanyId);
                        param4.Add("@BranchId", sm.BranchId);
                        param4.Add("@BranchName", sm.BranchName);
                        param4.Add("@CountryCode", sm.CountryCode);
                        param4.Add("@Phone", sm.Phone);
                        param4.Add("@CountryId", sm.CountryId);
                        param4.Add("@Address", sm.Address);
                        param4.Add("@ContactPerson", sm.ContactPerson);
                        param4.Add("@CP_CountryCode", sm.CP_CountryCode);
                        param4.Add("@CP_Phone", sm.CP_Phone);
                        param4.Add("@Email", sm.Email);
                        param4.Add("@Website", sm.Website);
                        param4.Add("@IsPreview", sm.IsPreview);
                        db.QuerySingle<DBResponse>("procInsertUserProfileCompanyBranch", param4, transaction: tran, commandType: CommandType.StoredProcedure);
                    }
                }
                tran.Commit();
            }
            catch (Exception ex)
            {
                if(tran!=null)
                { tran.Rollback(); }
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return DBResponse;
        }
        #endregion

        #region UpdateUserProfile
        public DBResponse UpdateUserProfile(UserProfile pobj)
        {
            _logger.LogInformation("Inside UpdateUserProfile() function in UserProfileRepository");
            DBResponse DBResponse = new DBResponse();
            IDbTransaction tran = null;
            try
            {
                string contactPersIDs = "";
                if (pobj.ContactPersonList.FirstOrDefault(x => x.ContactPersonId > 0) != null)
                {
                    contactPersIDs = string.Join(",", pobj.ContactPersonList.Where(x => x.ContactPersonId > 0).Select(x => x.ContactPersonId));
                }
                string cbranchIDs = "";
                if (pobj.CompanyBranchList.FirstOrDefault(x => x.BranchId > 0) != null)
                {
                    cbranchIDs = string.Join(",", pobj.CompanyBranchList.Where(x => x.BranchId > 0).Select(x => x.BranchId));
                }
                int CompanyId = pobj.CompanyId;
                db.Open();
                tran = db.BeginTransaction();
                DynamicParameters param = new DynamicParameters();
                param.Add("@UserId", pobj.UserId);
                param.Add("@CompanyName", pobj.CompanyName);
                param.Add("@CompanyRegNo", pobj.CompanyRegNo);
                param.Add("@CountryId", pobj.CountryId);
                param.Add("@IndustryTypeId", pobj.IndustryTypeId);
                param.Add("@ExpiryDate", pobj.ExpiryDate);
                param.Add("@City", pobj.City);
                param.Add("@Website", pobj.Website);
                param.Add("@EmailID", pobj.EmailID);
                param.Add("@Address", pobj.Address);
                param.Add("@About", pobj.About);
                param.Add("@CountryCode", pobj.CountryCode);
                param.Add("@Phone", pobj.Phone);
                param.Add("@LogoPath", pobj.LogoPath);
                param.Add("@AttachmentPath", pobj.AttachmentPath);
                param.Add("@ModifiedBy", pobj.ModifiedBy);
                param.Add("@CompanyId", pobj.CompanyId);
                param.Add("@ContactPersIds", contactPersIDs);
                param.Add("@BranchIds", cbranchIDs);
                DBResponse = db.QuerySingle<DBResponse>("procUpdateUserProfile", param, transaction: tran, commandType: CommandType.StoredProcedure);

                if (CompanyId != 0 && DBResponse.MessageType=="SUCCESS")
                {
                    foreach (ProfileSocialMedia sm in pobj.SocialMediaList)
                    {
                        DynamicParameters param2 = new DynamicParameters();
                        param2.Add("@CompanyId", CompanyId);
                        param2.Add("@Link", sm.Link);
                        param2.Add("@SocialMediaId", sm.SocialMediaId);
                        param2.Add("@Type", sm.Type);
                        var s=db.QuerySingle<DBResponse>("procInsertUserProfileSocialMedia", param2, transaction: tran, commandType: CommandType.StoredProcedure);
                    }
                    foreach (ContactPerson sm in pobj.ContactPersonList)
                    {
                        DynamicParameters param3 = new DynamicParameters();
                        param3.Add("@CompanyId", CompanyId);
                        param3.Add("@ContactPersonId", sm.ContactPersonId);
                        param3.Add("@Name", sm.Name);
                        param3.Add("@CountryCode", sm.CountryCode);
                        param3.Add("@PhoneNo", sm.PhoneNo);
                        param3.Add("@IsPreview", sm.IsPreview);
                        var s = db.QuerySingle<DBResponse>("procInsertUserProfileContactPerson", param3, transaction: tran, commandType: CommandType.StoredProcedure);
                    }
                    foreach (CompanyBranch sm in pobj.CompanyBranchList)
                    {
                        DynamicParameters param4 = new DynamicParameters();
                        param4.Add("@CompanyId", CompanyId);
                        param4.Add("@BranchId", sm.BranchId);
                        param4.Add("@BranchName", sm.BranchName);
                        param4.Add("@CountryCode", sm.CountryCode);
                        param4.Add("@Phone", sm.Phone);
                        param4.Add("@CountryId", sm.CountryId);
                        param4.Add("@Address", sm.Address);
                        param4.Add("@ContactPerson", sm.ContactPerson);
                        param4.Add("@CP_CountryCode", sm.CP_CountryCode);
                        param4.Add("@CP_Phone", sm.CP_Phone);
                        param4.Add("@Email", sm.Email);
                        param4.Add("@Website", sm.Website);
                        param4.Add("@IsPreview", sm.IsPreview);
                        var s = db.QuerySingle<DBResponse>("procInsertUserProfileCompanyBranch", param4, transaction: tran, commandType: CommandType.StoredProcedure);
                    }
                }
                tran.Commit();
            }
            catch (Exception ex)
            {
                if (tran != null)
                { tran.Rollback(); }
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return DBResponse;
        }
        #endregion

        #region GetServices
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<IndustryType> GetAllIndustryType()
        {
            _logger.LogInformation("Inside GetAllIndustryType() function in UserProfileRepository");
            List<IndustryType> rolemenu = new List<IndustryType>();
            try
            {
                db.Open();
                DynamicParameters param = new DynamicParameters();
                rolemenu = db.Query<IndustryType>("procSelectIndustryTypeList", param, commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex, ex.Message, null);
                throw ex;
            }
            finally
            {
                db.Close();
            }
            return rolemenu;
        }
        #endregion

        #region AddContactPerson
        /// <summary>
        /// To add new AddSubService
        /// </summary>
        /// <param name="rm">Service model</param>
        /// <returns>int</returns>
        public int AddContactPerson(ContactPerson rm)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                int ContactPersonId = 0;

                DynamicParameters param = new DynamicParameters();

                param.Add("@CompanyID", rm.CompanyId);
                param.Add("@ContactPersonId", rm.ContactPersonId);
                param.Add("@CountryCode", rm.CountryCode);
                param.Add("@Name", rm.Name);
                param.Add("@PhoneNo", rm.PhoneNo);
                param.Add("@IsPreview", rm.IsPreview);
                param.Add("@OutContactPersonId", dbType: DbType.Int32, direction: ParameterDirection.Output);


                db.Open();



                dbResponse = db.QuerySingle<DBResponse>("procInsertUserProfileContactPerson", param, commandType: CommandType.StoredProcedure);

                if (dbResponse.MessageType != "ERROR")
                    ContactPersonId = param.Get<int>("@OutContactPersonId");


                return ContactPersonId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        public DBResponse DeleteContactPerson(int CPId, int UserId)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {

                DynamicParameters param = new DynamicParameters();
              

                param.Add("@ContactPersonId", CPId);

                param.Add("@userId", UserId);


                db.Open();



                dbResponse = db.QuerySingle<DBResponse>("procDeleteContactPerson", param, commandType: CommandType.StoredProcedure);




                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region AddBranch
        /// <summary>
        /// To add new AddSubService
        /// </summary>
        /// <param name="rm">Service model</param>
        /// <returns>int</returns>
        public int AddBranch(CompanyBranch sm)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {
                int branchID = 0;


                DynamicParameters param4 = new DynamicParameters();
                param4.Add("@CompanyId", sm.CompanyId);
                param4.Add("@BranchId", sm.BranchId);
                param4.Add("@BranchName", sm.BranchName);
                param4.Add("@CountryCode", sm.CountryCode);
                param4.Add("@Phone", sm.Phone);
                param4.Add("@CountryId", sm.CountryId);
                param4.Add("@Address", sm.Address);
                param4.Add("@ContactPerson", sm.ContactPerson);
                param4.Add("@CP_CountryCode", sm.CP_CountryCode);
                param4.Add("@CP_Phone", sm.CP_Phone);
                param4.Add("@Email", sm.Email);
                param4.Add("@Website", sm.Website);
                param4.Add("@IsPreview", sm.IsPreview);
                param4.Add("@outBranchId", dbType: DbType.Int32, direction: ParameterDirection.Output);
                dbResponse = db.QuerySingle<DBResponse>("procInsertUserProfileCompanyBranch", param4,commandType: CommandType.StoredProcedure);


                if (dbResponse.MessageType != "ERROR")
                    branchID = param4.Get<int>("@outBranchId");

                return branchID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region DeleteBranch
        /// <summary>
        /// To add new AddSubService
        /// </summary>
        /// <param name="rm">Service model</param>
        /// <returns>int</returns>
        public DBResponse DeleteBranch(int BRId, int UserId)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {



                DynamicParameters param4 = new DynamicParameters();
                
                param4.Add("@BranchId", BRId);
                param4.Add("@UserId", UserId);
                dbResponse = db.QuerySingle<DBResponse>("procDeleteBranch", param4, commandType: CommandType.StoredProcedure);




                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region AddSocialMedia
        /// <summary>
        /// To add new AddSubService
        /// </summary>
        /// <param name="rm">Service model</param>
        /// <returns>int</returns>
        public DBResponse AddSocialMedia(List<ProfileSocialMedia> sm)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {

                foreach (ProfileSocialMedia smm in sm)
                {
                    DynamicParameters param2 = new DynamicParameters();
                    param2.Add("@CompanyId", smm.CompanyId);
                    param2.Add("@Link", smm.Link);
                    param2.Add("@SocialMediaId", smm.SocialMediaId);
                    param2.Add("@Type", smm.Type);
                    db.QuerySingle<DBResponse>("procInsertUserProfileSocialMedia", param2, commandType: CommandType.StoredProcedure);
                }

                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion

        #region AddAboutsUS
        /// <summary>
        /// To add new AddSubService
        /// </summary>
        /// <param name="rm">Service model</param>
        /// <returns>int</returns>
        public DBResponse AddAboutsUS(Company sm)
        {
            DBResponse dbResponse = new DBResponse();
            try
            {



                DynamicParameters param2 = new DynamicParameters();

                param2.Add("@CompanyId", sm.CompanyId);
                param2.Add("@AboutUs", sm.About);
               
                dbResponse = db.QuerySingle<DBResponse>("procInsertAboutUS", param2, commandType: CommandType.StoredProcedure);

                return dbResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Close();
            }
        }
        #endregion
    }
}
