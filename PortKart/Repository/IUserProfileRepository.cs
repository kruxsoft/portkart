﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public interface IUserProfileRepository
    {
        UserProfile GetUserProfile(int id);
        DBResponse AddUserProfile(UserProfile pobj);
        int AddContactPerson(ContactPerson cp);
        DBResponse DeleteContactPerson( int Cpid, int userId);
        int AddBranch(CompanyBranch cp);
        DBResponse DeleteBranch( int BRid, int userId);
        DBResponse AddSocialMedia(List<ProfileSocialMedia> cp);
        DBResponse AddAboutsUS(Company cp);
        DBResponse UpdateUserProfile(UserProfile pobj);
        List<IndustryType> GetAllIndustryType();
    }
}
