﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
  public  interface ICountryRepository
    {
        List<Country> GetAllCountries();
        DBResponse AddCountry(Country oObj);
        Country GetCountryById(int CountryId);
        DBResponse UpdateCountry(Country oObj);
        DBResponse DeleteCountry(Country Country);
        List<Country> GetCountryList();
    }
}
