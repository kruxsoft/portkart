﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public interface IAgencyRepository
    {
        List<AgencyServiceDetails> LoadAgencyService(int PortId, int AgencyId);
        List<VesselPort> GetVesselPortList(string search);
        List<ServiceList> GetServiceList();

        List<AgencyServiceDetails> LoadServicebyId(string[] ids);
        DBResponse SaveAgencyService(Domain.AgencyService obj);
        DBResponse UpdateAgencyService(Domain.AgencyService obj);
        List<ServiceList> GetServiceListByCategory(string[] ids);
        List<ServiceCategory> GetServiceCategoryList();
        List<User> GetOwnerList(string search);
        OwnerTemplateMapping LoadOwnerTemplate(int OwnerId, int AgencyId);
        DBResponse DeleteService(int id);
    }
}
