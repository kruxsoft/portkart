﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;


namespace Repository
{
   public interface IRoleRepository
    {
        DBResponse AddRole(Role RObj);
        List<Role> GetRoles();
        Role GetRoleById(int id);
        DBResponse UpdateRole(Role RObj);
        DBResponse DeleteRole(Role rObj);
        List<Role> GetRolesList();
        string GetRoleNameByRoleId(int roleId);
    }
}
