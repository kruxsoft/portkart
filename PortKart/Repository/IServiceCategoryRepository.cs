﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public interface IServiceCategoryRepository
    {
        List<ServiceCategory> GetAllServiceCategories();
        DBResponse AddServiceCategory(ServiceCategory oObj);
        ServiceCategory GetServiceCategoryById(int ServiceCategoryId);
        DBResponse UpdateServiceCategory(ServiceCategory oObj);
        DBResponse DeleteServiceCategory(ServiceCategory serviceCategory);

        List<ServiceCategory> GetServiceCategoryList();

        DataTableServerSideResult<ServiceCategory> GetAllServiceCategoriesServerSide(DataTableServerSideSettings DTtablesettings);
    }
}
